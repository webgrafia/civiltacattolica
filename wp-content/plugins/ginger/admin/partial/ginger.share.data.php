
<?php add_thickbox(); ?>
<div id="ginger-share-data" style="display:none;">
    <p>
        Ginger is a free tool but we need some information to improve it!
    </p>


    <p>
        <input type="checkbox" id="ginger_statistics_share" name="ginger_statistics_share" value="1">
        I accept the processing of data for statistical purposes.
    </p>
    <ul>
        <li>This data stored are:</li>
        <li>IP address</li>
        <li>number of posts</li>
        <li>number of users</li>
        <li>site language</li>
        <li>Taxonomies</li>
    </ul>
    <p>
        Ginger analyzes its data for statistical purposes and to identify trends, but in doing so adheres
        strictly to stringent data-security standards. Our analysis of this data does not include any attempt to
        link it to individuals.
    </p>
    <p>
        <input type="checkbox" id="ginger_informations_share" name="ginger_informations_share" value="1">
        I accept the processing of data for commercial information purposes and/or for sending advertising or direct
        selling about Ginger plugin.
    </p>
    <p>
        <button id="ginger_share_data" class="button button-primary"
                data-email="<?php echo wp_get_current_user()->user_email; ?>">Save
        </button>
    </p>
</div>


<?php
$ginger_share_data = get_option('ginger_share_data');
$ginger_share_data_time = get_option('ginger_share_data_time');
?>
<?php if (!$ginger_share_data || ($ginger_share_data == 'false' && (int)$ginger_share_data_time <= strtotime('-30 days'))): ?>
    <script language="javascript">
        jQuery(window).load(function () {
            ginger_share_data();
            tb_show("Please select", "#TB_inline?width=600&height=550&inlineId=ginger-share-data", null);
            jQuery("#TB_closeWindowButton").click(function () {
                const data_token = {
                    'action': 'ginger_save_info',
                    ginger_share_data_time: new Date().getTime(),
                    ginger_share_data: false
                };
                jQuery.post(ajaxurl, data_token, function (response) {
                    console.log(response);
                });
            });
        });
    </script>
<?php endif; ?>