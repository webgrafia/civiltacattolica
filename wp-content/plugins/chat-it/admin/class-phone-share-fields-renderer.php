<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * SEO Cleaner Admin Settings Class
 */
class Phone_Share_Fields_Renderer {

	private $current_tab;

	private $options_name;

	public function __construct( $current_tab ) {
		$this->current_tab  = $current_tab;
		$this->options_name = $options_name = PHONE_SHARE()->get_option_name( $this->current_tab );
	}

	public function select( $args ) {
		$name = sprintf( "%s[%s]", $this->options_name, $args['id'] );
		?>
		<label for="<?php echo $args['id'] ?>">
			<select style="width: 28%" id="<?php echo $args['id'] ?>" name="<?php echo $name ?>">
				<?php foreach ( $args['select_options'] as $option_value => $option_title ): ?>
					<option
						value="<?php echo $option_value ?>" <?php selected( $args['current_value'], $option_value ) ?>>
						<?php echo $option_title ?>
					</option>
				<?php endforeach; ?>
			</select>
		</label>
		<?php if ( isset( $args['description'] ) ): ?>
			<p class="description"><?php echo $args['description'] ?></p>
		<?php endif; ?>
		<?php
	}

	public function select_icon( $args ) {
		$name = sprintf( "%s[%s]", $this->options_name, $args['id'] );
		?>
		<label for="<?php echo $args['id'] ?>">
			<select style="width: 28%" id="<?php echo $args['id'] ?>" name="<?php echo $name ?>">
				<?php foreach ( $args['select_options'] as $option_value => $option_title ): ?>
					<option value="<?php echo $option_value ?>"
						<?php selected( $args['current_value'], $option_value ) ?>><?php echo $option_title ?>
					</option>
				<?php endforeach; ?>
			</select>
		</label>
		<?php if ( isset( $args['description'] ) ): ?>
			<p class="description"><?php echo $args['description'] ?></p>
		<?php endif; ?>
		<?php
	}

	public function select_multiple_messengers( $args ) {
		$name = sprintf( "%s[%s][]", $this->options_name, $args['id'] );
		?>
		<label for="<?php echo $args['id'] ?>">
			<select style="width: 40%" id="<?php echo $args['id'] ?>" name="<?php echo $name ?>" multiple>
				<?php
				$options = $args['current_value'];
				foreach ( array_keys( $args['select_options'] ) as $option ) {
					if ( ! in_array( $option, $options ) ) {
						array_push( $options, $option );
					}
				}
				?>
				<?php foreach ( $options as $option_value ): ?>
					<option
						value="<?php echo $option_value ?>" <?php selected( in_array( $option_value, $args['current_value'] ) ) ?>>
						<?php echo $args['select_options'][ $option_value ] ?>
					</option>
				<?php endforeach; ?>
			</select>
		</label>
		<?php if ( isset( $args['description'] ) ): ?>
			<p class="description"><?php echo $args['description'] ?></p>
		<?php endif; ?>
		<?php
	}

	public function select_multiple( $args ) {
		$name = sprintf( "%s[%s][]", $this->options_name, $args['id'] );
		?>
		<label for="<?php echo $args['id'] ?>">
			<select style="width: 30%" id="<?php echo $args['id'] ?>" name="<?php echo $name ?>"  multiple>
				<?php  ?>
				<?php foreach ( $args['select_options'] as $option_value => $option_title  ):  ?>
					<option value="<?php echo $option_value ?>" <?php selected( in_array( $option_value, $args['current_value'] ) ) ?>>
						<?php echo $option_title ?>
					</option>
				<?php endforeach; ?>
			</select>
		</label>
		<?php if ( isset( $args['description'] ) ): ?>
			<p class="description"><?php echo $args['description'] ?></p>
		<?php endif; ?>
		<?php
	}

	public function do_show_on( $args ) {
		$name = sprintf( "%s[%s][]", $this->options_name, $args['id'] );
		?>
		<label for="<?php echo $args['id'] ?>">
			<select style="width: 30%" id="<?php echo $args['id'] ?>" name="<?php echo $name ?>"  multiple>
				<?php  ?>
				<?php foreach ( $args['select_options'][ $this->current_tab ] as $option_value => $option_title  ):  ?>
					<option value="<?php echo $option_value ?>" <?php selected( in_array( $option_value, $args['current_value'] ) ) ?>>
						<?php echo $option_title ?>
					</option>
				<?php endforeach; ?>
			</select>
		</label>
		<?php if ( isset( $args['description'] ) ): ?>
			<p class="description"><?php echo $args['description'] ?></p>
		<?php endif; ?>
		<?php
	}

	public function text_input( $args ) {
		$name = sprintf( "%s[%s]", $this->options_name, $args['id'] );
		?>
		<input style="width: 28%" type="text" name="<?php echo $name ?>"
		       id="<?php echo $args['id'] ?>"
		       value="<?php echo esc_attr( $args['current_value'] ); ?>"
		       <?php echo ( isset( $args['required'] ) && true == $args['required'] ) ? 'required': '' ?>
		/>
		<?php if ( isset( $args['description'] ) ): ?>
			<p class="description"><?php esc_html_e( $args['description'], 'phone-share' ) ?></p>
		<?php endif;
	}

}