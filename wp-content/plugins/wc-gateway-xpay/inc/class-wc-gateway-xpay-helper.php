<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Handles Refunds.
 */
class WC_Gateway_XPay_Helper {

    private static $live_url = 'https://ecommerce.keyclient.it/ecomm/ecomm/DispatcherServlet';
    private static $sandbox_url = 'https://coll-ecommerce.keyclient.it/ecomm/ecomm/DispatcherServlet';

    /**
     * Returns the XPay URL to be used for payments.
     */
    public static function get_live_url( $is_sandbox ) {
        return $is_sandbox ? self::$sandbox_url : self::$live_url;
    }

    /**
     * Returns the site URL for orders completed.
     */
    public static function get_url_ok( $order, $mmlib ) {
      $url = $mmlib->adjust_url_lang( $mmlib->wc_url( 'order_received', $order ) );
      return esc_url( add_query_arg( 'utm_nooverride', '1',  $url ) );
    }

    /**
     * Returns the site URL for orders failed.
     */
    public static function get_url_ko( $order, $mmlib ) {
      return $mmlib->adjust_url_lang( $mmlib->wc_url( 'order_failed', $order ) );
    }

    /**
     * Returns the right XPay code for the selected language.
     */
    public static function get_language( $mmlib ) {
        $lang = $mmlib->get_current_language_2dgtlwr();
        if ( ! empty( $lang ) ) {
            switch ( $lang ) {
                case 'it' : return 'ITA'; // Italiano
                case 'es' : return 'SPA'; // Spagnolo
                case 'fr' : return 'FRA'; // Francese
                case 'de' : return 'GER'; // Tedesco
                case 'ja' : return 'JPG'; // Giapponese
                case 'zh' : return 'CHI'; // Cinese
                case 'ar' : return 'ARA'; // Arabo
                case 'ru' : return 'RUS'; // Russo
                default   : return 'ENG'; // Inglese
            }
        }

        return 'ENG';
    }

    /**
     * Validate order's prefix and update the settings value.
     */
    public static function get_cleaned_prefix( &$order_prefix ) {
        if ( ! empty( $order_prefix ) ) {

            // only alphanumeric charactes
            $order_prefix = preg_replace( "/[^A-Za-z0-9]/", '', $order_prefix );

            // max 15 char
            $order_prefix = substr( $order_prefix, 0, 15 );

            return $order_prefix;
        }

        return '';
    }

    /**
     * Returns the right ID used by WooCommerce, removing the prefix used to prevent duplicate order ids
     */
    public static function get_order_id( $raw_order_id, $order_prefix, $mmlib ) {
        // Sequential Order Number Pro Compatibility
        if ( function_exists( 'wc_seq_order_number_pro' ) ) {
            $order_id = wc_seq_order_number_pro()->find_order_by_order_number( $raw_order_id );
        }
        elseif ( substr( $raw_order_id, 0, strlen( $order_prefix ) ) == $order_prefix ) {
            // Remove the prefix from the beginning of the prefixed order id.
            $order_id = substr( $raw_order_id, strlen( $order_prefix ) );
        }
        else {
            $mmlib->log_add( "[get_order_id]: ERROR on get_order_id - raw_order_id: ".$raw_order_id." order_prefix: ".$order_prefix );

            $order_id = 0;
        }

        if ( $order_id ) {
            $wp_order_id = substr( $order_id, 0, -4 );

            $id_incr = get_post_meta( $wp_order_id, '_xpay_annullo_id_incr', true );
            if ( ! empty( $id_incr ) && is_numeric( $id_incr ) ) {
                $order_id = substr( $order_id, 0, -4 );
                $mmlib->log_add( "[get_order_id] GET ID incr $id_incr" );
            }
        }

        $mmlib->log_add( "[get_order_id] GET ID $order_id" );

        return $order_id;
    }
}