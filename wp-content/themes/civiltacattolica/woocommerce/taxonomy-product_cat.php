<?php get_template_part('includes/header'); ?>
<main role="main">
    <div class="section section_grey_light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="title_large">
                       <h1><?php
                          $cat = $wp_query->get_queried_object();
                          $product = "book";
                          if($cat->slug == "accenti"){
                              $product = "accenti";
                              echo $cat->name;
                          }else if($cat->slug == "escritos-fiorito"){
                                $product = "escritos-fiorito";
                              echo $cat->name;
                          }else if($cat->slug == "gadget"){
                              $product = "gadget";
                              echo $cat->name;
                          }
                          else
                              echo "Pubblicazioni";
                           ?>
                           </h1>
                        <p><?php
                            echo wpautop($cat->description);
//	                        echo substr(strip_tags($cat->description), 0, 300)."...";
                            ?></p>

                    </div>

                </div><!-- /col-md-12 -->
            </div><!-- /row -->


            <?php
            global $post;
            if(have_posts()): while(have_posts()): the_post();
                setup_postdata($post);
                $prod = new WC_Product( get_the_ID() );
                echo '<div class="col-md-6 margin-bottom-50">';
                    print_book_lista($prod);

            echo '</div>';

             endwhile; ?>

                <div class="row">

                    <div class="col-md-12">

                        <div class="pager">
                            <?php
                            $big = 999999999; // need an unlikely integer
                            echo paginate_links(array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => $paged,
                                'end_size' => 5,
                                'total' => $wp_query->max_num_pages
                            ));
                            ?>

                        </div>

                    </div><!-- /col-md-12 -->

                </div><!-- /row -->
            <?php endif; ?>

        </div><!-- /container -->

    </div><!-- /section -->

</main>




</div><!-- /main_container -->



<?php get_template_part('includes/footer'); ?>
