<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$base_stuff = array(
	'enabled' => array(
		'title' => __( 'Enable/Disable:', $this->plugin_textdomain ),
		'type' => 'checkbox',
		'label' => ' ',
		'description' => __( 'Enable XPay if ticked.', $this->plugin_textdomain ),
		'default' => 'yes'
	),
	'title' => array(
		'title' => __( 'Title:', $this->plugin_textdomain ),
		'type' => 'text',
		'description' => __( 'The title which the user sees during checkout.', $this->plugin_textdomain ),
		'default' => "CartaSì (XPay)"
	),
	'description' => array(
		'title' => __( 'Description:', $this->plugin_textdomain ),
		'type' => 'textarea',
		'description' => __( 'This controls the description which the user sees during checkout.', $this->plugin_textdomain ),
		'default' => "Paga in tutta sicurezza con XPay."
	),
);

$gateway = array(
	'alias' => array(
		'title' => 'Alias:',
		'type' => 'text',
		'description' => __( 'Please enter your Alias as provided by XPay.', $this->plugin_textdomain ),
		'default' => ''
	),
	'mac' => array(
		'title' => __( 'MAC Key:', $this->plugin_textdomain ),
		'type' => 'text',
		'description' => __( 'Your MAC (Message Authentication Code) key as provided by XPay.', $this->plugin_textdomain ),
		'default' => ''
	),
	'is_sandbox' => array(
		'title' => 'Sandbox',
		'type' => 'checkbox',
		'label' => ' ',
		'description' => 'Abilita la modalità di test (sandbox) se selezionato',
		'default' => 'no'
	),
	'max_retry' => array(
		'title' => __( 'Max Retry:', $this->plugin_textdomain ),
		'description' => __( 'Specify the max number of retry for each failed order as configured in the XPay backoffice.', $this->plugin_textdomain ),
		'default' => -1,
		'type' => 'select',
		'options' => array( -1 => '-- Unlimited --') + array_combine( range( 1, 10 ), range( 1, 10 ) ),
	),
	'empty_cart_on_ko' => array(
		'title' => 'Svuota carrello se errore:',
		'type' => 'checkbox',
		'label' => ' ',
		'description' => 'L\'arrivo sulla pagina d\'errore provoca la cancellazione del carrello.',
		'default' => 'yes'
	),
);

$options = array(
	'order_prefix' => array(
		'title' => __( 'Order prefix:', $this->plugin_textdomain ),
		'type' => 'text',
		'description' => __( 'Specify the prefix used for the orders: this prevent to process the same order ID into multiple stores when using the same XPay account and prevents the error of duplicate order ID.', $this->plugin_textdomain )
		. ' ' . __( 'You can enter up to 15 alphanumeric characters: all other symbols/characters will be automatically removed.', $this->plugin_textdomain ),
		'default' => ''
	),
	'show_order_message' => array(
		'title' => __( 'Show order messages:', $this->plugin_textdomain ),
		'label' => ' ',
		'description' => __( 'Checked shows a message to the buyer when returned to the site after the payment has been completed or the payment is failed.', $this->plugin_textdomain ),
		'default' => 'yes',
		'type' => 'checkbox'
	),
);

$cards = $this->get_cards_settings();

$testing = array(
	'testing' => array(
		'title' => __( 'Gateway Testing', $this->plugin_textdomain ),
		'type' => 'title',
		'description' => '',
	),
	'debug' => array(
		'title' => 'Debug Log:',
		'type' => 'checkbox',
		'label' => ' ',
        'description' => sprintf( __( 'Log gateway events on:<br> %s', $this->plugin_textdomain ), $this->plugin_logfile_name ),
		'default' => 'no',
	),
);


return apply_filters('wc-gateway-xpay-gateway-parameters', array_merge( $base_stuff, $gateway, $options, $cards, $testing ));