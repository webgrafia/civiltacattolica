<?php get_template_part('includes/header'); ?>



<?php
// se c'è una diretta
get_template_part('includes/diretta');
?>

<main role="main">

    <div class="section section_grey_dark section_events">

        <div class="section_title">
            <h2 class="title_big">Le Conferenze</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Ogni mese l'ampia «Sala Curci» della Civiltà Cattolica diviene sede di un incontro culturale che dà voce a figure di rilievo e a tematiche emergenti nel dibattito odierno. I dibattiti e le tavole rotonde hanno luogo normalmente un sabato al mese dalle ore 18.00 alle 19.30 circa ed è possibile seguirle anche in streaming, via web. Alle 17.15 del giorno dell’incontro viene sempre celebrata una messa prefestiva per coloro che volessero partecipare.</p>
                </div>
            </div>
        </div>
        <div class="events_fullwidth margin-bottom-30">
            <?php
            global $post;
            if(have_posts()): while(have_posts()): the_post();
            setup_postdata($post);
            ?>
            <div class="event">
                <?php        print_conferenza_lista($post); ?>
            </div>
            <?php
            endwhile;
            ?>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="pager">
                        <?php
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links(array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => $paged,
                            'total' => $wp_query->max_num_pages
                        ));
                        ?>
                    </div>
                </div><!-- /col-md-12 -->
            </div><!-- /row -->
        </div><!-- /container -->
<?php endif; ?>


    </div><!-- /section -->

</main>




</div><!-- /main_container -->




<?php get_template_part('includes/footer'); ?>

