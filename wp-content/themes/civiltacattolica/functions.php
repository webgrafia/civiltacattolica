<?php
/*
All the functions are in the PHP pages in the functions/ folder.
*/
require_once locate_template('/functions/acf.php');

require_once locate_template('/functions/cron.php');

require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');

require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/widgets.php');
require_once locate_template('/functions/search.php');
require_once locate_template('/functions/feedback.php');
require_once locate_template('/functions/woocommerce-setup.php');


require_once locate_template('/functions/CPT.php');


require_once locate_template('/functions/define.php');
require_once locate_template('/functions/display.php');


add_action('after_setup_theme', 'true_load_theme_textdomain');

function true_load_theme_textdomain(){
    load_theme_textdomain( 'wbst', get_template_directory() . '/languages' );
}


// rimuovo il post type post nativo
add_action('admin_menu', 'remove_default_post_type');
function remove_default_post_type()
{
    remove_menu_page('edit.php');
}

$labels = array(
    'add_new' => "Aggiungi nuovo",
    'add_new_item'  => "Aggiungi");

$articolo = new CPT(array(
    'post_type_name' => 'articolo',
    'singular' => 'Articolo',
    'plural' => 'Articoli',
    'slug' => 'articolo'
    ), array(
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'author', 'custom-fields'),
    'labels' => $labels
    )
);

$articolo->menu_icon("dashicons-book-alt");

$articolo->register_taxonomy(array(
    'taxonomy_name' => 'quaderno',
    'singular' => 'Quaderno',
    'plural' => 'Quaderni',
    'slug' => 'quaderno'
), array('labels' => $labels));

$articolo->register_taxonomy(array(
    'taxonomy_name' => 'geoarea',
    'singular' => 'Area Geografica',
    'plural' => 'Aree Geografiche',
    'slug' => 'geoarea'
), array('labels' => $labels));

$articolo->register_taxonomy(array(
    'taxonomy_name' => 'argomento',
    'singular' => 'Argomento',
    'plural' => 'Argomenti',
    'slug' => 'argomento'
), array('labels' => $labels));


$articolo->columns(array(
    'cb' => '<input type="checkbox" />',
    'title' => __('Title'),
    'author' => __('Author'),
    'quaderno' => __('Quaderno'),
    'geoarea' => __('Area Geografica'),
    'argomento' => __('Argomento'),
    'date' => __('Date')
));


/*
 * Recensioni
 */


$recensione = new CPT(array(
	'post_type_name' => 'recensione',
	'singular' => 'Recensione',
	'plural' => 'Recensioni',
	'slug' => 'recensione'
), array(
		'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'author', 'custom-fields'),
		'has_archive' => true,
		'labels' => $labels,
        'with_front' => true
	)
);

$recensione->menu_icon("dashicons-media-spreadsheet");

$recensione->register_taxonomy(array(
	'taxonomy_name' => 'quaderno',
	'singular' => 'Quaderno',
	'plural' => 'Quaderni',
	'slug' => 'quaderno'
), array('labels' => $labels));


$recensione->register_taxonomy(array(
	'taxonomy_name' => 'argomento',
	'singular' => 'Argomento',
	'plural' => 'Argomenti',
	'slug' => 'argomento'
), array('labels' => $labels));


$recensione->columns(array(
	'cb' => '<input type="checkbox" />',
	'title' => __('Title'),
	'author' => __('Author'),
	'quaderno' => __('Quaderno'),
	'argomento' => __('Argomento'),
	'date' => __('Date')
));


/*
 * News
 */


$news = new CPT(array(
    'post_type_name' => 'news',
    'singular' => 'News',
    'plural' => 'News',
    'slug' => 'news'
), array(
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'author', 'custom-fields'),
        'has_archive' => true,
        'labels' => $labels,
        'with_front' => true
    )
);

$news->menu_icon("dashicons-format-aside");


$news->register_taxonomy(array(
    'taxonomy_name' => 'argomento',
    'singular' => 'Argomento',
    'plural' => 'Argomenti',
    'slug' => 'argomento'
), array('labels' => $labels));



/***/

$conferenza = new CPT(array(
    'post_type_name' => 'conferenza',
    'singular' => 'Conferenza',
    'plural' => 'Conferenze',
    'slug' => 'conferenza'
), array(
        'supports' => array( 'title', 'editor', 'thumbnail', 'comments', 'author'),
    'has_archive' => true,
        'labels' => $labels,
    )
);
$conferenza->menu_icon("dashicons-media-video");


$rassegna = new CPT(array(
    'post_type_name' => 'rassegna',
    'singular' => 'Rassegna Stampa',
    'plural' => 'Rassegne Stampa',
    'slug' => 'rassegna'
), array(
        'supports' => array( 'title', 'editor'),
        'has_archive' => true,
        'labels' => $labels,
    )
);
$rassegna->menu_icon("dashicons-media-document");

/*
$book = new CPT(array(
    'post_type_name' => 'book',
    'singular' => 'Google Book',
    'plural' => 'Google Books',
    'slug' => 'book'
), array(
        'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt')
    )
);
$book->menu_icon("dashicons-book");

*/


//add_action('wp_head', 'show_template');
function show_template() {
    global $template;
    echo basename($template);
}


function cc_embed_html( $html ) {
    return '<div class="video-container">' . $html . '</div>';
}

//add_filter( 'embed_oembed_html', 'cc_embed_html', 10, 3 );
add_filter( 'video_embed_html', 'cc_embed_html' ); // Jetpack



function cc_enqueue() {
    wp_enqueue_script('cc_admin_script', get_bloginfo('template_url') . '/js/admin-script.js', array('jquery'), false, true);

}
add_action('admin_init', 'cc_enqueue');



function cc_custom_css() {
   echo "<style> #tagsdiv-product_tag{display:none ;} </style>";
}
add_action('admin_head', 'cc_custom_css');

function cc_bulk_edit($action, $result){
    if ('bulk-posts' == $action && $_GET['mm']!='00' && isset($_GET['jj']) && isset($_GET['aa']) && isset($_GET['hh']) && isset($_GET['mn']) ) {
        $date = $_GET['aa'].'-'.$_GET['mm'].'-'.$_GET['jj'].' '.$_GET['hh'].':'.$_GET['mn'].':00';
        $post_date = date("Y-m-d H:i:s", strtotime($date));
        $post_date_gmt = gmdate("Y-m-d H:i:s",strtotime($date));
        $post_status = (strtotime($post_date) > strtotime(date("Y-m-d H:i:s")))? 'future' : 'publish';

        $post_IDs = array_map('intval', (array) $_GET['post']);
        foreach ($post_IDs as $post_ID) {
            $post_data = array( 'ID' => $post_ID, 'post_date' => $post_date, 'post_date_gmt' => $post_date_gmt, 'post_status' => $post_status, 'edit_date' => true );
            //wp_insert_post( $post_data );
            wp_update_post( $post_data );
        }

    }
}
add_action('check_admin_referer', 'cc_bulk_edit', 10, 2);



add_action('manage_users_columns','cc_manage_users_columns');
function cc_manage_users_columns($column_headers) {
    unset($column_headers['posts']);
    $column_headers['custom_posts'] = 'Articoli';
    return $column_headers;
}


add_action('manage_users_custom_column','cc_manage_users_custom_column',10,3);
function cc_manage_users_custom_column($custom_column,$column_name,$user_id) {
    if ($column_name=='custom_posts') {
        $counts = _cc_get_author_post_type_counts();
        $custom_column = array();
        if (isset($counts[$user_id]) && is_array($counts[$user_id]))
            foreach($counts[$user_id] as $count)
                $custom_column[] = "\t<tr><th>{$count['label']}</th>" .
                    "<td>{$count['count']}</td></tr>";
        $custom_column = implode("\n",$custom_column);
    }
    if (empty($custom_column))
        $custom_column = "Nessuno!";
    else
        $custom_column = "<table>\n{$custom_column}\n</table>";
    return $custom_column;
}


function _cc_get_author_post_type_counts() {
    static $counts;
    if (!isset($counts)) {
        global $wpdb;
        global $wp_post_types;
        $sql = <<<SQL
SELECT
  post_type,
  post_author,
  COUNT(*) AS post_count
FROM
  {$wpdb->posts}
WHERE 1=1
  AND post_type NOT IN ('revision','nav_menu_item')
  AND post_status IN ('publish','pending')
GROUP BY
  post_type,
  post_author
SQL;
        $posts = $wpdb->get_results($sql);
        foreach($posts as $post) {
            $post_type_object = $wp_post_types[$post_type = $post->post_type];
            if (!empty($post_type_object->label))
                $label = $post_type_object->label;
            else if (!empty($post_type_object->labels->name))
                $label = $post_type_object->labels->name;
            else
                $label = ucfirst(str_replace(array('-','_'),' ',$post_type));
            if (!isset($counts[$post_author = $post->post_author]))
                $counts[$post_author] = array();
            $counts[$post_author][] = array(
                'label' => $label,
                'count' => $post->post_count,
            );
        }
    }
    return $counts;
}



// Our hooked in function - $fields is passed via the filter!
add_action( 'woocommerce_after_order_notes', 'cc_custom_checkout_field' );

function cc_custom_checkout_field( $checkout ) {

    echo '<div id="cc_custom_checkout_field">';
    //echo '<h2>'. __('Codice Fiscale o P.IVA','theme_name') .'</h2>';

    woocommerce_form_field( 'codice_fiscale', array(
        'type' => 'text',
        'class' => array('cc-field-class form-row-wide'),
        'label' => __('Codice Fiscale o Partita IVA','theme_name'),
        'placeholder' => __('Codice Fiscale o Partita IVA','theme_name'),
        'required' => false
    ), get_user_meta(get_current_user_id(), 'codice_fiscale', true ));


    woocommerce_form_field( 'telefono', array(
        'type' => 'text',
        'class' => array('cc-field-class form-row-wide'),
        'label' => __('Contatto telefonico','theme_name'),
        'placeholder' => __('Telefono','theme_name'),
        'required' => false
    ), get_user_meta(get_current_user_id(), 'telefono', true ));


    echo '</div>';
/*
    ?>
    <script type="text/javascript">
        jQuery('select#billing_country').live('change', function(){

            var country = jQuery('select#billing_country').val();
            var check_countries = new Array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK', 'IM', 'MC');

            if (country && jQuery.inArray( country, check_countries ) >= 0) {
                jQuery('#cc_custom_checkout_field').fadeIn();
            } else {
                jQuery('#cc_custom_checkout_field').fadeOut();
                jQuery('#cc_custom_checkout_field input').val('');
            }

        });
    </script>
    <?php
*/
}

/**
 * Process the checkout
 */
//add_action('woocommerce_checkout_process', 'cc_custom_checkout_field_process');

function cc_custom_checkout_field_process() {
    if ( ! $_POST['codice_fiscale'] )
        wc_add_notice( 'Inserisci Codice Fiscale o Partita IVA', 'error' );
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'cc_custom_checkout_field_update_order_meta' );

function cc_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['codice_fiscale'] ) ) {
        update_post_meta( $order_id, 'codice_fiscale', sanitize_text_field( $_POST['codice_fiscale'] ) );
        update_user_meta( get_current_user_id(), 'codice_fiscale', sanitize_text_field( $_POST['codice_fiscale'] ) );
    }

    if ( ! empty( $_POST['telefono'] ) ) {
        update_post_meta( $order_id, 'telefono', sanitize_text_field( $_POST['telefono'] ) );
        update_user_meta( get_current_user_id(), 'telefono', sanitize_text_field( $_POST['telefono'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'cc_custom_checkout_field_display_admin_order_meta', 10, 1 );
function cc_custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'. __('Codice Fiscale o P.IVA','theme_name') .'</strong> ' . get_post_meta( $order->id, 'codice_fiscale', true ) . '</p>';
}


add_action( 'show_user_profile', 'cc_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'cc_show_extra_profile_fields' );

function cc_show_extra_profile_fields( $user ) { ?>

    <h3>Campi extra</h3>

    <table class="form-table">

        <tr>
            <th><label for="codice_fiscale">Codice Fiscale / PIVA</label></th>

            <td>
                <input type="text" name="codice_fiscale" id="codice_fiscale" value="<?php echo esc_attr( get_the_author_meta( 'codice_fiscale', $user->ID ) ); ?>" class="regular-text" /><br />

            </td>
        </tr>

        <tr>
            <th><label for="telefono">Contatto telefonico</label></th>

            <td>
                <input type="text" name="telefono" id="telefono" value="<?php echo esc_attr( get_the_author_meta( 'telefono', $user->ID ) ); ?>" class="regular-text" /><br />

            </td>
        </tr>

    </table>
<?php }



function cc_custom_add_to_cart_redirect() {
    return get_bloginfo("url")."/".URL_ACQUISTA."/";
}
//add_filter( 'woocommerce_add_to_cart_redirect', 'cc_custom_add_to_cart_redirect' );



function cc_redirect_attachment_page() {
    if ( is_attachment() ) {
        global $post;
        if ( $post && $post->post_parent ) {
            wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
            exit;
        } else {
            wp_redirect( esc_url( home_url( '/' ) ), 301 );
            exit;
        }
    }
}
add_action( 'template_redirect', 'cc_redirect_attachment_page' );


/*
 * ssl https facebook
 */
function rsssl_recover_shares($html) {
	//replace the https url back to http
	$html = str_replace('og:url="https://www.laciviltacattolica.it', 'og:url="http://www.laciviltacattolica.it', $html);
	$html = str_replace('data-href="https://www.laciviltacattolica.it', 'data-href="http://www.laciviltacattolica.it', $html);
	return $html;
}
add_filter("rsssl_fixer_output","rsssl_recover_shares");



//add_filter( 'posts_request', 'dump_request' );

function dump_request( $input ) {

	error_log($input);

	return $input;
}

function cc_filesystem_method($method, $args, $context, $allow_relaxed_file_ownership) {
    return "direct";
    }

add_filter( 'filesystem_method', 'cc_filesystem_method', 10, 4);



// aggiungo coupon usato alla mail
// The email function hooked that display the text
add_action( 'woocommerce_email_order_details', 'display_applied_coupons', 10, 4 );
function display_applied_coupons( $order, $sent_to_admin, $plain_text, $email ) {
    // Only for admins and when there at least 1 coupon in the order
    if ( ! $sent_to_admin && count($order->get_items('coupon') ) == 0 ) return;

    foreach( $order->get_items('coupon') as $coupon ){
            $coupon_codes[] = $coupon->get_code();
    }

   // For one coupon
   if( count($coupon_codes) == 1 ){
       $coupon_code = reset($coupon_codes);
       echo '<p>'.__( 'Coupon: ').$coupon_code.'<p>';
   } else {
            $coupon_codes = implode( ', ', $coupon_codes);
            echo '<p>'.__( 'Coupons: ').$coupon_codes.'<p>';
    }
}



add_action( 'woocommerce_single_product_summary',
    'woocommerce_template_single_add_to_cart', 10 );

// forzo svuotamento  cache kinsta
function empty_cache_on_all_status_transitions( $new_status, $old_status, $post ) {
    if( $new_status != $old_status ) {
        wp_remote_get("https://www.laciviltacattolica.it/kinsta-clear-cache-all/");
    }
}
add_action(  'transition_post_status',  'empty_cache_on_all_status_transitions', 10, 3 );


/**
 * fix utenti autori in edit post
 */

add_filter( 'wp_dropdown_users_args', 'cc_change_user_dropdown', 10, 2 );

function cc_change_user_dropdown( $query_args, $r ){
    // get screen object
    $screen = get_current_screen();

    if( in_array($screen->post_type, array("post", "page", "articolo", "recensione", "news", "conferenza", "rassegna")) ):
        $query_args['capability'] =  [];
        $query_args['role__in'] =  ["Author", "Administrator", "Civcat", "Redazione", "Editor"];
        unset( $query_args['who'] );
    endif;

    return $query_args;
}

