<?php

$arg = array(
    'posts_per_page' => 6,
    'post_type' => 'conferenza',
    'meta_key'			=> 'data',
    'orderby'			=> 'meta_value_num',
    'order'				=> 'DESC'
);
$conferenze = get_posts($arg);
?><div class="section section_grey_dark section_events">
    <div class="video_container">
        <h2 class="title_large">Conferenze</h2>
    </div>
    <div class="owl-carousel carousel-theme carousel_events">

<?php
foreach ($conferenze as $conferenza) {
?>
        <div class="item">
        <?php        print_conferenza_lista($conferenza); ?>
        </div><!-- /item -->

<?php
}
?>
    </div><!-- /carousel_events -->

    <div class="all_large">

        <a href="<?php echo get_post_type_archive_link("conferenza"); ?>">Vedi tutti i Video</a>

    </div>

</div><!-- /section -->