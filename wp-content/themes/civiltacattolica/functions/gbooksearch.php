<?php

class Gbooksearch {

    /* Google Service Object */
    public $serviceAPI;

    /* Number of successfuly inserted items */
    public $inserted = 0;


    public function __construct() {

        include 'src/Google_Client.php';
        include 'src/contrib/Google_BooksService.php';

        $key = "AIzaSyBqS6k9Ly3raOh4iCDRAjwekyKblIXiUWE";
        $client = new Google_Client();

        if(!empty($key)) {
            $client->setDeveloperKey($key);
		}

        $client->setApplicationName('Google Books Search');
        $this->serviceAPI = new Google_BooksService($client);

    }


    /* Search books
     *
     * @param string keyword for search
     * @param array search parameters
     * @return array with 'kind', 'totalItems' and 'items' parameters. 'items' contains books which we need. 'totalItems' is total number of items returned
     * */
    public function searchBooks($query, $params = array()) {

        try {
            $result = $this->serviceAPI->volumes->listVolumes($query, $params);
            if($result['totalItems'] != 0) {
                return $result;
            } else {
                return array();
            }
        } catch (Google_ServiceException $e) {
            return 'Error while pulling search results';
        }

    }

    /* This function will pull volume data from object, filter values
     * and return needed values in array format. Returned values will include:
     *
     * id
     * --- volume info ---
     * title,subtitle,authors,publisher,publishedDate,description,pageCount,
     * categories,averageRating,smallThumbnail,thumbnail,language,previewLink,
     * infoLink
     * --- sale info ---
     * saleability,isEbook
     * --- access info ---
     * viewability,epub acsTokenLink,pdf acsTOkenLink,downloadLink,webReaderLink
     *
     * @param int id of the book
     * @return array book data
     * */
    public function formatBookdata($id) {

        $data = array();

        // raw data
        $book = $this->serviceAPI->volumes->get($id);


        // https check
        foreach($book as $key => $val) {
            if($key == 'volumeInfo') {
                foreach($book['volumeInfo'] as $key => $val) {
                    if(is_string($val) && is_int(strpos($val, 'http://'))) {
                        $book['volumeInfo'][$key] = str_replace('http://', 'https://', $val);
                    }
                }

                foreach($book['volumeInfo']['imageLinks'] as $key => $val) {
                    $book['volumeInfo']['imageLinks'][$key] = str_replace('http://', 'https://', $val);
                }
            } // volumeInfo

            if($key == 'accessInfo') {
                foreach($book['accessInfo'] as $key => $val) {
                    if(is_string($val) && is_int(strpos($val, 'http://'))) {
                        $book['accessInfo'][$key] = str_replace('http://', 'https://', $val);
                    }
                }
            } // accessInfo
        }


        $data['id'] = isset($book['id']) ? $book['id'] : '';
        // volume info
        $data['title'] = isset($book['volumeInfo']['title']) ? $book['volumeInfo']['title'] : '';
        $data['subtitle'] = isset($book['volumeInfo']['subtitle']) ? $book['volumeInfo']['subtitle'] : '';
        $data['authors'] = isset($book['volumeInfo']['authors']) ? implode(', ', $book['volumeInfo']['authors']) : '';
        $data['publisher'] = isset($book['volumeInfo']['publisher']) ? $book['volumeInfo']['publisher'] : '';
        $data['publishedDate'] = isset($book['volumeInfo']['publishedDate']) ? $book['volumeInfo']['publishedDate'] : '';
        $data['description'] = isset($book['volumeInfo']['description']) ? $book['volumeInfo']['description'] : '';
        $data['pageCount'] = isset($book['volumeInfo']['pageCount']) ? $book['volumeInfo']['pageCount'] : '';
        $data['categories'] = isset($book['volumeInfo']['categories']) ? $book['volumeInfo']['categories'] : array();
        $data['averageRating'] = isset($book['volumeInfo']['averageRating']) ? $book['volumeInfo']['averageRating'] : '';
        $data['smallThumbnail'] = isset($book['volumeInfo']['imageLinks']['smallThumbnail']) ? $book['volumeInfo']['imageLinks']['smallThumbnail'] : '';
        $data['mediumImage'] = isset($book['volumeInfo']['imageLinks']['medium']) ? $book['volumeInfo']['imageLinks']['medium'] : '';
        $data['largeImage'] = isset($book['volumeInfo']['imageLinks']['large']) ? $book['volumeInfo']['imageLinks']['large'] : '';
        $data['thumbnail'] = isset($book['volumeInfo']['imageLinks']['thumbnail']) ? $book['volumeInfo']['imageLinks']['thumbnail'] : '';
        $data['language'] = isset($book['volumeInfo']['language']) ? $book['volumeInfo']['language'] : '';
        $data['previewLink'] = isset($book['volumeInfo']['previewLink']) ? $book['volumeInfo']['previewLink'] : '';
        $data['infoLink'] = isset($book['volumeInfo']['infoLink']) ? $book['volumeInfo']['infoLink'] : '';

        // sale info
        $data['saleability'] = isset($book['saleInfo']['saleability']) ? $book['saleInfo']['saleability'] : '';
        $data['isEbook'] = isset($data['saleInfo']['isEbook']) ? 'yes' : 'no';

        //access info
        $data['viewability'] = isset($book['accessInfo']['viewability']) ? $book['accessInfo']['viewability'] : '';
        $data['epubacsTokenLink'] = isset($book['accessInfo']['epub']['acsTokenLink']) ? $book['accessInfo']['epub']['acsTokenLink'] : '';

        // if download link exists it will be passed here, otherwise pdf access token link is used
        if(isset($book['accessInfo']['pdf']['acsTokenLink'])) {
            $data['pdfdownloadlink'] = $book['accessInfo']['pdf']['acsTokenLink'];
        } else if(isset($book['accessInfo']['pdf']['downloadLink'])) {
            $data['pdfdownloadlink'] = $book['accessInfo']['pdf']['downloadLink'];
        } else {
            $data['pdfdownloadlink'] = '';
        }

        $data['webReaderLink'] = isset($book['accessInfo']['webReaderLink']) ? $book['accessInfo']['webReaderLink'] : '';

        return $data;

    }


}

