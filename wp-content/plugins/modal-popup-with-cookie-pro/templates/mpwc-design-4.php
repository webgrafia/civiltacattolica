<?php
/**
 * Popup design4
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;
?>
<div class="splash mpwc-popup-wrp <?php echo $design;?>" id="mpwc-popup-wrp-<?php echo $post_id;?>">
	<div class="mpwc-popup-body">		
		<?php if( empty($popup_meta['hideclsbtn']) ) { ?>
		<a href="javascript:void(0);" class="mpwc-popup-close" title="<?php _e('Close', 'mpwc'); ?>"></a>
		<?php } ?>	
		<div class="mpwc-popup-cnt-wrp">
			<div class="mpwc-popup-cnt-inr-wrp mpwc-clearfix">
				<div class="mpwc-content-wrp">
					<div class="mpwc-medium-5 mpwc-columns mpwc-content-left">
						<?php  
							$feat_image = mpwc_pro_get_post_featured_image($post_id);
							if(!empty($feat_image))
							{
							?>
								<img src="<?php echo $feat_image;?>">
							<?php
							}
						?>
					</div>
					<div class="mpwc-medium-7 mpwc-columns mpwc-content-center">
						<div class="mpwc-top-bar">
							<?php if( !empty($popup_meta['main_heading']) ) { ?>
								<div class="mpwc_main_heading"><?php echo $popup_meta['main_heading']; ?></div>
							<?php } 
							if( !empty($popup_meta['sub_heading']) ) { ?>
								<div class="mpwc_sub_heading"><?php echo $popup_meta['sub_heading']; ?></div>
							<?php } ?>
						</div>
				
						<div class="mpwc-content-block">
						<?php 
							$content_post = get_post($post_id);
							$popupaoc_content = $content_post->post_content;
							$popupaoc_content = apply_filters('the_content', $popupaoc_content);
							$popupaoc_content = str_replace(']]>', ']]&gt;', $popupaoc_content);				
							echo $popupaoc_content;
						?>
					</div>
						<?php 
							if(!empty($popup_meta['action_btn_txt'])){
						?>
						<div class="mpwc-btn">
							<a href="<?php  echo $action_link;?>" class="mpwc-popup-action-btn" style="<?php echo $btn_style;?>">
								<?php echo $popup_meta['action_btn_txt'];?>
							</a>
						</div>
						<?php
							}
						?>	
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>