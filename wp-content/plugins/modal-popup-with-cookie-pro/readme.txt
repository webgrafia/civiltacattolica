=== Modal Popup with Cookie Integration Pro ===
Contributors: wponlinesupport, anoopranawat
Tags: advertise, lightbox, marketing, pop over, pop-up, popover, popup, subsription popup, shortcode popup, promotion, cookie popup, age restriction popup, automatic popup, exit popup, fly-in, full screen popup, html popup, layer popup, lead, lightbox popup, Mailing list pop-up, modal, modal window, notification bar, optin, optinmonster, pop over, pop-up, popover, popup, popup builder, popup contact form, popup form, popup lock, popup subscription, Responsive Popup, simple popup, social popup, video popup, wordpress popup, youtube popup
Requires at least: 3.1
Tested up to: 4.8
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create and manage powerful promotion modal popups for your WordPress blog or website. Plugin that will help you to grab your visitors’ attention to introduce them your offers, discounts or other kind of promotional notices.

== Description ==

Create and manage powerful promotion modal popups for your WordPress blog or website. Plugin that will help you to grab your visitors’ attention to introduce them your offers, discounts or other kind of promotional notices.

Insert your pop-up into any page or a post, easily and fast. Popups that open automatically, are the best solution to attract your visitor’s attention. With 8 Display positions, utilize any location on screen. 


== Installation ==

1. Upload the 'WP Modal Popup with Cookie Integration Pro' Plugin folder to the '/wp-content/plugins/' directory.
2. Activate the "WP Modal Popup with Cookie Integration Pro" Plugin list plugin through the 'Plugins' menu in WordPress.
3. Go to 'PopUps Pro' menu tab and design your popup with different settings.

== Changelog ==

= 1.1.1 (8-7-2017) =
* Updated plugin licence file
* Fixed some css issue

= 1.0.1 =
* [+] Added plugin translation .PO and .MO files under language folder
* Fixed some css issue

= 1.0 =
* Initial release.

== Upgrade Notice ==

= 1.1.1 (8-7-2017) =
* Updated plugin licence file
* Fixed some css issue

= 1.0.1 =
* [+] Added plugin translation .PO and .MO files under language folder
* Fixed some css issue

= 1.0 =
* Initial release.