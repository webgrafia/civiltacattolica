(function( $ ) {
    $(phone_share.color_picker_fields).each(function () {
        $('#' + this).wpColorPicker({
            defaultColor: phone_share.default_options[phone_share.current_tab][this]
        });
    });

    var iconStyle = $('#icon_style');
    var titleColor = $('tr.hidden');

    if ('button' === $(':selected', iconStyle).val()) {
        titleColor.toggle();
    }

    iconStyle.change(function () {
        if ('button' === $(':selected', iconStyle).val()) {
            titleColor.show();
        } else {
            titleColor.hide();
        }
    });

    /**
     * Rewrite translation
     */
    $.uix.multiselect.i18n[''].itemsAvailable_nil = 'No options available';

    $("#messengers").multiselect({
        availableListPosition: 'left',
        sortable: true,
        collapsableGroups: false,
        searchField: false,
        splitRatio: 0.50
    });
})( jQuery );