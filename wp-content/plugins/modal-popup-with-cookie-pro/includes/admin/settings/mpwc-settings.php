<?php
/**
 * Settings Page
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

$post_types = mpwc_pro_get_post_types();

?>

<div class="wrap mpwc-settings">
	
	<h2><?php _e( 'Modal Popup  Pro Settings', 'modal-popup-with-cookie' ); ?></h2><br/>
	
	<?php
	// Success message
	if( isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true' ) 
	{
		echo '<div id="message" class="updated notice notice-success is-dismissible">
				<p><strong>'.__("Your changes saved successfully.", "modal-popup-with-cookie").'</strong></p>
			  </div>';
	}
	?>	
	<form action="options.php" method="POST" id="mpwc-settings-form" class="mpwc-settings-form">
		
		<?php
		    settings_fields( 'mpwc_pro_plugin_options' );
		    global $mpwc_pro_options;
		    $mpwc_post_types = mpwc_pro_get_option('mpwc_pro_post_types',array());
		   	$welcome_popup_posts = mpwc_pro_get_mpwc_popups();
		   	$exit_popup_posts = mpwc_pro_get_mpwc_popups('exit');
		?>
		
		
		<div id="mpwc-global-settings" class="post-box-container mpwc-global-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="mpwc-settings" class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

							<!-- Settings box title -->
							<h3 class="hndle">
								<span><?php _e( 'Popup Global Settings', 'modal-popup-with-cookie' ); ?></span>
							</h3>
							
							<div class="inside">
							
							<table class="form-table mpwc-settings-tbl">
								<tbody>
									<tr>
										<th scope="row">
											<label for="mpwc-enable"><?php _e('Enable', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<input id="mpwc-enable" type="checkbox" name="mpwc_pro_options[mpwc_pro_enable]" value="1" <?php checked(mpwc_pro_get_option('mpwc_pro_enable'),1);?>><br>
											<span class="description"><?php _e('Check this box to enable modal popup.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="mpwc-enable"><?php _e('Modal Popup Settings Display On', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<?php 
												foreach ($post_types as $post_key => $post_type) {
													$checked_cls = '';
													if(in_array($post_key,$mpwc_post_types))
													{
														$checked_cls = 'checked = "checked"';
													}
													?>
													<label for="type-<?php echo $post_type;?>">
														<input type="checkbox" id="type-<?php echo $post_type;?>" name="mpwc_pro_options[mpwc_pro_post_types][]" value="<?php echo $post_key;?>" <?php echo $checked_cls;?>><?php echo $post_type;?>
													</label><br>
													<?php
												}
											?>
											<span class="description"><?php _e('Check post type to display modal popup setting metabox on these post types.', 'modal-popup-with-cookie'); ?></span>											
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="mpwc-cookie-prefix"><?php _e('Cookie Prefix', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<input type="text" id="mpwc-cookie-prefix" name="mpwc_pro_options[mpwc_pro_cookie_prefix]" value="<?php echo mpwc_pro_escape_attr(mpwc_pro_get_option('mpwc_pro_cookie_prefix'));?>"><br>
											<span class="description"><?php _e('You can use it for separate cookies from different sites on the same domain.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="mpwc-mpwc_pro_enable_mobile"><?php _e('Enable For Mobile Device', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<input id="mpwc-mpwc_pro_enable_mobile" type="checkbox" name="mpwc_pro_options[mpwc_pro_enable_mobile]" value="1" <?php checked(mpwc_pro_get_option('mpwc_pro_enable_mobile'),1);?>><br>
											<span class="description"><?php _e('Check this box to enable modal popup in mobile device.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>
									<tr>
										<td colspan="2" valign="top" scope="row">
											<input type="submit" id="mpwc-settings-reset" name="mpwc-settings-reset" class="button button-default right" value="<?php _e('Reset All Changes','modal-popup-with-cookie');?>" />
											<input type="submit" id="mpwc-settings-submit" name="mpwc-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','modal-popup-with-cookie');?>" /> 
											
										</td>
									</tr>
								</tbody>
							 </table>

						</div><!-- .inside -->
					</div><!-- #mpwc-settings -->
				</div><!-- .meta-box-sortables ui-sortable -->
			</div><!-- .metabox-holder -->
		</div><!-- #mpwc-global-settings -->

		<div id="mpwc-popup-welcome-settings" class="post-box-container mpwc-global-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="mpwc-welcome-settings" class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

							<!-- Settings box title -->
							<h3 class="hndle">
								<span><?php _e( 'Popup Welcome Settings', 'modal-popup-with-cookie' ); ?></span>
							</h3>
							
							<div class="inside">
							
							<table class="form-table mpwc-settings-tbl">
								<tbody>
									<tr>
										<th scope="row">
											<label for="mpwc_pro_default_popup"><?php _e('Select Wecome Popup', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<select name="mpwc_pro_options[mpwc_pro_default_popup]" id="mpwc_pro_default_popup">
												<option value="disable" <?php echo selected('disable',mpwc_pro_get_option('mpwc_pro_default_popup'));?>><?php _e('Select Welcome Popup','modal-popup-with-cookie');?></option>
												<?php 
												if(!empty($welcome_popup_posts ))
												{
													foreach ($welcome_popup_posts  as $popup_id => $popup_title) {
														echo '<option value="'.$popup_id.'" '.selected($popup_id,mpwc_pro_get_option('mpwc_pro_default_popup')).'>'.$popup_title.'</option>';
													}
												}
												?>
											</select><br>
											<span class="description"><?php _e('Select default welcome popup.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="mpwc-settings"><?php _e('Display In', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<label for="mpwc_display_in_home"><input  id="mpwc_display_in_home" type="checkbox" name="mpwc_pro_options[mpwc_pro_display_in][home]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_display_in']['home'])) checked($mpwc_pro_options['mpwc_pro_display_in']['home'],1);?>><?php _e('Home','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_display_in_pages"><input id="mpwc_display_in_pages" type="checkbox" name="mpwc_pro_options[mpwc_pro_display_in][pages]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_display_in']['pages'])) checked($mpwc_pro_options['mpwc_pro_display_in']['pages'],1);?>><?php _e('Pages','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_display_in_posts"><input id="mpwc_display_in_posts" type="checkbox" name="mpwc_pro_options[mpwc_pro_display_in][posts]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_display_in']['posts'])) checked($mpwc_pro_options['mpwc_pro_display_in']['posts'],1);?>><?php _e('Posts','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_display_in_404"><input id="mpwc_display_in_404" type="checkbox" name="mpwc_pro_options[mpwc_pro_display_in][404]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_display_in']['404'])) checked($mpwc_pro_options['mpwc_pro_display_in']['404'],1);?>><?php _e('404 Page','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_display_in_others"><input id="mpwc_display_in_others" type="checkbox" name="mpwc_pro_options[mpwc_pro_display_in][others]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_display_in']['others'])) checked($mpwc_pro_options['mpwc_pro_display_in']['others'],1);?>><?php _e('Categories, Archive Or Tags.','modal-popup-with-cookie');?></label><br>
											<span class="description"><?php _e('check for default welcome popup display on these pages.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>
									<tr>
										<td colspan="2" valign="top" scope="row">
											<input type="submit" id="mpwc-settings-reset" name="mpwc-settings-reset" class="button button-default right" value="<?php _e('Reset All Changes','modal-popup-with-cookie');?>" />
											<input type="submit" id="mpwc-settings-submit" name="mpwc-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','modal-popup-with-cookie');?>" />
										</td>
									</tr>
								</tbody>
							 </table>

						</div><!-- .inside -->
					</div><!-- #mpwc-welcome-settings -->
				</div><!-- .meta-box-sortables ui-sortable -->
			</div><!-- .metabox-holder -->
		</div><!-- #mpwc-popup-welcome-settings -->

		<div id="mpwc-popup-exit-settings" class="post-box-container mpwc-exit-settings">
			<div class="metabox-holder">
				<div class="meta-box-sortables ui-sortable">
					<div id="mpwc-exit-settings" class="postbox">

						<button class="handlediv button-link" type="button"><span class="toggle-indicator"></span></button>

							<!-- Settings box title -->
							<h3 class="hndle">
								<span><?php _e( 'Popup Exit Settings', 'modal-popup-with-cookie' ); ?></span>
							</h3>
							
							<div class="inside">
							
							<table class="form-table mpwc-settings-tbl">
								<tbody>
									<tr>
										<th scope="row">
											<label for="mpwc_pro_exit_default_popup"><?php _e('Select Exit Popup', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<select name="mpwc_pro_options[mpwc_pro_exit_default_popup]" id="mpwc_pro_exit_default_popup">
												<option value="disable" <?php echo selected('disable',mpwc_pro_get_option('mpwc_pro_exit_default_popup'));?>><?php _e('Select Exit Popup','modal-popup-with-cookie');?></option>
												<?php 
												if(!empty($exit_popup_posts ))
												{
													foreach ($exit_popup_posts  as $popup_id => $popup_title) {
														echo '<option value="'.$popup_id.'" '.selected($popup_id,mpwc_pro_get_option('mpwc_pro_exit_default_popup')).'>'.$popup_title.'</option>';
													}
												}
												?>
											</select><br>
											<span class="description"><?php _e('Select default exit popup.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>
									<tr>
										<th scope="row">
											<label for="mpwc-settings"><?php _e('Display In', 'modal-popup-with-cookie'); ?></label>
										</th>
										<td>
											<label for="mpwc_exit_display_in_home"><input  id="mpwc_exit_display_in_home" type="checkbox" name="mpwc_pro_options[mpwc_pro_exit_display_in][home]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_exit_display_in']['home'])) checked($mpwc_pro_options['mpwc_pro_exit_display_in']['home'],1);?>><?php _e('Home','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_exit_display_in_pages"><input id="mpwc_exit_display_in_pages" type="checkbox" name="mpwc_pro_options[mpwc_pro_exit_display_in][pages]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_exit_display_in']['pages'])) checked($mpwc_pro_options['mpwc_pro_exit_display_in']['pages'],1);?>><?php _e('Pages','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_exit_display_in_posts"><input id="mpwc_exit_display_in_posts" type="checkbox" name="mpwc_pro_options[mpwc_pro_exit_display_in][posts]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_exit_display_in']['posts'])) checked($mpwc_pro_options['mpwc_pro_exit_display_in']['posts'],1);?>><?php _e('Posts','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_exit_display_in_404"><input id="mpwc_exit_display_in_404" type="checkbox" name="mpwc_pro_options[mpwc_pro_exit_display_in][404]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_exit_display_in']['404'])) checked($mpwc_pro_options['mpwc_pro_exit_display_in']['404'],1);?>><?php _e('404 Page','modal-popup-with-cookie');?></label><br>
											<label for="mpwc_exit_display_in_others"><input id="mpwc_exit_display_in_others" type="checkbox" name="mpwc_pro_options[mpwc_pro_exit_display_in][others]" value="1" <?php if(isset($mpwc_pro_options['mpwc_pro_exit_display_in']['others'])) checked($mpwc_pro_options['mpwc_pro_exit_display_in']['others'],1);?>><?php _e('Categories, Archive Or Tags.','modal-popup-with-cookie');?></label><br>
											<span class="description"><?php _e('check for default exit popup display on these pages.', 'modal-popup-with-cookie'); ?></span>
										</td>
									</tr>								
									<tr>
										<td colspan="2" valign="top" scope="row">
											<input type="submit" id="mpwc-settings-reset" name="mpwc-settings-reset" class="button button-default right" value="<?php _e('Reset All Changes','modal-popup-with-cookie');?>" />
											<input type="submit" id="mpwc-settings-submit" name="mpwc-settings-submit" class="button button-primary right" value="<?php _e('Save Changes','modal-popup-with-cookie');?>" />
										</td>
									</tr>
								</tbody>
							 </table>

						</div><!-- .inside -->
					</div><!-- #mpwc-exit-settings -->
				</div><!-- .meta-box-sortables ui-sortable -->
			</div><!-- .metabox-holder -->
		</div><!-- #mpwc-popup-exit-settings -->
		
		
	</form><!-- end .mpwc-settings-form -->
	
</div><!-- end .mpwc-settings -->