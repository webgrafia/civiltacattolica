<?php
wp_redirect(home_url());
exit();


get_template_part('includes/header'); ?>
<main role="main">
    <div class="section section_white section_nopadding_top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="search_form form-group">
                        <h1>Errore 404</h1>
                        <br>
                        <p>Ci dispiace! Il contenuto che cerchi non è all’indirizzo indicato. Prova con il nostro motore di ricerca:</p>

                        <form action="<?php bloginfo("url"); ?>">
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="s">
                            </div>
                            <div class="col-md-3">
                                <input type="submit" class="btn btn-default btn-lg btn-block" value="Cerca">
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
