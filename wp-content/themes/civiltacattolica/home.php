<?php get_template_part('includes/header'); ?>

<main role="main">

<?php
$apertura = get_field("apertura", "option");
if($apertura == "slider")
    get_template_part('includes/slider');
else if($apertura == "diretta")
    get_template_part('includes/diretta');

?>

<?php get_template_part('includes/home-quaderni'); ?>

</main>

<?php get_template_part('includes/home-ricerca'); ?>

<?php get_template_part('includes/home-recensioni'); ?>


<?php // get_template_part('includes/home-newsletter'); ?>
<?php get_template_part('includes/home-conference'); ?>


<?php get_template_part('includes/home-books'); ?>


<?php get_template_part('includes/home-external'); ?>

<?php get_template_part('includes/footer'); ?>
