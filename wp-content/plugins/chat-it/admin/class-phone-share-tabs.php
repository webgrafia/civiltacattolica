<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * SEO Cleaner Admin Settings Class
 */
class Phone_Share_Tabs {

	private $page;

	private $tabs;

	public function __construct( $page, $tabs, $default_tab ) {
		$this->page    = $page;
		$this->tabs    = $tabs;
		$this->default = $default_tab;
	}

	public function get_tabs() {
		return $this->tabs;
	}

	/**
	 * Retrieve current tab
	 */
	public function get_current_tab() {

		$data = ( isset( $_POST['action'] ) && 'update' === $_POST['action'] ) ? $_POST : $_GET;

		if ( isset( $data['tab'] ) ) {

			$tab  = (string) $data['tab'];
			$tabs = $this->get_tabs();

			if ( isset( $tabs[ $tab ] ) ) {
				return $tab;
			}
		}

		return $this->default;
	}

	public function get_tab_url( $tab ) {
		return add_query_arg( array(
			'page' => $this->page,
			'tab'  => $tab
		), admin_url( 'options-general.php' )
		);
	}

	public function get_tab_fields( $fields, $current_tab ) {
		$tab_fields = array();

		foreach ( $fields as $name => $field ) {
			if ( in_array( $current_tab, $field['tabs'] ) ) {
				array_push( $tab_fields, $name );
			}
		}

		return $tab_fields;
	}

	public function do_tab_navigation() {
		?>
		<h2 class="nav-tab-wrapper">
			<?php foreach ( $this->get_tabs() as $name => $tab ): ?>
				<a href="<?php echo $this->get_tab_url( $name ) ?>"
				   class="nav-tab <?php echo( $this->get_current_tab() == $name ? 'nav-tab-active' : '' ) ?>">
					<?php echo $tab['title'] ?>
				</a>
			<?php endforeach; ?>
		</h2>
		<?php
	}

}