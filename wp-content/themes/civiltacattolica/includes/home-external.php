<div class="section section_grey_light section_boxes">

    <div class="container">

        <div class="row">

            <div class="col-md-4 col-sm-4">
                <h2><a href="<?php echo get_post_type_archive_link("rassegna"); ?>">Rassegna Stampa</a></h2>
                <ul class="list_bullet list_bullet_square">
                    <?php
                    $arg = array(
                        'posts_per_page' => 5,
                        'post_type' => 'rassegna',
                    );
                    $rassegne = get_posts($arg);
                    foreach ($rassegne as $rassegna) {
                        ?>
                    <li>
                        <p><?php echo get_field("fonte", $rassegna->ID); ?> · <?php echo get_field("data", $rassegna->ID); ?></p>
                        <h4><a href="<?php echo get_permalink($rassegna->ID); ?>"><?php echo $rassegna->post_title; ?></a></h4>
                    </li>

<?php
                    }
                    ?>
                </ul>

            </div><!-- /col-md-4 -->

            <div class="col-md-4 col-sm-4">

                <div class="twitter_boxes">
                    <a class="twitter-timeline" data-height="500" href="https://twitter.com/CivCatt"></a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div><!-- /twitter_boxes -->

            </div><!-- /col-md-4 -->

            <div class="col-md-4 col-sm-4 fbstream">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.5&appId=443271375714375";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));

                    jQuery(document).ready(function($) {
                        $(window).bind("load resize", function(){
                            setTimeout(function() {
                                var container_width = $('#container').width();
                                $('#container').html('<div class="fb-page" ' + 'data-href="http://www.facebook.com/civiltacattolica"' + ' data-width="' + container_width + '" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://www.facebook.com/civiltacattolica"><a href="http://www.facebook.com/civiltacattolica">La Civiltà Cattolica</a></blockquote></div></div>');
                                FB.XFBML.parse( );
                            }, 100);
                        });
                    });

                </script>
                <div id="container" style="width:100%;">
                    <div class="fb-page" data-href="http://www.facebook.com/civiltacattolica" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="http://www.facebook.com/IniciativaAutoMat"><a href="http://www.facebook.com/civiltacattolica">La Civiltà Cattolica</a></blockquote></div></div>
                </div>            </div><!-- /col-md-4 -->

        </div><!-- /row -->

    </div><!-- /container -->

</div><!-- /section -->

</div><!-- /main_container -->