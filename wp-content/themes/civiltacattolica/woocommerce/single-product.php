<?php get_template_part('includes/header'); ?>
    <main role="main">
    <div class="section section_white section_nopadding_top">

<?php while ( have_posts() ) : the_post(); ?>

    <?php wc_get_template_part( 'content', 'single-product' ); ?>

<?php endwhile; // end of the loop. ?>
</div>
    </main>
<?php get_template_part('includes/footer'); ?>