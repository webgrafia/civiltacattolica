<?php get_template_part('includes/header');
global $wp_query;

?>

<main role="main">
    <div class="section section_large_padding section_grey_light">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
                    <?php
                    if(is_tax()){
                        ?>
                        <div class="author_content"><?php
                            the_archive_title( '<h1>', '</h1>' );
                            ?></div>

                    <?php
                    }else if(is_post_type_archive("news")){
                    ?>
                        <div class="author_content">
                            <h1 style="color: #9f0038">News</h1>
                        </div>
                    <?php
                    }
                    ?>

                    <div class="grid scroll">
                        <?php
                        global $wp_query;
                        $paged = get_query_var('paged') ? get_query_var('paged') : 1;
                        if (have_posts()) {

                            while (have_posts()) { the_post();
                                ?>
                                <div class="grid-item grid-item-small">
                                    <div class="copybooks_container copybooks_container_small copybooks_container_small_author">
                                        <?php  print_box_lista($post, false, true); ?>
                                    </div>
                                    <!-- /copybooks_container -->
                                </div><!-- /grid-item -->
                            <?php
                            }
                        }
                        ?>
                    </div>
                    <!-- /grid -->
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="pager">
                        <?php
                        $big = 999999999; // need an unlikely integer
                        echo paginate_links(array(
                            'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                            'format' => '?paged=%#%',
                            'current' => max( 1, $paged ),
                            'total' => $wp_query->max_num_pages
                        ));
                        ?>

                    </div>
                </div>
                <!-- /col-md-12 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /section -->

</main>


</div><!-- /main_container -->


<?php get_template_part('includes/footer'); ?>
