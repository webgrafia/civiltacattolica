<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $post,$wp_version;

$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix

// Getting saved values
$welcome_popupid 		= get_post_meta( $post->ID, $prefix.'welcome_popupid', true );
$exit_popupid 		= get_post_meta( $post->ID, $prefix.'exit_popupid', true );

// Get modal popup posts list
$welcome_popup_posts = mpwc_pro_get_mpwc_popups();
$exit_popup_posts = mpwc_pro_get_mpwc_popups('exit');
?>
<table class="form-table mpwc-post-sett-table">
	<tbody>
		<tr>
			<th scope="row">
				<label for="mpwc-welcome-popup"><?php _e('Welcome Popup', 'modal-popup-with-cookie'); ?>:</label>
			</th>
			<td>
				<select name="<?php echo $prefix;?>welcome_popupid" id="mpwc-welcome-popup" class="mpwc-select-box mpwc-welcome-popup">
					<option value="default" <?php selected('default',$welcome_popupid);?>> <?php _e('Select Welcome Popup','modal-popup-with-cookie');?> </option>
					<option value="disable" <?php selected('disable',$welcome_popupid);?>> <?php _e('Disable','modal-popup-with-cookie');?> </option>
					<?php 
					if(!empty($welcome_popup_posts ))
					{
						foreach ($welcome_popup_posts  as $popup_id => $popup_title) {
							echo '<option value="'.$popup_id.'" '.selected($popup_id,$welcome_popupid).'>'.$popup_title.'</option>';
						}
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<th scope="row">
				<label for="mpwc-exit-popup"><?php _e('Exit Popup', 'modal-popup-with-cookie'); ?>:</label>
			</th>
			<td>
				<select name="<?php echo $prefix;?>exit_popupid" id="mpwc-exit-popup" class="mpwc-select-box mpwc-exit-popup">
					<option value="default" <?php selected('default',$exit_popupid);?>> <?php _e('Select Exit Popup','modal-popup-with-cookie');?> </option>
					<option value="disable" <?php selected('disable',$exit_popupid);?>> <?php _e('Disable','modal-popup-with-cookie');?> </option>
					<?php 
					if(!empty($exit_popup_posts ))
					{
						foreach ($exit_popup_posts  as $popup_id => $popup_title) {
							echo '<option value="'.$popup_id.'" '.selected($popup_id,$exit_popupid).'>'.$popup_title.'</option>';
						}
					}
					?>
				</select>
			</td>
		</tr>		
	</tbody>
</table><!-- end .mpwc-post-sett-table-->