<?php
/**
 * Post Type Functions
 *
 * Handles all custom post types of plugin
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Setup Popup Post Type
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 **/
function mpwc_pro_register_post_types() {
	
	// Popup post type
	$popup_post_type_labels = array(
									'name'					=>	__('PopUps Pro','modal-popup-with-cookie'),
									'singular_name'			=>	__('PopUp','modal-popup-with-cookie'),
									'add_new'				=>	__('Add New Popup','modal-popup-with-cookie'),
									'add_new_item'			=>	__('Add New Popup','modal-popup-with-cookie'),
									'edit_item'				=>	__('Edit Popup','modal-popup-with-cookie'),
									'new_item'				=>	__('New Popup','modal-popup-with-cookie'),
									'all_items'				=>	__('All Popups','modal-popup-with-cookie'),
									'view_item'				=>	__('View Popup','modal-popup-with-cookie'),
									'search_items'			=>	__('Search Popup','modal-popup-with-cookie'),
									'not_found'				=>	__('No Popup found','modal-popup-with-cookie'),
									'not_found_in_trash'	=>	__('No Popup found in Trash','modal-popup-with-cookie'),
									'parent_item_colon'		=>	'',
									'featured_image'		=> __('Popup Image', 'modal-popup-with-cookie'),
									'set_featured_image'	=> __('Set Popup Image', 'modal-popup-with-cookie'),
									'remove_featured_image'	=> __('Remove Popup Image', 'modal-popup-with-cookie'),
									'menu_name'				=>	__('PopUps Pro','modal-popup-with-cookie'),
								);
	$popup_post_type_args	= array(
									'labels'				=> $popup_post_type_labels,
									'public'				=> false,
									'query_var'				=> false,
									'rewrite'				=> false,
									'show_ui'				=> true,
									'capability_type'		=> 'post',
									'menu_icon'				=> 'dashicons-megaphone',
									'map_meta_cap'			=> true,
									'supports'				=> apply_filters('mpwc_pro_post_supports', array('title','editor','thumbnail'))
							 	);
	
	// Filter to modify popup post type arguments
	$popup_post_type_args = apply_filters( 'mpwc_pro_register_popup__post_type', $popup_post_type_args );
	
	// Register popup post type
	register_post_type( MPWC_PRO_POPUP_POST_TYPE, $popup_post_type_args );
}

// Action to register post type
add_action( 'init', 'mpwc_pro_register_post_types' );

/**
 * Function to update post message for button
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_post_updated_messages( $messages ) {
	
	global $post, $post_ID;
	
	$messages[MPWC_PRO_POPUP_POST_TYPE] = array(
		0	=>	'', // Unused. Messages start at index 1.
		1	=>	sprintf( __( 'Popup updated.', 'modal-popup-with-cookie' ) ),
		2	=>	__( 'Custom field updated.', 'modal-popup-with-cookie' ),
		3	=>	__( 'Custom field deleted.', 'modal-popup-with-cookie' ),
		4	=>	__( 'Popup updated.', 'modal-popup-with-cookie' ),
		5	=>	isset( $_GET['revision'] ) ? sprintf( __( 'Popup restored to revision from %s', 'modal-popup-with-cookie' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6	=>	sprintf( __( 'Popup published.', 'modal-popup-with-cookie' ) ),
		7	=>	__( 'Popup saved.', 'modal-popup-with-cookie' ),
		8	=>	sprintf( __( 'Popup submitted.', 'modal-popup-with-cookie' ) ),
		9	=>	sprintf( __( 'Popup scheduled for: <strong>%1$s</strong>.', 'modal-popup-with-cookie' ),date_i18n( 'M j, Y @ G:i', strtotime( $post->post_date ) ) ),
		10	=>	sprintf( __( 'Popup draft updated.', 'modal-popup-with-cookie' ) ),
	);
	
	return $messages;
}

// Filter to update slider post message
add_filter( 'post_updated_messages', 'mpwc_pro_post_updated_messages' );