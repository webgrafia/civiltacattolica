<?php
/**
 * Functions File
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Update default settings
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_default_settings(){
	
	global $mpwc_pro_options;
	
	$mpwc_pro_options = array(
								'mpwc_pro_enable'						=> '1',
								'mpwc_pro_default_popup'				=> '0',
								'mpwc_pro_cookie_enable'				=>'1',
								'mpwc_pro_cookie_prefix'				=> '',
								'mpwc_pro_post_types'					=> array('page','post'),
								'mpwc_pro_enable_mobile'				=> 1,
								'mpwc_pro_display_in' 					=>	array(	
																				'home' 	=> 0,
																				'pages'	=> 0,
																				'posts'	=> 0,
																				'404'	=> 0,
																				'others'=> 0
																			),
								'mpwc_pro_exit_default_popup'			=> '0',
								'mpwc_pro_exit_display_in' 				=>	array(	
																				'home' 	=> 0,
																				'pages'	=> 0,
																				'posts'	=> 0,
																				'404'	=> 0,
																				'others'=> 0
																			),							
							);
	
	$default_options = apply_filters('mpwc_pro_options_default_values', $mpwc_pro_options );
	
	// Update default options
	update_option( 'mpwc_pro_options', $default_options );
	
	// Overwrite global variable when option is update
	$mpwc_pro_options = mpwc_pro_get_settings();
}

/**
 * Reset default settings
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_reset_default_settings(){
	$mpwc_pro_options = array(
								'mpwc_pro_enable'						=> '1',
								'mpwc_pro_default_popup'				=> '0',
								'mpwc_pro_cookie_enable'				=>'1',
								'mpwc_pro_cookie_prefix'				=> '',
								'mpwc_pro_post_types'					=> array('page','post'),
								'mpwc_pro_enable_mobile'				=> 1,
								'mpwc_pro_display_in' 					=>	array(	
																				'home' 	=> 0,
																				'pages'	=> 0,
																				'posts'	=> 0,
																				'404'	=> 0,
																				'others'=> 0
																			),
								'mpwc_pro_exit_default_popup'			=> '0',
								'mpwc_pro_exit_display_in' 				=>	array(	
																				'home' 	=> 0,
																				'pages'	=> 0,
																				'posts'	=> 0,
																				'404'	=> 0,
																				'others'=> 0
																			),							
							);
	return $mpwc_pro_options;
}

/**
 * Get Settings From Option Page
 * 
 * Handles to return all settings value
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_settings() {
	
	$options = get_option('mpwc_pro_options');

	$settings = is_array($options) 	? $options : array();
	
	return $settings;
}

/**
 * Get an option
 * Looks to see if the specified setting exists, returns default if not
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_option( $key = '', $default = false ) {
	global $mpwc_pro_options;
	
	$value = ! empty( $mpwc_pro_options[ $key ] ) ? $mpwc_pro_options[ $key ] : $default;
	$value = apply_filters( 'mpwc_pro_get_option', $value, $key, $default );
	return apply_filters( 'mpwc_pro_get_option_' . $key, $value, $key, $default );
}

/**
 * Escape Tags & Slashes
 *
 * Handles escapping the slashes and tags
 *
 * @package  Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_escape_attr($data) {
	return esc_attr( stripslashes($data) );
}

/**
 * Strip Slashes From Array
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_slashes_deep($data = array(), $flag = false) {
	
	if($flag != true) {
		$data = mpwc_pro_nohtml_kses($data);
	}
	$data = stripslashes_deep($data);
	return $data;
}

/**
 * Strip Html Tags 
 * 
 * It will sanitize text input (strip html tags, and escape characters)
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_nohtml_kses($data = array()) {
	
	if ( is_array($data) ) {
		
		$data = array_map('mpwc_pro_nohtml_kses', $data);
		
	} elseif ( is_string( $data ) ) {
		
		$data = wp_filter_nohtml_kses($data);
	}
	
	return $data;
}

/**
 * Function to get post types
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_all_post_types() {	
	$buildin = array('page','post');
	$args = array('public'=>true,'_builtin'=>false);
	$cp_types = get_post_types($args,'name');
	$args = array('public'=>true);
	$default_post_types = get_post_types($args,'name');
	$all_post_types = array_merge($default_post_types,$cp_types);
	return apply_filters('mpwc_pro_get_post_types', $all_post_types );	
}

/**
 * Function to get mpwc post 
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_post_types() {
	$exclude_post = array('attachment','revision','nav_menu_item');	
	$all_post_types = mpwc_pro_get_all_post_types();
	$post_types = array();
	foreach ($all_post_types as $post_type => $post_data) {
		if(!in_array( $post_type,$exclude_post)){
			$post_types[$post_type] = $post_data->label;
		}	
	}
	return apply_filters('mpwc_pro_get_post_types', $post_types );	
}

/**
 * Function to unique number value
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_unique() {
	static $unique = 0;
	$unique++;

	return $unique;
}

/**
 * Function to add array after specific key
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_add_array(&$array, $value, $index, $from_last = false) {
    
    if( is_array($array) && is_array($value) ) {

        if( $from_last ) {
            $total_count    = count($array);
            $index          = (!empty($total_count) && ($total_count > $index)) ? ($total_count-$index): $index;
        }
        
        $split_arr  = array_splice($array, max(0, $index));
        $array      = array_merge( $array, $value, $split_arr);
    }
    
    return $array;
}

/**
 * Function to get popup featured image
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
*/
function mpwc_pro_get_post_featured_image( $post_id = '', $size = 'full', $default_img = false ) {
    $size   = !empty($size) ? $size : 'full';
    $image  = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), $size );

    if( !empty($image) ) {
        $image = isset($image[0]) ? $image[0] : '';
    }

    return $image;
}


/**
 * Plugin Popup designs
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_popup_designs(){

	$design_arr = array(
						'mpwc-design-1'	=>	__('Layout- 1','modal-popup-with-cookie'),
						'mpwc-design-2'	=>	__('Layout- 2','modal-popup-with-cookie'),
						'mpwc-design-3'	=>	__('Layout- 3','modal-popup-with-cookie'),
						'mpwc-design-4'	=>	__('Layout- 4','modal-popup-with-cookie'),
						'mpwc-design-5'	=>	__('Layout- 5','modal-popup-with-cookie'),
						'mpwc-design-6'	=>	__('Layout- 6','modal-popup-with-cookie'),
						'mpwc-design-7'	=>	__('Layout- 7','modal-popup-with-cookie'),
						'mpwc-design-8'	=>	__('Layout- 8','modal-popup-with-cookie'),
						'mpwc-design-9'	=>	__('Layout- 9','modal-popup-with-cookie'),
						'mpwc-design-10'=>	__('Layout- 10','modal-popup-with-cookie'),
						
					);

	return apply_filters('mpwc_pro_popup_designs', $design_arr );
}

/**
 * Plugin Popup designs
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_popup_type(){

	$type_arr = array(
						'mpwc-center-center'	=>	__('Center','modal-popup-with-cookie'),
						'mpwc-fullscreen'		=>	__('Full Screen','modal-popup-with-cookie'),
						'mpwc-left-top'			=>	__('Left Top','modal-popup-with-cookie'),
						'mpwc-left-center'		=>	__('Left Center','modal-popup-with-cookie'),
						'mpwc-left-bottom'		=>	__('Left Bottom','modal-popup-with-cookie'),
						'mpwc-right-top'		=>	__('Right Top','modal-popup-with-cookie'),
						'mpwc-right-center'		=>	__('Right Center','modal-popup-with-cookie'),
						'mpwc-right-bottom'		=>	__('Right Bottom','modal-popup-with-cookie'),						
						
					);

	return apply_filters('mpwc_pro_popup_type', $type_arr );
}


/**
 * Plugin Popup when appear
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_popup_when_appear(){

	$when_appear = array(
						'page_load'		=>	__('On Page Load','modal-popup-with-cookie'),
						'inactivity'	=>	__('After X second of inactivity','modal-popup-with-cookie'),
						'scroll'		=>	__('When Page Scroll','modal-popup-with-cookie'),
						'exit'			=> 	__('On Exit Intent','modal-popup-with-cookie'),
					);

	return apply_filters('mpwc_pro_popup_when_appear', $when_appear );
}

function mpwc_get_popup_type($post_id){
	$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix
	$popup_type_meta = get_post_meta( $post_id, $prefix.'when_popup_appear', true );
	$popup_types = mpwc_pro_popup_when_appear();
	if(isset($popup_types[$popup_type_meta])){
		return $popup_types[$popup_type_meta];
	}
	return '';
}


/**
 * Function get repeat options
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_repeat_options()
{
	$repeat_options = array(	'no-repeat' => __('No-Repeat','modal-popup-with-cookie'),
								'repeat' 	=> __('Repeat','modal-popup-with-cookie'),								
								'repeat-x' 	=> __('Repeat-X','modal-popup-with-cookie'),
								'repeat-y' 	=> __('Repeat-Y','modal-popup-with-cookie'),
							);
	return $repeat_options;
}

/**
 * Get popup post type list
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_mpwc_popups($popup_type ='welcome'){
	$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix
	$results = array();
	$arg = array('post_type'	=>MPWC_PRO_POPUP_POST_TYPE,
				'post_status' 	=>  array('publish'),
				'posts_per_page'=>-1);
	if($popup_type == 'welcome')
	{
		$arg['meta_query'] =	array(array('key' 		=> $prefix.'when_popup_appear',
											'value'		=> 'exit',
											'compare' 	=> '!=',
										)
								);
	}
	else{
		$arg['meta_query'] =	array(array('key' 		=> $prefix.'when_popup_appear',
											'value'		=> $popup_type,
											'compare'	=> '=',
										)
								);
	}

	$post_type_query  = new WP_Query($arg);
	// The Loop
	if ( $post_type_query->have_posts() ) :
		while ( $post_type_query->have_posts() ) : $post_type_query->the_post();
			$id = get_the_ID();
			$title = get_the_title();
		 	$results[$id]=$title;
		endwhile;
	endif;
	// Reset Post Data
	wp_reset_postdata();  
	return $results;
}

/**
 * Function get popup meta
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 **/
function mpwc_pro_post_metadata($post_id)
{
	$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix

	$post_data = array();
	
	// set values in $post_data
	$post_data['designs']						= get_post_meta( $post_id, $prefix.'designs', true );
	$post_data['popuptype']						= get_post_meta( $post_id, $prefix.'popuptype', true );
	$post_data['main_heading'] 					= get_post_meta( $post_id, $prefix.'main_heading', true );
	$post_data['sub_heading'] 					= get_post_meta( $post_id, $prefix.'sub_heading', true );
	$post_data['action_btn_txt'] 				= get_post_meta( $post_id, $prefix.'action_btn_txt', true );
	$post_data['action_btn_link'] 				= get_post_meta( $post_id, $prefix.'action_btn_link', true );
	$post_data['when_popup_appear'] 			= get_post_meta( $post_id, $prefix.'when_popup_appear', true );
	$post_data['delay'] 						= get_post_meta( $post_id, $prefix.'delay', true );
	$post_data['x_second'] 						= get_post_meta( $post_id, $prefix.'x_second', true );
	$post_data['x_scroll'] 						= get_post_meta( $post_id, $prefix.'x_scroll', true );
	$post_data['disappear'] 					= get_post_meta( $post_id, $prefix.'disappear', true );
	$post_data['exptime'] 						= get_post_meta( $post_id, $prefix.'exptime', true );
	$post_data['hideclsbtn'] 					= get_post_meta( $post_id, $prefix.'hideclsbtn', true );
	$post_data['clsonesc'] 						= get_post_meta( $post_id, $prefix.'clsonesc', true );
	$post_data['height'] 						= get_post_meta( $post_id, $prefix.'height', true );
	$post_data['width'] 						= get_post_meta( $post_id, $prefix.'width', true );
	$post_data['backgroundclr']					= get_post_meta( $post_id, $prefix.'backgroundclr', true );
	$post_data['backgroundimg']					= get_post_meta( $post_id, $prefix.'backgroundimg', true );
	$post_data['backgroundimg_repeat']			= get_post_meta( $post_id, $prefix.'backgroundimg_repeat', true );
	$post_data['overlay_backgroundclr']			= get_post_meta( $post_id, $prefix.'overlay_backgroundclr', true );
	$post_data['overlay_background_opacity']	= get_post_meta( $post_id, $prefix.'overlay_background_opacity', true );
	$post_data['main_head_font_size']			= get_post_meta( $post_id, $prefix.'main_head_font_size', true );
	$post_data['main_head_fontclr']				= get_post_meta( $post_id, $prefix.'main_head_fontclr', true );
	$post_data['sub_head_font_size']			= get_post_meta( $post_id, $prefix.'sub_head_font_size', true );
	$post_data['sub_head_fontclr']				= get_post_meta( $post_id, $prefix.'sub_head_fontclr', true );
	$post_data['action_btn_link_fontclr']		= get_post_meta( $post_id, $prefix.'action_btn_link_fontclr', true );
	$post_data['action_btn_link_fontsize']		= get_post_meta( $post_id, $prefix.'action_btn_link_fontsize', true );
	$post_data['action_btn_link_hover_fontclr']	= get_post_meta( $post_id, $prefix.'action_btn_link_hover_fontclr', true );
	$post_data['action_btn_link_bgclr']			= get_post_meta( $post_id, $prefix.'action_btn_link_bgclr', true );
	$post_data['action_btn_link_hover_bgclr']	= get_post_meta( $post_id, $prefix.'action_btn_link_hover_bgclr', true );
	$post_data['fontclr'] 						= get_post_meta( $post_id, $prefix.'fontclr', true );
	$post_data['border_width'] 					= get_post_meta( $post_id, $prefix.'border_width', true );
	$post_data['border_radius']					= get_post_meta( $post_id, $prefix.'border_radius', true );
	$post_data['border_color'] 					= get_post_meta( $post_id, $prefix.'border_color', true );
	$post_data['custom_css'] 					= get_post_meta( $post_id, $prefix.'custom_css', true );
	$post_data['custom_js'] 					= get_post_meta( $post_id, $prefix.'custom_js', true );
	return $post_data;
}

/**
 * Function get popup meta
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 **/
function mpwc_pro_popup_config($post_id)
{
	$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix

	$post_data = array();
	
	// set values in $post_data
	$post_data['popup_ID'] 		= $post_id;
	$post_data['when_popup_appear'] 		= get_post_meta( $post_id, $prefix.'when_popup_appear', true );
	$post_data['delay'] 					= get_post_meta( $post_id, $prefix.'delay', true );
	$post_data['x_second'] 					= get_post_meta( $post_id, $prefix.'x_second', true );
	$post_data['x_scroll'] 					= get_post_meta( $post_id, $prefix.'x_scroll', true );
	$post_data['disappear'] 				= get_post_meta( $post_id, $prefix.'disappear', true );
	$post_data['exptime'] 					= get_post_meta( $post_id, $prefix.'exptime', true );
	$post_data['hideclsbtn'] 				= get_post_meta( $post_id, $prefix.'hideclsbtn', true );
	$post_data['clsonesc'] 					= get_post_meta( $post_id, $prefix.'clsonesc', true );
	return $post_data;
}

/**
 * Function check is popup enable or not
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 **/
function mpwc_pro_popup_enable($page_id,$popup_type = 'welcome')
{
	$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix
	$popup_enable = true;
	$mpwc_pro_options = mpwc_pro_get_settings();
	$mpwc_popup_enable = $mpwc_pro_options['mpwc_pro_enable'];
	$mpwc_popup_mobile_enable = $mpwc_pro_options['mpwc_pro_enable_mobile'];
	$welcome_popup_meta = $exit_popup_meta = '';

	if($mpwc_popup_enable == ''){
		return false;
	}

	if($page_id != 'default')
	{	
		$welcome_popup_meta = get_post_meta($page_id,$prefix.'welcome_popupid',true);
		$exit_popup_meta = get_post_meta($page_id,$prefix.'exit_popupid',true);
	}	

	if(wp_is_mobile() && !$mpwc_popup_enable){
		return false;
	}

	if($popup_type == 'welcome' && $welcome_popup_meta == 'disable'){
		return false;
	}

	if($popup_type == 'welcome' && $welcome_popup_meta == 'default'){

		$mpwc_default_welcome_popup = $mpwc_pro_options['mpwc_pro_default_popup'];
		if($mpwc_default_welcome_popup == 'disable'){
			return false;
		}
	}

	if($popup_type == 'exit' && $exit_popup_meta == 'disable'){
		return false;
	}

	if($popup_type == 'exit' && $exit_popup_meta == 'default'){
		$mpwc_default_exit_popup = $mpwc_pro_options['mpwc_pro_exit_default_popup'];
		if($mpwc_default_exit_popup == 'disable'){
			return false;
		}
	}

	return $popup_enable;
}

/**
 * Function check cookie set or not
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0 
 **/
function mpwc_pro_is_cooke_set($post_id,$type='welcome'){
	$mpwc_pro_options = mpwc_pro_get_settings();
	$cookie_prefix = $mpwc_pro_options['mpwc_pro_cookie_prefix'];
	$cookie_var = $cookie_prefix.'mpwc_'.$type.'_'.$post_id;
	if(isset($_COOKIE[$cookie_var])){
		return true;
	}
	return false;
}
/**
 * Convert color hex value to rgb format
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_hex2rgb( $hex = '', $format = 'string' ) {

	if( empty($hex) ) return false;

	$hex = str_replace("#", "", $hex);

	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}

	$rgb = array($r, $g, $b);

	if( $format == 'string' ) {
		$rgb = implode(",", $rgb);
	}

	return $rgb;
}