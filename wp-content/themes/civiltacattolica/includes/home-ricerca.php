<div class="section section_grey section_search">

    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <div class="search_form form-group">

                    <form>

                        <div class="row">

                            <div class="col-md-5">

                                <input class="form-control" type="text" title="Cerca" placeholder="Cerca tra i Quaderni" name="s" value="">

                            </div><!-- /col-md-5 -->

                            <div class="col-md-4">

<?php

$args = array(
    'show_option_none' => __( 'Quaderno N°' ),
    'show_count'       => 0,
    'name'       => "quaderno",
    'orderby'          => 'name',
    'order'          => 'desc',
    'echo'             => 1,
    'taxonomy'         => "quaderno",
    'value_field'      => "slug",
);
wp_dropdown_categories( $args);
?>

                            </div><!-- /col-md-4 -->

                            <div class="col-md-3">

                                <input type="submit" class="btn btn-default btn-lg btn-block" value="Cerca">

                            </div><!-- /col-md-3 -->

                        </div><!-- /row -->

                    </form>

                    <div class="form_links">
                        <a href="<?php bloginfo("url"); ?>/<?php echo URL_RICERCA_AVANZATA; ?>">Ricerca Avanzata</a>
                        <a href="<?php bloginfo("url"); ?>/<?php echo URL_ARGOMENTI; ?>">Ricerca per Argomenti</a>
                    </div>

                </div>

                <div id="map"></div>

            </div><!-- /col-md-8 -->

            <div class="col-md-4">
                <?php get_template_part('includes/box-abbonamenti'); ?>
            </div><!-- /col-md-4 -->


        </div><!-- /row -->

    </div><!-- /container -->

</div><!-- /section -->