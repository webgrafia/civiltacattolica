<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Handles Refunds.
 */
class WC_Gateway_XPay_Refund {

    public static $this_gateway;
    private static $live_url = 'https://ecommerce.keyclient.it/ecomm/ecomm/XPayBo';
    private static $sandbox_url = 'https://coll-ecommerce.keyclient.it/ecomm/ecomm/XPayBo';

    /**
     * Can the order be refunded?
     * @param  WC_Order $order
     * @return bool
     */
    public static function can_refund_order( $order ) {
        return $order && $order->get_transaction_id();
    }

    /**
     * Process a refund if supported.
     * @param  int    $order_id
     * @param  float  $amount
     * @param  string $reason
     * @return bool True or false based on success, or a WP_Error object
     */
    public static function process_xpay_refund( $order_id, $amount = null, $reason = '', $this_gateway ) {
        self::$this_gateway = $this_gateway;

        $order = wc_get_order( $order_id );

        if ( ! self::can_refund_order( $order ) ) {
            self::$this_gateway->mmlib->log_add( 'Refund Failed: No transaction ID' );
            return new WP_Error( 'error', __( 'Refund Failed: No transaction ID', 'woocommerce' ) );
        }

        $result = self::refund_order( $order, $amount, $reason );

        if ( is_wp_error( $result ) ) {
            self::$this_gateway->mmlib->log_add( 'Refund Failed: ' . $result->get_error_message() );
            return new WP_Error( 'error', $result->get_error_message() );
        }

        if ( !empty( $result ) ) {
            self::$this_gateway->mmlib->log_add( 'Refund OK' );
            $order->add_order_note( sprintf( __( 'Refunded %s - Refund ID: %s', 'woocommerce' ), $amount, $result ) );
            return true;
        }

        return false;
    }

    public static function get_id_op( $order_id ) {
        $id_incr = get_post_meta( $order_id, '_xpay_refund_id_incr', true );

        if (empty($id_incr)) {
            $id_incr = (int) str_pad($order_id, 10, 0);
        }
        else {
            $id_incr+= 1;
        }

        update_post_meta( $order_id, '_xpay_refund_id_incr', $id_incr );

        return $id_incr;
    }

    /**
     * Get refund request args.
     * @see  // 4.4.1. Messaggio ECREQ (per la richiesta)
     * 
     * @param  WC_Order $order
     * @param  float    $amount
     * @param  string   $reason
     * @return array
     */
    public static function get_request( $order, $amount = null, $reason = '' ) {
        $alias = self::$this_gateway->alias;

        // Order total import - Remove the decimals: 23,50 must become 2350
        // get_total_refunded() rende la quantità rimborsata sia in passato che nel momento della richiesta,
        // quindi riaggiungiamo l'amount.
        // Es. se il totale fosse 0,04, con un refund già effettuato di 0,01 e un secondo refund di 0,01
        // get_total_refunded() renderebbe 0,02.
        $total = (float) ( $order->get_total() - $order->get_total_refunded() + $amount ) * 100;
        $importo = str_pad($total, 9, 0, STR_PAD_LEFT);

        // Refound total import - Remove the decimals: 23,50 must become 2350
        $amount = (float) $amount * 100;
        $importo_op = str_pad($amount, 9, 0, STR_PAD_LEFT);

        $divisa = 978;
        $type_op = 'R';
        $codAut = get_post_meta( $order->id, '_wc_gestpay_xpay_cod_aut', true );

        // Get original order id with prefix
        $codTrans = self::$this_gateway->set_order_id( $order->id, $order, false );
        // Gest order id with incremental prefix
        $id_op = self::get_id_op( $order->id );

        // Calculate the mac to be sent
        $sha1_str = $alias.$codTrans.$id_op.$type_op.$importo.$divisa.$codAut.$importo_op . self::$this_gateway->mac;
        $mac_full = strtoupper(sha1( $sha1_str ));

        $xml = '<?xml version="1.0" encoding="ISO-8859-15"?>';
        $xml.= '<VPOSREQ>';
        $xml.= '<alias>'.$alias.'</alias>';
        $xml.= '<ECREQ>';
        $xml.= '<codTrans>'.$codTrans.'</codTrans>';
        $xml.= '<request_type>FA</request_type>';
        $xml.= '<id_op>'.$id_op.'</id_op>';
        $xml.= '<type_op>'.$type_op.'</type_op>';
        $xml.= '<importo>'.$importo.'</importo>';
        $xml.= '<divisa>'.$divisa.'</divisa>';
        $xml.= '<codAut>'.$codAut.'</codAut>';
        $xml.= '<importo_op>'.$importo_op.'</importo_op>';
        $xml.= '</ECREQ>';
        $xml.= '<mac>'.$mac_full.'</mac>';
        $xml.= '</VPOSREQ>';

        return $xml;
    }

    /**
     * Refund an order.
     * @see  // 4.4.2. Messaggio ECRES (per la risposta)
     * 
     * @param  WC_Order $order
     * @param  float    $amount
     * @param  string   $reason
     * @return array|wp_error The parsed response from paypal, or a WP_Error object
     */
    public static function refund_order( $order, $amount = null, $reason = '' ) {
        $xml_body = self::get_request( $order, $amount, $reason );
        self::$this_gateway->mmlib->log_add( var_export( $xml_body, true ) );

        $url = self::$this_gateway->is_sandbox ? self::$sandbox_url : self::$live_url;

        $response = wp_remote_post( $url, array(
            'method'      => 'POST',
            'headers'     => 'Content-Type: text/xml',
            'body'        => $xml_body,
            'timeout'     => 60,
            'sslverify'   => !self::$this_gateway->is_sandbox,
        ));

        if ( is_wp_error( $response ) ) {
            return $response;
        }

        if ( empty( $response['body'] ) ) {
            return new WP_Error( 'xpay-refunds', 'Empty Response' );
        }

        $xml = simplexml_load_string( $response['body'] );
        
        self::$this_gateway->mmlib->log_add( var_export( $response['body'], true ) );

        if ( isset( $xml->ECRES[0]->esitoRichiesta ) ) {

            $esitoRichiesta = (string) $xml->ECRES[0]->esitoRichiesta;

            // Richiesta eseguita correttamente
            if ( $esitoRichiesta == 0 ) {
                return $xml->ECRES->codTrans;
            }

            // Errore nella richiesta
            $error_messages = array(
                 1 => "Errore nella richiesta: Formato del messaggio errato o campo mancante o errato",
                 3 => "Errore nella richiesta: Campo id_op duplicato",
                16 => "Errore nella richiesta: Campo alias sconosciuto o non abilitato",
                18 => "Errore nella richiesta: operazione negata dall'emittente della carta di credito",
                 2 => "Errore nella richiesta: Errore imprevisto durante l'elaborazione della richiesta",
                 8 => "Errore nella richiesta: mac errato",
                21 => "Errore nell'operazione: Campo codTrans sconosciuto",
                22 => "Errore nell'operazione: operazione non eseguibile (es. storno superiore all'incasso)",
            );

            if ( isset( $error_messages[$esitoRichiesta] ) ) {
                self::$this_gateway->mmlib->log_add( var_export( $error_messages[$esitoRichiesta], true ) );
                return new WP_Error( 'xpay-refunds', $error_messages[$esitoRichiesta] );
            }

        }
        
        return new WP_Error( 'xpay-refunds', 'Errore generico' );
    }
}
