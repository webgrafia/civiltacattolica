<?php
/*
 * Do-Validate-Then-Update (for Wordpress Developers)
 *
 * URI: http://www.mauromascia.com/shop/product/do-validate-then-update-system-for-wordpress-developers
 * Version: 1.0
 * Author: Mauro Mascia (baba_mmx)
 * Author URI: http://www.mauromascia.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Support: info@mauromascia.com
 * Copyright: © 2013-2015 Mauro Mascia
 *
 * Thanks to:
 * @ Jeremy Clark (clark-technet.com) https://github.com/jeremyclark13/automatic-theme-plugin-update
 * @ http://wp.tutsplus.com/tutorials/plugins/a-guide-to-the-wordpress-http-api-automatic-plugin-updates/
 * @ The book "Professional WordPress Plugin Development" by Brad Williams, Ozh Richard, Justin Tadlock
 * @ WooThemes - WooCommerce Software Add-on (http://www.woothemes.com/products/software-add-on/)
 *
 * Side notes
 * http://www.gnu.org/licenses/gpl-faq.html#GPLCommercially
 *
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
 
if ( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'MMascia_Update_Validation_System_Requests' ) ) :

/**
 * Class MMascia_Update_Validation_System_Requests
 *
 * @see woothemes-software-add-on/classes/requests/*
 */
class MMascia_Update_Validation_System_Requests {

  private $token;
  private $api_url;
  private $errors;
  private $testing = false;

  public function __construct( $api_url, $testing ) {
    $this->token = 'update-validation-system';
    $this->api_url = $api_url;
    $this->errors = array();
    $this->testing = $testing;
  }

  /**
   * Activate/Deactivate a given license key for this installation.
   *
   * @param $action
   *  The action to be executed: activate|deactivate|check
   * @param string $args
   *  - email: the user email
   *  - licence_key: the license
   *  - product_id: the product ID as defined in the software tab
   *
   * @return boolean - Whether or not the activation was successful.
   */
  public function do_action( $action, $args, $echo = false, $testing = false ) {
    if ( empty( $args['email'] ) || empty( $args['licence_key'] ) ) {
      return true;
    }

    $style = array(
      'width: 80%',
      'margin: 0 5%',
      'padding: 2% 5%',
      'height: 20px',
      '-webkit-border-radius: 3px',
      'border-radius: 3px',
      'border-width: 1px',
      'border-style: solid',
    );

    $response = $this->request(
      $action,
      array(
        'email' => $args['email'],
        'licence_key' => $args['licence_key'],
        'product_id' => $args['product_id'],
        'instance' => md5( get_bloginfo( 'url' ) )
      )
    );

    if ( isset( $response->error ) ) {
      $this->log_request_error( $response->error );

      $style = array(
        'background-color: #ffebe8',
        'border-color: #c00',
      ) + $style;

      $output = $response->error == 'Invalid License Key' ? 'Chiave di licenza o email non valida' : $response->error;
    }
    else {
      if ( $action == 'is_active' && $response->success == 1 ) {
        $style = array(
          'background-color: #ffffe0',
          'border-color: #e6db55',
        ) + $style;
      }
      else {
        $style = array(
          'background-color: #ffffe0',
          'border-color: #e6db55',
        ) + $style;
      }

      $output = '';
      if ( $action == 'activation' && isset( $response->activated ) && $response->activated ) {
        $output.= "Chiave di licenza attivata correttamente: " . $response->message;
      }
      elseif ( $action == 'deactivation' && isset( $response->reset ) && $response->reset ) {
        $output.= "Chiave di licenza disattivata correttamente.";
      }
      elseif ( $action == 'is_active' ) {
        $output.= $response->success == 1 ? "Chiave di licenza correntemente attiva." : "Chiave di licenza correntemente inattiva.";
      }
      else {
        $output.= "Controllo...";
      }
    }

    if ( $echo ) {
      echo '<div id="request-status" style="' . implode( '; ', $style ) . '">' . $output . '</div>';
    }

    $this->log_request_error( "RESPONSE " . var_export( $response, true ) );

    if ( isset( $response->error ) ) {
      return false;
    }
    if ( $action == 'is_active' && $response->success == 1 ) {
      return true;
    }
    if ( $action == 'activation' && isset( $response->activated ) && $response->activated ) {
      return true;
    }
    if ( $action == 'deactivation' && isset( $response->reset ) && $response->reset ) {
      return true;
    }

    return false;
  }

  /**
   * Make a request to the WooCommerce Software Add On API.
   */
  private function request( $endpoint = 'check', $params = array( ) ) {
    $url = add_query_arg( 'wc-api', 'software-api', $this->api_url );

    $supported_methods = array( 'check', 'activation', 'deactivation', 'is_active' );
    $supported_params = array( 'licence_key', 'email', 'product_id', 'home_url', 'instance' );

    if ( in_array( $endpoint, $supported_methods ) ) {
      $url = add_query_arg( 'request', $endpoint, $url );
    }

    if ( 0 < count( $params ) ) {
      foreach ( $params as $k => $v ) {
        if ( in_array( $k, $supported_params ) ) {
          $url = add_query_arg( $k, $v, $url );
        }
      }
    }

    $this->log_request_error( "REQUEST URL " . $url );

    // Pass if this is a network install on all requests
    $url = add_query_arg( 'network', is_multisite() ? 1 : 0, $url );

    $response = wp_remote_get( $url, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'blocking' => true,
      'headers' => array( ),
      'cookies' => array( ),
      'ssl_verify' => false
      )
    );

    if ( is_wp_error( $response ) ) {
      $data->error = __( 'Request Error' );
    }
    else {
      $data = $response['body'];
      $data = json_decode( $data );
    }

    // Store errors in a transient, to be cleared on each request.
    if ( isset( $data->error ) && ( '' != $data->error ) ) {
      $error = esc_html( $data->error );
      $error = '<strong>' . $error . '</strong>';
      if ( isset( $data->additional_info ) ) {
        $error .= '<br /><br />' . esc_html( $data->additional_info );
      }
      $this->log_request_error( $error );
    }
    elseif ( empty( $data ) ) {
      $error = '<strong>There was an error making your request, please try again.</strong>';
      $this->log_request_error( $error );
    }

    return $data;
  }

  /**
   * Log an error from an API request.
   */
  public function log_request_error( $error ) {
    if ( ! $this->testing ) {
      return;
    }
    error_log( "[MMASCIA_UVS_REQUEST] $error\n" );
    $this->errors[] = $error;
  }

  /**
   * Store logged errors in a temporary transient, such that they survive a page load.
   */
  public function store_error_log() {
    set_transient( $this->token . '-request-error', $this->errors );
  }

  /**
   * Get the current error log.
   */
  public function get_error_log() {
    return get_transient( $this->token . '-request-error' );
  }

  /**
   * Clear the current error log.
   */
  public function clear_error_log() {
    return delete_transient( $this->token . '-request-error' );
  }
}

endif;
?>