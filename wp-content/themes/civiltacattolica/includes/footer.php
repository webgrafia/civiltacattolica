
</div>
<footer role="contentinfo" id="footer" class="footer">

    <div class="section section_grey section_claim">

        <div class="container">

            <div class="row text-center">

                <div class="col-md-12">

                    <p>Beatus Populus Cuius Dominus Deus Eius</p>

                </div><!-- /col-md-12 -->

            </div><!-- /row -->

        </div><!-- /container -->

    </div><!-- /section -->

    <div class="section section_white section_nopadding_bottom section_footer">

        <div class="container">

            <div class="row">

                <div class="col-md-4 col-sm-4">

                    <div class="logo_footer">

                        <a href="#" class="logo"><img src="<?php bloginfo("template_url"); ?>/img/logo_grey.svg" alt=""></a>

                        <p>La Civiltà Cattolica</p>

                        <img class="copybook_footer hidden-xs" src="<?php bloginfo("template_url"); ?>/img/copybook_footer.png" alt="" style="border-bottom: solid 1px #ccc;">

                    </div><!-- /logo_footer -->

                </div><!-- /col-md-4 -->

                <div class="col-md-8 col-sm-8">

                    <div class="row margin-bottom-40 hidden-xs">

                        <div class="col-md-6 col-sm-6">
                            <?php wp_nav_menu(array("theme_location" => "footer-left", "container" => ""));  ?>
                        </div><!-- /col-md-6 -->

                        <div class="col-md-6 col-sm-6">
                            <?php wp_nav_menu(array("theme_location" => "footer-right", "container" => ""));  ?>
                        </div><!-- /col-md-6 -->

                    </div><!-- /row -->


                    <div class="row margin-bottom-40 appstore">

                        <div class="col-md-6 col-sm-6 margin-bottom-10">
                            <a href="<?php echo URL_APPSTORE_IPHONE; ?>"><img class="" src="<?php bloginfo("template_url"); ?>/img/iphone.png" alt=""></a>
                        </div><!-- /col-md-6 -->

                        <div class="col-md-6 col-sm-6">
                            <a href="<?php echo URL_APPSTORE_ANDROID; ?>"><img class="" src="<?php bloginfo("template_url"); ?>/img/android.png" alt=""></a>
                        </div><!-- /col-md-6 -->
<!--
                        <div class="col-md-4 col-sm-4">
                            <a href="<?php echo URL_APPSTORE_AMAZON; ?>"><img class="" src="<?php bloginfo("template_url"); ?>/img/amazon.png" alt=""></a>
                        </div>
//-->
                        <div class="col-md-4 col-sm-4">
                        </div><!-- /col-md-6 -->

                    </div><!-- /row -->


                    <div class="row">

                        <div class="col-md-12">
                            <div class="col-md-12 col-sm-12 margin-bottom-10">
                                © LA CIVILTÀ CATTOLICA <?php echo date("Y"); ?> | Partita iva 00946771003 | Iscrizione R.O.C. 6608 <br>
				<i>La testata fruisce dei contributi diretti editoria L. 198/2016 e d.lgs 70/2017 (ex L. 250/90) (<a style="text-decoration:underline;" href="<?php echo home_url("importo-contributi-editoria"); ?>">importo erogato</a>)</i>
                            </div>
                            <div class="col-md-8 col-sm-8 margin-bottom-10">
                                <small>«I diritti delle immagini e dei testi sono riservati. È espressamente vietata la loro riproduzione con qualsiasi mezzo e l'adattamento totale o parziale» </small>
                            </div>
                            <div class="col-md-4 col-sm-4 margin-bottom-10" style="text-align:center">
                                <A href="http://www.w3.org/WAI/WCAG1A-Conformance"
                                   title="Spiegazione alla conformità del Livello A">
                                    <IMG height="32" width="88"
                                         src="https://www.w3.org/WAI/wcag1A"
                                         alt="Icona del Livello A di conformità alle linee guida 1.0 del W3C-WAI riguardanti l'accessibilità dei contenuti del Web"></A>
                            </div>
                        </div><!-- /col-md-12 -->

                    </div><!-- /row -->

                </div><!-- /col-md-8 -->

            </div><!-- /row -->

        </div><!-- /container -->

    </div><!-- /section -->

</footer><!-- /footer -->

<script src="<?php bloginfo("template_url"); ?>/js/scripts.js"></script>

<script type="text/javascript">

    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('.main_container').offset().top;
        if (window_top > div_top) {
            $('.sticky-wrapper').addClass('is-sticky');
            $('body').addClass('sticked-menu');
        } else {
            $('.sticky-wrapper').removeClass('is-sticky');
            $('body').removeClass('sticked-menu');
        }
    }

    $(function() {
        $(window).scroll(sticky_relocate);
        sticky_relocate();
    });

</script>



<?php wp_footer(); ?>
<?php if(is_home()) cc_gmap_js(); ?>
<?php
if(is_search()){
    ?>
    <script type="text/javascript">
        $(window).load(function() {
            $(".grid").packery();
        });
    </script>
<?php
}
?>
<script>
    jQuery(".lang_menu").hide();
    jQuery(".social_container #language").on('mouseenter',function() {
        jQuery('.lang_menu').toggle(200);
    });

    jQuery(".social_container").on('mouseleave',function() {
        jQuery('.lang_menu').toggle(200);
    });


    jQuery(document).ready(function() {
         jQuery(".add_to_cart_button").removeClass("ajax_add_to_cart")
    });
</script>
</body>
</html>