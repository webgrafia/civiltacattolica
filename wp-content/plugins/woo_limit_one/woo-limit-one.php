<?php
/**
Plugin Name: Woo Limit One Purchase
Plugin URI: http://
Description: This is a plugin to limit a product to one purchase. 
Author: Adam Bowen
Version: 2.1.3
Author URI: http://www.pnw-design.com/
*/

// Display Fields
add_action( 'woocommerce_product_options_general_product_data', 'woo_limit_one_general_fields' );
// Save Fields
add_action( 'woocommerce_process_product_meta', 'woo_limit_one_general_fields_save' );
function woo_limit_one_general_fields() {
  global $woocommerce, $post;
  echo '<div class="options_group">';
  woocommerce_wp_select( 
  array( 
    'id'      => 'woo_limit_one_select_dropdown', 
    'label'   => __( 'Can only purchase once', 'woocommerce' ), 
    'options' => array(
      '0'   => __( 'No', 'woocommerce' ),
      '1'   => __( 'Yes', 'woocommerce' )
      )
    )
  );
  //Disable By IP
  woocommerce_wp_select( 
  array( 
    'id'      => 'woo_limit_one_based_on_ip_select_dropdown', 
    'label'   => __( 'Limit Purchase by IP address', 'woocommerce' ),
    'description' => __( 'If enabled, will prevent guest users from purchasing again if their ip was used in a previous order.', 'woocommerce' ),
    'options' => array(
      '0'   => __( 'No', 'woocommerce' ),
      '1'   => __( 'Yes', 'woocommerce' )
      )
    )
  );
  // Removed from cart message
  woocommerce_wp_text_input(
    array(
    'id' => 'woo_limit_one_remove_from_cart_message',
    'label' => __( 'Removed from cart text', 'woocommerce' ),
    'placeholder' => 'If left empty default is "Looks like you purchased an item in your cart before, it can only be purchased once. Item has been removed from your cart."',
    'desc_tip' => 'true',
    'description' => __( 'Enter the custom removed from cart message, woocommerce error text if product is manually added to cart.', 'woocommerce' )
    )
  ); 
  //Time Period
  woocommerce_wp_select( 
  array( 
    'id'      => 'woo_limit_one_time_dropdown', 
    'label'   => __( 'Time Period', 'woocommerce' ), 
    'options' => array(
      'all'   => __( 'Forever', 'woocommerce' ),
      'day'   => __( 'Days', 'woocommerce' ),
      'month'   => __( 'Month', 'woocommerce' )
      )
    )
  );
  // Time Range
  woocommerce_wp_text_input(
    array(
    'id' => 'woo_limit_one_time_range',
    'label' => __( 'Time Range', 'woocommerce' ),
    'desc_tip' => 'true',
    'description' => __( 'Enter a number 1 for one month, one year, etc', 'woocommerce' )
    )
  ); 
  // Purchased Text
  woocommerce_wp_text_input(
    array(
    'id' => 'woo_limit_one_purchased_text',
    'label' => __( 'Purchased Text', 'woocommerce' ),
    'placeholder' => 'If left empty default is "Purchased"',
    'desc_tip' => 'true',
    'description' => __( 'Enter the custom purchased text here. This is what displays if already purchased', 'woocommerce' )
    )
  ); 
  // Text Field
  woocommerce_wp_checkbox(
    array(
    'id' => 'woo_limit_one_days_left',
    'label' => __( 'Show Days Remaining', 'woocommerce' ),
    'desc_tip' => 'true',
    'description' => __( 'Show the user how many days till they can order again.', 'woocommerce' )
    )
  ); 
  // Text Field
  woocommerce_wp_text_input(
    array(
    'id' => 'woo_limit_one_before_days',
    'label' => __( 'Before Days Left', 'woocommerce' ),
    'placeholder' => 'If left empty default is "Purchasble in"',
    'desc_tip' => 'true',
    'description' => __( 'Text displayed before the days left to purchase', 'woocommerce' )
    )
  ); 
  // Text Field
  woocommerce_wp_text_input(
    array(
    'id' => 'woo_limit_one_after_days',
    'label' => __( 'After Days Left', 'woocommerce' ),
    'placeholder' => 'If left empty default is "days"',
    'desc_tip' => 'true',
    'description' => __( 'Text displayed after the days left to purchase', 'woocommerce' )
    )
  ); 
  echo '</div>';
}
function woo_limit_one_general_fields_save( $post_id ){
  // Select
  $woocommerce_select = $_POST['woo_limit_one_select_dropdown'];
  update_post_meta( $post_id, 'woo_limit_one_select_dropdown', esc_attr( $woocommerce_select ) );  
  $woo_limit_one_based_on_ip_select_dropdown = $_POST['woo_limit_one_based_on_ip_select_dropdown'];
  update_post_meta( $post_id, 'woo_limit_one_based_on_ip_select_dropdown', esc_attr( $woo_limit_one_based_on_ip_select_dropdown ) ); 
  $woo_limit_one_remove_from_cart_message = $_POST['woo_limit_one_remove_from_cart_message'];
  update_post_meta( $post_id, 'woo_limit_one_remove_from_cart_message', esc_attr( $woo_limit_one_remove_from_cart_message ) ); 
  $woo_limit_one_time_dropdown = $_POST['woo_limit_one_time_dropdown'];
  update_post_meta( $post_id, 'woo_limit_one_time_dropdown', esc_attr( $woo_limit_one_time_dropdown ) );  
  $woo_limit_one_time_range = $_POST['woo_limit_one_time_range'];
  update_post_meta( $post_id, 'woo_limit_one_time_range', esc_attr( $woo_limit_one_time_range ) ); 
  $woo_limit_one_purchased_text = $_POST['woo_limit_one_purchased_text'];
  update_post_meta( $post_id, 'woo_limit_one_purchased_text', esc_attr( $woo_limit_one_purchased_text ) ); 
  $woo_limit_one_days_left = $_POST['woo_limit_one_days_left'];
  update_post_meta( $post_id, 'woo_limit_one_days_left', esc_attr( $woo_limit_one_days_left ) );
  $woo_limit_one_before_days = $_POST['woo_limit_one_before_days'];
  update_post_meta( $post_id, 'woo_limit_one_before_days', esc_attr( $woo_limit_one_before_days ) );
  $woo_limit_one_after_days = $_POST['woo_limit_one_after_days'];
  update_post_meta( $post_id, 'woo_limit_one_after_days', esc_attr( $woo_limit_one_after_days ) );
}

add_filter( 'woocommerce_locate_template', 'woo_limit_one_woocommerce_locate_template', 10, 3 );
function woo_limit_one_woocommerce_locate_template( $template, $template_name, $template_path ) {
  global $woocommerce;

  $_template = $template;
  if ( ! $template_path ) {
    $template_path = $woocommerce->template_url;
  }
  $plugin_path  = plugin_dir_path( __FILE__ ) . 'woocommerce/';

  // Look within passed path within the theme - this is priority
  $template = locate_template( array(
    $template_path . $template_name,
    $template_name
  ) );

  // Modification: Get the template from this plugin, if it exists
  if ( ! $template && file_exists( $plugin_path . $template_name ) ) {
    $template = $plugin_path . $template_name;
  }

  // Use default template
  if ( ! $template ) {
    $template = $_template;
  }
  // Return what we found
  return $template;
}

add_action( 'template_redirect', 'remove_product_from_cart' );
function remove_product_from_cart() {
  // Run only in the Cart or Checkout Page
  if( is_cart() || is_checkout() and !is_wc_endpoint_url( 'order-received' ) ) {
    // Cycle through each product in the cart
    foreach( WC()->cart->cart_contents as $prod_in_cart => $cart_item) {
      // Get the Variation or Product ID
      if (isset( $cart_item['variation_id'] ) && $cart_item['variation_id'] != 0) {
        $variation_id = $cart_item['variation_id'];
        $prod_id = wp_get_post_parent_id( $variation_id );
      }else{
        $prod_id = $cart_item['product_id'];
      }

      // Set the current user
      if ( is_user_logged_in() ) {
          $current_user = wp_get_current_user();
          if (wc_customer_bought_product( $current_user->user_email, $current_user->ID, $prod_id)) {
              $customer_bought_product = 1;
          }
      }

      $purchasable_once = get_post_meta( $prod_id, 'woo_limit_one_select_dropdown', true );
      $woo_limit_one_based_on_ip_select_dropdown = get_post_meta( $prod_id, 'woo_limit_one_based_on_ip_select_dropdown', true );
      $woo_limit_one_remove_from_cart_message = get_post_meta( $prod_id, 'woo_limit_one_remove_from_cart_message', true );
      
      //If disable by ip enabled, check previous orders for users ip address
      if ($woo_limit_one_based_on_ip_select_dropdown == "1") {
        $user_ip = $_SERVER['REMOTE_ADDR'];
        //Check orders for ip address
        $args = array(
          'post_type' =>  wc_get_order_types(),
          'post_status' => array_keys( wc_get_order_statuses() ),
          'meta_key' => '_customer_ip_address',
          'meta_value' => $user_ip,
          'posts_per_page' => -1
        );
        $my_query = new WP_Query($args);

        $wc_orders = $my_query->posts;

        foreach ($wc_orders as $order) {
          //print_r($order);
          //Get order meta to get ip address of order
          $order_meta = get_post_meta($order->ID);
          $order_ip = $order_meta['_customer_ip_address'][0];

          //Compare ip addresses
          if ($user_ip == $order_ip) {
            //Get items in order
            $the_order = wc_get_order( $order->ID );
            $items = $the_order->get_items();

            foreach ($items as $item) {
              //print_r($item);
              $itemid = $item['product_id'];
              if ( $itemid == $prod_id ) {
                $ip_purchased_before = 1;
              }
            }
          }
        }
      }
      // Check to see if IDs match
      if ( $purchasable_once == '1' and $ip_purchased_before == '1' || $customer_bought_product == '1') {
        // Get it's unique ID within the Cart
        $prod_unique_id = WC()->cart->generate_cart_id( $prod_id );
        //print_r($prod_unique_id);
        //remove variable product
        if ($cart_item['variation_id'] == $variation_id) {
          //remove single product
          WC()->cart->remove_cart_item($prod_in_cart);
        }
        //remove simple product
        if ($cart_item['product_id'] == $prod_id) {
          WC()->cart->remove_cart_item($prod_in_cart);
        }

        if ($woo_limit_one_remove_from_cart_message) {
          wc_add_notice( $woo_limit_one_remove_from_cart_message, 'error' );
        }else{
          wc_add_notice( 'Looks like you purchased an item in your cart before, it can only be purchased once. Item has been removed from your cart.', 'error' );
        }
      }
    }
  }
}

//Get the order date for this product
function limit_one_check_ordered_product( $id ) {
    // Get All order of current user
    $orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $current_user->ID,
        'post_type'   => wc_get_order_types( 'view-orders' ),
        'post_status' => array_keys( wc_get_order_statuses() )
    ) );

    if ( !$orders ) return false; // return if no order found

    $all_ordered_product = array(); // store products ordered in an array

    foreach ( $orders as $order => $data ) { // Loop through each order
        $order_data = new WC_Order( $data->ID ); // create new object for each order
        foreach ( $order_data->get_items() as $key => $item ) {  // loop through each order item
            // store in array with product ID as key and order date a value
            $all_ordered_product[ $item['product_id'] ] = $data->post_date; 
        }
    }
    if ( isset( $all_ordered_product[ $id ] ) ) { // check if defined ID is found in array
      return date('Y-m-d', strtotime( $all_ordered_product[ $id ] ) );
    }
}