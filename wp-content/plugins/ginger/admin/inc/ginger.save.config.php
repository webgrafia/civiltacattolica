<?php
/**
 * Created by PhpStorm.
 * User: matteobarale
 * Date: 28/05/18
 * Time: 11:49
 */


add_action('wp_ajax_ginger_save_action', 'ginger_save_action');

function ginger_save_action()
{
    if (isset($_POST["key"]) && !empty($_POST["key"])) {
        $key = $_POST["key"];
    } else {
        list($tab, $key) = tabSelection();
    }
    echo $key;
    if (isset($_POST["submit"]) && !wp_verify_nonce($_POST['ginger_options'], 'save_ginger_options')) {
        return;
    }

    if (isset($_POST["submit"])) {
        $params = $_POST;
        unset($params["submit"]);
        unset($params["ginger_options"]);
        unset($params["_wp_http_referer"]);
        unset($params["pll_ajax_backend"]);
        unset($params["action"]);
        unset($params["action"]);

        if ($key == 'ginger_banner') {

            if (isset($params["disable_cookie_button_status"]) && $params["disable_cookie_button_status"] != '1') {
                $params["disable_cookie_button_status"] = '0';
            }
            if (isset($params["read_more_button_status"]) && $params["read_more_button_status"] != '1') {
                $params["read_more_button_status"] = '0';
            }

        }
        if ($key == 'ginger_policy') {
            if ($params["choice"] == "new_page") {
                // controllo se il nome della privacy page è già esistente.
                $title_post = sanitize_text_field($params["privacy_page_title"]);
                $content_post = wp_kses_post($params["privacy_page_content"]);
                $control_page = get_page_by_title($title_post, '', 'page');
                if ($control_page) {
                    $title_post = $title_post . ' ginger';
                }
                $id_privacy_new_page = save_privacy_page($title_post, $content_post);
                $privacy_page_id = (int)$id_privacy_new_page;
                $params["choice"] = 'page';
                $params["ginger_privacy_page"] = $privacy_page_id;

            } elseif (isset($params["ginger_privacy_page"]) && !empty($params["ginger_privacy_page"])) {
                $params["ginger_privacy_page"] = (int)$params["ginger_privacy_page"];
            }
            update_option($key . '_disable_ginger', $params["ginger_privacy_click_scroll"]);
            update_option($key . '_disable_logger', $params["ginger_disable_logger"] == '1');
        }
        update_option($key, $params);
        echo '<div class="updated"><p>' . __('Updated!', 'ginger') . '</p></div>';
    }

    wp_die(); // this is required to terminate immediately and return a proper response
}


// Write our JS below here

function ginger_save_javascript()
{
    list($tab, $key) = tabSelection();

    ?>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            jQuery('#submit').on('click', function (e) {
                e.preventDefault();

                jQuery("body").append('<div id="overloader" ' +
                    'style="position: fixed;\n' +
                    '  top: 0;\n' +
                    '  left: 0;\n' +
                    '  z-index: 99999;\n' +
                    '  width: 100%;\n' +
                    '  height: 100%;\n' +
                    '  background: rgba(0, 0, 0, 0.5);">' +
                    '<div id="saveMessage" style="color: white; width: 100%; text-align: center;height: 100%;top: 50%;position: absolute;font-size: 2rem;">Saving....</div></div>');

                const data = {
                    'action': 'ginger_save_action',
                    'submit': true,
                    'key': "<?php echo $key; ?>"
                };

                jQuery.each(jQuery('#ginger_save_data').serializeArray(), function (i, field) {
                    data[field.name] = field.value;
                });

                console.log(data);


                jQuery.post(ajaxurl, data, function (response) {

                    jQuery('#saveMessage').text('Saved!');
                    setTimeout(function () {
                        jQuery('#overloader').remove();
                    }, 1000);

                    console.log(response);
                });
            });

        });
    </script>
    <?php
}


add_action('wp_ajax_ginger_save_info', 'ginger_save_info');

function ginger_save_info()
{

    if (isset($_POST) && !empty($_POST)) {
        foreach ($_POST as $key => $value) {
            if (substr($key, 0, 7) === 'ginger_') {
                update_option($key, $value, false);
                echo 'Added: ' . $key;
            }
        }
    }

    wp_die(); // this is required to terminate immediately and return a proper response
}



add_action('wp_ajax_ginger_share_to_ws', 'ginger_share_to_ws');

function ginger_share_to_ws()
{
    ginger_send_data();
    wp_die(); // this is required to terminate immediately and return a proper response
}