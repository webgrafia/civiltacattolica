<?php get_template_part('includes/header'); ?>

<main role="main">
    <div class="section section_white section_nopadding_top">

        <?php get_template_part('includes/loops/content', 'rassegna'); ?>

    </div>
</main><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
