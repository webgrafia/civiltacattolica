<?php
function dd($array)
{
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

add_action('admin_head', 'cc_custom_fonts');

function cc_custom_fonts() {
    echo '<style>

.post-type-product #postdivrich{ display:none;}
.post-type-product #woocommerce-product-images{ display:none;}
.post-type-articolo  #quaderno-adder{ display:none; }
.post-type-articolo  #geoarea-adder{ display:none; }
.post-type-page #postimagediv{display:none;}
  </style>';
}

function cc_setup()
{
    // add_editor_style('css/editor-style.css');
    if (function_exists('add_theme_support')) {
        add_theme_support('post-thumbnails');
        //set_post_thumbnail_size( 150, 150 ); // dimensioni di default della miniatura
        update_option('thumbnail_size_w', 170);
        update_option('medium_size_w', 470);
        update_option('large_size_w', 970);
    }

    if (function_exists('add_image_size')) {
        add_image_size('slider', 2000, 900, true);
        add_image_size('apertura', 1200, 600, true);
        add_image_size('lista', 800, 395, true);
        add_image_size('lista_small', 450, 220, true);
        add_image_size('book', 300, 9999, false);
        add_image_size('conferenza', 500, 500, true);
        add_image_size('userthumb', 400, 400, true);
    }
}
add_action('init', 'cc_setup');

function cc_connection_types() {
    p2p_register_connection_type( array(
        'name' => 'articoli_to_articoli',
        'from' => 'articolo',
        'to' => 'articolo',
        'reciprocal' => true,

    ) );

    p2p_register_connection_type( array(
        'name' => 'rassegna_to_articoli',
        'from' => 'articolo',
        'to' => 'rassegna',
        'reciprocal' => true,

    ) );

}
add_action( 'p2p_init', 'cc_connection_types' );


if (!isset($content_width))
    $content_width = 600;

function wbst_excerpt_readmore()
{
    return '&nbsp; <a href="' . get_permalink() . '">' . '&hellip; ' . __('Read more', 'wbst') . ' <i class="glyphicon glyphicon-arrow-right"></i>' . '</a></p>';
}

add_filter('excerpt_more', 'wbst_excerpt_readmore');

function wbst_browser_body_class($classes)
{
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

    if ($is_lynx) $classes[] = 'lynx';
    elseif ($is_gecko) $classes[] = 'gecko';
    elseif ($is_opera) $classes[] = 'opera';
    elseif ($is_NS4) $classes[] = 'ns4';
    elseif ($is_safari) $classes[] = 'safari';
    elseif ($is_chrome) $classes[] = 'chrome';
    elseif ($is_IE) {
        $browser = $_SERVER['HTTP_USER_AGENT'];
        $browser = substr("$browser", 25, 8);
        if ($browser == "MSIE 7.0") {
            $classes[] = 'ie7';
            $classes[] = 'ie';
        } elseif ($browser == "MSIE 6.0") {
            $classes[] = 'ie6';
            $classes[] = 'ie';
        } elseif ($browser == "MSIE 8.0") {
            $classes[] = 'ie8';
            $classes[] = 'ie';
        } elseif ($browser == "MSIE 9.0") {
            $classes[] = 'ie9';
            $classes[] = 'ie';
        } else {
            $classes[] = 'ie';
        }
    } else $classes[] = 'unknown';

    if ($is_iphone) $classes[] = 'iphone';

    return $classes;
}

add_filter('body_class', 'wbst_browser_body_class');


// Bootstrap pagination

if (!function_exists('wbst_pagination')) {
    function wbst_pagination()
    {
        global $wp_query;
        $big = 999999999; // This needs to be an unlikely integer
        // For more options and info view the docs for paginate_links()
        // http://codex.wordpress.org/Function_Reference/paginate_links
        $paginate_links = paginate_links(array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages,
            'mid_size' => 5,
            'prev_next' => True,
            'prev_text' => __('<i class="glyphicon glyphicon-chevron-left"></i> Newer'),
            'next_text' => __('Older <i class="glyphicon glyphicon-chevron-right"></i>'),
            'type' => 'list'
        ));
        $paginate_links = str_replace("<ul class='page-numbers'>", "<ul class='pagination'>", $paginate_links);
        $paginate_links = str_replace("<li><span class='page-numbers current'>", "<li class='active'><a href='#'>", $paginate_links);
        $paginate_links = str_replace("</span>", "</a>", $paginate_links);
        $paginate_links = preg_replace("/\s*page-numbers/", "", $paginate_links);
        // Display the pagination if more than one page is found
        if ($paginate_links) {
            echo $paginate_links;
        }
    }
}

add_theme_support('woocommerce');




function cc_query_filter($query) {
    if ( !is_admin() && $query->is_main_query() ) {
        if(is_author()){
            $query->set('post_type', 'articolo');
            $query->set('posts_per_page', 9);
        }
        if(is_tax("product_cat")){
            $query->set('posts_per_page', 8);
        }
        if(is_post_type_archive("conferenza")){
            $query->set('meta_key', 'data');
            $query->set('orderby', 'meta_value_num');
            $query->set('order','DESC');
            $query->set('posts_per_page', 12);

        }
//        if(is_archive()){
//            $query->set('post_type', 'articolo');
//        }
    }
}

add_action('pre_get_posts','cc_query_filter');


function cc_request($qv) {
    if (isset($qv['feed']) && !isset($qv['post_type']))
        $qv['post_type'] = array("articolo", "news", "recensione", "conferenza","rassegna");
    return $qv;
}
add_filter('request', 'cc_request');