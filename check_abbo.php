<?php
// script per paperlit, verifica l'abbonamento alla rivista

require_once("wp-load.php");
global $wpdb;
$foundit = false;

$mydb = new wpdb('civicatt','Formula188','civicatt','10.0.0.208');

$query = "SELECT * FROM abbonamenti where codice='$_GET[user]' and fattura='$_GET[password]' and datsca>=CURDATE()";

$result = $mydb->get_results($query);

if($result){
$foundit = true;
foreach($result as $row){
    $datainizio= $row->DATABB;
    $datafine= $row->DATSCA;

}

$data1["civiltacattolica"]["issubscribed"] = true;
$data1["civiltacattolica"]["startedon"] = $datainizio."T00:00:00+01:00";
$data1["civiltacattolica"]["expireson"] = $datafine."T23:59:00+01:00";

echo "singlesignon(",json_encode($data1),");";


}else{

// se non c'è nella tabella custom (precedente al nuovo sito) verifico se esiste l'utente wordpress con quelle credenziali

$user = get_user_by( 'login', $_GET["user"] );
if(!$user)
	$user = get_user_by( 'email', $_GET["user"] );
	
      if ( $user && wp_check_password( $_GET["password"], $user->data->user_pass, $user->ID) ){
      // esiste l'utente, controllo che abbia l'abbonamento attivo

$customer_orders = get_posts( array(
    'numberposts' => -1,
    'meta_key'    => '_customer_user',
    'meta_value'  => $user->ID,
    'post_type'   => wc_get_order_types(),
    'post_status' => array_keys( wc_get_order_statuses() ),
) );


          foreach ($customer_orders as $customer_order) {

//print_r($customer_order);

              $orders = new WC_Order($customer_order->ID);
              foreach ($orders->get_items() as $order){

                  $order = new WC_Order($customer_order->ID);
//		  print_r($order->post_status);
if($order->post_status != "wc-completed")
continue;
                  $items = $order->get_items();
                  foreach ( $items as $item ) {
                      $term_list = wp_get_post_terms($item["product_id"], 'product_cat', array("fields" => "all"));

                      foreach ($term_list as $product_cat) {

                          if($product_cat->slug == "abbonamenti"){
                              if(strpos(strtolower($item["name"]),"1 anno")){
                                  $timeend = strtotime($order->order_date." + 1 YEAR");
                              }else if(strpos(strtolower($item["name"]),"2 anni")){
                                  $timeend = strtotime($order->order_date." + 2 YEARS");
                              }else if(strpos(strtolower($item["name"]),"3 anni")){
                                  $timeend = strtotime($order->order_date." + 3 YEARS");
                              }else if(strpos(strtolower($item["name"]),"4 anni")){
                                  $timeend = strtotime($order->order_date." + 4 YEARS");
                              }else if(strpos(strtolower($item["name"]),"5 anni")){
                                  $timeend = strtotime($order->order_date." + 5 YEARS");
                              }

                              if($timeend > time()){
                                  // abbonamento ancora valido
                                  $skipold=true;

                                  $data["civiltacattolica"]["issubscribed"] = true;
                                  $data["civiltacattolica"]["startedon"] = date("c",strtotime($order->order_date));
                                  $data["civiltacattolica"]["expireson"] = date("c",$timeend);
                                  echo "singlesignon(",json_encode($data),");";
                                  $foundit = true;

                                  exit;
                              }

                          }
                      }

                  }

              }


          }
      }
}


if(!$foundit){
	$data = array("issubscribed"=>false,"message"=>"Abbonamento non attivo o dati account non corretti");
	echo "singlesignon(" . json_encode($data) . ");";
}
