<?php /* Template Name: Quaderni */ ?>

<?php get_template_part('includes/header');
global $post;

// parametri paginazione
$num = 12;
$paged = get_query_var("paged");
if (!$paged) $paged = 1;
$offset = (($paged - 1) * $num) + 1;
$all = wp_count_terms('quaderno', array('hide_empty' => true));
$totpages = ceil($all / $num);
?>

<main role="main">

  	  <div class="section section_grey_light">

    	  <div class="container">
<?php if ($paged == 1) {     ?>
    <div class="row margin-bottom-50">

        <div class="col-md-5">

            <div class="heading">

                <div class="title">
                    <h1><?php the_title(); ?></h1>
                </div>
                <?php echo wpautop($post->post_content); ?>
                <a class="btn btn-primary btn-lg btn-anchor btn-block"
                   href="<?php bloginfo("url"); ?>/<?php echo URL_ABBONAMENTI; ?>">Scopri i nostri Abbonamenti</a>

            </div>

        </div>
        <!-- /col-md-5-->

        <div class="col-md-6 col-md-offset-1">

            <div class="box_abbonamenti_large">

                <div class="box_abbonamenti_large_content">

                    <div class="row margin-bottom-30">

                        <div class="col-md-5 col-sm-5">

                            <div class="icons_abbonamenti icon_book_plus_smartphone">

                                <img src="<?php bloginfo("template_url") ?>/img/book_plus_smartphone.svg">

                            </div>

                        </div>
                        <!-- /col-md-5 -->

                        <div class="col-md-7 col-sm-7">

                            <h3><a href="<?php bloginfo("url"); ?>/<?php echo URL_ABBONAMENTI_CARTA; ?>"><strong>Abbonamento</strong>
                                    Quaderno + Digitale</a></h3>

                        </div>
                        <!-- /col-md-7 -->

                    </div>
                    <!-- /row -->

                    <div class="row">

                        <div class="col-md-5 col-sm-5">

                            <div class="icons_abbonamenti icon_tablet">

                                <img src="<?php bloginfo("template_url") ?>/img/tablet.svg">

                            </div>

                        </div>
                        <!-- /col-md-5 -->

                        <div class="col-md-7 col-sm-7">

                            <h3><a href="<?php bloginfo("url"); ?>/<?php echo URL_ABBONAMENTI_DIGITAL; ?>"><strong>Abbonamento</strong>
                                    Digitale</a></h3>

                        </div>
                        <!-- /col-md-7 -->

                    </div>
                    <!-- /row -->

                </div>

                <div class="box_abbonamenti_large_image">
                    <img src="<?php bloginfo("template_url") ?>/img/abbonamenti_large.png">
                </div>

            </div>

        </div>
        <!-- /col-md-6 -->

    </div><!-- /row -->


    <?php

    $last_quaderno = get_terms('quaderno', array(
            'hide_empty' => false,
        'orderby' => "name",
        "order" => "desc",
        "meta_query" => array(
            'relation' => 'OR',
            array(
                'key'       => 'pubblicazione_posticipata',
                'compare'   => 'NOT EXISTS'
            ),
            array(
                'key'       => 'pubblicazione_posticipata',
                'value'       => '1',
                'compare'   => '!='
            )
        ),
        "number" => 1
    ));
    $quaderno = $last_quaderno[0];

// recupero 6 articoli del quaderno

    $arg = array(
        'posts_per_page' => 6,
        'post_type' => 'articolo',
        'tax_query' => array(
            array(
                'taxonomy' => 'quaderno',
                'field' => 'term_id',
                'terms' => $quaderno->term_id,
            )
        )
    );
    $articoli = get_posts($arg);

    ?>
    <div class="row margin-bottom-50">

        <div class="col-md-12">

            <div class="copybooks_container copybooks_container_lead">

                <div class="row margin-bottom-20">

                    <div class="col-md-4">

                        <div class="copybook_lead_title">

                            <p class="lead_title">Ultima Pubblicazione</p>

                            <p class="number"><?php echo str_replace("-","<br>",substr($quaderno->name, 0, -2)); ?><em><?php echo substr($quaderno->name, -2); ?></em></p>

                            <p class="date"><?php echo date_i18n("j F Y", strtotime(get_field("data_pubblicazione", "quaderno_" . $quaderno->term_id))); ?></p>

                        </div>

                    </div>
                    <!-- /col-md-4 -->

                    <div class="col-md-4">

                        <ul>
                            <?php for ($i = 0; $i < 3; $i++) { ?>
                                <li>
                                    <a href="<?php echo get_permalink($articoli[$i]->ID); ?>"><?php echo($articoli[$i]->post_title); ?></a>
                                </li>
                            <?php } ?>
                        </ul>

                    </div>
                    <!-- /col-md-4 -->

                    <div class="col-md-4">

                        <ul>
                            <?php for ($i = 3; $i < 6; $i++) { ?>
                                <li>
                                    <a href="<?php echo get_permalink($articoli[$i]->ID); ?>"><?php echo($articoli[$i]->post_title); ?></a>
                                </li>
                            <?php } ?>
                        </ul>

                    </div>
                    <!-- /col-md-4 -->

                </div>
                <!-- /row -->

                <div class="row text-center">

                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-xs-6">

                        <a class="btn btn-default btn-lg btn-anchor"
                           href="<?php echo get_term_link($quaderno) ?>">Leggi</a>

                    </div>
                    <!-- /col-md-4 -->

                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <?php if (cc_link_buy_quaderno($quaderno)) { ?>
                            <a class="btn btn-primary btn-lg btn-anchor"
                               href="<?php echo cc_link_buy_quaderno($quaderno); ?>">Acquista</a>
                        <?php } ?>
                    </div>
                    <!-- /col-md-4 -->

                </div>
                <!-- /row -->

            </div>
            <!-- /copybooks_container -->

        </div>
        <!-- /col-md-12 -->

    </div><!-- /row -->

    <hr class="margin-bottom-40">
<?php
}



$arg = array('hide_empty' => false, 'orderby' => "name", "order" => "desc",  "meta_query" => array(
    'relation' => 'OR',
    array(
        'key'       => 'pubblicazione_posticipata',
        'compare'   => 'NOT EXISTS'
    ),
    array(
        'key'       => 'pubblicazione_posticipata',
        'value'       => '1',
        'compare'   => '!='
    )
),"number" => $num, "offset" => $offset);

$quadernipaged = get_terms('quaderno', $arg);
$quaderni = array();
foreach ($quadernipaged as $quadp) {
    $quaderni[]=$quadp;
}

//dd($quaderni);

?>

          <div class="row margin-bottom-30 no-margin-bottom-xs">



        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[0]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[1]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
        	   <?php copybook_box($quaderni[2]); ?>
        	  </div><!-- /col-md-4 -->

          </div><!-- /row -->

          <div class="row margin-bottom-30 no-margin-bottom-xs">
        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[3]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[4]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
        	   <?php copybook_box($quaderni[5]); ?>
        	  </div><!-- /col-md-4 -->
          </div><!-- /row -->

          <div class="row margin-bottom-30 no-margin-bottom-xs">
        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[6]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[7]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
        	   <?php copybook_box($quaderni[8]); ?>
        	  </div><!-- /col-md-4 -->
          </div><!-- /row -->

          <div class="row margin-bottom-30 no-margin-bottom-xs">
        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[9]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
                <?php copybook_box($quaderni[10]); ?>
        	  </div><!-- /col-md-4 -->

        	  <div class="col-md-4 col-sm-4">
        	   <?php copybook_box($quaderni[11]); ?>
        	  </div><!-- /col-md-4 -->
          </div><!-- /row -->


          <div class="row">

        	  <div class="col-md-12">

          	  <div class="pager">

<?php
$big = 999999999; // need an unlikely integer

echo paginate_links(array(
    'format' => '?paged=%#%',
    'current' => $paged,
    'total' => $totpages
));
?>

          	  </div>

            </div><!-- /col-md-12 -->

          </div><!-- /row -->

        </div><!-- /container -->

  	  </div><!-- /section -->

	  </main>


    </div><!-- /main_container -->





<?php get_template_part('includes/footer'); ?>