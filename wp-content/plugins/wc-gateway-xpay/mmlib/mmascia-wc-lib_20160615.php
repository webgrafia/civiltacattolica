<?php
/*
 * === Mauro Mascia WooCommerce Library ===
 * 
 * Version: 20160615
 * Author: Mauro Mascia
 * Author URI: http://www.mauromascia.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Support: info@mauromascia.com
 * Copyright © 2013-2016 MAURO MASCIA
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'MMascia_WC_Lib_20160615' ) ) :

class MMascia_WC_Lib_20160615 {

  public $plugin_url;
  public $plugin_path;
  public $plugin_slug;
  public $plugin_textdomain;
  public $plugin_logfile;
  public $wc_logger;
  private $file;

  function __construct( $file = null ) {
    if ( null == $file ) {
      echo '<div id="message" class="error woocommerce-error"><p>FATAL ERROR: MMascia_WC_Lib_20160615 requires the main plugin file!</p></div>';
    }

    $this->plugin_main_file      = $file;
    $this->plugin_url            = trailingslashit( plugins_url( '', $plugin = $file ) );
    $this->plugin_dir_path       = plugin_dir_path( $file );
    $this->plugin_path           = dirname( plugin_basename( $file ) );
    $this->plugin_slug           = basename( $file, '.php' );
    $this->plugin_slug_dashed    = str_replace( "-", "_", $this->plugin_slug );
    $this->plugin_textdomain     = $this->plugin_slug;
    $this->plugin_logfile_name   = $this->get_plugin_logfile_name();
    $this->mmlogo                = $this->plugin_url.basename(dirname( __FILE__ )).'/mm-logo.png';
    $this->form_fields_file      = dirname( $this->plugin_main_file ) . '/inc/init_form_fields.php';
  }

  /**
   * Localize, script and init the gateway
   */
  function __init_gateway( &$this_gw ) {
    $this->gw = $this_gw;

    // Localize
    load_plugin_textdomain( $this->plugin_slug, false, $this->plugin_path . "/languages" );
    load_plugin_textdomain( 'mmlib', false, $this->plugin_path . "/mmlib/lang" );

    // Style
    wp_register_style( $this->plugin_slug.'-css', $this->plugin_url.'/'.$this->plugin_slug.'.css' );
    wp_enqueue_style( $this->plugin_slug.'-css' );

    // Maybe load the strings used on this plugin
    if ( method_exists( $this_gw, 'init_strings' ) ) {
      $this_gw->init_strings();
    }

    // Load form fields and settings
    $this_gw->form_fields = file_exists( $this->form_fields_file ) ? include $this->form_fields_file : $this_gw->init_form_fields();
    $this_gw->init_settings();
    $this->load_card_icons();
  }


  function set_gw( &$this_gw ) {
    $this->send_debug_email = isset( $this_gw->send_debug_email ) ? $this_gw->send_debug_email : false;

    // Maybe check if this gateway is valid for use.
    if ( method_exists( $this_gw, 'is_valid_for_use' ) && ! $this_gw->is_valid_for_use() ) {
      $this_gw->enabled = 'no';
    }
  }

  function get_cards_settings() {
    return array(
      // -- ICONS
      'cards' => array(
        'title' => __( 'Overwrite Card Icons', 'mmlib' ),
        'type' => 'title',
        'description' => __( 'Select the accepted cards to show them as icon', 'mmlib' ),
        'class'       => 'mmnomargin',
      ),
      'card_visa' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'Visa Electron',
        'default' => 'no'
      ),
      'card_mastercard' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'Mastercard',
        'default' => 'no'
      ),
      'card_maestro' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'Maestro',
        'default' => 'no'
      ),
      'card_ae' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'American Express',
        'default' => 'no'
      ),
      'card_dci' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'Diners Club International',
        'default' => 'no'
      ),
      'card_paypal' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'PayPal',
        'default' => 'no'
      ),
      'card_jcb' => array(
        'title' => '',
        'type' => 'checkbox',
        'label' => 'JCB Cards',
        'default' => 'no'
      ),
      'card_postepay' => array(
        'title'       => '',
        'type'        => 'checkbox',
        'label'       => 'PostePay',
        'default'     => 'no'
      ),
    );
  }


  /**
   * Load card icons.
   * Copyright: © 2013-2015 MAURO MASCIA
   */
  function load_card_icons() {
    $cards = array();
    $card_path = $this->plugin_url . basename( dirname( __FILE__ ) ) . '/cards/';

    if ( isset( $this->gw->settings['card_visa'] ) && $this->gw->settings['card_visa'] == "yes" )               $cards[] = $card_path . 'card_visa.jpg';
    if ( isset( $this->gw->settings['card_mastercard'] ) && $this->gw->settings['card_mastercard'] == "yes" )   $cards[] = $card_path . 'card_mastercard.jpg';
    if ( isset( $this->gw->settings['card_maestro'] ) && $this->gw->settings['card_maestro'] == "yes" )         $cards[] = $card_path . 'card_maestro.jpg';
    if ( isset( $this->gw->settings['card_ae'] ) && $this->gw->settings['card_ae'] == "yes" )                   $cards[] = $card_path . 'card_ae.jpg';
    if ( isset( $this->gw->settings['card_dci'] ) && $this->gw->settings['card_dci'] == "yes" )                 $cards[] = $card_path . 'card_dci.jpg';
    if ( isset( $this->gw->settings['card_paypal'] ) && $this->gw->settings['card_paypal'] == "yes" )           $cards[] = $card_path . 'card_paypal.jpg';
    if ( isset( $this->gw->settings['card_jcb'] ) && $this->gw->settings['card_jcb'] == "yes" )                 $cards[] = $card_path . 'card_jcb.jpg';
    if ( isset( $this->gw->settings['card_postepay'] ) && $this->gw->settings['card_postepay'] == "yes" )       $cards[] = $card_path . 'card_postepay.jpg';

    if ( empty( $cards ) ) return;

    // workaround for get_icon() of WC_Payment_Gateway
    // @see abstract-wc-payment-gateway.php
    $cards_string = '';
    foreach ( $cards as $card ) {
      $cards_string .= $card . ( end( $cards ) == $card ? '' : '" /><img src="' );
    }

    $this->gw->icon = $cards_string;
  }


  function get_plugin_logfile_name() {
    return $this->is_wc_gte_20()
      ? ( defined( 'WC_LOG_DIR' ) ? WC_LOG_DIR : '' ) .$this->plugin_slug."-".sanitize_file_name( wp_hash( $this->plugin_slug ) ).'.log'
      : 'wp-content/plugins/woocommerce/logs/' . $this->plugin_slug.'.txt';
  }


  function log_add( $message ) {
    if ( $this->gw->debug ) {
      if ( ! isset( $this->log ) || empty( $this->log ) ) {
        $this->log = $this->wc_logger();
      }
      $this->log->add( $this->plugin_slug, $message );
    }
  }


  // clean and validate order's prefix
  function get_order_prefix( &$settings ) {
    if ( isset( $settings['order_prefix'] ) && ! empty( $settings['order_prefix'] ) ) {
      // allows only alphanumeric charactes
      $prefix = preg_replace( "/[^A-Za-z0-9]/", '', $settings['order_prefix'] );

      // max 15 char
      $prefix = substr( $prefix, 0, 15 );

      // Update the order prefix value
      $settings['order_prefix'] = $prefix;

      return $prefix;
    }

    return '';
  }


  /**
   * Backwards compatible woocommerce log.
   */
  function wc_logger() {
    if ( ! $this->is_wc_gte_21() ) {
      global $woocommerce;
      return $woocommerce->logger();
    }
    else {
      // See wp-admin/admin.php?page=wc-status&tab=logs
      return new WC_Logger();
    }
  }

  /**
   * Returns the WooCommerce version number, backwards compatible to WC 1.x
   * @return null|string
   */
  function get_wc_version() {
    if ( defined( 'WC_VERSION' ) && WC_VERSION ) return WC_VERSION;
    if ( defined( 'WOOCOMMERCE_VERSION' ) && WOOCOMMERCE_VERSION ) return WOOCOMMERCE_VERSION;
    return null;
  }

  /* short checks */
  function is_wc_gte_20() { return version_compare( $this->get_wc_version(), '2.0.0', '>=' ); }
  function is_wc_gte_21() { return version_compare( $this->get_wc_version(), '2.1.0', '>=' ); }
  function is_wc_gte_22() { return version_compare( $this->get_wc_version(), '2.2.0', '>=' ); }
  function is_wc_gte_23() { return version_compare( $this->get_wc_version(), '2.3.0', '>=' ); }

  /**
   * Backwards compatible get URL
   */
  function wc_url( $path, $order ) {
    switch ( $path ) {

      case 'view_order':

        return $this->is_wc_gte_21() ? $order->get_view_order_url() : get_permalink( woocommerce_get_page_id( 'view_order' ) );
        break;

      case 'order_received':

        return $this->gw->get_return_url( $order );
        break;

      case 'order_failed':

        if ( $this->is_wc_gte_21() ) {
          return WC()->cart->get_checkout_url();
        }
        else {
          global $woocommerce;
          return $woocommerce->cart->get_checkout_url();
        }
        
        break;

      case 'pay':

        if ( $this->is_wc_gte_21() ) {
          return $order->get_checkout_payment_url( true );
        }
        else {
          return add_query_arg( array( 'order' => $order->id, 'key' => $order->order_key ), get_permalink( woocommerce_get_page_id( 'pay' ) ) );
        }

        break;

      default:
        return '';
    }
    return '';
  }


  /**
   * Compatibility get order
   */
  function wc_get_order( $the_order = false ) {
    if ( $this->is_wc_gte_22() ) {
      return wc_get_order( $the_order );
    }

    global $post;
    if ( false === $the_order ) {
      $order_id = $post->ID;
    }
    elseif ( $the_order instanceof WP_Post ) {
      $order_id = $the_order->ID;
    }
    elseif ( is_numeric( $the_order ) ) {
      $order_id = $the_order;
    }
    return new WC_Order( $order_id );
  }

  function wc_empty_cart() {
    if ( $this->is_wc_gte_21() ) {
      WC()->cart->empty_cart();
    }
    else {
      global $woocommerce;
      $woocommerce->cart->empty_cart();
    }
  }
  
  // Update order status, add admin order note and empty the cart
  function wc_order_completed( $order, $message, $tx_id='' ) {
    $order->payment_complete( $tx_id );
    $order->add_order_note( $message );
    $this->wc_empty_cart();
    $this->log_add( 'ORDER COMPLETED: ' . $message );
  }
  

  function wc_enqueue_autosubmit() {
    $code = $this->get_autosubmit_js();

    if ( ! $this->is_wc_gte_21() ) {
      global $woocommerce;
      return $woocommerce->add_inline_js( $code );
    }
    else {
      wc_enqueue_js( $code );
    }
  }

  function get_autosubmit_js() {
    $assets_path = str_replace( array( 'http:', 'https:' ), '', WC()->plugin_url() ) . '/assets/';
    $imgloader = $assets_path . 'images/ajax-loader@2x.gif';
    $js = <<<JS
      jQuery('html').block({
        message: '<img src="$imgloader" alt="Redirecting&hellip;" style="float:left;margin-right:10px;"/>Thank you! We are redirecting you to make payment.',
        overlayCSS: {
          background: '#fff',
          opacity: 0.6
        },
        css: {
          padding: 20,
          textAlign: 'center',
          color: '#555',
          border: '3px solid #aaa',
          backgroundColor: '#fff',
          cursor: 'wait',
          lineHeight: '32px'
        }
      });
      jQuery('#submit__{$this->plugin_slug_dashed}').click();
JS;
    return $js;
  }

  /**
   * Create the gateway form, loading the autosubmit javascript.
   */
  function get_gw_form( $action_url, $method, $input_params, $order ) {
    $action_url        = esc_url_raw( $action_url );
    $cancel_url        = esc_url_raw( $order->get_cancel_order_url() );
    $pay_order_str     = 'Pay via '.$this->gw->method_title;
    $cancel_order_str  = 'Cancel order &amp; restore cart';

    $this->wc_enqueue_autosubmit();

    $input_fields = "";
    foreach ( $input_params as $key => $value ) {
      $input_fields.= '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $value ) . '" />';
    }

    return <<<HTML
      <form action="{$action_url}" method="{$method}" id="form__{$this->plugin_slug_dashed}" target="_top">
        $input_fields
        <input type="submit" class="button-alt" id="submit__{$this->plugin_slug_dashed}" value="{$pay_order_str}" />
        <a class="button cancel" href="$cancel_url">{$cancel_order_str}</a>
      </form>
HTML;
  }

  /**
   * Backwards compatible add error
   */
  function wc_add_error( $error ) {
    if ( ! $this->is_wc_gte_21() ) {
      global $woocommerce;
      $woocommerce->add_error( $error );
    }
    else {
      if ( function_exists( 'wc_add_notice' ) ) {
        wc_add_notice( $error, 'error' );
      }
    }
  }

  /**
   * Check if qTranslate-X or mqTranslate is enabled.
   *
   * @return bool true if one of them is active, false otherwise.
   */
  function is_qtranslate_enabled() {
    return ( defined('QTX_VERSION') ||
      in_array( 'qtranslate/qtranslate.php',   (array) get_option( 'active_plugins', array() ) ) ||
        in_array( 'mqtranslate/mqtranslate.php', (array) get_option( 'active_plugins', array() ) ) );
  }

  /**
   * Checks if WooCommerce Subscriptions is active
   *
   * @return bool true if WCS is active, false otherwise.
   */
  function is_subscriptions_active() {
    return in_array( 'woocommerce-subscriptions/woocommerce-subscriptions.php', (array) get_option( 'active_plugins', array() ) );
  }


  /**
   * Returns current language checking for qTranslate|mqTranslate|WPML
   * Fallback on get_locale() if nothing found.
   * @return string
   */
  function get_current_language() {
    if ( $this->is_qtranslate_enabled() ) {
      if ( function_exists( 'qtranxf_getLanguage' ) ) {
        return qtranxf_getLanguage(); // -- qTranslate X
      }
      else if ( function_exists( 'qtrans_getLanguage' ) ) {
        return qtrans_getLanguage(); // -- qTranslate / mqTranslate
      }
    }
    elseif ( defined( 'ICL_LANGUAGE_CODE' ) ) { // --- Wpml
      return ICL_LANGUAGE_CODE;
    }
    return get_locale();
  }

  /**
   * Returns the two characters of the language in lowercase
   * @return string
   */
  function get_current_language_2dgtlwr() {
    return substr( strtolower( $this->get_current_language() ), 0, 2 );
  }

  /**
   * Adjust the URL (pre-path, pre-domain or query)
   * ATM only qTranslate/mqtranslate is supported
   */
  function adjust_url_lang( $url ) {
    if ( $this->is_qtranslate_enabled() && function_exists( 'qtrans_convertURL' ) ) {
      return qtrans_convertURL( $url, $this->get_current_language_2dgtlwr() );
    }
    return $url;
  }


  // Generate the option list
  function get_page_list_as_option() {
    $opt_pages = array( 0 => " -- Select -- " );
    foreach ( get_pages() as $page ) {
      $opt_pages[ $page->ID ] = __( $page->post_title );
    }
    return $opt_pages;
  }

  /**
   * Mostra un messaggio d'errore.
   */
  function show_error( $msg ) {
    echo '<div id="woocommerce_errors" class="error fade"><p>ERRORE: '.$msg.'</p></div>';
  }

  /**
   * Create a SOAP client using the specified URL
   */
  function get_soap_client( $url ) {
    try {
      $client = new SoapClient( $url );
    }
    catch ( Exception $e ) {
      $err = sprintf( __( 'Soap Client Request Exception with error %s' ), $e->getMessage() );
      $this->wc_add_error( $err );
      $this->log_add( '[FATAL ERROR]: ' . $err );

      return false;
    }

    return $client;
  }
  

  /**
   * La maggior parte dei gateway che creo utilizzano la libreria SOAP di PHP.
   * Se questa non è installata nel sistema il gateway non si può utilizzare.
   */
  function check_fatal_soap( $plugin_name ) {
    if ( ! extension_loaded( 'soap' ) ) {
      $this->show_error( 'Per poter utilizzare <strong>'.$plugin_name.'</strong> la libreria SOAP client di PHP deve essere abilitata!' );
      return false;
    }
    return true;
  }

  /**
   * Le query string restituite da alcuni gateway sono molto lunghe e se si utilizza
   * suhosin con un valore basso nella get, allora verranno troncate.
   * Per questo motivo è meglio bloccare l'utilizzo del plugin.
   * Thanks to Andrea Cardinali
   */
  function check_fatal_suhosin( $plugin_name ) {
    if ( is_numeric( @ini_get( 'suhosin.get.max_value_length' ) ) && ( @ini_get( 'suhosin.get.max_value_length' ) < 1024 ) ) {
      $err_suhosin = 'Sul tuo server è presente <a href="http://www.hardened-php.net/suhosin/index.html" target="_blank">PHP Suhosin</a>.<br>Devi aumentare il valore di';
      $err_suhosin.= ' <a href="http://suhosin.org/stories/configuration.html#suhosin-get-max-value-length" target="_blank">suhosin.get.max_value_length</a> almeno a 1024';
      $err_suhosin.= ', perché <strong>'.$plugin_name.'</strong> utilizza delle query string molto lunghe.<br>';
      $err_suhosin.= '<strong>'.$plugin_name.'</strong> non potrà essere utilizzato finché non si aumenta tale valore!';

      $this->show_error( $err_suhosin );
      return false;
    }
    return true;
  }

  /**
   * Safely get and trim data from $_POST
   */
  function get_post( $key ) {

    if ( isset( $_POST[ $key ] ) ) {
      return trim( $_POST[ $key ] );
    }

    return '';
  }

  /**
   * Send an email to the admin
   */
  function admin_mail( $subject, $message ) {
    if ( ! $this->send_debug_email ) return;
    global $woocommerce;
    $mailer = $woocommerce->mailer();
    $mailer->wrap_message( $subject, $message );
    $mailer->send( get_settings( 'admin_email' ), '['.$this->plugin_name.'] ' . $subject, $message );
  }

  /**
   * Check for plugin updates
   */
  function mmascia_start_uvs( $with_debug = false ) {
    // Esegui solo lato admin e se l'utente può aggiornare i plugin
    if ( ! is_admin() || ! current_user_can( 'update_plugins' ) ) return;

    // Verifica che non venga eseguito in tutte le pagine
    // (is_admin è valido anche quando vengono effettuate le chiamate ad admin-ajax)
    global $pagenow;
    if ( in_array( $pagenow, array( 'plugins.php', 'update-core.php' ) )
      || ( $pagenow == 'admin-ajax.php' && isset( $_GET['action'] ) && $_GET['action'] == $this->plugin_slug.'_license' ) ) {

      if ( ! class_exists( 'MMascia_Update_Validation_System' ) ) require_once 'uvs.php';
      if ( class_exists( 'MMascia_Update_Validation_System' ) ) {

        if ( $with_debug ) {
          error_log( "[MMASCIA_LIB] Checking " . $this->plugin_slug . ' updates / Debug: ' . ($with_debug?'true':'false') );
        }

        $uvs = new MMascia_Update_Validation_System(
          array(
            'api_url'       => 'http://www.mauromascia.com',
            'updater_url'   => 'http://updater.mauromascia.com/api',
            'slug'          => $this->plugin_slug,
            'plugin_path'   => plugin_basename( $this->plugin_main_file ),
            'plugin_url'    => $this->plugin_url
          ),
          $with_debug
        );
      }

    }
  }

}

endif; // ! class_exists( 'MMascia_WC_Lib_20160615' )