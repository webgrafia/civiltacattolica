<?php get_template_part('includes/header');

global $post;
$apertura = get_field("apertura", $post->ID);

$immagine = $apertura["sizes"]["slider"];

$dida = $apertura["caption"];
if(!$immagine){
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "slider");
    if ($thumb){
        $immagine = $thumb[0];


        $imgobj = get_post(get_post_thumbnail_id($post->ID));
        $dida = $imgobj->post_excerpt;
    }

}
//else
  //  $immagine = get_bloginfo("template_url") . "/placeholders/placeholder_2000x900.jpg";
unset($immagine);
?>
<main role="main">
    <div class="section section_white section_nopadding_top">
          <div class="container-fullscreen margin-bottom-50">
              <?php if($immagine) { ?>
                <div class="lead_wrapper lead_wrapper_article" style="background-image: url('<?php echo $immagine; ?>');"></div>
                  <?php
                  if($dida != ""){
                      ?><div class="container"><div class="row" style="text-align:right;"> <div class="col-md-12"><i class="didascalia"><?php echo $dida; ?></i></div></div></div><?php
                  }
              } ?>
            </div>
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                $quad = wp_get_post_terms($post->ID, "quaderno");
                ?>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <article>
                                <div class="article_wrapper">
                                    <strong style="font-size:16px;color:#9D0039;">RECENSIONE</strong>
                                    <?php
                                    $autore_recensito = get_field("autore_recensito");
                                    if($autore_recensito){
                                        ?>
                                        <h3 style="text-transform: uppercase; margin-top:14px;"><?php echo $autore_recensito; ?></h3>
                                        <?php
                                    }
                                    ?>
				    <div class="article_title">
                                        <h1><?php echo mb_strtoupper(get_the_title()); ?></h1>
                                        <?php
                                        $sottotitolo = get_field("sottotitolo");
                                        if($sottotitolo) {echo "<h2>".$sottotitolo."</h2>";}
                                        ?>

                                    </div>
                                    <div class="article_details">
                                        <div class="author_container">
                                            <?php
                                            $autore_recensione = get_field("autore_recensione");
	                                            if ( $autore_recensione != "" ) {
		                                            $img  = get_bloginfo("template_url")."/img/book.jpg";
		                                            $link_autore = false;

	                                            }else{
	                                                $userimg = get_field("foto", "user_" . $post->post_author);
	                                                if ($userimg) {
	                                                    $img = $userimg["sizes"]["userthumb"];
	                                                } else {
	                                                    $img  = get_bloginfo("template_url")."/img/logo.svg";
	                                                }
	                                                $link_autore = get_author_posts_url($post->post_author); 	                                                $autore_recensione = get_userdata($post->post_author)->display_name;
		                                            $autore_recensione = get_userdata($post->post_author)->display_name;
	                                            }
	                                               ?>
	                                               <div class="author_thumb">
	                                               <img src="<?php echo $img; ?>"  alt="<?php echo esc_attr(get_the_title()); ?>">
                                                   </div>
                                                   <!-- /author_thumb -->
                                            <p>
	                                            <?php if($link_autore) echo '<a href="'.$link_autore.'">'; ?>
                                                <b><?php echo $autore_recensione; ?></b>
	                                            <?php if($link_autore) echo '</a>'; ?>
                                            </p>
                                        </div>

                                        <!-- /author_container -->
                                        <?php
                                        /*if(comments_open()){ ?>
                                        <p class="comment"><a href="#anchor_comment">Commenta</a></p>
                                        <?php }*/ ?>
                                        <?php
                                        if(!is_wp_error($quad)){
                                            if(is_array($quad)) {
                                                $quadlink = get_term_link($quad[0]);
	                                            $quadname = $quad[0]->name;
                                                if(!is_wp_error($quadlink)){



                                                    ?>
                                                    <p class="comment"><a href="<?php echo $quadlink; ?>">Quaderno <?php echo($quadname); ?></a>
                                                    </p>
                                                    <?php

                                                    if($numpag = get_field("numero_di_pagina",$elem->ID)){
                                                        echo "<p class=\"comment\"> pag. ".$numpag;
                                                        if($numpagfine = get_field("numero_di_pagina_fine",$elem->ID))
                                                            echo " - ".$numpagfine;
                                                        echo "</p>";
                                                    }
                                                    if($data = get_field("data_pubblicazione", $quad[0]->taxonomy."_".$quad[0]->term_id)){
                                                        echo "<p class=\"comment\">Anno ".date("Y", strtotime($data))."</p>";
                                                    }

                                                    if($volume = get_field("volume", $quad[0]->taxonomy."_".$quad[0]->term_id)){
                                                        echo "<p class=\"comment\">Volume ".$volume."</p>";
                                                    }

                                                }
                                            }
                                        }
                                        ?>
                                        <div class="date_inside" style="padding: 6px 0px 2px 0px;"><?php 
                                        
                                            if($quad[0]->term_id)
                                                echo date_i18n("j F Y", strtotime(get_field("data_pubblicazione", "quaderno_" . $quad[0]->term_id)));
                                            else
	                                            echo date_i18n("j F Y", strtotime($post->post_date));
                                        
                                        ?></div>
                                    </div>
                                    <?php
                                    $by = get_userdata($post->post_author)->display_name;
                                    $altro_autore = get_field("altro_autore");
                                    if(!is_wp_error($altro_autore)){
                                        if($altro_autore != ""){
                                            $by .=  " - ".$altro_autore;
                                        }

                                    }

                                    cc_share_icons(get_permalink($post->ID), $post->post_title , false, $by) ?>

                                    <?php
				    //echo do_shortcode('[responsivevoice_button voice="Italian Female" buttontext="Ascolta l\'audio"]');

				    ?>

                                    <div class="article_content">
                                        <?php
                                        $image = get_field("immagine_libro", $post->ID);
                                        if($image){
                                            $image_url = $image["sizes"]["medium"];
                                            $image_tag = '<img src="'.$image_url.'" class="alignleft book_recensione" />';
                                        }

                                        $content = get_the_content();
                                        $content = $image_tag.strip_tags($content, "<b><i><strong><iframe><ul><ol><li><p><a><blockquote><del><img><em>");

//                                        the_content();
                                        echo apply_filters("the_content", $content);
                                        ?>
                                       <!-- <p style="font-size:14px; font-style: italic; text-align:right;">Ultima modifica:  <?php the_modified_date('j F Y'); ?> alle <?php the_modified_date('G:i'); ?></p> //-->
                                    </div>
                                    <?php // cc_share_icons(get_permalink($post->ID), $post->post_title , false); ?>

                                    <div id="anchor_comment"></div>
                                    <?php

                                    if($buylink = cc_link_buy_quaderno($quad[0]))
                                    echo '<a class="btn btn-primary btn-lg btn-anchor btn-block" href="'.$buylink.'">Acquista il Quaderno</a><br>'; ?>


                                    <?php
                                    $argomenti = wp_get_post_terms($post->ID, "argomento");
                                    $str_argomenti = "";
                                    //                                    dd($argomenti);
                                    if ($argomenti) {
                                        ?>
                                        <div class="article_tags">
                                            <i class="icon icon-icon-tag"></i>

                                            <div class="tags_list" id="tags_list">
                                                <?php
                                                foreach ($argomenti as $argomento) {
                                                    $str_argomenti.=" ".$argomento->name;
                                                    echo '<a href="' . get_term_link($argomento) . '">' . $argomento->name . '</a>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>

                                    <?php
                                    /*
                                    // Find connected posts
                                    $connected = get_posts(array(
                                        'connected_type' => 'articoli_to_articoli',
                                        'connected_items' => get_queried_object(),
                                        'nopaging' => true,
                                        'suppress_filters' => false
                                    ));
                                    //dd($connected);
                                    if ($connected) {
                                        ?>
                                        <div class="articles_similar">
                                            <?php
                                            foreach ($connected as $connection) {
                                                ?>
                                                <div class="article_similar">
                                                    <div class="article_title">
                                                        <h3>
                                                            <a href="<?php echo get_permalink($connection->ID); ?>"><?php echo $connection->post_title; ?></a>
                                                        </h3>
                                                    </div>
                                                    <!-- /article_title -->
                                                    <div class="article_content">
                                                        <p><?php echo wp_trim_words($connection->post_content, 40, "..."); ?></p>
                                                    </div>
                                                    <!-- /article_content -->
                                                </div>
                                                <!-- /article_similar -->
                                            <?php
                                            }
                                            ?>
                                        </div>
                                        <!-- /articles_similar -->
                                    <?php
                                    }*/
                                    ?>
                                </div>
                                <!-- /article_wrapper -->
                            </article>
                        </div>
                        <!-- /col-md-12 -->

                    </div>
                    <!-- /row -->

                </div><!-- /container -->
            <?php
            }
        }

        if(comments_open()) {
            ?>
            <div class="container-small">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <aside>
                            <div class="comments" id="comments">

                                <div id="fb-root"></div>
                                <script>(function (d, s, id) {
                                        var js, fjs = d.getElementsByTagName(s)[0];
                                        if (d.getElementById(id)) return;
                                        js = d.createElement(s);
                                        js.id = id;
                                        js.src = "//connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v2.7&appId=<?php echo FB_APP_ID; ?>";
                                        fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));</script>
                                <div class="fb-comments" data-href="<?php echo str_replace("https://", "http://", get_permalink($post)); ?>"
                                     data-width="100%" data-numposts="5"></div>
                            </div>
                        </aside>
                    </div>
                    <!-- /col-md-10 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container-small -->
        <?php
        }
        ?>
    </div>
</main>

<?php


// cerco i correlati in base alle geoaree
$geo = wp_get_post_terms($post->ID,"geoarea");
foreach ($geo as $geoarea) {
    $arraygeo[]=$geoarea->term_id;
}

$argsc = array(
    'post_type' => 'articolo',
    'posts_per_page'   => 10,
    'tax_query' => array(
        array(
            'taxonomy' => 'quaderno',
            'field' => 'term_id',
            'terms' => $quad[0],
            'include_children' => 0,
        )
    )
);


$correlati = get_posts($argsc);

if($correlati) {
    ?>
    <div class="section section_grey_light">
        <div class="section_title padding-bottom-20">
            <h2>Quaderno <?php echo $quadname; ?></h2>
        </div>

        <div class="owl-carousel carousel-theme carousel_related">
            <?php
            //dd($arraygeo);
            //dd($correlati);
            foreach ($correlati as $correlato) {
                print_articolo_correlati($correlato);
            }

            ?>
        </div>
        <!-- /carousel_related -->

    </div><!-- /section -->

<?php
}
// cerco i correlati in base al quaderno
$quad = wp_get_post_terms($post->ID, "quaderno");
foreach ($quad as $quader) {
    $arrayquader[] = $quader->slug;
}
?>
<div class="section section_white section_same_copybook">
    <div class="section_title">
        <h2>Altre Recensioni</h2>
    </div>


    <div class="container">
        <div class="row margin-bottom-40">
            <?php
            //dd($arraygeo);
            $args = array(
                'post_type' => 'recensione',
                'post__not_in' => array($post->ID),
                'posts_per_page' => 9,
                'tax_query' => array(
                    array(
                        'taxonomy' => 'quaderno',
                        'field' => 'slug',
                        'terms' => $arrayquader
                    )
                )
            );

            $dalquaderno = get_posts($args);
            foreach ($dalquaderno as $dalquad) {
                print_articolo_stessoquaderno($dalquad);
            }
            ?>
        </div>
        <!-- /container -->

    </div>
    <!-- /section -->

    <?php
    // cerco i prodotti correlati per autore
/*

    $args = array(
        'post_type' => 'product',
        'stock' => 1,
        'posts_per_page' => 6,
        'meta_key' => "autore_per_correlazione",
        'meta_value' => $post->post_author,
        'product_cat' => 'pubblicazioni',
        'orderby' => 'date',
        'order' => 'DESC');

    $loop = new WP_Query($args);
    if($loop->have_posts()){
        ?>
        <div class="section section_grey_light">
            <div class="section_title padding-bottom-20">
                <h2>Libri dello stesso autore</h2>
            </div>

            <div class="owl-carousel carousel-theme carousel_books">
                <?php
                while ($loop->have_posts()) :
                    $loop->the_post();
                    global $product;
                    echo '<div class="item">';
                    print_book_lista($product);
                    echo '</div>';
                endwhile;
                wp_reset_query();
                ?>
            </div>
            <!-- /carousel_related -->

        </div><!-- /section -->
        <div class="all_large" style="padding-bottom: 10px; border-bottom: dotted 1px #ccc; background-color: #f2f2f2;">

        </div>

        <?php
    }

*/


    /*
        require_once('functions/gbooksearch.php');

        $search = new Gbooksearch();
        $searchresults = $search->searchBooks("la civilt� cattolica ".$str_argomenti, array("maxResults" => 3));

        if($searchresults) {


            ?>
            <div class="section section_grey_light">

                <div class="section_title">
                    <h2>Google Books</h2>
                </div>

                <div class="container">

                    <div class="row margin-bottom-30">

                        <?php
                        foreach($searchresults["items"] as $gbook){
    //                        dd($gbook);

                            ?>
                        <div class="col-md-4">

                            <div class="google_article">

                                <p class="date"><?php echo $gbook["volumeInfo"]["publishedDate"] ?></p>

                                <p class="author"><?php
                                    $a=0;
                                    if(is_array($gbook["volumeInfo"]["authors"])){
                                    foreach ($gbook["volumeInfo"]["authors"] as $author) {
                                        if($a) echo ", ";
                                        echo $author;
                                        $a++;

                                    }
                                    }
                                    ?></p>

                                <h3><a href="<?php echo $gbook["volumeInfo"]["canonicalVolumeLink"]; ?>"><?php echo $gbook["volumeInfo"]["title"]; ?></a></h3>

                            </div>

                        </div>
                        <!-- /col-md-4 -->
                        <?php
                        }
                        ?>
                    </div>
                    <!-- /row -->

                </div>
                <!-- /container -->

            </div><!-- /section -->


        <?php
        }
    */
?>


</div><!-- /main_container -->


<?php get_template_part('includes/footer'); ?>
