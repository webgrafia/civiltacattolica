<div class="section section_white section_books">

    <div class="section_title">
        <h2>Pubblicazioni</h2>
    </div>

    <div class="owl-carousel carousel-theme carousel_books">
        <?php
        $args = array(
            'post_type' => 'product',
            'stock' => 1,
            'posts_per_page' => 6,
            'product_cat' => 'pubblicazioni',
            'orderby' => 'date',
            'order' => 'DESC');

        $loop = new WP_Query($args);
        while ($loop->have_posts()) :
            $loop->the_post();
            global $product;
            echo '<div class="item">';
            print_book_lista($product);
            echo '</div>';
        endwhile;
        wp_reset_query();
        ?>
    </div>
    <!-- /carousel_books -->

    <div class="all_large">

        <a href="<?php echo get_term_link(get_term_by("slug","pubblicazioni", "product_cat")); ?>">Vedi tutti i Libri</a>

    </div>

</div><!-- /section -->