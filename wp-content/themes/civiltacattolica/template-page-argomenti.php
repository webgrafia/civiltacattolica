<?php /* Template Name: Argomenti */ ?>
<?php get_template_part('includes/header'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article>
                    <div class="article_wrapper">

                        <div class="article_title">
                            <h1><?php the_title(); ?></h1>
                        </div>
                        <div class="article_details">
                            <div class="list_az clearfix">
                                <?php
                                global $post;
                                $lettera = $_GET["lettera"];
                                if ($lettera == "")
                                    $lettera = "a";
                                $page = $_GET["pagina"];
                                if ($page == "")
                                    $page = 1;

                                $letters = range('A', 'Z');
                                foreach ($letters as $letter) {

                                    $linkl = get_bloginfo("url") . "/argomenti/?lettera=" . $letter . "";
                                    $classe = "";
                                    if ($letter == $lettera)
                                        $classe = "selected";

                                    if ($letter == "A")
                                        echo '<a class="first ' . $classe . '" href="' . $linkl . '">' . $letter . '</a>';
                                    else if ($letter == "Z")
                                        echo '<a class="last ' . $classe . '" href="' . $linkl . '">' . $letter . '</a>';
                                    else
                                        echo '<a class="' . $classe . '" href="' . $linkl . '   ">' . $letter . '</a>';
                                }
                                ?>
                            </div><!-- /list_az -->
                            <?php


                            $taxonomies = array(
                                'geoarea',
                                'argomento'
                            );

                            $orderby = "name";
                            $order = "ASC";

                            $args = array(
                                'orderby'           => $orderby,
                                'order'             => $order,
                                'hide_empty'        => false,
                                'exclude'           => array(),
                                'exclude_tree'      => array(),
                                'include'           => array(),
                                'number'            => '',
                                'fields'            => 'all',
                                'slug'              => '',
                                'parent'            => '',
                                'hierarchical'      => true,
                                'child_of'          => 0,
                                'get'               => '',
                                'description__like' => '',
                                'pad_counts'        => false,
                                'offset'            => '',
                                'search'            => '',
                                'cache_domain'      => 'core'
                            );

                            $terms = get_terms($taxonomies, $args);
                            echo "<ul class='term-list'>";
                            $counter=0;
                            foreach($terms as $term){

                                if(strtolower(substr($term->name, 0, 1)) != strtolower($lettera) )
                                    continue;

                                echo "<li><a href='".  get_term_link($term)."'>".ucfirst($term->name)."</a></li>";
                                $counter++;
                            }

                            if($counter == 0){
                                echo "<li class='no-results'>Nessun risultato</li>";
                            }
                            echo "</ul>";

                            ?>

                        </div>
                </article>
            </div>
        </div>
    </div>

<?php get_template_part('includes/footer'); ?>