<?php
function cc_share_tool($url, $title = false)
{
    ?>

    <div class="share_tool_container">
        <a class="share_tool_icon" href="#"><i class="icon icon-icon-share"></i> <span>Condividi</span></a>

        <div class="share_tool_icons_wrapper">
        <?php cc_share_icons($url, $title, true); ?>
        </div>
        <!-- /share_tool_icons_wrapper -->
    </div>
    <!-- /share_tool_container -->

<?php

}


function cc_share_icons($url, $title = false, $simple_icons = true, $author=false){
    $url = str_replace("https://", "http://", $url);

//    echo do_shortcode("[chat-it messengers=telegram,whatsapp show_on=ios,android,other_mobile]");
?>

    <div class="social-sharing <?php if($simple_icons) echo "simple-icons"; ?>" data-permalink="<?php echo $url; ?>">

        <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $url; ?>"
           class="share-facebook">
            <span class="socialicon socialicon-facebook" aria-hidden="true"></span>
            <span class="share-title">Share</span>
        </a>

        <a target="_blank" href="http://twitter.com/share?url=<?php echo $url."&text=".urlencode(str_replace("|","-",$title))."%20by%20".urlencode($author); ?>" class="share-twitter">
            <span class="socialicon socialicon-twitter" aria-hidden="true"></span>
            <span class="share-title">Tweet</span>
        </a>
        <!--
                <a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php echo $url; ?>"
                   class="share-pinterest">
                    <span class="socialicon socialicon-pinterest" aria-hidden="true"></span>
                    <span class="share-title">Pin it</span>
                </a>


        <a target="_blank" href="http://plus.google.com/share?url=<?php echo $url; ?>" class="share-google">
            <span class="socialicon socialicon-google" aria-hidden="true"></span>
            <span class="share-title">+1</span>
        </a>
	//-->
        <!--
                <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>"
                   class="share-linkedin">
                    <span class="socialicon socialicon-linkedin" aria-hidden="true"></span>
                    <span class="share-title">Share</span>
                </a>
//-->
    </div>

    <?php

}

function copybook_number($quaderno, $position = "left", $link=false){
    if(!$quaderno) return;
    ?>
    <div class="copybook_number copybook_number_<?php echo $position; ?>">
        <?php if($link){   echo '<a href="'.get_term_link($quaderno).'">'; } ?>
        <span>Quaderno</span>
        <p class="number"><?php echo str_replace("-", " ",substr($quaderno->name, 0, -2)); ?><em><?php echo substr($quaderno->name, -2); ?></em></p>
        <p class="date"><?php echo get_field("data_pubblicazione", "quaderno_" . $quaderno->term_id); ?></p>
        <?php if($link){   echo '</a>'; } ?>
    </div>
<?php

}

function copybook_box($quaderno){
    if(!$quaderno) return;
    ?>

    <div class="copybooks_container copybook_simple">

        <div class="copybook_simple_text">
            <p class="number"><?php echo substr($quaderno->name, 0, -2); ?><em><?php echo substr($quaderno->name, -2); ?></em></p>
            <p class="date"><?php echo date_i18n("j F Y", strtotime(get_field("data_pubblicazione", "quaderno_" . $quaderno->term_id))); ?></p>
        </div>

        <div class="copybook_controls">
            <ul>
                <li><a href="<?php echo get_term_link($quaderno) ?>">Leggi</a></li>
                <?php if($buylink = cc_link_buy_quaderno($quaderno)) echo '<li><a href="'.$buylink.'">Acquista</a></li>'; ?>
            </ul>
        </div>

    </div><!-- /copybooks_container -->

<?php
}

function print_box_lista($elem, $large = false, $authorlist = false, $search = false){
    if(!$elem) return;

    if($large){
        $imgsize = 'apertura';
        $class="copybook_large";
        $altimag = get_bloginfo("template_url")."/placeholders/placeholder_1200x440.jpg";

    }else{
        $imgsize = 'lista_small';
        $class="copybook_small";
        $altimag = get_bloginfo("template_url")."/placeholders/placeholder_800x395.jpg";

    }

    if($search && $_GET["ptype"] == "libri")
	    $imgsize = 'book';
    $sottotitolo = get_field("sottotitolo", $elem->ID);
    if($sottotitolo) $sottotitolo = "<br>".$sottotitolo;

    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($elem->ID), $imgsize);
    $immagine = $thumb[0];
//    if($immagine == "")
  //      $immagine = $altimag;

    $link = get_permalink($elem->ID);
    ?>
    <div class="copybook <?php echo $class; ?>">
        <?php
        if($immagine) {
         ?>
            <div class="copybook_thumb">
                <?php cc_share_tool($link, $elem->post_title); ?>
                <?php if ($immagine) echo '<a href="' . $link . '"><img src="' . $immagine . '" alt="'.esc_attr($elem->post_title).'"></a>'; ?>
            </div>
        <?php
        }
        ?>
        <!-- /copybook_thumb -->
        <?php
        if($authorlist){
            $quad = wp_get_post_terms($elem->ID,"quaderno");
//            dd($quad);
            $quaderno = $quad[0];
            $quadlink = get_term_link($quaderno);

                ?>
                <p class="date"><?php
                    if($quaderno)
                        echo date_i18n("j.m.Y", strtotime(get_field("data_pubblicazione", "quaderno_" . $quaderno->term_id)));
                    else  if($search && $_GET["ptype"] == "libri")
                        echo "";
                    else
                        echo date_i18n("j.m.Y", strtotime($elem->post_date));
                    ?></p>
                <h2><a href="<?php echo $link; ?>"><?php echo $elem->post_title; ?></a></h2>
            <?php
            if(!is_wp_error($quadlink)) {
            ?>
                <div class="all">
                    <p>Dal quaderno <a
                            href="<?php echo get_term_link($quaderno); ?>"><?php echo substr($quaderno->name, 0, -2); ?>
                            <em><?php echo substr($quaderno->name, -2); ?></em></a>
                        <?php
                        if ($numpag = get_field("numero_di_pagina", $elem->ID)) {
                            ?> - pag. <?php echo $numpag; ?><?php
                        }
                        if($search){
                            $volume = get_field("volume", $quaderno->taxonomy."_".$quaderno->term_id);
                            if($volume){
                                echo " - Volume ".$volume;
                            }

                        }
                        ?>



                    </p>
                </div>
                <?php
            }
        }else{
            ?>
            <h2><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($elem->post_title).$sottotitolo; ?></a></h2>
            <p class="author">di <a href="<?php echo get_author_posts_url( $elem->post_author); ?>"><?php echo get_userdata($elem->post_author)->display_name; ?></a></p>
        <?php

        }
        ?>
    </div>
    <!-- /copybook_large -->
<?php
}


function print_minibox_lista($elem, $mini = true, $display_author = true){
    if(!$elem) return;

    $sottotitolo = get_field("sottotitolo", $elem->ID);
    if($sottotitolo) $sottotitolo = "<br>".$sottotitolo;


    if($mini){
        $class="copybook_mini";
        $imgsize = "thumbnail";
        $altimag = get_bloginfo("template_url")."/placeholders/placeholder_500x500.jpg";
    }else{
        $class="copybook_small";
        $imgsize = "lista";
        $altimag = get_bloginfo("template_url")."/placeholders/placeholder_800x395.jpg";
    }

    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($elem->ID), $imgsize);
    $immagine = $thumb[0];
//    if($immagine == "")
  //      $immagine = $altimag;
    $link = get_permalink($elem->ID);
    ?>

    <div class="copybook <?php echo $class; ?>">
        <div class="copybook_thumb">
            <?php if(!$mini){ cc_share_tool($link, $elem->post_title); }  ?>
            <?php if($immagine) echo '<a href="' . $link . '"><img src="' . $immagine . '" alt="'.esc_attr($elem->post_title).'"></a>'; ?>
        </div>
        <!-- /copybook_thumb -->
        <h2><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($elem->post_title).$sottotitolo; ?></a></h2>

        <?php
        if($display_author) {
            ?>
            <p class="author">di <a
                        href="<?php echo get_author_posts_url($elem->post_author); ?>"><?php echo get_userdata($elem->post_author)->display_name; ?></a>
            </p>
            <?php
        }
            ?>

    </div>
    <!-- /<?php echo $class; ?>-->

<?php
}




function print_conferenza_lista($elem, $buy = true){
    if(!$elem) return;


    $imgsize = "conferenza";
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($elem->ID), $imgsize);
    $immagine = $thumb[0];
   // if(!$immagine)
     //   $immagine = get_bloginfo("template_url")."/placeholders/placeholder_500x500.jpg";
    $link = get_permalink($elem->ID);
    ?>


    <div class="event_wrapper">
        <?php
        if($immagine) {
            ?>
            <div class="event_thumb">
                <div class="event_thumb_bg"></div>
                <img src="<?php echo $immagine; ?>" alt="<?php echo esc_attr($elem->post_title); ?>">
            </div>
        <?php
        }
        ?>
        <div class="event_container">
            <?php cc_share_tool($link,$elem->post_title); ?>
            <h3><a href="<?php echo $link; ?>"><?php echo $elem->post_title; ?></a></h3>

            <p class="date"><?php echo ucfirst(get_field("data", $elem->ID)); ?></p>

        </div><!-- /event_container -->

    </div><!-- /event_wrapper -->
<?php
}


function cc_link_buy_quaderno($quaderno){

    if(!$quaderno) return;


    // dal quaderno recupero il prodotto


// fix dato quaderno
//if($quaderno->term_id == 470){
    /*
  $tmpc = get_term_meta($quaderno->term_id, "prodotto_correlato", true);
  if(!is_array($tmpc) && ($tmpc != "")){
    $update = unserialize($tmpc);
    update_term_meta($quaderno->term_id, "prodotto_correlato",$update);
  }
    */
//}
    


$correlato = get_field("prodotto_correlato", $quaderno->taxonomy."_" . $quaderno->term_id);

    if($correlato[0]->ID)
        return get_bloginfo("url")."/".URL_ACQUISTA."/?add-to-cart=".$correlato[0]->ID;
    else
        return false;

}



function print_recensione_lista($post, $class="",  $size = "large"){
	if(!$post) return;

	$postimg = get_field("immagine_libro", $post->ID);
	if($postimg) {
		$img = $postimg["sizes"][$size];
	}else {
		$img = get_bloginfo( "template_url" ) . "/img/logo.svg";
	}

	$autore_recensione = get_field("autore_recensione", $post);
	if ( $autore_recensione == "" ) {
		$autore_recensione = get_userdata($post->post_author)->display_name;
	}
    $autore_recensito = get_field("autore_recensito", $post);
	$sottotitolo = get_field("sottotitolo", $post->ID);
	$link = get_permalink($post->ID);
	?>
        <div class="book_container <?php echo $class; ?>">

            <div class="book_thumb">
                <a href="<?php echo $link; ?>"><img src="<?php echo $img; ?>" alt="<?php echo esc_attr($post->post_title); ?>"></a>
            </div>
            <div class="book_text">
                <?php
                if($autore_recensito){
                    ?>
                    <p class="author" style="margin-bottom: 4px;"><b><?php echo $autore_recensito; ?></b></p>
                        <?php
                }
                ?>
                <!-- <p class="author"><?php echo $autore_recensione; ?></p> //-->
                <h3><a href="<?php echo $link; ?>"><?php echo $post->post_title; ?></a></h3>
                <p>Recensione di <span class="author author-recensione"><b><?php echo $autore_recensione; ?></b></span> </p>
                <!-- <p class="abstract" style="font-weight:bold;"><?php echo $sottotitolo; ?></p> //-->
                <p class="abstract"><?php echo wp_trim_words($post->post_content, 18, "..."); ?></p>

                <a  href="<?php echo $link; ?>" class="btn btn-primary btn-sm btn-anchor btn-block">Leggi l'articolo</a>
            </div>
        </div>
	<?php
}

function print_book_lista($elem, $buy = true){
    if(!$elem) return;

    $imgsize = "full";
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), $imgsize);
    $immagine = $thumb[0];
   // if(!$immagine)
     //   $immagine = get_bloginfo("template_url")."/placeholders/placeholder_350x550.jpg";
    $link = get_permalink();

    $price =   strip_tags($elem->get_price_html(), "<del><span><ins>");
    ?>

    <div class="book_container">
        <?php  if($immagine){
            ?>
            <div class="book_thumb">
                <a href="<?php echo $link; ?>"><img src="<?php echo $immagine; ?>" alt="<?php echo esc_attr(get_the_title()); ?>"></a>
            </div>
            <!-- /book_thumb -->
        <?php
        } ?>
        <div class="book_text">
            <p class="author"><?php echo get_field("autore"); ?></p>
            <h3><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h3>
            <p class="abstract"><?php
	            $terms = get_the_terms( $elem->ID, 'product_cat' );
	            foreach ($terms as $term) {
		            if(($term->slug == "accenti") || ($term->slug == "escritos-fiorito")){
			            $product_details = $elem->get_data();
			            $product_full_description = $product_details['description'];
			            $product_short_description = $product_details['short_description'];
//			            echo $product_short_description;
			            echo substr(strip_tags($product_short_description), 0, 300)."... <a href='".$link."' style='color:#920611;'>leggi tutto</a>";
		            }
	            }

	            the_content(); ?></p>
            <div class="book_details">
                <ul class="margin-bottom-30">
                    <?php
                    $npag = get_field("numero_di_pagine");
                    if($npag){
                        ?>
                        <li><span>Pagine:</span>
                            <p><?php echo $npag; ?></p></li>
                        <?php
                    }
                    ?>

                    <?php
                    if($price){
                        ?>
                        <li><span>Prezzo:</span>
                            <p><?php echo $price; ?></p></li>
                        <?php
                    }
                    ?>
                    <?php if($elem->sku){ ?>
                        <li><span>Codice:</span>
                            <p><?php echo $elem->sku; ?></p></li>
                    <?php } ?>
                </ul>
                <?php
                if($buy){
                  ?>
                    <a class="btn btn-primary btn-sm btn-anchor btn-block" href="<?php bloginfo("url"); ?>/<?php echo URL_ACQUISTA; ?>/?add-to-cart=<?php the_ID(); ?>">Acquista</a>
                    <?php
                }
                ?>

            </div>
            <!-- /book_details -->
        </div>
        <!-- /book_text -->
    </div>
    <!-- /book_container -->
<?php
}


function print_articolo_lista($elem){
    if(!$elem) return;
    $quaderno = wp_get_post_terms($elem->ID, "quaderno");
    $imgsize = "apertura";
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($elem->ID), $imgsize);
    $immagine = $thumb[0];
   // if($immagine == "")
     //   $immagine = get_bloginfo("template_url")."/placeholders/placeholder_1200x440.jpg";
    $link = get_permalink($elem->ID);
    ?>
    <div class="copybook copybook_large">
        <div class="copybook_thumb">
            <?php cc_share_tool($link, $elem->post_title); ?>
            <?php if($immagine) echo '<a href="' . $link . '"><img src="' . $immagine . '" alt="'.esc_attr($elem->post_title).'"></a>'; ?>
        </div><!-- /copybook_thumb -->
        <div class="copybook_details">
            <p class="author">di <a href="<?php echo get_author_posts_url( $elem->post_author); ?>"><?php echo get_userdata($elem->post_author)->display_name; ?></a>
            </p>
<!--            <p><a href="<?php echo $link; ?>#anchor_comment">Commenta</a></p> //-->
            <?php

            if($numpag = get_field("numero_di_pagina",$elem->ID)){?><p class="page_number">pag. <?php echo $numpag;
                if($quaderno){
//                    $volume = get_field("volume", $quaderno[0]->taxonomy."_".$quaderno[0]->term_id);
  //                  if($volume){
    //                echo " - Volume ".$volume;
      //              }

                }

                ?></p><?php }

                ?>
        </div>
        <?php
        $sottotitolo = get_field("sottotitolo", $elem->ID);
        if($sottotitolo) $sottotitolo = "<br>".$sottotitolo;
        ?>
        <h2><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($elem->post_title).$sottotitolo; ?></a></h2>
        <p class="abstract"><?php echo wp_trim_words($elem->post_content, 40, "..."); ?></p>
    </div><!-- /copybook_large -->
<?php
}


function print_articolo_correlati($correlato){
    if(!$correlato) return;

    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($correlato->ID), "lista");
    $immagine = $thumb[0];
    if(!$immagine)
        $immagine = get_bloginfo("template_url")."/placeholders/placeholder_800x395.jpg";
    $link = get_permalink($correlato->ID);
    ?>
    <div class="item">
        <div class="copybooks_container copybooks_container_small">
            <div class="copybook copybook_small">
                <?php
                if($immagine) {
                ?>
                    <div class="copybook_thumb">
                        <?php
                        cc_share_tool($link, $correlato->post_title);
                        ?>
                        <a href="<?php echo $link; ?>"><img src="<?php echo $immagine; ?>" alt="<?php echo esc_attr($correlato->post_title); ?>"></a>
                    </div>
                <?php
                }
                ?>
                <!-- /copybook_thumb -->
                <?php
                $sottotitolo = get_field("sottotitolo", $correlato->ID);
                if($sottotitolo) $sottotitolo = "<br>".$sottotitolo;
                ?>
                <h2><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($correlato->post_title).$sottotitolo; ?></a></h2>
                <?php
                if($correlato->post_type == "recensione"){
	                $autore_recensione = get_field("autore_recensione", $correlato->ID);
	                if(!is_wp_error($autore_recensione)){
		                if($autore_recensione != "") {
		                    ?>
                            <p class="author">di <?php echo $autore_recensione; ?></p>
			                <?php
		                }
	                }

                }else{
                    ?>
                    <p class="author">di <a href="<?php echo get_author_posts_url( $correlato->post_author); ?>"><?php echo get_userdata($correlato->post_author)->display_name; ?></a></p>
	                <?php
                }
                ?>
                <div class="all">
                    <?php
                    $geo = wp_get_post_terms($correlato->ID, "quaderno");
                    if($geo){
                        ?>
                        <p>Dal quaderno <a href="<?php echo get_term_link($geo[0]); ?>"><?php echo substr($geo[0]->name, 0, -2); ?><em><?php echo substr($geo[0]->name, -2); ?></em></a></p>
                    <?php
                    }
                    ?>
                </div>
            </div>
            <!-- /copybook_small -->
        </div>
        <!-- /copybooks_container -->
    </div>
    <!-- /item -->
<?php
}

function print_articolo_stessoquaderno($dalquad){
    $sottotitolo = get_field("sottotitolo", $dalquad->ID);
    if($sottotitolo) $sottotitolo = "<br>".$sottotitolo;

    $link = get_permalink($dalquad->ID); ?>
    <div class="col-md-4 margin-bottom-40">
        <div class="simple_article">
            <h3><a href="<?php echo $link; ?>"><?php echo mb_strtoupper($dalquad->post_title).$sottotitolo; ?></a></h3>
            <p class="author">di <a href="<?php echo get_author_posts_url( $dalquad->post_author); ?>"><?php echo get_userdata($dalquad->post_author)->display_name; ?></a></p>
        </div>
    </div>
    <!-- /col-md-4 -->
<?php
}

function cc_filter_wp_dropdown_users_args($query_args) {
    $query_args['role'] = 'author';
    $query_args['meta_key'] = 'last_name';
    $query_args['orderby'] = 'meta_value';

    return $query_args;
}

add_action('pre_get_posts','search_alter_query', 1000);

function search_alter_query($query) {
    //gets the global query var object
    global $wp_query;

    if ( !$query->is_main_query() )
        return;

    if ( $query->is_admin )
        return $query;

    if(!is_search())
        return;

    $query->set( 'ep_integrate', true );

    if(isset($_REQUEST["onlytitle"]) && $_REQUEST["onlytitle"] == "1"){
//        $query->set('search_fields',  array("post_title"));
        // cerco senza elasticpress
        $query->set( 'ep_integrate', false );

/*
        add_filter( 'ep_search_fields', function($search_fields, $args ) {
            $fields = array('post_title');
            return $fields;
        }, 10,2 );
*/
    }

    if($_GET["ptype"] != ""){

        if($_GET["ptype"] == "libri"){

	        $query->set('post_type' ,"product");
	        $query->set( 'tax_query',
		        array(
			        array(
				        'taxonomy' => 'product_cat',
				        'field' => 'slug',
				        'terms' => "pubblicazioni",
			        )
		        )
	        );

        }else{
	        $query->set('post_type' ,$_GET["ptype"]);
        }

    }else{
	    $query->set('post_type' ,'articolo');
    }

	if($_GET["autore"] > 0)
        $query->set('author' ,$_GET["autore"]);


    if($_GET["search_from"] != "" || $_GET["search_to"] != ""){

        if($_GET["search_from"] != "" && $_GET["search_to"] != ""){
            $from_array = explode("-", $_GET["search_from"]);
            $to_array = explode("-", $_GET["search_to"]);

            $date_query =array(
                array(
                    'after' => array(
                        'year' => $from_array[2], 'month' => $from_array[1], 'day' => $from_array[0]
                    ),
                    'before' => array(
                        'year' => $to_array[2], 'month' => $to_array[1], 'day' => $to_array[0]
                    ),
                    'inclusive' => true
                )
            );
        }else if($_GET["search_from"] != "" ){
            $from_array = explode("-", $_GET["search_from"]);
            $date_query =array(
                array(
                    'after' => array(
                        'year' => $from_array[2], 'month' => $from_array[1], 'day' => $from_array[0]
                    ),
                    'inclusive' => true
                )
            );

        }else if($_GET["search_to"] != "" ){
            $to_array = explode("-", $_GET["search_to"]);
            $date_query =array(
                array(
                    'before' => array(
                        'year' => $to_array[2], 'month' => $to_array[1], 'day' => $to_array[0]
                    ),
                    'inclusive' => true
                )
            );

        }

        if($date_query)
            $query->set('date_query', $date_query);
    }

	 $query->set('orderby', "post_date");

	if($_GET["ptype"] != "") {
		if ( $_GET["ptype"] == "libri" ) {
//			$query->set('orderby', "title");
//			$query->set('order', "ASC");
		}
	}
    //we remove the actions hooked on the '__after_loop' (post navigation)
    remove_all_actions ( '__after_loop');
}



function cc__search_by_title_only( $search, $wp_query )
{
    global $wpdb;

    if ( empty( $search ) )
        return $search; // skip processing - no search term in query

/*    if(isset($_REQUEST["fullsearch"]) && $_REQUEST["fullsearch"] == "1"){
        return $search;
    }
*/
    if(isset($_REQUEST["onlytitle"]) && $_REQUEST["onlytitle"] == "1"){

    }else{
        return $search; // skip processing - no search term in query

    }

    $q = $wp_query->query_vars;
    $n = ! empty( $q['exact'] ) ? '' : '%';

    $search =
    $searchand = '';

    foreach ( (array) $q['search_terms'] as $term ) {
        $term = esc_sql( like_escape( $term ) );
        $search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";
        $searchand = ' AND ';
    }

    if ( ! empty( $search ) ) {
        $search = " AND ({$search}) ";
        if ( ! is_user_logged_in() )
            $search .= " AND ($wpdb->posts.post_password = '') ";
    }

    return $search;
}
add_filter( 'posts_search', 'cc__search_by_title_only', 5000, 2 );


function cc_wp_dropdown_users( $args = '' ) {
	$defaults = array(
		'show_option_all' => '', 'show_option_none' => '', 'hide_if_only_one_author' => '',
		'orderby' => 'display_name', 'order' => 'ASC',
		'include' => '', 'exclude' => '', 'multi' => 0,
		'show' => 'display_name', 'echo' => 1,
		'selected' => 0, 'name' => 'user', 'class' => '', 'id' => '',
		'blog_id' => get_current_blog_id(), 'who' => '', 'include_selected' => false,
		'option_none_value' => -1,
		'role' => '',
		'role__in' => array(),
		'role__not_in' => array(),
	);

	$defaults['selected'] = is_author() ? get_query_var( 'author' ) : 0;

	$r = wp_parse_args( $args, $defaults );

	$query_args = wp_array_slice_assoc( $r, array( 'blog_id', 'include', 'exclude', 'orderby', 'order', 'who', 'role', 'role__in', 'role__not_in' ) );

	$fields = array( 'ID', 'user_login' );

	$show = ! empty( $r['show'] ) ? $r['show'] : 'display_name';
	if (( 'display_name_with_login' === $show ) || ( 'last_first' === $show )) {
		$fields[] = 'display_name';
	} else {
		$fields[] = $show;
	}

	$query_args['fields'] = $fields;
	//print_r($query_args);

	$show_option_all = $r['show_option_all'];
	$show_option_none = $r['show_option_none'];
	$option_none_value = $r['option_none_value'];

	/**
	 * Filters the query arguments for the list of users in the dropdown.
	 *
	 * @since 4.4.0
	 *
	 * @param array $query_args The query arguments for get_users().
	 * @param array $r          The arguments passed to wp_dropdown_users() combined with the defaults.
	 */
	$query_args = apply_filters( 'wp_dropdown_users_args', $query_args, $r );

	$users = get_users( $query_args );
//print_r($users);
	$output = '';
	if ( ! empty( $users ) && ( empty( $r['hide_if_only_one_author'] ) || count( $users ) > 1 ) ) {
		$name = esc_attr( $r['name'] );
		if ( $r['multi'] && ! $r['id'] ) {
			$id = '';
		} else {
			$id = $r['id'] ? " id='" . esc_attr( $r['id'] ) . "'" : " id='$name'";
		}
		$output = "<select name='{$name}'{$id} class='" . $r['class'] . "'>\n";

		if ( $show_option_all ) {
			$output .= "\t<option value='0'>$show_option_all</option>\n";
		}

		if ( $show_option_none ) {
			$_selected = selected( $option_none_value, $r['selected'], false );
			$output .= "\t<option value='" . esc_attr( $option_none_value ) . "'$_selected>$show_option_none</option>\n";
		}

		if ( $r['include_selected'] && ( $r['selected'] > 0 ) ) {
			$found_selected = false;
			$r['selected'] = (int) $r['selected'];
			foreach ( (array) $users as $user ) {
				$user->ID = (int) $user->ID;
				if ( $user->ID === $r['selected'] ) {
					$found_selected = true;
				}
			}

			if ( ! $found_selected ) {
				$users[] = get_userdata( $r['selected'] );
			}
		}

		foreach ( (array) $users as $user ) {
            if($user->display_name == "Recensioni")
                continue;
			if ( 'last_first' === $show ) {
				$last_name = get_user_meta( $user->ID, 'last_name', true );
				$first_name = get_user_meta( $user->ID, 'first_name', true );

				$display = $last_name." ".$first_name;
			} elseif ( 'display_name_with_login' === $show ) {
				/* translators: 1: display name, 2: user_login */
				$display = sprintf( _x( '%1$s (%2$s)', 'user dropdown' ), $user->display_name, $user->user_login );
			} elseif ( ! empty( $user->$show ) ) {
				$display = $user->$show;
			} else {
				$display = '(' . $user->user_login . ')';
			}

			$_selected = selected( $user->ID, $r['selected'], false );
			$output .= "\t<option value='$user->ID'$_selected>" . esc_html( $display ) . "</option>\n";
		}

		$output .= "</select>";
	}

	/**
	 * Filters the wp_dropdown_users() HTML output.
	 *
	 * @since 2.3.0
	 *
	 * @param string $output HTML output generated by wp_dropdown_users().
	 */
	$html = apply_filters( 'wp_dropdown_users', $output );

	if ( $r['echo'] ) {
		echo $html;
	}
	return $html;
}

function remove_snowball( $mapping ) {
    unset( $mapping['settings']['analysis']['analyzer']['default']['filter'][4] );
}
add_filter( 'ep_config_mapping', 'remove_snowball', 10, 2 );


function wc_deactivate_ep_fuzziness( $fuzz ) {
    return 0;
}
add_filter( 'ep_fuzziness_arg', 'wc_deactivate_ep_fuzziness' );