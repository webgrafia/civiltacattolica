<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

/**
 * Chat it Admin Settings Class
 */
class Phone_Share_Admin {

	const PAGE = 'phone-share';

	const PAGE_TITLE = 'Chat it!';

	const MENU_TITLE = 'Chat it!';

	const OPTION_GROUP = 'phone-share';

	protected $tabs;

	protected $fields_renderer;

	/**
	 * Hook into the appropriate actions when the class is constructed
	 */
	public function __construct() {

		add_action( 'admin_menu', array( $this, 'add_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'add_settings' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
		add_filter( 'plugin_action_links_' . PHONE_SHARE()->get_plugin_basename(), array( $this, 'add_action_links' ) );

		require_once PHONE_SHARE()->get_plugin_dir_path() . 'admin/class-phone-share-tabs.php';
		require_once PHONE_SHARE()->get_plugin_dir_path() . 'admin/class-phone-share-fields-renderer.php';

		$this->tabs = new Phone_Share_Tabs( self::PAGE, $this->get_tabs(), 'general' );

		$this->fields_renderer = new Phone_Share_Fields_Renderer( $this->tabs->get_current_tab() );

	}

	/**
	 * List of settings fields
	 */
	public function get_fields() {

		$messenger_tabs = array( 'telegram', 'viber', 'whatsapp', 'facebook', 'skype' );

		return array(
			'messengers'       => array(
				'title'          => __( 'Messengers', 'phone-share' ),
				'callback'       => array( $this->fields_renderer, 'select_multiple_messengers' ),
				'tabs'           => array( 'general' ),
				'select_options' => array(
					'telegram' => __( 'Telegram', 'phone-share' ),
					'viber'    => __( 'Viber', 'phone-share' ),
					'whatsapp' => __( 'WhatsApp', 'phone-share' ),
					'facebook' => __( 'FB Messenger', 'phone-share' ),
					'skype'    => __( 'Skype', 'phone-share' )
				),
			),
			'post_types'       => array(
				'title'          => __( 'Post Types', 'phone-share' ),
				'callback'       => array( $this->fields_renderer, 'select_multiple' ),
				'tabs'           => array( 'general' ),
				'select_options' => array(
					'post'    => __( 'Post', 'phone-share' ),
					'page'    => __( 'Page', 'phone-share' ),
					'product' => __( 'Product', 'phone-share' ),
					'articolo' => __( 'Articoli', 'phone-share' ),
				)
			),
			'position'         => array(
				'title'          => __( 'Position', 'phone-share' ),
				'callback'       => array( $this->fields_renderer, 'select_multiple' ),
				'tabs'           => array( 'general' ),
				'select_options' => array(
					'before_content' => __( 'Before Content' ),
					'after_content'  => __( 'After Content' )
				)
			),
			'title'            => array(
				'title'    => __( 'Title', 'phone-share' ),
				'callback' => array( $this->fields_renderer, 'text_input' ),
				'tabs'     => $messenger_tabs
			),
			'icon_style'       => array(
				'title'          => __( 'Icon Style', 'phone-share' ),
				'callback'       => array( $this->fields_renderer, 'select_icon' ),
				'tabs'           => $messenger_tabs,
				'select_options' => array(
					'round'  => __( 'Round Icon', 'phone-share' ),
					'square' => __( 'Square Icon', 'phone-share' ),
					'button' => __( 'Button', 'phone-share' ),
				)
			),
			'show_on'          => array(
				'title'          => __( 'Show On', 'phone-share' ),
				'callback'       => array( $this->fields_renderer, 'do_show_on' ),
				'tabs'           => $messenger_tabs,
				'select_options' => array(
					'telegram' => array(
						'ios'          => __( 'iOs', 'phone-share' ),
						'android'      => __( 'Android', 'phone-share' ),
						'other_mobile' => __( 'Other mobile platforms', 'phone-share' ),
						'desktop'      => __( 'Desktop', 'phone-share' )
					),
					'viber'    => array(
						'ios'          => __( 'iOs', 'phone-share' ),
						'android'      => __( 'Android', 'phone-share' ),
						'other_mobile' => __( 'Other mobile platforms', 'phone-share' ),
						'desktop'      => __( 'Desktop', 'phone-share' )
					),
					'whatsapp' => array(
						'ios'          => __( 'iOs', 'phone-share' ),
						'android'      => __( 'Android', 'phone-share' ),
						'other_mobile' => __( 'Other mobile platforms', 'phone-share' ),
					),
					'facebook' => array(
						'ios'          => __( 'iOs', 'phone-share' ),
						'android'      => __( 'Android', 'phone-share' ),
						'other_mobile' => __( 'Other mobile platforms', 'phone-share' ),
					),
					'skype'    => array(
						'ios'          => __( 'iOs', 'phone-share' ),
						'android'      => __( 'Android', 'phone-share' ),
						'other_mobile' => __( 'Other mobile platforms', 'phone-share' ),
						'desktop'      => __( 'Desktop', 'phone-share' )
					),
				)
			),
			'text_color'       => array(
				'title'    => __( 'Button Title Color', 'phone-share' ),
				'callback' => array( $this->fields_renderer, 'text_input' ),
				'tabs'     => $messenger_tabs,
				'required' => true,
				'class'    => 'hidden'
			),
			'background_color' => array(
				'title'       => __( 'Item Fill Color', 'phone-share' ),
				'callback'    => array( $this->fields_renderer, 'text_input' ),
				'tabs'        => $messenger_tabs,
				'description' => __( 'This option affects button and icon background colors, icon title color', 'phone-share' )
			),
		);
	}

	public function get_tabs() {
		return array(
			'general'  => array(
				'title'       => __( 'General', 'phone-share' ),
				'description' => __( '', 'phone-share' ),
			),
			'telegram' => array(
				'title'       => __( 'Telegram', 'phone-share' ),
				'description' => __( '', 'phone-share' )
			),
			'viber'    => array(
				'title'       => __( 'Viber', 'phone-share' ),
				'description' => __( '', 'phone-share' )
			),
			'whatsapp' => array(
				'title'       => __( 'WhatsApp', 'phone-share' ),
				'description' => __( '', 'phone-share' )
			),
			'facebook' => array(
				'title'       => __( 'FB Messenger', 'phone-share' ),
				'description' => __( '', 'phone-share' )
			),
			'skype'    => array(
				'title'       => __( 'Skype', 'phone-share' ),
				'description' => __( '', 'phone-share' )
			),
		);
	}


	public function register_scripts() {

		wp_enqueue_style( 'wp-color-picker' );

		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'jquery-ui-button' );
		wp_enqueue_script( 'jquery-ui-droppable' );

		wp_enqueue_script( 'pshare-multiselect', PHONE_SHARE()->get_plugin_url() . '/js/jquery.uix.multiselect.js', array(
			'jquery',
		), '', true );

		wp_enqueue_script( 'phone-share', PHONE_SHARE()->get_plugin_url() . '/js/phone-share.js', array(
			'jquery',
			'wp-color-picker',
			'pshare-multiselect',
		), '', true );

		wp_localize_script( 'phone-share', 'phone_share',
			array(
				'color_picker_fields' => array( 'background_color', 'text_color' ),
				'current_tab'         => $this->tabs->get_current_tab(),
				'default_options'     => PHONE_SHARE()->get_default_options(),
			)
		);

		wp_enqueue_style( 'phone-share-jquery-ui', PHONE_SHARE()->get_protocol() . "://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" );
		wp_enqueue_style( 'pshare-multiselect', PHONE_SHARE()->get_plugin_url() . '/css/jquery.uix.multiselect.css' );
		wp_enqueue_style( 'phone-share-style', PHONE_SHARE()->get_plugin_url() . '/css/style.css' );

	}

	/**
	 * Add settings link
	 */
	public function add_action_links( $links ) {

		$settings_link = array(
			'<a href="' . admin_url( 'options-general.php?page=' . self::PAGE ) . '">' . __( 'Settings', 'phone-share' ) . '</a>',
		);

		return array_merge( $links, $settings_link );

	}

	/**
	 * Add menu page for plugin settings
	 */
	public function add_admin_menu() {

		add_submenu_page(
			'options-general.php',
			self::PAGE_TITLE,
			self::MENU_TITLE,
			'manage_options',
			self::PAGE,
			array( $this, 'do_options_page' )
		);

	}

	/**
	 * Register plugin settings
	 */
	public function add_settings() {

		$current_tab = $this->tabs->get_current_tab();

		register_setting( self::OPTION_GROUP, PHONE_SHARE()->get_option_name( $current_tab ), array(
			$this,
			'sanitize_options'
		) );

		$section_id = 'phone_share_section';
		add_settings_section(
			$section_id,
			'',
			array( $this, 'settings_section_callback' ),
			self::PAGE
		);

		foreach ( $this->get_fields() as $field_id => $field_data ) {

			if ( ! in_array( $current_tab, $field_data['tabs'] ) ) {
				continue;
			}
			$field_data['current_value'] = PHONE_SHARE()->get_options( $current_tab, $field_id );
			$field_data['id']            = $field_id;
			add_settings_field(
				$field_id,
				__( $field_data['title'], self::PAGE ),
				$field_data['callback'],
				self::PAGE,
				$section_id,
				$field_data
			);
		}
	}

	public function sanitize_options( $options ) {
		return $options;
	}

	/**
	 * Callback for setting tab description
	 */
	public function settings_section_callback() {
		_e( '', 'phone-share' );
	}

	public function get_tab_url( $tab ) {
		return add_query_arg( array(
			'page' => self::PAGE,
			'tab'  => $tab
		), admin_url( 'options-general.php' )
		);
	}

	/**
	 * Settings page output
	 */
	public function do_options_page() {

		?>
		<form id="phone-share" action='options.php' method='post'>
			<div class="wrap">
				<h1><?php _e( 'Chat it!', 'phone-share' ); ?></h1>

				<?php
				$this->tabs->do_tab_navigation();
				settings_fields( self::OPTION_GROUP );
				echo '<input type="hidden" name="tab" value="' . $this->tabs->get_current_tab() . '" />';
				do_settings_sections( self::PAGE );
				submit_button();
				?>
			</div>
		</form>
		<?php
	}

}

new Phone_Share_Admin();