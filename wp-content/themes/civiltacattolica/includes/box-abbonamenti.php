
    <div class="box_abbonamenti_wrapper">

        <div class="box_abbonamenti">

            <div class="row margin-bottom-30">

                <div class="col-md-5 col-sm-5">

                    <div class="icons_abbonamenti icon_book_plus_smartphone">

                        <img src="<?php bloginfo("template_url"); ?>/img/book_plus_smartphone_white.svg" alt="smartphone">

                    </div>

                </div><!-- /col-md-5 -->

                <div class="col-md-7 col-sm-7">

                    <h3><a href="<?php bloginfo("url"); ?>/<?php echo URL_ABBONAMENTI_CARTA; ?>"><strong>Abbonamento</strong> Quaderno + Digitale</a></h3>

                </div><!-- /col-md-7 -->

            </div><!-- /row -->

            <div class="row margin-bottom-30">

                <div class="col-md-5 col-sm-5">

                    <div class="icons_abbonamenti icon_tablet">

                        <img src="<?php bloginfo("template_url"); ?>/img/tablet_white.svg" alt="Tablet">

                    </div>

                </div><!-- /col-md-5 -->

                <div class="col-md-7 col-sm-7">

                    <h3><a href="<?php bloginfo("url"); ?>/<?php echo URL_ABBONAMENTI_DIGITAL; ?>"><strong>Abbonamento</strong> Digitale</a></h3>

                </div><!-- /col-md-7 -->

            </div><!-- /row -->

            <a class="btn btn-default btn-sm btn-anchor btn-block" href="<?php bloginfo("url"); ?>/<?php echo URL_ABBONAMENTI; ?>">Scopri i nostri Abbonamenti</a>

        </div>

    </div>

