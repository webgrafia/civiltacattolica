<?php
// recuperola lista dei quaderni in ordine inverso
$quaderni = get_terms('quaderno', array('hide_empty' => false, 'orderby' => "name", "order" => "desc",  "meta_query" => array(
    'relation' => 'OR',
    array(
        'key'       => 'pubblicazione_posticipata',
        'compare'   => 'NOT EXISTS'
    ),
    array(
        'key'       => 'pubblicazione_posticipata',
        'value'       => '1',
        'compare'   => '!='
    )
),"number" => 10));

// recupero gli ultimi 5 articoli della tassonomia quaderno
$arg = array(
    'posts_per_page' => -1,
    'post_type' => 'articolo',
    'orderby' => "rand",
    'tax_query' => array(
        array(
            'taxonomy' => 'quaderno',
            'field' => 'term_id',
            'terms' => $quaderni[0]->term_id,
        )
    )
);
$articoli = get_posts($arg);
?>

<div class="section section_grey_light section_articles">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="copybooks_container copybooks_container_large copybooks_container_number_left">
                    <?php copybook_number($quaderni[0], "left"); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?php print_box_lista($articoli[0], true); ?>
                        </div>
                        <!-- /col-md-12 -->
                    </div>
                    <!-- /row -->
		    <?php
			$c=0;
			$x=0;
            echo '<div class="row">';
			foreach($articoli as $articolo){
			if($c == 0){
			$c++;
			continue;
			}
			$c++;

			if($x == 0){
                //  echo '<div class="row">';
            } else if($x%2 == 0) {
                echo '</div><div class="row">';

            }

            $x++;
		    ?>
                        <div class="col-md-6 col-sm-6">
                            <?php print_box_lista($articolo); ?>
                        </div>
                        <!-- /col-md-6 -->
		    <?php
		    }
            echo '</div>';

            /*
            ?>

            <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <?php print_box_lista($articoli[1]); ?>
                        </div>
                        <!-- /col-md-6 -->
                        <div class="col-md-6 col-sm-6">
                            <?php print_box_lista($articoli[2]); ?>
                        </div>
                        <!-- /col-md-6 -->
                    </div>

                    <!-- /row -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <?php print_box_lista($articoli[3]); ?>
                        </div>
                        <!-- /col-md-6 -->
                        <div class="col-md-6 col-sm-6">
                            <?php print_box_lista($articoli[4]); ?>
                        </div>
                        <!-- /col-md-6 -->
                    </div>
                    <!-- /row -->
            <div class="all">
                        <a href="<?php echo get_term_link($quaderni[0]) ?>">Vedi tutti gli articoli</a>
                    </div>
             <?php
            */
		    ?>

	    <div class="all col-md-12">
	         <a href="<?php echo get_term_link($quaderni[0]) ?>">Vai al quaderno</a>
                    </div>

                </div>
                <!-- /copybooks_container -->
            </div>
            <!-- /col-md-8 -->

            <div class="col-md-4">

                <div class="row margin-bottom-20 padding-left-15 padding-right-15" style="text-align:center;">
                    <a href="https://laciviltacattolica.us14.list-manage.com/subscribe?u=441b2200731c72bb723ede881&id=9d2f468610" target="_BLANK"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/banner-newsletter.png" style="max-width: 100%;"></a>
                </div>

                <?php
                $home_enable_news = get_field("home_enable_news", "option");
                if($home_enable_news) {
                    $numero_news = get_field("numero_news", "option");
                    if(!$numero_news)
                        $numero_news = 2;
                    $arg = array(
                        'posts_per_page' => $numero_news,
                        'post_type' => 'news',
                    );
                    $news = get_posts($arg);
                    ?>
                    <div class="copybooks_container copybooks_container_small copybooks_container_number_right margin-bottom-30 block-news">

                        <div class="copybook_number copybook_number_right copybook_number_news">
                            <a href="<?php echo get_post_type_archive_link("news"); ?>">
                                <p class="number"><em>News</em></p>
                            </a>
                        </div>

                        <?php
                        $v = 0;
                        foreach ($news as $n) {
                            print_minibox_lista($n, $v, false);
                            $v++;
                        }
                        ?>
                        <div class="all">
                            <a href="<?php echo get_post_type_archive_link("news"); ?>">Vedi tutte le news</a>
                        </div>
                    </div>
                    <!-- /copybooks_container -->

                    <?php
                }
                $arg = array(
                    'posts_per_page' => 5,
                    'post_type' => 'articolo',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'quaderno',
                            'field' => 'term_id',
                            'terms' => $quaderni[1]->term_id,
                        )
                    )
                );
                $articoli = get_posts($arg);
                ?>
                <div class="copybooks_container copybooks_container_small copybooks_container_number_right margin-bottom-30">
                    <?php copybook_number($quaderni[1], "right"); ?>
                    <?php
                    for($i=0;$i<7;$i++){
		      if(isset($articoli[$i]))
                         print_minibox_lista($articoli[$i], $i);
                    }
                    ?>
                    <div class="all">
                        <a href="<?php echo get_term_link($quaderni[1]) ?>">Vedi tutti gli articoli</a>
                    </div>
                </div>
                <!-- /copybooks_container -->

                <div class="copybooks_numbers">

                    <div class="row margin-bottom-30">
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[2], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[3], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                    </div>
                    <!-- /row -->
                    <?php
                    
                    ?>
                    <div class="row margin-bottom-30">
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[4], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[5], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                    </div>
                    <!-- /row -->
                    <?php  ?>

                    <?php
                    /*
                    ?>
                    <div class="row margin-bottom-30">
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[6], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[7], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                    </div>
                    <!-- /row -->
                    <?php  ?>

                    <?php
                    
                    ?>
                    <div class="row margin-bottom-30">
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[8], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                        <div class="col-md-6">
                            <?php copybook_number($quaderni[9], "none", true); ?>
                        </div>
                        <!-- /col-md-6 -->
                    </div>
                    <!-- /row -->
                    <?php
                    */
                     ?>


                </div>


                <div class="row margin-bottom-20 padding-left-15 padding-right-15" style="text-align:center;">
                    <a href="https://www.spreaker.com/user/civcatt" target="_BLANK"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/<?php echo rand(1, 3); ?>.png" style="max-width: 100%;"></a>
                </div>





            </div>
            <!-- /col-md-4 -->

        </div>
        <!-- /row -->

    </div>
    <!-- /container -->

</div><!-- /section -->