<?php get_template_part('includes/header');

global $post;
$apertura = get_field("apertura", $post->ID);

$immagine = $apertura["sizes"]["slider"];

$dida = $apertura["caption"];
if(!$immagine){
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), "slider");
    if ($thumb){
        $immagine = $thumb[0];
        $imgobj = get_post(get_post_thumbnail_id($post->ID));
        $dida = $imgobj->post_excerpt;
    }
}


?>

<main role="main">
    <div class="section section_white section_nopadding_top">
          <div class="container-fullscreen margin-bottom-50">
              <?php if($immagine) { ?>
                <div class="lead_wrapper lead_wrapper_article" style="background-image: url('<?php echo $immagine; ?>');"></div>
                  <?php
                  if($dida != ""){
                      ?><div class="container"><div class="row" style="text-align:right;"> <div class="col-md-12"><i class="didascalia"><?php echo $dida; ?></i></div></div></div><?php
                  }
              } ?>
            </div>
        <?php
        if (have_posts()) {
            while (have_posts()) {
                the_post();
                ?>

                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <article>
                                <div class="article_wrapper">

                                    <div class="article_title">
                                        <h1><?php echo mb_strtoupper(get_the_title()); ?></h1>
                                        <?php
                                        $sottotitolo = get_field("sottotitolo");
                                        if($sottotitolo) echo "<h2>".$sottotitolo."</h2>";
                                        ?>
                                    </div>
                                    <div class="article_details">
                                        <div class="author_container">
                                            <?php
                                            $userimg = get_field("foto", "user_" . $post->post_author);
                                            if ($userimg)
                                                $img = $userimg["sizes"]["userthumb"];
                                            else
                                                $img  = get_bloginfo("template_url")."/img/logo.svg";

                                            if($img) {
                                            ?>
                                                <div class="author_thumb">
                                                    <img src="<?php echo $img; ?>" alt="<?php echo esc_attr(get_the_title()); ?>">
                                                </div>
                                                <!-- /author_thumb -->
                                            <?php
                                            }
                                            ?>
                                            <p>
                                                <b><a href="<?php echo get_author_posts_url($post->post_author); ?>"><?php echo get_userdata($post->post_author)->display_name; ?></a></b>
                                            </p>
                                        </div>
                                        <!-- /author_container -->
                                        <div class="date_inside" style="padding: 6px 0px 2px 0px;"><?php echo date_i18n("j F Y", strtotime($post->post_date));  ?></div>
                                    </div>
                                    <?php
                                    $by = get_userdata($post->post_author)->display_name;
                                    cc_share_icons(get_permalink($post->ID), $post->post_title , false, $by) ?>
				    
                                    <div class="article_content">
                                        <?php
                                        $content = get_the_content();
                                        $content = strip_tags($content, "<b><i><strong><iframe><ul><ol><li><p><a><blockquote><del><img><em>");
                                        echo apply_filters("the_content", $content);
                                        ?>
                                    </div>
                                    <?php // cc_share_icons(get_permalink($post->ID), $post->post_title , false); ?>

                                    <div id="anchor_comment"></div>
                                    <?php
                                    $argomenti = wp_get_post_terms($post->ID, "argomento");
                                    $str_argomenti = "";
                                    //                                    dd($argomenti);
                                    if ($argomenti) {
                                        ?>
                                        <div class="article_tags">
                                            <i class="icon icon-icon-tag"></i>
                                            <div class="tags_list" id="tags_list">
                                                <?php
                                                foreach ($argomenti as $argomento) {
                                                    $str_argomenti.=" ".$argomento->name;
                                                    echo '<a href="' . get_term_link($argomento) . '">' . $argomento->name . '</a>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <!-- /article_wrapper -->
                            </article>
                        </div>
                        <!-- /col-md-12 -->
                    </div>
                    <!-- /row -->
                </div><!-- /container -->
            <?php
            }
        }

        if(comments_open()) {
            ?>

            <div class="container-small">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <aside>
                            <div class="comments" id="comments">
                                <div id="fb-root"></div>
                                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v10.0&appId=<?php echo FB_APP_ID; ?>&autoLogAppEvents=1" nonce="Lr8F31vG"></script>
                                <div class="fb-comments" data-href="<?php echo str_replace("https://", "http://", get_permalink($post)); ?>" data-width="" data-numposts="5"></div>
                            </div>
                        </aside>
                    </div>
                    <!-- /col-md-10 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container-small -->
        <?php
        }
        ?>
    </div>
</main>


</div><!-- /main_container -->


<?php get_template_part('includes/footer'); ?>
