<?php get_template_part('includes/header'); ?>
<main role="main">

    <div class="section section_grey_light">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="title_large">
                        <h1>Rassegna Stampa</h1>
                    </div>

                </div><!-- /col-md-12-->

            </div><!-- /row -->


            <div class="row">

                <div class="col-md-8">

                    <main role="main">

<?php
global $post;
if(have_posts()): while(have_posts()): the_post();
    setup_postdata($post);
?>

    <div class="article_simple">
        <p><?php echo get_field("fonte"); ?> · <?php echo get_field("data"); ?></p>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <p><?php the_excerpt(); ?></p>
    </div><!-- /article_simple -->

    <?php endwhile; ?>

               <div class="pager">
                <?php
                $big = 999999999; // need an unlikely integer
                echo paginate_links(array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => $paged,
                    'total' => $wp_query->max_num_pages
                ));
                ?>
            </div>
<?php endif; ?>
                    </main>
                </div><!-- /col-md-8 -->

                <div class="col-md-4">
                <?php get_template_part('includes/box-abbonamenti'); ?>
                </div><!-- /col-md-4 -->

            </div><!-- /row -->

        </div><!-- /container -->

    </div><!-- /section -->

</main>


</div><!-- /main_container -->




<?php get_template_part('includes/footer'); ?>

