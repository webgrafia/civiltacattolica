<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <link rel="alternate" type="application/rss+xml" title="La Civiltà Cattolica" href="<?php echo home_url('/feed/rss'); ?>" />

    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo("url"); ?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo("url"); ?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo("url"); ?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo("url"); ?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo("url"); ?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo("url"); ?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo("url"); ?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo("url"); ?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo("url"); ?>/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php bloginfo("url"); ?>/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo("url"); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo("url"); ?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo("url"); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo("url"); ?>/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo("url"); ?>/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <?php wp_head(); ?>
    <link rel='stylesheet' id='custom'  href='<?php bloginfo("template_url") ?>/custom.css' type='text/css' media='all' />

<?php
if(is_singular("articolo")){
global $post;
    $facebook = get_field('facebook', 'user_' . $post->post_author);


    $autore = get_userdata($post->post_author)->display_name;
    $altro_autore = get_field("altro_autore");
    if(!is_wp_error($altro_autore)){
        if($altro_autore != ""){
            $autore .=  " - ".$altro_autore;
        }

    }
    if($autore)
        echo "<meta name=\"author\" content=\"".$autore."\" />";
    if($facebook)
        echo "<meta property=\"article:author\" content=\"".$facebook."\" />";
}
 ?>
    <meta property="fb:app_id" content="<?php echo FB_APP_ID; ?>" />
    <meta property="fb:admins" content="1449035898" />
    <script src="//connect.facebook.net/it_IT/all.js#xfbml=1"></script>
    <style>
        .lang_menu{
            position:absolute;
            right: 30px;
            background: #666;
            z-index:999;
            width: 100px;
        }

        .lang_menu li{
            background: #333;
            width: 100%;
            float:left;
        }
    </style>

</head>

<body <?php body_class(); ?>>

<?php if(is_user_logged_in()){ ?>
    <div class="nav_secondary">
        <div class="container">
        <div class="row clearfix ">
            <div class="col-md-12">
                <?php wp_nav_menu(array("theme_location" => "loggedin", "container_class" => "nav_menu"));  ?>
            </div><!-- /col-sm-12 -->
        </div><!-- /row -->
        </div>
    </div>
<?php } ?>

<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->

<!-- Left menu mobile -->
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left perfect-scrollbar">
       <?php wp_nav_menu(array("theme_location" => "main", "container_class" => "nav_menu",'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s<li><a href="https://laciviltacattolica.us14.list-manage.com/subscribe?u=441b2200731c72bb723ede881&id=9d2f468610">Newsletter</a></li></ul>'));  ?>
       <?php wp_nav_menu(array("theme_location" => "evidenza","container_class" => "nav_menu nav_menu_black")); ?>
       <?php wp_nav_menu(array("theme_location" => "languages", "container_class" => "nav_menu", 'items_wrap' => '<ul id="%1$s" class="%2$s"><li><b style="padding-left:10px;">Lingue</b></li>%3$s</ul>'));  ?>
</nav>
<!-- End Left menu mobile -->


<header class="header" role="banner">

    <div class="header_content text-center">

        <div class="logo_container">
            <a href="<?php bloginfo("url"); ?>" class="logo"><img src="<?php bloginfo("template_url"); ?>/img/logo.svg" alt="La Civilta Cattolica"></a>
            <p class="logo_title"><a href="<?php bloginfo("url"); ?>" >La Civiltà <strong>Cattolica</strong></a></p>
            <p class="logo_subtitle">La rivista più antica in lingua italiana, Dal 1850</p>
        </div><!-- /logo_container -->

    </div>

</header>



<div class="main_container" id="main">

    <div class="sticky-wrapper clearfix">

        <div class="main_nav">

            <div class="hamburger hamburger--slider-r toggle-menu menu-left push-body hidden-lg hidden-md">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>

            <div class="logo_container hidden-lg hidden-md">
                <a href="<?php bloginfo("url"); ?>" class="logo"><img src="<?php bloginfo("template_url"); ?>/img/logo_white.svg" alt="La Civilta Cattolica"></a>
            </div><!-- /logo_container -->

            <div class="logo_title hidden-lg hidden-md"><a style="color:#fff !important;" href="<?php bloginfo("url"); ?>" >La Civiltà <strong>Cattolica</strong></a></div>

            <div class="container hidden-sm hidden-xs">

                <div class="row clearfix">

                    <div class="col-sm-12 clearfix">
                        <?php wp_nav_menu(array("theme_location" => "main", "container" => "nav", "container_class" => "main_nav_content clearfix", "items_wrap" => '<ul id="%1$s" class="%2$s"><li class="homeicon"><a href="'.get_bloginfo("home").'"><img src="'.get_bloginfo("template_url").'/img/home.png" style="width:26px; height:auto;" alt="Home"></a></li>%3$s</ul>')); ?>
                    </div><!-- /col-sm-12 -->

                </div><!-- /row -->

            </div><!-- /container -->

            <div id="sb-search-nav" class="sb-search">
                <form method="get" action="<?php bloginfo("url"); ?>">
                    <input class="sb-search-input" placeholder="" type="text" value="" name="s" id="search">
                    <input class="sb-search-submit" type="submit" value="">
                    <span class="sb-icon-search"></span>
                </form>
            </div>

        </div><!-- /main_nav -->
    </div><!-- /sticky-wrapper -->

    <div class="nav_secondary clearfix">

        <div class="container">

            <div class="row clearfix">

                <div class="col-md-12">
                    <?php wp_nav_menu(array("theme_location" => "evidenza", "container" => "")); ?>
                    <div class="social_container">
                      <a href="http://t.me/civcatt"><i class="icon icon-telegram"></i></a>
		      <a href="https://www.facebook.com/civiltacattolica/"><i class="icon icon-icon-facebook"></i></a>
                        <a href="https://twitter.com/civcatt"><i class="icon icon-icon-twitter"></i></a>
                        <a href="https://www.instagram.com/civcatt/"><i class="icon icon-instagram"></i></a>
                        <a href="https://laciviltacattolica.us14.list-manage.com/subscribe?u=441b2200731c72bb723ede881&id=9d2f468610"><i class="icon icon-email"></i></a>
                        <a href="#" id="language" onmouseover=""><img src="<?php bloginfo("template_url"); ?>/img/world.png" width="26" style="margin-top: -10px;" alt="World"></a>
                            <?php wp_nav_menu(array("theme_location" => "languages", "container_class" => "lang_menu"));  ?>

                    </div>


                </div><!-- /col-md-12 -->

            </div><!-- /row -->

        </div><!-- /container -->

    </div>


