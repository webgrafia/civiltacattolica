<?php

$arg  = array(
	'posts_per_page' => 6,
	'post_type' => 'recensione'
);
$recensioni = get_posts($arg);
?>
<div class="section section_white section_books">

	<div class="section_title">
		<h2>Rassegna Bibliografica</h2>
	</div>
    <div class="owl-carousel carousel-theme carousel_books carousel_recensioni">

		<?php
		foreach ($recensioni as $recensione) {
			?>
			<div class="item">
				<?php
				print_recensione_lista($recensione, "recensione_container");
				?>
			</div><!-- /item -->

			<?php
		}
		?>
	</div><!-- /carousel_events -->

	<div class="all_large">

		<a href="<?php echo get_post_type_archive_link("recensione"); ?>">Tutte le Recensioni</a>

	</div>

</div><!-- /section -->