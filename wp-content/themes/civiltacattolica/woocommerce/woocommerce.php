<?php get_template_part('includes/header'); ?>
<main role="main">
    <div class="section section_white section_nopadding_top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <article class="padding-top-20">
                        <div class="woocommerce">
                            <?php woocommerce_content(); ?>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</main><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
