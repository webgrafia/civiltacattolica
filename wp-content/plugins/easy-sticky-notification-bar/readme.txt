=== WordPress Easy Sticky Notification Bar ===
Contributors: designorbital
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: sticky bar, notification bar, attention bar, fixed bar, top bar, sticky header, top sticky bar, top notification bar, notice, notification, plugin
Requires at least: 4.6
Tested up to: 4.8.1
Stable tag: 0.4

Add an elegant, responsive and clean sticky notification bar on the top of your WordPress site by using the Easy Sticky Notification Bar plugin.

== Description ==

Easy Sticky Notification Bar plugin helps you to promote your products, highlight your messages or grab the attention of your visitors via great looking sticky bar.

= Features =
* Very Easy to Setup and Use
* Responsive Layout
* Google Fonts
* Sticky Notification Bar
* Translation Ready
* Multisite Support

= Further Useful Stuff =
Easy Sticky Notification Bar plugin is developed and maintained by DesignOrbital. We invite you to use our [Premium WordPress Themes](https://designorbital.com/) or [Free WordPress Themes](https://designorbital.com/free-wordpress-themes/) to see the difference and easy to use options.

== Installation ==

= Using The WordPress Dashboard =

1. Navigate to the `Add New` in the plugins dashboard
2. Search for `Easy Sticky Notification Bar`
3. Click `Install Now`
4. Activate the plugin on the Plugin dashboard
5. Configure the plugin by going to the `Settings > Easy Sticky Notification Bar` options.

= Uploading in WordPress Dashboard =

1. Navigate to the `Add New` in the plugins dashboard
2. Navigate to the `Upload` area
3. Select `easy-sticky-notification-bar.zip` from your computer
4. Click `Install Now`
5. Activate the plugin in the Plugin dashboard
6. Configure the plugin by going to the `Settings > Easy Sticky Notification Bar` options.

= Using FTP =

1. Download `easy-sticky-notification-bar.zip`
2. Extract the `easy-sticky-notification-bar` directory to your computer
3. Upload the `easy-sticky-notification-bar` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard
5. Configure the plugin by going to the `Settings > Easy Sticky Notification Bar` options.

== Frequently Asked Questions ==

= Where is the *.pot file for translating the plugin in any language? =

If you want to contribute a translation of the plugin in your language it would be great! You would find the *.pot file in the 'languages' directory of this plugin. If you would send the *.po file to me I would include it in the next release of the plugin.

== Screenshots ==

1. Configuration options for the WordPress Easy Sticky Notification Bar plugin.
2. Notificaton content settings for the WordPress Easy Sticky Notification Bar plugin.

== Changelog ==

= 0.4 - September 10, 2017 =

* Enhancement: Settings page improvements.
* Update: POT file updated.
* Update: Readme file updated.

= 0.3 - November 12, 2014 =

* Enhancement: Google Fonts added.
* Enhancement: Improved notification bar interface.

= 0.2 - June 09, 2014 =

* Enhancement: Call to Action button added.

= 0.1 =

* Initial release.
