
http://www.mauromascia.com/shop/product/woocommerce-xpay-payment-gateway-cartasi/


CHANGELOG

- 20160803
	- Aggiunte nuove lingue (Giapponese, Cinese, Arabo e Russo) per la pagina di pagamento, come da specifiche 10.5 (2016/04)
	- Rimossa scelta delle URL di ritorno perché genera confusione: verranno utilizzate le pagine di default di WooCommerce.
	- Aggiunta classe WC_Gateway_XPay_Helper per funzioni di utilità.
	- Aggiunta classe WC_Gateway_XPay_Refund per abilitare il rimborso.
	- Rimosso check sulla presenza del modulo SOAP client di PHP, perché non utilizzato in XPay.
	- Fix per GET vuote, utilizzando REQUEST_URI.

- 20160615
	- Fix parametro lingua qTranslate-X

- 20160609
	- Fix integrazione XPay Addon

- 20160603
	- Fix problema risposta con $_GET vuota

- 20160205
	- Integrazioni per compatibilità con XPay Addon
	- Fix messaggio pagina errore (quando l'ordine fallisce)
	- Verifica di compatibilità con WooCommerce 2.5.2

- 20151007
	- Aggiunta nella pagina di configurazione un campo opzionale per poter specificare la URL da chiamare per il pagamento (potrebbero averne bisogno i nuovi utenti di test)
	- Altre piccole correzioni per compatibilità con Woocommerce 2.4.7.

- 20150510
	- Piccola correzione per poter tradurre la descrizione del gateway con WPML (testato su versione 3.1.9.7).
