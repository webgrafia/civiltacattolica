/*!
 * Mpwc_Pro_Pop 1.0.0
 * http://wponlinesupport.com
 *
 * Copyright (c) 2016 WP Online Support
 */

(function (Mpwc_Pro_Pop, undefined) {

    'use strict';

    // set default settings
    var settings = {

            // set default cover id
            coverId: 'mpwc-popup-cover',

            // duration (in days) before it pops up again
            expires: 1,

            // close if someone clicks an element with this class and prevent default action
            closeClassNoDefault: 'mpwc-popup-close',

            // close if someone clicks an element with this class and continue default action
            closeClassDefault: 'mpwc-close-go',            

            // on popup open function callback
            onPopUpOpen: null,

            // on popup close function callback
            onPopUpClose: null,

            // hash to append to url to force display of popup
            forceHash: 'splash',

            // hash to append to url to delay popup for 1 day
            delayHash: 'go',

            // close if the user clicks escape
            closeOnEscape:1,

            // set an optional delay (in milliseconds) before showing the popup
            delay: 2000,

            // automatically close the popup after a set amount of time (in milliseconds)
            hideAfter: null,
        },


        // grab the elements to be used
        $el = {
            html: document.getElementsByTagName('html')[0],
            cover: document.getElementById(settings.coverId),
            closeClassDefaultEls: document.querySelectorAll('.' + settings.closeClassDefault),
            closeClassNoDefaultEls: document.querySelectorAll('.' + settings.closeClassNoDefault)
        },


        /**
         * Helper methods
         */

        util = {

            hasClass: function(el, name) {
                return new RegExp('(\\s|^)' + name + '(\\s|$)').test(el.className);
            },

            addClass: function(el, name) {
                if (!util.hasClass(el, name)) {
                    el.className += (el.className ? ' ' : '') + name;
                }
            },

            removeClass: function(el, name) {
                if (util.hasClass(el, name)) {
                    el.className = el.className.replace(new RegExp('(\\s|^)' + name + '(\\s|$)'), ' ').replace(/^\s+|\s+$/g, '');
                }
            },
            removeElement:function(id){
                    var elem = document.getElementById(id);
                    return elem.parentNode.removeChild(elem);
            },
            addListener: function(target, type, handler) {
                if (target.addEventListener) {
                    target.addEventListener(type, handler, false);
                } else if (target.attachEvent) {
                    target.attachEvent('on' + type, handler);
                }
            },

            removeListener: function(target, type, handler) {
                if (target.removeEventListener) {
                    target.removeEventListener(type, handler, false);
                } else if (target.detachEvent) {
                    target.detachEvent('on' + type, handler);
                }
            },

            isFunction: function(functionToCheck) {
                var getType = {};
                return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
            },

            setCookie: function(name, days) {
                var date = new Date();
                date.setTime(+ date + (days * 86400000));
                document.cookie = name + '=true; expires=' + date.toGMTString() + '; path=/';
            },

            hasCookie: function(name) {
                if (document.cookie.indexOf(name) !== -1) {
                    return true;
                }
                return false;
            },

            // check if there is a hash in the url
            hashExists: function(hash) {
                if (window.location.hash.indexOf(hash) !== -1) {
                    return true;
                }
                return false;
            },

            preventDefault: function(event) {
                if (event.preventDefault) {
                    event.preventDefault();
                } else {
                    event.returnValue = false;
                }
            },

            mergeObj: function(obj1, obj2) {
                for (var attr in obj2) {
                    obj1[attr] = obj2[attr];
                }
            }
        },


        /**
         * Private Methods
         */

        // close popup when user hits escape button
        onDocUp = function(e) {
          
            if (settings.closeOnEscape) {
                if (e.keyCode === 27) {
                    Mpwc_Pro_Pop.close();
                }
            }
        },

        openCallback = function() {

            // if not the default setting
            if (settings.onPopUpOpen !== null) {

                // make sure the callback is a function
                if (util.isFunction(settings.onPopUpOpen)) {
                    settings.onPopUpOpen.call();
                } else {
                    throw new TypeError('Mpwc_Pro_Pop open callback must be a function.');
                }
            }
        },

        closeCallback = function() {

            // if not the default setting
            if (settings.onPopUpClose !== null) {

                // make sure the callback is a function
                if (util.isFunction(settings.onPopUpClose)) {
                    settings.onPopUpClose.call();
                } else {
                    throw new TypeError('Mpwc_Pro_Pop close callback must be a function.');
                }
            }
        };



    /**
     * Public methods
     */

    //font-size
    jQuery("html").addClass('sp-overflow-hidden');
    check_responsive_font_sizes();
    function check_responsive_font_sizes() {
        //  Apply font sizes
        jQuery(".mpwc_main_heading[data-font-size-init]").each(function(index, el) {
            var p = jQuery(el);
            var data = jQuery( this ).html();

            if ( data.toLowerCase().indexOf("sp_font") >= 0 && data.match("^<div") && data.match("</div>$") ) {
                p.addClass('sp-no-responsive');
            } else {
                p.removeClass('sp-no-responsive');
            }
        });
    }

    Mpwc_Pro_Pop.open = function(opt) {
      
        var i, len;
         // do not display the popup if one is showing already
        if(opt.coverId =='mpwc-popup-welcome'){
           
            if( jQuery( 'html' ).hasClass( 'mpwc-popup-exit' ) ) {
                return;
            }
        }else if(opt.coverId =='mpwc-popup-exit'){
           
            if( jQuery( 'html' ).hasClass( 'mpwc-popup-welcome' ) ) {
                return;
            }
        }

        if (util.hashExists(settings.delayHash)) {

            util.setCookie(opt.cookieName, 1); // expire after 1 day
            return;
        }       
        //check is cookie set
        if(util.hasCookie(opt.cookieName))
        {
            return;
        }
        
        util.addClass($el.html,opt.coverId);
      
        // bind close events and prevent default event
        if ($el.closeClassNoDefaultEls.length > 0) {
            for (i=0, len = $el.closeClassNoDefaultEls.length; i < len; i++) {
                util.addListener($el.closeClassNoDefaultEls[i], 'click', function(e) {
                    if (e.target === this) {
                        util.preventDefault(e);
                        Mpwc_Pro_Pop.close(opt);
                    }
                });
            }
        }

        if(opt.closeOnEscape)
        {
             util.addListener(document, 'keyup',  function (e){
                if(e.keyCode == 27){
                    Mpwc_Pro_Pop.close(opt);
                }            
            });            
        }
        if (opt.hideAfter) {           
                    setTimeout(Mpwc_Pro_Pop.close, opt.hideAfter,opt);           
        }

        openCallback();  

    };

    Mpwc_Pro_Pop.close = function(options) {
      
        util.removeClass($el.html,options.coverId);
        util.removeElement(options.coverId);
        util.setCookie(options.cookieName, options.expires);

        // unbind escape detection to document
        util.removeListener(document, 'keyup', onDocUp);
        closeCallback();
    };
    
}(window.Mpwc_Pro_Pop = window.Mpwc_Pro_Pop || {}));

 
// Custom Code
jQuery( document ).ready(function($) {
    
    var mpwc_conf = {};
    //set cookie prefix
    var cookie_prefix = '';
    if(Mpwc_Pro_Popup.cookie_prefix)
    {
        cookie_prefix =Mpwc_Pro_Popup.cookie_prefix;
    }    
    
    if (jQuery('#mpwc-popup-exit').length > 0)
    {       
        var mpwc_popup_exit = false;
        jQuery(document).bind('mouseleave', function (e) {
       
            mpwc_conf     = $.parseJSON( $('#mpwc-popup-exit').find('.mpwc-popup-config').text());
            var rightD = jQuery(window).width() - e.pageX;
          
            if (mpwc_popup_exit == false && rightD > 20) {
                var popup_sett = {
                            'coverId'           : 'mpwc-popup-exit',
                            'delay'             : 1000,
                            'closeOnEscape'     : (mpwc_conf.clsonesc == '1') ? true : false,
                            'hideclsbtn'        : mpwc_conf.hideclsbtn ? false : true ,
                            'cookie_expires'    : parseInt(mpwc_conf.exptime),
                            'hideAfter'         : (parseInt(mpwc_conf.disappear) * 1000),
                            'popup_ID'          : parseInt(mpwc_conf.popup_ID),                      
                            'x_scroll'          : parseInt(mpwc_conf.x_scroll),
                            'inactivity_second' : parseInt(mpwc_conf.x_second),
                            'cookieName'        : cookie_prefix+'mpwc_exit_'+parseInt(mpwc_conf.popup_ID),
                            };

            Mpwc_Pro_Pop.open(popup_sett);
            //mpwc_popup_exit = true;
            }
        
        });
    }
    if (jQuery('#mpwc-popup-welcome').length > 0)
    {
        mpwc_conf     = $.parseJSON( $('#mpwc-popup-welcome .mpwc-popup-config').text());
        var popup_sett = {
                            'coverId'           : 'mpwc-popup-welcome',
                            'display_on'        : mpwc_conf.when_popup_appear,
                            'delay'             : parseInt(mpwc_conf.delay) * 1000,
                            'closeOnEscape'     : (mpwc_conf.clsonesc == '1') ? true : false,
                            'hideclsbtn'        : mpwc_conf.hideclsbtn ? false : true ,
                            'cookie_expires'    : parseInt(mpwc_conf.exptime),
                            'hideAfter'         : parseInt(mpwc_conf.disappear)*1000,
                            'popup_ID'          : parseInt(mpwc_conf.popup_ID),                      
                            'x_scroll'          : parseInt(mpwc_conf.x_scroll),
                            'inactivity_second' : parseInt(mpwc_conf.x_second),
                            'cookieName'        : cookie_prefix+'mpwc_welcome_'+parseInt(mpwc_conf.popup_ID),
                            };
         
        var mpwc_op_welcome = false;  

        if(popup_sett.display_on == 'scroll')
        {

            jQuery(window).scroll(function () {
                var h = jQuery(document).height() - jQuery(window).height();
                var sp = jQuery(window).scrollTop();
                var p = parseInt(sp / h * 100);
                popup_sett.delay = 0;
                if (p >= popup_sett.x_scroll) {
                    Mpwc_Pro_Pop.open(popup_sett);
                   
                }
            });
        }
        else if(popup_sett.display_on == 'inactivity')
        {
            //alert(popup_sett.inactivity_second);
            var setTimervalue;
            var idleInterval = setInterval(Increment_timervalue, 1000); // 1 second

            //Zero the idle timer on mouse movement.
            $(this).mousemove(function (e) {
                setTimervalue = 0;
            });
            $(this).keypress(function (e) {
                setTimervalue = 0;
            });

            function Increment_timervalue() {
                setTimervalue = setTimervalue + 1;
                if (setTimervalue > popup_sett.inactivity_second) {
                    setTimervalue = 0;
                    Mpwc_Pro_Pop.open(popup_sett);
                    mpwc_op_welcome = true;     
                }
            }
        }
        else{
                if (popup_sett.delay === 0) {
                    Mpwc_Pro_Pop.open(popup_sett);
                } else {
                    // delay showing the popup
                    setTimeout(Mpwc_Pro_Pop.open,popup_sett.delay ,popup_sett);                    
                }
        }
       
        
    }

});