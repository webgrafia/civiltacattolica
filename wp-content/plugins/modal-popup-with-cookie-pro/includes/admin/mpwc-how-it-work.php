<?php
/**
 * Pro Designs and Plugins Feed
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// Action to add menu
add_action('admin_menu', 'mpwc_pro_register_design_page');

/**
 * Register plugin design page in admin menu
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_register_design_page() {
	add_submenu_page( 'edit.php?post_type='.MPWC_PRO_POPUP_POST_TYPE, __('How it works - Modal Popup With Cookie Pro', 'modal-popup-with-cookie'), __('How It Works', 'modal-popup-with-cookie'), 'manage_options', 'mpwc-designs', 'mpwc_pro_designs_page' );
}

/**
 * Function to display plugin design HTML
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_designs_page() {

	$wpos_feed_tabs = mpwc_pro_help_tabs();
	$active_tab 	= isset($_GET['tab']) ? $_GET['tab'] : 'how-it-work';
?>
		
	<div class="wrap mpwc-wrap">

		<h2 class="nav-tab-wrapper">
			<?php
			foreach ($wpos_feed_tabs as $tab_key => $tab_val) {
				$tab_name	= $tab_val['name'];
				$active_cls = ($tab_key == $active_tab) ? 'nav-tab-active' : '';
				$tab_link 	= add_query_arg( array( 'post_type' => MPWC_PRO_POPUP_POST_TYPE, 'page' => 'mpwc-designs', 'tab' => $tab_key), admin_url('edit.php') );
			?>

			<a class="nav-tab <?php echo $active_cls; ?>" href="<?php echo $tab_link; ?>"><?php echo $tab_name; ?></a>

			<?php } ?>
		</h2>
		
		<div class="mpwc-tab-cnt-wrp">
		<?php
			if( isset($active_tab) && $active_tab == 'how-it-work' ) {
				mpwc_pro_howitwork_page();
			}
			else if( isset($active_tab) && $active_tab == 'plugins-feed' ) {
				echo mpwc_pro_get_plugin_design( 'plugins-feed' );
			} else {
				echo mpwc_pro_get_plugin_design( 'offers-feed' );
			}
		?>
		</div><!-- end .mpwc-tab-cnt-wrp -->

	</div><!-- end .mpwc-wrap -->

<?php
}

/**
 * Gets the plugin design part feed
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_get_plugin_design( $feed_type = '' ) {
	
	$active_tab = isset($_GET['tab']) ? $_GET['tab'] : '';
	
	// If tab is not set then return
	if( empty($active_tab) ) {
		return false;
	}

	// Taking some variables
	$wpos_feed_tabs = mpwc_pro_help_tabs();
	$transient_key 	= isset($wpos_feed_tabs[$active_tab]['transient_key']) 	? $wpos_feed_tabs[$active_tab]['transient_key'] 	: 'mpwc_' . $active_tab;
	$url 			= isset($wpos_feed_tabs[$active_tab]['url']) 			? $wpos_feed_tabs[$active_tab]['url'] 				: '';
	$transient_time = isset($wpos_feed_tabs[$active_tab]['transient_time']) ? $wpos_feed_tabs[$active_tab]['transient_time'] 	: 172800;
	$cache 			= get_transient( $transient_key );
	
	if ( false === $cache ) {
		
		$feed 			= wp_remote_get( esc_url_raw( $url ), array( 'timeout' => 120, 'sslverify' => false ) );
		$response_code 	= wp_remote_retrieve_response_code( $feed );
		
		if ( ! is_wp_error( $feed ) && $response_code == 200 ) {
			if ( isset( $feed['body'] ) && strlen( $feed['body'] ) > 0 ) {
				$cache = wp_remote_retrieve_body( $feed );
				set_transient( $transient_key, $cache, $transient_time );
			}
		} else {
			$cache = '<div class="error"><p>' . __( 'There was an error retrieving the data from the server. Please try again later.', 'modal-popup-with-cookie' ) . '</div>';
		}
	}
	return $cache;	
}

/**
 * Function to get plugin feed tabs
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_help_tabs() {
	$wpos_feed_tabs = array(
						'how-it-work' 	=> array(
													'name' => __('How It Works', 'modal-popup-with-cookie'),
												),
						'plugins-feed' 	=> array(
													'name' 				=> __('Our Plugins', 'modal-popup-with-cookie'),
													'url'				=> 'http://wponlinesupport.com/plugin-data-api/plugins-data.php',
													'transient_key'		=> 'wpos_plugins_feed',
													'transient_time'	=> 172800
												),
						'offers-feed' 	=> array(
													'name'				=> __('WPOS Offers', 'modal-popup-with-cookie'),
													'url'				=> 'http://wponlinesupport.com/plugin-data-api/wpos-offers.php',
													'transient_key'		=> 'wpos_offers_feed',
													'transient_time'	=> 86400,
												)
					);
	return $wpos_feed_tabs;
}

/**
 * Function to get 'How It Works' HTML
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_howitwork_page() { ?>
	
	<style type="text/css">
		.wpos-pro-box .hndle{background-color:#0073AA; color:#fff;}
		.wpos-pro-box .postbox{background:#dbf0fa none repeat scroll 0 0; border:1px solid #0073aa; color:#191e23;}
		.postbox-container .wpos-list li:before{font-family: dashicons; content: "\f139"; font-size:20px; color: #0073aa; vertical-align: middle;}
		.mpwc-wrap .wpos-button-full{display:block; text-align:center; box-shadow:none; border-radius:0;}
		.mpwc-shortcode-preview{background-color: #e7e7e7; font-weight: bold; padding: 2px 5px; display: inline-block; margin:0 0 2px 0;}
	</style>

	<div class="post-box-container">
		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
			
				<!--How it workd HTML -->
				<div id="post-body-content">
					<div class="metabox-holder">
						<div class="meta-box-sortables ui-sortable">
							<div class="postbox">
								
								<h3 class="hndle">
									<span><?php _e( 'How It Works - Display', 'modal-popup-with-cookie' ); ?></span>
								</h3>
								
								<div class="inside">
									<table class="form-table">
										<tbody>
											<tr>
												<th>
													<label><?php _e('Getting Started', 'modal-popup-with-cookie'); ?>:</label>
												</th>
												<td>
													<ul>
														<li><?php _e('Step-1. Go to "PopUps Pro --> Add New Popup".', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-2. Add popup Title, content', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-3. Select popup design, width, heading and action link', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-4. Select popup color, backgroud color and other settings', 'modal-popup-with-cookie'); ?></li>
													</ul>
												</td>
											</tr>

											<tr>
												<th>
													<label><?php _e('How Popup Works', 'modal-popup-with-cookie'); ?>:</label>
												</th>
												<td>
													<ul>
														<li><?php _e('Step-1. Go PopUps Pro -->  Settings', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-2. Under Popup Global Settings, enable popup.', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-3. Select Modal Popup Settings Display On.', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-4. Select Popup Welcome Settings.', 'modal-popup-with-cookie'); ?></li>
														<li><?php _e('Step-5. Select Popup Exit Settings.', 'modal-popup-with-cookie'); ?></li>
													</ul>
												</td>
											</tr>											
											<tr>
												<th>
													<label><?php _e('Layouts for reference', 'modal-popup-with-cookie'); ?>:</label>
												</th>
												<td>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-1.jpg" /><br /><span>Layout 1</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-2.jpg" /><br /><span>Layout 2</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-3.jpg" /><br /><span>Layout 3</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-4.jpg" /><br /><span>Layout 4</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-5.jpg" /><br /><span>Layout 5</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-6.jpg" /><br /><span>Layout 6</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-7.jpg" /><br /><span>Layout 7</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-8.jpg" /><br /><span>Layout 8</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-9.jpg" /><br /><span>Layout 9</span></div>
												<div class="mpwcadmin-medium-6 mpwcadmin-columns"><img  src="<?php echo MPWC_PRO_URL; ?>includes/admin/popup-designs/design-10.jpg" /><br /><span>Layout 10</span></div>
												</td>
											</tr>
										</tbody>
									</table>
								</div><!-- .inside -->
							</div><!-- #general -->
						</div><!-- .meta-box-sortables ui-sortable -->
					</div><!-- .metabox-holder -->
				</div><!-- #post-body-content -->
				
				<!--Upgrad to Pro HTML -->
				<div id="postbox-container-1" class="postbox-container">
					<div class="metabox-holder wpos-pro-box">
						<div class="meta-box-sortables ui-sortable">
							<div class="postbox" style="">
								<h3 class="hndle">
									<span><?php _e('Need Support?', 'modal-popup-with-cookie'); ?></span>
								</h3>
								<div class="inside">										
									<p><?php _e('Check plugin document.', 'modal-popup-with-cookie'); ?></p>
									<a class="button button-primary wpos-button-full" href="https://www.wponlinesupport.com/wp-plugin/wp-modal-popup-cookie-integration/" target="_blank"><?php _e('Documentation', 'modal-popup-with-cookie'); ?></a>	
									<p><a class="button button-primary wpos-button-full" href="https://www.wponlinesupport.com/wp-plugin/wp-modal-popup-cookie-integration/" target="_blank"><?php _e('Demo ', 'modal-popup-with-cookie'); ?></a></p>
								</div><!-- .inside -->
							</div><!-- #general -->
						</div><!-- .meta-box-sortables ui-sortable -->
					</div><!-- .metabox-holder -->

					<!-- Help to improve this plugin! -->
					<div class="metabox-holder">
						<div class="meta-box-sortables ui-sortable">
							<div class="postbox">
									<h3 class="hndle">
										<span><?php _e( 'Help to improve this plugin!', 'modal-popup-with-cookie' ); ?></span>
									</h3>									
									<div class="inside">										
										<p><?php _e('Enjoyed this plugin? You can help by rate this plugin', 'modal-popup-with-cookie'); ?> <a href="https://wordpress.org/support/plugin/wp-modal-popup-with-cookie-integration/reviews/?filter=5" target="_blank">5 stars!</a></p>
									</div><!-- .inside -->
							</div><!-- #general -->
						</div><!-- .meta-box-sortables ui-sortable -->
					</div><!-- .metabox-holder -->
				</div><!-- #post-container-1 -->

			</div><!-- #post-body -->
		</div><!-- #poststuff -->
	</div><!-- #post-box-container -->
<?php }