<?php
/*

Plugin Name: WooCommerce Monetaweb Setefi
Plugin URI: https://filippoburatti.net/woocommerce-setefi/
Description: Extends WooCommerce with Setefi Monetaweb gateway.
Version: 2.9.1
Author: F. Buratti
Author URI: https://filippoburatti.net/
Textdomain: woocommerce-setefi
Domain Path: /languages
WC tested up to: 3.4.5
WC requires at least: 2.3

Copyright 2014  Buratti Filippo (info@filippoburatti.net)
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This software is NOT to be distributed, but can be INCLUDED in WP themes: Premium or Contracted.
This software is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
	
*/
 
add_action('plugins_loaded', 'woocommerce_setefi_init');
 
function woocommerce_setefi_init() {
 
	if ( !class_exists( 'WC_Payment_Gateway' ) ) return;
 
	/**
 	 * Localisation
	 */
	load_plugin_textdomain('woocommerce-setefi', false,  dirname( plugin_basename( __FILE__ ) ) . '/languages');
  
	/**
 	 * Gateway class
 	 */
	class WC_Setefi extends WC_Payment_Gateway {
		
		public function __construct() {
      		
      		$this->id 					= 'setefi';
      		$this->method_title 		= __( 'Setefi Monetaweb', 'woocommerce-setefi' );
			$this->method_description 	= __('Monetaweb gateway redirects customers to Monetaweb website to enter their payment information.', 'woocommerce-setefi');
      		$this->has_fields   		= false;
			$this->icon 				= apply_filters('wc_setefi_checkout_icon', plugins_url( 'images/cards.png' , __FILE__ ) );
			
			$this->supported_currencies = apply_filters('wc_setefi_supported_currencies', array('EUR' => '978', 'CHF' => '756', 'GBP' => '826', 'USD' => '840'));
			
			$this->supports     = array(
				'products',
				'refunds'
			);

     		$this->init_form_fields();
			$this->init_settings();

	  		// Define user set variables
      		$this->title 				= $this->get_option('title');
      		$this->description 			= $this->get_option('description');		
			$this->debug 				= $this->get_option('debug') == "yes" ? true : false;
			$this->testmode				= $this->get_option('testmode') == "yes" ? true : false;
			$this->submission_method 	= $this->get_option('submission_method') == 'yes' ? true : false;
			$this->shop_id 				= $this->get_option('shop_id');
			$this->shop_password 		= $this->get_option('shop_password');
			$this->shop_language		= $this->get_option('shop_language');			
			
	  
      		// Set Web Service process url to test or real
			if ( $this->testmode ) {
				$this->gateway_url = "https://test.monetaonline.it/monetaweb/payment/2/xml";
			}
			else {
				$this->gateway_url = "https://www.monetaonline.it/monetaweb/payment/2/xml";
			}
			

			// Actions
			add_action( 'woocommerce_receipt_' . $this->id, array( $this, 'receipt_page' ) );
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
			
			// Payment listener/API hook
			add_action( 'woocommerce_api_wc_setefi', array( $this, 'check_auth_response' ) );
						
			if ( !$this->is_valid_for_use() ) $this->enabled = false;
			
    	}
		
		/**
		 * Use WooCommerce logger if debug is enabled.
		 */
		function add_log($message) {
			if ($this->debug == 'yes') {
				if (empty($this->log))
					$this->log = new WC_Logger();
				$this->log->add($this->id, $message);
			}
		}
		
		/**
     	* Check if this gateway is enabled and available in the user's country
     	*/
		function is_valid_for_use() {
			
			return in_array(get_woocommerce_currency(), array_keys($this->supported_currencies));	
			
		}
		
		/**
     	* Get the Setefi currency code according to Woocommerce currency
     	*/
		function get_currency_code($currency='') {
			
			$currency = ($currency!='') ? $currency : get_woocommerce_currency();
			
			if ( in_array($currency, array_keys($this->supported_currencies))) {
            	$currency_code = $this->supported_currencies[$currency];
			}
			else {
				$currency_code = '978'; // Set EUR as default currency code
			}
			
			return apply_filters('wc_setefi_currency_code', $currency_code, $currency );
						
		}
		
		/**
     	* Initialise Gateway Settings Form Fields
    	*/
		function init_form_fields() {
	
			$this->form_fields = array(
				'enabled' => array(
								'title' => __( 'Enable/Disable', 'woocommerce-setefi' ),
								'type' => 'checkbox',
								'label' => __( 'Enable Setefi', 'woocommerce-setefi' ),
								'default' => 'yes'
							),
				'title' => array(
								'title' => __( 'Title', 'woocommerce-setefi' ),
								'type' => 'text',
								'description' => __( 'This controls the title which the user sees during checkout.', 'woocommerce-setefi' ),
								'default' => __( 'Monetaweb Setefi', 'woocommerce-setefi' ),
								'desc_tip'      => true,
							),
				'description' => array(
								'title' => __( 'Description', 'woocommerce-setefi' ),
								'type' => 'textarea',
								'description' => __( 'This controls the description which the user sees during checkout.', 'woocommerce-setefi' ),
								'default' => __( 'Pay securely by your credit card (Visa, Mastercard) through Monetaonline\'s Secure Servers.', 'woocommerce-setefi' ),
								'desc_tip'      => true
							),
				'testmode' => array(
								'title' => __( 'Gateway test', 'woocommerce-setefi' ),
								'type' => 'checkbox',
								'label' => __( 'Enable sandbox', 'woocommerce-setefi' ),
								'default' => 'yes',
								'description' => __( 'Sandbox can be used to test payments.', 'woocommerce-setefi' ),
								'desc_tip'      => true
							),				
				'shop_id' => array(
								'title' => __( 'Terminal ID', 'woocommerce-setefi' ),
								'type' => 'text',
								'description' => __( 'Terminal id provided by Setefi.', 'woocommerce-setefi' ),
								'default' => '',
								'desc_tip'      => true,
							),
				'shop_password' => array(
								'title' => __( 'Password', 'woocommerce-setefi' ),
								'type' => 'text',
								'description' => __( 'Terminal password provided by Setefi.', 'woocommerce-setefi' ),
								'default' => '',
								'desc_tip'      => true
							),					
        		'shop_language' => array(
								'title' => __( 'Language', 'woocommerce-setefi' ),
								'type' => 'select',
								'default' => 'ITA',
								'options' => array(
									'ITA' => __( 'Italian', 'woocommerce-setefi' ),
									'USA' => __( 'English', 'woocommerce-setefi' ),
									'SPA' => __( 'Spanish', 'woocommerce-setefi' ),
									'FRA' => __( 'French', 'woocommerce-setefi' ),
									'DEU' => __( 'German', 'woocommerce-setefi' )
								),
								'description' => __( 'This option allow to set the language on the payment page', 'woocommerce-setefi' ),
								'desc_tip'      => true
       						 ),
				'submission_method' => array(
								'title' => __( 'Submission method', 'woocommerce-setefi' ),
								'type' => 'checkbox',
								'label' => __( 'Use form submission method.', 'woocommerce-setefi' ),
								'description' => __( 'Enable this to post order data to Setefi via a form instead of using a redirect/querystring.', 'woocommerce-setefi' ),
								'default' => 'no',
								'desc_tip'      => true
							),
				'debug' => array(
								'title' => __( 'Debug Log', 'woocommerce-setefi' ),
								'type' => 'checkbox',
								'label' => __( 'Enable logging', 'woocommerce-setefi' ),
								'default' => 'no',
								'description' => sprintf( __( 'Log events, such as IPN requests, inside <code>%s</code>', 'woocommerce-setefi' ), wc_get_log_file_path( $this->id ) )
							)
				);
	
		}
		
		/**
		* Admin Panel Options 
		**/       
		public function admin_options() {
			?>
			<h3><?php echo $this->method_title; ?></h3>
			<p><?php echo $this->method_description; ?></p>
			<table class="form-table">
			<?php           
				if ( $this->is_valid_for_use() ) :
	
					// Generate the HTML For the settings form.
					$this->generate_settings_html();
	
				else :
	
					?>
						<div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'woocommerce-setefi' ); ?></strong>: <?php _e( 'Setefi does not support your store currency.', 'woocommerce-setefi' ); ?></p></div>
					<?php
	
				endif;
			?>
			</table><!--/.form-table-->
			<?php
		}
		
		/**
         *  There are no payment fields for Setefi, but we want to show the description if set.
         **/
        function payment_fields(){
            if($this -> description) echo wpautop(wptexturize($this -> description));
        }
		
        /**
         * Receipt Page
         **/
        function receipt_page($order_id){
			
            $order = new WC_Order($order_id);
            	
			$gateway_hosted_page = $this->get_hosted_page($order_id);
		
			if ($gateway_hosted_page) {
				echo '<p>'.__('Thank you for your order, please click the button below to pay with your credit card.', 'woocommerce-setefi').'</p>';
			   	echo "<div id=\"setefi_payment_form\"><a class=\"button\" href=\"".$gateway_hosted_page."\">".__('Pay now', 'woocommerce-setefi')."</a> ";
			   	echo "<a class=\"button alt\" href=\"".esc_url($order->get_cancel_order_url())."\">".__('Cancel order', 'woocommerce-setefi')."</a></div>";
			   
			}
			else {
				wc_add_notice( __('Payment error: Sorry, an error occurred! We can\'t process your payment right now, so please try again later.', 'woocommerce-setefi'), 'error' );
				wc_print_notices();
			   
			}
			
        }
		
		/**
         * Connect with Setefi and return hosted page url
         **/
        function get_hosted_page($order_id){
            
            $order 			= new WC_Order($order_id);
													
			$description 	= get_bloginfo( 'name' ) ." - ". __('Order', 'woocommerce-setefi') ." ".$order_id;
			$description 	= preg_replace("/[^a-z0-9\s]/i", "", $description);
  			$description 	= substr($description, 0, 64);
			
			$full_name		= method_exists( $order, 'get_formatted_billing_full_name' ) ? $order->get_formatted_billing_full_name() : $order -> billing_first_name .' '. $order -> billing_last_name; // < 2.4 compatibility
  							
			$customField    = "";	
			
			$parameters = array(
			  'id' 						=> $this->shop_id,
			  'password' 				=> $this->shop_password,
			  'operationType' 			=> 'initialize',
			  'amount' 					=> method_exists( $order, 'get_total' ) ? $order->get_total() : $order -> order_total,
			  'currencyCode' 			=> $this->get_currency_code(method_exists( $order, 'get_currency' ) ? $order->get_currency() : $order -> order_currency),
			  'language' 				=> $this->shop_language,
			  'responseToMerchantUrl' 	=> WC()->api_request_url( 'WC_Setefi' ),
			  'recoveryUrl' 			=> (function_exists('wc_get_checkout_url')) ? wc_get_checkout_url() : WC()->cart->get_checkout_url(),
			  'merchantOrderId' 		=> $order_id,
			  'cardHolderName' 			=> $full_name,
			  'cardHolderEmail' 		=> method_exists( $order, 'get_billing_email' ) ? $order->get_billing_email() : $order -> billing_email,
			  'description' 			=> $description,
			  'customField' 			=> $customField
		  	);
			
			
			$parameters = apply_filters( 'wc_setefi_payment_args', $parameters );
			
			// GDPR compliance
			$log_params = $parameters;
			if (function_exists('wp_privacy_anonymize_data')) {
				$log_params['cardHolderName'] 	= wp_privacy_anonymize_data( 'text', $log_params['cardHolderName'] );
				$log_params['cardHolderEmail'] 	= wp_privacy_anonymize_data( 'email', $log_params['cardHolderEmail'] );
			} else {
				unset($log_params['cardHolderName']);
				unset($log_params['cardHolderEmail']);
			}
			
			
			$this->add_log('[INIT REQUEST]: ' . print_r($log_params, true));
 
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $this->gateway_url);			
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curlHandle, CURLOPT_POST, true);
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, http_build_query($parameters));
			$xmlResponse = curl_exec($curlHandle);
			curl_close($curlHandle);
			
			$this->add_log('[INIT RESPONSE]: ' . $xmlResponse);
		  
			$response 		= new SimpleXMLElement($xmlResponse);
			
			$paymentId 		= isset($response->paymentid) ? (string)$response->paymentid : 0;
			$paymentUrl 	= isset($response->hostedpageurl) ? $response->hostedpageurl : 0;
			
			$securitytoken 	= isset($response->securitytoken) ? (string)$response->securitytoken : 0;
			
			if ($paymentId && $paymentUrl) {
				
				update_post_meta( $order_id, 'securitytoken', $securitytoken );
				update_post_meta( $order_id, 'paymentid', $paymentId );
								
				return $paymentUrl."?PaymentID=".$paymentId;	  
				
			} else {
				
				return false;
				
			}
		
        }
		
        /**
        * Process the payment and return the result
        **/
        function process_payment($order_id){
	
            $order = new WC_Order($order_id);
			
			$gateway_hosted_page = $this->get_hosted_page($order_id);
		
			if ($gateway_hosted_page) {
						
				if ( ! $this->submission_method ) {
					
					return array(
						'result' 	=> 'success',
						'redirect'	=> $gateway_hosted_page
					);
					
				} else {
					
					return array(
						'result' => 'success', 
						'redirect' => $order->get_checkout_payment_url( true )
					);
					
				}								
				
			} else {
							
				wc_add_notice( __('Payment error: Sorry, an error occurred! We can\'t process your payment right now, so please try again later.', 'woocommerce-setefi'), 'error' );
				return;
				
			}
            
        }
		
		/**
	   	* Check for Setefi AUTH Response
	   	**/
		function check_auth_response() {
			
			if(isset($_POST["paymentid"]) && isset($_POST["responsecode"])){
				
				$this->add_log( '[PAYMENT AUTHORIZATION RESPONSE]: ' . print_r( $_POST, true ));			
					
				$paymentid 		= $_POST["paymentid"];
				$responsecode	= $_POST["responsecode"];
				$result 		= $_POST['result'];
				$trackid 		= $_POST['merchantorderid'];
				$securitytoken  = $_POST['securitytoken'];
				
				$stored_token  	= get_post_meta( $trackid, 'securitytoken', true );
				
				
				$order = new WC_Order( $trackid );
				
				if ($responsecode=="000" && $securitytoken == $stored_token) {
				
					$msg = sprintf( __( 'Transaction for order %s has been completed successfully.', 'woocommerce-setefi' ), $trackid );
	
					$order->payment_complete($paymentid);
					WC()->cart->empty_cart();
				
				} else {
					
					$msg = sprintf( __( 'Transaction failed for order %s.', 'woocommerce-setefi' ), $trackid );
					$order->update_status('failed', __( 'Transaction failed', 'woocommerce-setefi' ));
					
				}
				
				$order->add_order_note( $msg, 1 );
				
				update_post_meta( $trackid, 'payment_result', $result );
				update_post_meta( $trackid, 'Response code', $responsecode );	
				
				$url = $this->get_return_url( $order);
				
				$this->add_log('[RETURN URL]: ' . print_r( $url, true ));
				
				do_action( 'wc_setefi_payment_response', $_POST );
								
				print($url);
				exit();
	
			} else {
				
				$this->add_log('[INVALID PAYMENT AUTHORIZATION RESPONSE]:' . print_r( $_POST, true ));
				
				if(!isset($_POST["paymentid"])) {
					wp_die( "AUTH Request Failure" );
				} else {
				
					if (function_exists('wc_get_checkout_url')) {
						$checkout_url = wc_get_checkout_url(); // woocommerce >= 2.5
					} else {
						$checkout_url = WC()->cart->get_checkout_url(); // woocommerce < 2.5
					}
					
					if(isset($_POST["errorcode"])) {
						$checkout_url = add_query_arg('setefi-error', '1', $checkout_url);	
					}
					
					print($checkout_url);
					
					exit();	
				}
				
			}
	
		}
	
		function can_refund_order( $order ) {
			return $order && $order->get_transaction_id();
		}
	
		/**
		* Process a refund if supported
		*/
		public function process_refund( $order_id, $amount = null, $reason = '' ) {
			
			$order = wc_get_order( $order_id );
				
			if ( ! $this->can_refund_order( $order ) ) {
				$this->add_log('[REFUND FAILED]: No transaction ID');
				return false;
			}
			
			$payment_id	= $order->get_transaction_id();
	
			$parameters = array(
			  'id' 				=> $this->shop_id,
			  'password' 		=> $this->shop_password,
			  //'operationType' => 'voidauthorization', // Annullamento dell'autorizzazione (no operazioni confermate)
			  'operationType' 	=> 'voidconfirmation', // Storno contabile (solo operazioni confermate)
			  'amount'			=> $amount,
			  'currencyCode' 	=> $this->get_currency_code(method_exists( $order, 'get_currency' ) ? $order->get_currency() : $order -> order_currency),
			  'merchantOrderId' => $order_id,
			  'paymentId' 		=> $order->get_transaction_id()
			);
			
			$parameters = apply_filters( 'wc_setefi_refund_args', $parameters );
			
			$this->add_log('[INIT REFUND REQUEST]: ' . print_r($parameters, true));
	  
			$curlHandle = curl_init();
			curl_setopt($curlHandle, CURLOPT_URL, $this->gateway_url);
			curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curlHandle, CURLOPT_POST, true);
			curl_setopt($curlHandle, CURLOPT_POSTFIELDS, http_build_query($parameters));
			$xmlResponse = curl_exec($curlHandle);
			curl_close($curlHandle);
			
			$this->add_log('[INIT REFUND RESPONSE]: ' . $xmlResponse);
			  
			$response = new SimpleXMLElement($xmlResponse);
						
			$paymentId_result = isset($response->paymentid) ? (int)$response->paymentid : 0;
			$responsecode = isset($response->responsecode) ? (string)$response->responsecode: 0;
			
			if ($paymentId_result == $payment_id && $responsecode=='000') {
				
				$order->add_order_note( sprintf( __( 'Refunded &euro; %s', 'woocommerce-setefi' ), $amount) );
				update_post_meta( $order_id, 'refund_result', (string)$response->result );
								
				return true;	  
				
			} else {
				
				return false;
				
			}
		
		}	
			
	}
	
	/**
 	* Add the Gateway to WooCommerce
 	**/
	function woocommerce_add_setefi_gateway($methods) {
		$methods[] = 'WC_Setefi';
		return $methods;
	}
	
	add_filter('woocommerce_payment_gateways', 'woocommerce_add_setefi_gateway' );
	
	/**
	* Add custom action links
	**/
	function woocommerce_setefi_action_links( $links ) {
		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout&section=wc_setefi' ) . '">' . __( 'Settings', 'woocommerce-setefi' ) . '</a>',
		);
		return array_merge( $plugin_links, $links );	
	}
	
	add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'woocommerce_setefi_action_links', 10, 2 );

	/**
	 * Add the meta _transaction_id in the search fields.
	 */
	function wc_custom_order_filter_fields( $fields ) {
		if ( ! in_array( '_transaction_id', $fields ) ) {
			array_push( $fields, '_transaction_id' );
		}
		return $fields;
	}
	add_filter( 'woocommerce_shop_order_search_fields', 'wc_custom_order_filter_fields' );
	
	/**
 	* Add Transaction Id to order detail
 	**/
	function add_transaction_id( $total_rows, $order ) {

    	if (isset($total_rows['payment_method'])) {
			
			$transaction_id = $order->get_transaction_id();
			if($transaction_id) {
				$total_rows['payment_method']['value'] = $total_rows['payment_method']['value']." (".__( 'Transaction ID', 'woocommerce-setefi' )." ".$transaction_id.")";
			} 
			
				
		}
		return $total_rows;

	}
	add_filter( 'woocommerce_get_order_item_totals', 'add_transaction_id', 10, 2 );
	
	/**
 	* Add Error on checkout page
 	**/
	function woocommerce_setefi_add_checkout_error() {
		wc_add_notice( __('Payment error: Sorry, an error occurred! We can\'t process your payment right now, so please try again later.', 'woocommerce-setefi'), 'error' );
	}
	
	function woocommerce_setefi_query_vars($vars) {
		array_push( $vars, 'setefi-error' );
		return $vars;
	}
	add_filter('query_vars', 'woocommerce_setefi_query_vars');
	
	function woocommerce_setefi_parse_request($wp) {
		if (array_key_exists('setefi-error', $wp->query_vars) && $wp->query_vars['setefi-error'] == 1) {
			add_action( 'woocommerce_before_checkout_form', 'woocommerce_setefi_add_checkout_error', 1 );
		}
	}
	add_action('parse_request', 'woocommerce_setefi_parse_request');

} 