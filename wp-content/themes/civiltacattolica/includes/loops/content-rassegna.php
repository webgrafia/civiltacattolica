<?php
/*
The Page Loop
=============
*/
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <article>
                    <div class="article_wrapper" >
                        <div class="section_title margin-top-20">
                            <h1 class="title"><?php the_title(); ?></h1>
                            <h2><?php echo get_field("fonte", $post->ID); ?> · <?php echo get_field("data", $post->ID); ?></h2>
                        </div>

                        <div class="article_content">
                            <?php the_content(); ?>
    <?php wp_link_pages(); ?>
                        </div>
                    </div>

                                <?php
    // Find connected posts
    $argsc = array(
        'connected_type' => 'rassegna_to_articoli',
        'connected_items' => get_queried_object(),
        'nopaging' => true,
        'suppress_filters' => false);
    $connected = get_posts($argsc);
    if ($connected) {
        ?>
        <div class="section_title">
            <h2>Correlati</h2>
        </div>
        <div class="articles_similar">
            <?php
            foreach ($connected as $connection) {
                ?>
                <div class="article_similar">
                    <div class="article_title">
                        <h3>
                            <a href="<?php echo get_permalink($connection->ID); ?>"><?php echo $connection->post_title; ?></a>
                        </h3>
                    </div>
                    <!-- /article_title -->
                    <div class="article_content">
                        <p><?php echo wp_trim_words($connection->post_content, 40, "..."); ?></p>
                    </div>
                    <!-- /article_content -->
                </div>
                <!-- /article_similar -->
            <?php
            }
            ?>
        </div>
        <!-- /articles_similar -->
    <?php
    }
    ?>
                                </div>

                </article>
            </div>
        </div>
    </div>

<?php endwhile; else: ?>
    <?php wp_redirect(get_bloginfo('siteurl').'/404', 404); ?>
    <?php exit; ?>
<?php endif; ?>
