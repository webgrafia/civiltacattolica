<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $product;
if ( ! $product->is_purchasable() ) {
	return;
}
date_default_timezone_set(get_option('timezone_string'));
	//Check if user is logged in, determine if we need to execute code below
	// Set the current user
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		if ( wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product->id) ) {
			$customer_bought_product = 1;
		}
	}

	//Get purchaseble once values
	$purchasable_once = get_post_meta( get_the_ID(), 'woo_limit_one_select_dropdown', true );
	$woo_limit_one_based_on_ip_select_dropdown = get_post_meta( get_the_ID(), 'woo_limit_one_based_on_ip_select_dropdown', true );

	//If disable by ip enabled, check previous orders for users ip address
	if ($purchasable_once == '1' and $woo_limit_one_based_on_ip_select_dropdown == '1') {
		$user_ip = $_SERVER['REMOTE_ADDR'];
	    //Check orders for ip address
        $args = array(
	        'post_type' =>  wc_get_order_types(),
	        'post_status' => array_keys( wc_get_order_statuses() ),
	        'meta_key' => '_customer_ip_address',
	        'meta_value' => $user_ip,
	        'posts_per_page' => -1
        );
        $my_query = new WP_Query($args);

		$wc_orders = $my_query->posts;

		foreach ($wc_orders as $order) {
			//print_r($order);
			//Get order meta to get ip address of order
			$order_meta = get_post_meta($order->ID);
			$order_ip = $order_meta['_customer_ip_address'][0];

			//Compare ip addresses
			if ($user_ip == $order_ip) {
				//Get items in order
				$the_order = wc_get_order( $order->ID );
				$items = $the_order->get_items();

				foreach ($items as $item) {
					//print_r($item);
					$itemid = $item['product_id'];
					if ( $itemid == $product->id ) {
						$ip_purchased_before = 1;
					}
				}
			}
		}
	}

	//Check if purchasable_once is enabled and customer purchased product otherwise use default purchase button
	if ( $purchasable_once == '1' and $ip_purchased_before == '1' || $customer_bought_product == '1' ) {
		$purchasable_once_text = get_post_meta( get_the_ID(), 'woo_limit_one_purchased_text', true );
		//Check if we are using month or year
		$woo_limit_one_time_period = get_post_meta( get_the_ID(), 'woo_limit_one_time_dropdown', true );
		if ($woo_limit_one_time_period != 'all') {
			//If Month Selected
			$woo_limit_one_time_range = get_post_meta( get_the_ID(), 'woo_limit_one_time_range', true );
			if ($woo_limit_one_time_period == 'month' || $woo_limit_one_time_period == 'day') {
				if ($woo_limit_one_time_period == 'month') {
					$ordered_date = limit_one_check_ordered_product( get_the_ID() );
					$newDate = date('Y-m-d', strtotime($ordered_date. ' + '.($woo_limit_one_time_range).' months'));
				}else if($woo_limit_one_time_period == 'day') {
					$ordered_date = limit_one_check_ordered_product( get_the_ID() );
					$newDate = date('Y-m-d', strtotime($ordered_date. ' + '.($woo_limit_one_time_range).' days'));
				}
				$diff=date_diff(new DateTime(date("Y-m-d")), new DateTime($newDate));
				if ( 0 >= intval($diff->format("%R%a")) ) {//If purchased within same day disable purchase
					echo wc_get_stock_html( $product );
					if ( $product->is_in_stock() ) : ?>
						<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
						<form class="cart" method="post" enctype='multipart/form-data'>
							<?php
								/**
								 * @since 2.1.0.
								 */
								do_action( 'woocommerce_before_add_to_cart_button' );
								/**
								 * @since 3.0.0.
								 */
								do_action( 'woocommerce_before_add_to_cart_quantity' );
								woocommerce_quantity_input( array(
									'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
									'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
									'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
								) );
								/**
								 * @since 3.0.0.
								 */
								do_action( 'woocommerce_after_add_to_cart_quantity' );
							?>
							<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
							<?php
								/**
								 * @since 2.1.0.
								 */
								do_action( 'woocommerce_after_add_to_cart_button' );
							?>
						</form>
						<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
					<?php endif;
				}else{//If its been longer than a day allow purchase
					if ($purchasable_once_text) {
						echo '<p class="woo-limit-one">'.$purchasable_once_text.'</p>';
					}else{
						echo '<p class="woo-limit-one">Purchased</p>'; 
					}
					$showDaysLeft = get_post_meta( get_the_ID(), 'woo_limit_one_days_left', true );
					if ($showDaysLeft) {
						$beforeDays = get_post_meta( get_the_ID(), 'woo_limit_one_before_days', true );
						$afterDays = get_post_meta( get_the_ID(), 'woo_limit_one_after_days', true );
						if (empty($beforeDays)) {
							$beforeDays = "Purchasable in";
						}
						if (empty($afterDays)) {
							$afterDays = "days";
						}

						echo '<p class="woo-limit-one-remaining">'.$beforeDays." ".intval($diff->format("%R%a"))." ".$afterDays.'</p>';
					}
				}	
			}
		}else{//Else can only purchase once all time
			if ($purchasable_once_text) {
				echo '<p class="woo-limit-one">'.$purchasable_once_text.'</p>';
			}else{
				echo '<p class="woo-limit-one">Purchased</p>'; 
			}		
		}
	}else{
		echo wc_get_stock_html( $product );
		if ( $product->is_in_stock() ) : ?>
			<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
			<form class="cart" method="post" enctype='multipart/form-data'>
				<?php
					/**
					 * @since 2.1.0.
					 */
					do_action( 'woocommerce_before_add_to_cart_button' );
					/**
					 * @since 3.0.0.
					 */
					do_action( 'woocommerce_before_add_to_cart_quantity' );
					woocommerce_quantity_input( array(
						'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
						'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
						'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
					) );
					/**
					 * @since 3.0.0.
					 */
					do_action( 'woocommerce_after_add_to_cart_quantity' );
				?>
				<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
				<?php
					/**
					 * @since 2.1.0.
					 */
					do_action( 'woocommerce_after_add_to_cart_button' );
				?>
			</form>
			<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
		<?php endif;
	}
?>