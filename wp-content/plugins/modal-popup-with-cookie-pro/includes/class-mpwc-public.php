<?php
/**
 * Public Class
 *
 * Handles the public side functionality of plugin
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Mpwc_Pro_Public {
	
	function __construct() {
		
		// Action to add popup
		add_action( 'wp_footer', array($this, 'mpwc_pro_add_popup') );
	}

	/**
	 * Function to check is popup enable
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0 
	 **/
	function mpwc_pro_enable_for_user() {
		$disable_for_login_user = mpwc_pro_get_option('mpwc_pro_disable_for_logged_in');
		
		if($disable_for_login_user && is_user_logged_in()) {
			return false;
		}
		return true;
	}

	/**
	 * Function to add popup to site
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0 
	 **/
	function mpwc_pro_add_popup() {		
		global $post;
		
		$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix
		$is_enable = mpwc_pro_get_option('mpwc_pro_enable');		
		$mpwc_pro_enable_mobile = mpwc_pro_get_option('mpwc_pro_enable_mobile');		
		
		if(!$is_enable){
			return false;
		}
		if(wp_is_mobile() && !$mpwc_pro_enable_mobile){
			return false;
		}

		$welcome_popup = $exit_popup = ''; 
		if($post)
		{
			$is_welcome_popup = mpwc_pro_popup_enable($post->ID,$popup_type = 'welcome');
			$is_exit_popup = mpwc_pro_popup_enable($post->ID,$popup_type = 'exit');
			$welcome_popup = get_post_meta($post->ID,$prefix.'welcome_popupid',true);
			$exit_popup = get_post_meta($post->ID,$prefix.'exit_popupid',true);
		}

		if($is_welcome_popup){

			if(!empty($welcome_popup)){
				
				$this->mpwc_pro_creat_popup($welcome_popup,'welcome');		
			}
			else
			{
				
				$this->mpwc_pro_creat_popup('default','welcome');
			}
		}

		if($is_exit_popup){
			if(!empty($exit_popup) && $exit_popup != 'disable'){
				$this->mpwc_pro_creat_popup($exit_popup,'exit');		
			}
			else{
				
				$this->mpwc_pro_creat_popup('default','exit');
				
			}			
		}		
	}

	/**
	 * Function for create popup
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0 
	 **/
	function mpwc_pro_creat_popup($ID,$type = '')
	{
		$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix

		$popup_id = $this->mpwc_get_popup_id($ID,$type);
		
		if(!empty($popup_id))
		{
			$post_detail = get_post($popup_id);			
			if($post_detail){

				$is_cookie_set = mpwc_pro_is_cooke_set($post_detail->ID,$type);	

				if(!$is_cookie_set){
					if($post_detail && $post_detail->post_status =='publish' ){
						
						$this->mpwc_build_popup_layout($post_detail->ID,$type);
					}
				}
			}		
		}
	}	

	/**
	 * Function build popup layout
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0 
	 **/
	function mpwc_build_popup_layout($post_id,$type = ''){		
		$unique_id = mpwc_pro_get_unique();
		$popup_meta = mpwc_pro_post_metadata($post_id);
		$popup_meta['popup_ID'] = $post_id;
		$popup_postion = $popup_meta['popuptype'];
		$popup_config_data = mpwc_pro_popup_config($post_id);
		$design = file_exists( MPWC_PRO_DIR . '/templates/'. $popup_meta['designs'].'.php' ) ? $popup_meta['designs'] : 'mpwc-design-1';
		
		if(!empty($popup_meta['action_btn_txt'])){
			$btn_style = '';
			$action_link = '#';
			if(!empty($popup_meta['action_btn_link_fontclr'])){
				$btn_style .= 'color: '.$popup_meta['action_btn_link_fontclr'].';';
			}
			if(!empty($popup_meta['action_btn_link_fontsize'])){
				$btn_style .= 'font-size : '.$popup_meta['action_btn_link_fontsize'].'px;';
			}
			if(!empty($popup_meta['action_btn_link_bgclr'])){
				$btn_style .= 'background-color : '.$popup_meta['action_btn_link_bgclr'].';';
			}

			if(!empty($popup_meta['action_btn_link'])){
				$action_link = $popup_meta['action_btn_link'];
			}
		}
		?>		
		<div id="mpwc-popup-<?php echo $type;?>" class="mpwc-popup-block mpwc-popup-<?php echo $post_id.' '.$popup_postion;?>" data-mpwc-type="<?php echo $type;?>">
			<?php
			echo $this->mpwc_pro_popup_css($post_id,$popup_meta); // Popup custom css	
			include( MPWC_PRO_DIR . "/templates/{$design}.php" );
			?>
			<div class="mpwc-popup-config">
				<?php echo json_encode($popup_config_data);?>
			</div>
		</div>
		<?php
	}

	/**
	 * Function get popupid
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0 
	 **/
	function mpwc_get_popup_id($post_id,$type = '')
	{
		$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix
		
		if($post_id == 'default' && $type == "welcome")
		{
			$display_in = mpwc_pro_get_option('mpwc_pro_display_in');
			
			if(is_front_page())
			{
				if(isset($display_in['home'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_default_popup');
				}
			}
			elseif(is_page())
			{
				if(isset($display_in['pages'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_default_popup');			
				}
			}
			elseif(is_single())
			{
				if(isset($display_in['posts'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_default_popup');	
				}
			}
			elseif(is_404())
			{
				if(isset($display_in['404'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_default_popup');	
				}
			}
			else {
				if(isset($display_in['others'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_default_popup');
				}
			}
		}

		if($post_id == 'default' && $type == "exit")
		{
			$display_in = mpwc_pro_get_option('mpwc_pro_exit_display_in');
			
			if(is_front_page())
			{
				if(isset($display_in['home'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_exit_default_popup');
				}
			}
			elseif(is_page())
			{
				if(isset($display_in['pages'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_exit_default_popup');			
				}
			}
			elseif(is_single())
			{
				if(isset($display_in['posts'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_exit_default_popup');	
				}
			}
			elseif(is_404())
			{
				if(isset($display_in['404'])){
					$post_id = mpwc_pro_get_option('mpwc_pro_exit_default_popup');	
				}
			}
			elseif(isset($display_in['others'])) {
				
					$post_id = mpwc_pro_get_option('mpwc_pro_exit_default_popup');
				
			}
		}
		return $post_id;
	}

	/**
	 * Function for popup css to site
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0 
	 **/
	function mpwc_pro_popup_css($post_id,$popup_meta = array()) {
		
		$opacity = !empty($popup_meta['overlay_background_opacity']) ? $popup_meta['overlay_background_opacity'] :0.5;
		
		$html = '<style type="text/css">';
		#mpwc-popup-wrp
		$html .= '.mpwc-popup-'.$post_id.' #mpwc-popup-wrp-'.$post_id.'{';
		$html 	.= 'bottom: 0;';
		$html 	.= 'display: none;';
		$html 	.= ' left: 0;';
		$html 	.= 'overflow-y: auto;';
		$html 	.= 'position: fixed;';
		$html 	.= 'right: 0;';
		$html 	.= 'top: 0;';
		$html 	.= 'z-index: 9999999;';
		$html .= '}';
		//Overlay CSS
		$html .= '.mpwc-popup-'.$post_id.' .mpwc-popup-wrp{';
		if( !empty($popup_meta['overlay_backgroundclr']) ) {
			$html .= "background-color: rgba( ".mpwc_hex2rgb($popup_meta['overlay_backgroundclr']).",{$opacity});";
		}		
		$html .= '}';	
		
		// popup baclground color css
		$html .= '.mpwc-popup-'.$post_id.' .mpwc-popup-body{';
		if( !empty($popup_meta['backgroundclr']) ) {
			$html .= "background-color: {$popup_meta['backgroundclr']} !important;";
		}
		if( !empty($popup_meta['backgroundimg']) ) {
			$html .= "background: url({$popup_meta['backgroundimg']}) {$popup_meta['backgroundimg_repeat']} top center ; ";
			$html .= "-webkit-background-size: cover;";
			$html .= "-moz-background-size: cover;";
			$html .= "-o-background-size: cover;";
			$html .= "background-size: cover;";
		}

		// popup border width css
		if( ($popup_meta['border_width']) != '' ) {
			$html .= "border-width: {$popup_meta['border_width']}px  !important; border-style: solid;";
		}

		// popup width css
		if( !empty($popup_meta['width']) ) {
			$html .= "max-width: {$popup_meta['width']};";
		}

		// popup font color css
		if( !empty($popup_meta['fontclr']) ) {
			$html .= "color:{$popup_meta['fontclr']};";
		}

		// popup border color css
		if( !empty($popup_meta['border_color']) ) {
			$html .= "border-color: {$popup_meta['border_color']}  !important;";
		}

		

		// popup border redius css
		if( !empty($popup_meta['border_radius']) ) {
			$html .= "border-radius: {$popup_meta['border_radius']}px;";
		}
		$html .= '}';		
		
		//popup height
		$html .= '.mpwc-popup-'.$post_id.' #mpwc-popup-wrp-'.$post_id.' .mpwc-popup-cnt-inr-wrp{';
		if( !empty($popup_meta['height']) ) {
			$html .= "height:{$popup_meta['height']} ;";
		}
		$html .= '}';

		//Main heading css
		$html .= ".mpwc-popup-".$post_id." .mpwc-popup-body .mpwc_main_heading{";		
		if( !empty($popup_meta['main_head_fontclr']) ) {
			$html .= "color:{$popup_meta['main_head_fontclr']};";
		}
		if( !empty($popup_meta['main_head_font_size']) ) {
			$html .= "font-size:{$popup_meta['main_head_font_size']}px;";
		}			
		$html .= '}';

		//Sub heading css
		$html .= ".mpwc-popup-".$post_id." .mpwc-popup-body .mpwc_sub_heading{";		
		if( !empty($popup_meta['sub_head_fontclr']) ) {
			$html .= "color:{$popup_meta['sub_head_fontclr']};";
		}
		if( !empty($popup_meta['sub_head_font_size']) ) {
			$html .= "font-size:{$popup_meta['sub_head_font_size']}px;";
		}			
		$html .= '}';

		//Action button css
		if( !empty($popup_meta['action_btn_link_hover_fontclr']) ) {
		$html .= ".mpwc-popup-".$post_id." .mpwc-popup-body .mpwc-popup-action-btn:hover{";		
		
			$html .= "color:{$popup_meta['action_btn_link_hover_fontclr']} !important;";
		
		$html .= '}';
		}
		if( !empty($popup_meta['action_btn_link_hover_bgclr']) ) {
		$html .= ".mpwc-popup-".$post_id." .mpwc-popup-wrp .mpwc-popup-action-btn:hover {";		
			$html .= " background :{$popup_meta['action_btn_link_hover_bgclr']} !important;";
				
		$html .= '}';
		}

		$html .= ".mpwc-popup-".$post_id." .mpwc-popup-body .mpwc-popup-cnt-inr-wrp{";		
		if( !empty($popup_meta['fontclr']) ) {
			$html .= "color:{$popup_meta['fontclr']};";
		}

		$html .= '}';

		//Add custom css
		if(!empty($popup_meta['custom_css']))
		{
			$html .= $popup_meta['custom_css'];
		}
		$html .='</style>';

		//Add custom js
		if(!empty($popup_meta['custom_js']))
		{
			$html .= '<script>';
			$html .= $popup_meta['custom_js'];
			$html .= '</script>';
		}
		
		return $html;
	}
}

$mpwc_pro_public = new Mpwc_Pro_Public();