<?php
/**
 * Handles Post Setting metabox HTML
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

global $post,$wp_version;

$prefix = MPWC_PRO_META_PREFIX; // Metabox prefix

// Getting saved values
$designs 						= get_post_meta( $post->ID, $prefix.'designs', true );
$popuptype 						= get_post_meta( $post->ID, $prefix.'popuptype', true );
$main_heading 					= get_post_meta( $post->ID, $prefix.'main_heading', true );
$sub_heading 					= get_post_meta( $post->ID, $prefix.'sub_heading', true );
$action_btn_txt					= get_post_meta( $post->ID, $prefix.'action_btn_txt', true );
$action_btn_link				= get_post_meta( $post->ID, $prefix.'action_btn_link', true );
$when_popup_appear 				= get_post_meta( $post->ID, $prefix.'when_popup_appear', true );
$delay 							= get_post_meta( $post->ID, $prefix.'delay', true );
$x_second 						= get_post_meta( $post->ID, $prefix.'x_second', true );
$x_scroll 						= get_post_meta( $post->ID, $prefix.'x_scroll', true );
$disappear 						= get_post_meta( $post->ID, $prefix.'disappear', true );
$exptime 						= get_post_meta( $post->ID, $prefix.'exptime', true );
$hideclsbtn 					= get_post_meta( $post->ID, $prefix.'hideclsbtn', true );
$clsonesc 						= get_post_meta( $post->ID, $prefix.'clsonesc', true );
$height 						= get_post_meta( $post->ID, $prefix.'height', true );
$width 							= get_post_meta( $post->ID, $prefix.'width', true );
$backgroundclr					= get_post_meta( $post->ID, $prefix.'backgroundclr', true );
$backgroundimg 					= get_post_meta( $post->ID, $prefix.'backgroundimg', true );
$backgroundimg_repeat 			= get_post_meta( $post->ID, $prefix.'backgroundimg_repeat', true );
$overlay_backgroundclr 			= get_post_meta( $post->ID, $prefix.'overlay_backgroundclr', true );
$overlay_background_opacity 	= get_post_meta( $post->ID, $prefix.'overlay_background_opacity', true );
$main_head_font_size 			= get_post_meta( $post->ID, $prefix.'main_head_font_size', true );
$main_head_fontclr 				= get_post_meta( $post->ID, $prefix.'main_head_fontclr', true );
$sub_head_font_size 			= get_post_meta( $post->ID, $prefix.'sub_head_font_size', true );
$sub_head_fontclr 				= get_post_meta( $post->ID, $prefix.'sub_head_fontclr', true );
$action_btn_link_fontclr 		= get_post_meta( $post->ID, $prefix.'action_btn_link_fontclr', true );
$action_btn_link_fontsize 		= get_post_meta( $post->ID, $prefix.'action_btn_link_fontsize', true );
$action_btn_link_hover_fontclr 	= get_post_meta( $post->ID, $prefix.'action_btn_link_hover_fontclr', true );
$action_btn_link_bgclr 			= get_post_meta( $post->ID, $prefix.'action_btn_link_bgclr', true );
$action_btn_link_hover_bgclr 	= get_post_meta( $post->ID, $prefix.'action_btn_link_hover_bgclr', true );
$fontclr 						= get_post_meta( $post->ID, $prefix.'fontclr', true );
$border_width 					= get_post_meta( $post->ID, $prefix.'border_width', true );
$border_radius					= get_post_meta( $post->ID, $prefix.'border_radius', true );
$border_color 					= get_post_meta( $post->ID, $prefix.'border_color', true );
$custom_css 					= get_post_meta( $post->ID, $prefix.'custom_css', true );
$custom_js 						= get_post_meta( $post->ID, $prefix.'custom_js', true );

//Get popup  designs
$popup_designs_list 	= mpwc_pro_popup_designs();
$popup_type_list 		= mpwc_pro_popup_type();
$when_popup_appear_list = mpwc_pro_popup_when_appear();
$mpwc_repeat_options 	= mpwc_pro_repeat_options();

if(empty($height))
{
	$height = '100%';
}
if(empty($width))
{
	$width = '600px';
}
?>

<table class="form-table mpwc-post-sett-tbl">
	<tbody>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-popup-designs"><?php _e('Popup Layout', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>designs"  class="mpwc-select-box mpwc-popup-designs" id="mpwc-popup-designs" >
				<?php 
					if(!empty($popup_designs_list))
						foreach ($popup_designs_list as $design_key => $design_val) {
							echo '<option value="'.$design_key.'" '.selected($designs,$design_key).'>'.$design_val.'</option>';
						}
				?>
				</select><br/>			
				<span class="description"><?php _e('Select Popup Layout. You can check all layouts under PopUps Pro-> How it works', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-popup-popuptype"><?php _e('Popup Position', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>popuptype"  class="mpwc-select-box mpwc-popup-popuptype" id="mpwc-popup-popuptype" >
				<?php 
					if(!empty($popup_type_list))
						foreach ($popup_type_list as $popup_type_key => $popup_type_val) {
							echo '<option value="'.$popup_type_key.'" '.selected($popuptype,$popup_type_key).'>'.$popup_type_val.'</option>';
						}
				?>
				</select><br/>			
				<span class="description"><?php _e('Select Popup Position', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr>
			<th scope="row">
				<label for="mpwc-popup-height"><?php _e('Popup Height and Width', 'modal-popup-with-cookie'); ?>:</label>
			</th>
			<td>
				<input type="text" name="<?php echo $prefix; ?>height" value="<?php echo $height; ?>" class="mpwc-popup-height" id="mpwc-popup-height" size="6" /> <label for="mpwc-popup-height"><?php _e('Height', 'modal-popup-with-cookie'); ?></label> &nbsp;&nbsp;
				<input type="text" name="<?php echo $prefix; ?>width" value="<?php echo $width; ?>" class="mpwc-popup-width" id="mpwc-popup-width" size="6" /> <label for="mpwc-popup-width"><?php _e('Width', 'modal-popup-with-cookie'); ?></label> <br/>
				<span class="description"><?php _e('Enter custom width and width for popup. Leave empty to use default. (i.e 600px OR 60%)', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-main-heading"><?php _e('Main Heading', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>main_heading" value="<?php echo $main_heading;?>" class="mpwc-main-heading regular-text" id="mpwc-main-heading" type="text"><br/>
				<span class="description"><?php _e('Enter Main Heading', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-sub-heading"><?php _e('Sub Heading', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>sub_heading" value="<?php echo $sub_heading;?>" class="mpwc-sub-heading regular-text" id="mpwc-sub-heading" type="text"><br/>
				<span class="description"><?php _e('Enter Sub Heading', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_txt"><?php _e('Action Button Text', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>action_btn_txt" value="<?php echo $action_btn_txt;?>" class="mpwc-action_btn_txt regular-text" id="mpwc-action_btn_txt" type="text"><br/>
				<span class="description"><?php _e('Enter action button text.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_link"><?php _e('Action Button Link', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>action_btn_link" value="<?php echo $action_btn_link;?>" class="mpwc-action_btn_link regular-text" id="mpwc-action_btn_link" type="text"><br/>
				<span class="description"><?php _e('Enter action button link.(e.g http://www.wponlinesupport.com)', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Appearance, Delay, Cookie Expiry Time etc setting', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-when_popup_appear"><?php _e('When Popup appear?', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>when_popup_appear" id="mpwc-when_popup_appear" class="mpwc-when_popup_appear mpwc-select-box">
				<?php 
					if(!empty($when_popup_appear_list))
						foreach ($when_popup_appear_list as $appear_key => $appear_val) {
							echo '<option value="'.$appear_key.'" '.selected($when_popup_appear,$appear_key).'>'.$appear_val.'</option>';
						}
				?>
				</select><br/>
				<span class="description"><?php _e('Select when popup appear.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top" id="mpwc-delay" class="mpwc-appear page_load">
			<th scope="row">
				<label for="mpwc-delay"><?php _e('Popup Delay', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>delay" value="<?php echo $delay;?>" class="mpwc-delay small-text" id="mpwc-delay" type="number" min="0"> <?php _e('Sec','modal-popup-with-cookie');?><br/>
				<span class="description"><?php _e('Enter no of second to open popup after page load.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top" id="mpwc-x_second" class="mpwc-appear inactivity">
			<th scope="row">
				<label for="mpwc-open-on-x_second"><?php _e('Open after X seconds of inactivity', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>x_second" value="<?php echo $x_second;?>" class="mpwc-open-on-x_second small-text" id="mpwc-open-on-x_second" type="number" min="0"> <?php _e('Sec','modal-popup-with-cookie');?><br/>
				<span class="description"><?php _e('Enter no of second to open popup after x second of inactivity.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top" id="mpwc-x_scroll" class="mpwc-appear scroll">
			<th scope="row">
				<label for="mpwc-x_scroll"><?php _e('Open when user scroll % of page', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>			
				<?php
				echo '<div style="display: inline-block; width: 40%; margin-right: 25px;" id="slider1-div"></div>';
				?>
				<input name="<?php echo $prefix; ?>x_scroll" value="<?php echo $x_scroll;?>" class="mpwc-x_scroll" id="slider1" type="text" readonly="true" style="width:40px; text-align:center;"> %<br/>
				<span class="description"><?php _e('100% - end of page', 'modal-popup-with-cookie'); ?></span>

			</td>
		</tr>

		<tr valign="top">
			<th scope="row">
				<label for="mpwc-disappear"><?php _e('Popup Disappear', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>disappear" value="<?php echo $disappear;?>" class="mpwc-disappear small-text" id="mpwc-disappear" type="number" min="0"> <?php _e('Sec','modal-popup-with-cookie');?><br/>
				<span class="description"><?php _e('Enter no of second to hide popup.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-exptime"><?php _e('Cookie Expiry Time', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>exptime" value="<?php echo $exptime;?>" class="mpwc-exptime small-text" id="mpwc-exptime" type="number"> <?php _e('Days.','modal-popup-with-cookie'); ?><br/>
				<span class="description"><?php _e('Enter expiry time when user click on close button. Upon exiry user will see popup again.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-hideclsbtn"><?php _e('Hide Close Button', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input type="checkbox" name="<?php echo $prefix; ?>hideclsbtn" value="1" class="mpwc-hideclsbtn" id="mpwc-hideclsbtn" <?php checked( $hideclsbtn, 1 ); ?> /><br/>

				<span class="description"><?php _e('Check this box if you want to hide the close button of popup.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-clsonesc"><?php _e('Close Popup On Esc', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input type="checkbox" name="<?php echo $prefix; ?>clsonesc" value="1" class="mpwc-clsonesc" id="mpwc-clsonesc" <?php checked( $clsonesc, 1 ); ?> /><br/>

				<span class="description"><?php _e('Check this box if you want to close the popup on esc key.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Font Size', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-main_head_font_size"><?php _e('Main Heading Font size', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>				
				<input name="<?php echo $prefix; ?>main_head_font_size" value="<?php echo $main_head_font_size;?>" class="mpwc-main-heading small-text" id="mpwc-main_head_font_size" type="number" min="10"> PX<br/>
				<span class="description"><?php _e('Select main heading font size.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>		
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-sub_head_font_size"><?php _e('Sub Heading Font size', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>sub_head_font_size" value="<?php echo $sub_head_font_size;?>" class="mpwc-main-heading small-text" id="mpwc-sub_head_font_size" type="number" min="10"> PX<br/>
				<span class="description"><?php _e('Select sub heading font size.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_link_fontsize"><?php _e('Action Link Font size', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>				
				<input name="<?php echo $prefix; ?>action_btn_link_fontsize" value="<?php echo $action_btn_link_fontsize;?>" class="mpwc-main-heading small-text" id="mpwc-action_btn_link_fontsize" type="number" min="10"> PX<br/>
				<span class="description"><?php _e('Select action link font size.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Fonts Colors', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-main_head_fontclr"><?php _e('Main Heading Font Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-main_head_fontclr" value="<?php echo $main_head_fontclr; ?>" id="mpwc-main_head_fontclr" name="<?php echo $prefix; ?>main_head_fontclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-main_head_fontclr" value="<?php echo $main_head_fontclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>main_head_fontclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select main heading font color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-sub_head_fontclr"><?php _e('Sub Heading Font Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-sub_head_fontclr" value="<?php echo $sub_head_fontclr; ?>" id="mpwc-sub_head_fontclr" name="<?php echo $prefix; ?>sub_head_fontclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-sub_head_fontclr" value="<?php echo $sub_head_fontclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>sub_head_fontclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select sub heading font color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-fontclr"><?php _e('Popup Content Fonts Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-fontclr" value="<?php echo $fontclr; ?>" id="mpwc-fontclr" name="<?php echo $prefix; ?>fontclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-fontclr" value="<?php echo $fontclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>fontclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select Popup font color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Action Link Colors', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_link_fontclr"><?php _e('Action Link Font Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-action_btn_link_fontclr" value="<?php echo $action_btn_link_fontclr; ?>" id="mpwc-action_btn_link_fontclr" name="<?php echo $prefix; ?>action_btn_link_fontclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-action_btn_link_fontclr" value="<?php echo $action_btn_link_fontclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>action_btn_link_fontclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select action link font color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_link_hover_fontclr"><?php _e('Action Link Hover Font Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-action_btn_link_hover_fontclr" value="<?php echo $action_btn_link_hover_fontclr; ?>" id="mpwc-action_btn_link_hover_fontclr" name="<?php echo $prefix; ?>action_btn_link_hover_fontclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-action_btn_link_hover_fontclr" value="<?php echo $action_btn_link_hover_fontclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>action_btn_link_hover_fontclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select action link hover font color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_link_bgclr"><?php _e('Action Link Background Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-action_btn_link_bgclr" value="<?php echo $action_btn_link_bgclr; ?>" id="mpwc-action_btn_link_bgclr" name="<?php echo $prefix; ?>action_btn_link_bgclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-action_btn_link_bgclr" value="<?php echo $action_btn_link_bgclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>action_btn_link_bgclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select action link background color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-action_btn_link_hover_bgclr"><?php _e('Action Link Hover Background Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-action_btn_link_hover_bgclr" value="<?php echo $action_btn_link_hover_bgclr; ?>" id="mpwc-action_btn_link_hover_bgclr" name="<?php echo $prefix; ?>action_btn_link_hover_bgclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-action_btn_link_hover_bgclr" value="<?php echo $action_btn_link_hover_bgclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>action_btn_link_hover_bgclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select action link hover background color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Background and Overlay Settings', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-backgroundclr"><?php _e('Popup Background Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-backgroundclr" value="<?php echo $backgroundclr; ?>" id="mpwc-backgroundclr" name="<?php echo $prefix; ?>backgroundclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-backgroundclr" value="<?php echo $backgroundclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>backgroundclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select Popup background color', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-backgroundimg"><?php _e('Popup Background Image', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input type="text" name="<?php echo $prefix; ?>backgroundimg" value="<?php echo $backgroundimg; ?>" id="mpwc-background-img" class="regular-text mpwc-default-img mpwc-img-upload-input" />
				<input type="button" name="mpwc_pro_default_img" class="button-secondary mpwc-image-upload" value="<?php _e( 'Upload Image', 'modal-popup-with-cookie'); ?>" data-uploader-title="<?php _e('Choose Image', 'modal-popup-with-cookie'); ?>" data-uploader-button-text="<?php _e('Insert Image', 'modal-popup-with-cookie'); ?>" /> 
				<input type="button" name="mpwc_pro_default_img_clear" id="mpwc-background-img-clear" class="button button-secondary mpwc-image-clear" value="<?php _e( 'Clear', 'modal-popup-with-cookie'); ?>" /> <br />
											
				<?php
					if( $backgroundimg != '' ) { 
						$backgroundimg = '<img src="'.$backgroundimg.'" alt="" />';
					}
				?>
				<div class="mpwc-img-view"><?php echo $backgroundimg; ?></div>
				<span class="description"><?php _e('Select Popup background Image', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-backgroundimg_repeat"><?php _e('Popup Background Image Repeat', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<select name="<?php echo $prefix; ?>backgroundimg_repeat" id="mpwc-backgroundimg_repeat" class="mpwc-backgroundimg_repeat mpwc-select-box">
				<?php 
					if(!empty($mpwc_repeat_options))
						foreach ($mpwc_repeat_options as $repeat_key => $repeat_val) {
							echo '<option value="'.$repeat_key.'" '.selected($backgroundimg_repeat,$repeat_key).'>'.$repeat_val.'</option>';
						}
				?>
				</select><br/>
				<span class="description"><?php _e('Select Popup background repeat', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-overlay_backgroundclr"><?php _e('Popup Overlay Background Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-overlay_backgroundclr" value="<?php echo $overlay_backgroundclr; ?>" id="mpwc-overlay_backgroundclr" name="<?php echo $prefix; ?>overlay_backgroundclr" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-overlay_backgroundclr" value="<?php echo $overlay_backgroundclr; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>overlay_backgroundclr" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Select Popup overlay background color.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-overlay-background-opacity"><?php _e('Overlay Background Opacity', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>overlay_background_opacity" value="<?php echo $overlay_background_opacity;?>" min="0" max="1" step="0.01" class="mpwc-overlay-background-opacity small-text" id="mpwc-overlay-background-opacity" type="number"><br/>
				<span class="description"><?php _e('Background opacity (0 to 1).', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>	
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Border Setting', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-border-width"><?php _e('Popup Border Width', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>border_width" value="<?php echo $border_width;?>" class="mpwc-border-width small-text" id="mpwc-border-width" type="number"> <?php _e('Px.','modal-popup-with-cookie');?><br/>
				<span class="description"><?php _e('Enter width of popup border.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-border-radius"><?php _e('Popup Border Radius', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<input name="<?php echo $prefix; ?>border_radius" value="<?php echo $border_radius;?>" class="mpwc-border-radius small-text" id="mpwc-border-radius" type="number"> <?php _e('Px.','modal-popup-with-cookie');?><br/>
				<span class="description"><?php _e('Enter radius of popup border.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-border_color"><?php _e('Popup Border Color', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<?php 
				if( $wp_version >= 3.5 ) { ?>
				<input type="text" id="mpwc-border_color" value="<?php echo $border_color;?>" id="mpwc-border_color" name="<?php echo $prefix;?>border_color" class="mpwc-color-box" /><br/>
				<?php } else { ?>
				<div style='position:relative;'>
					<input type='text' id="mpwc-border_color" value="<?php echo $border_color; ?>" id="mpwc-color-box-farbtastic-inp" name="<?php echo $prefix;?>border_color" class="mpwc-color-box-farbtastic-inp" data-default-color="" />
					<input type="button" class="mpwc-color-box-farbtastic button button-secondary" value="<?php _e('Select Color', 'modal-popup-with-cookie'); ?>" />
					<div class="colorpicker" style="background-color: #666; z-index:100; position:absolute; display:none;"></div>
				</div>
				<?php } ?>
				<span class="description"><?php _e('Popup Border Color.', 'modal-popup-with-cookie'); ?></span>

				
			</td>
		</tr>
		<tr valign="top">
			<th colspan="2">
				<div class="mpwc-sett-title"><?php _e('Popup Custom CSS and JS', 'modal-popup-with-cookie'); ?></div>
			</th>			
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-custom_css"><?php _e('Custom css', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<textarea id="mpwc-custom_css" name="<?php echo $prefix; ?>custom_css" class="mpwc-custom_css regular-text"><?php echo $custom_css;?></textarea><br>
				<span class="description"><?php _e('Enter custom css.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">
				<label for="mpwc-custom_js"><?php _e('Custom js', 'modal-popup-with-cookie'); ?></label>
			</th>
			<td>
				<textarea id="mpwc-custom_js" name="<?php echo $prefix; ?>custom_js" class="mpwc-custom_js regular-text"><?php echo $custom_js;?></textarea><br>
				<span class="description"><?php _e('Enter custom js.', 'modal-popup-with-cookie'); ?></span>
			</td>
		</tr>
	</tbody>
</table><!-- end .mpwc-post-sett-table-->
<script>
		jQuery(document).ready(function(){
			jQuery( "#slider1-div" ).slider({
			value: jQuery( "#slider1" ).val(),
			min: 1,
			max:100,
			step: 1,
		    slide: function( event, ui ) {
		      jQuery( "#slider1" ).val( ui.value );
		    }
			});
			jQuery( "#slider1" ).keyup( function(){
			     jQuery( "#slider1-div" ).slider( "value", jQuery(this).val() );
			});
		});
		</script>