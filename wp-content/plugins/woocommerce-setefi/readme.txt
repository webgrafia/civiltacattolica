=== Woocommerce Monetaweb Setefi ===
Contributors: fburatti
Tags:
Requires at least: 3.8
Tested up to: 4.9.8
Stable tag: 2.9.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Descrizione ==

Extends WooCommerce with Setefi Monetaweb gateway.

Per maggiori informazioni contattatemi all'indirizzo: **info@filippoburatti.net**


== Installazione ==

Estrarre la cartella woocommerce-setefi nella cartella wp-content/plugins

Attivare il plugin nella sezione plugin di WordPress

Configurare le impostazioni tramite il pannello accessibile da WooCommerce -> Settings -> Checkout cliccando su Setefi Monetaweb


== Changelog ==

= 2.9.1 =
* Aggiornamento settings Api Woocommerce
* Aggiornamento stringhe di traduzione
* Salvataggio paymentID in fase di inizializzazione pagamento
* Correzione regex della descrizione pagamento (per Monetaonline)

= 2.9 =
* Nuovo filtro "wc_setefi_supported_currencies"

= 2.8.9 =
* Conformità GDPR
* aggiornamento compatibilità Woocommerce 3.x
* rimozione opzione obsoleta curl TLSV1

= 2.8.8 =
* gestione errore mancata autorizzazione 3D secure
* controllo security token
* Aggiornati documentazione ed esempi (currency switcher)

= 2.8.7 =
* aggiornamento compatibilità Woocommerce 3.x

= 2.8.6 =
* fix formattazione file wpml-config.xml (Polylang Wpml) 

= 2.8.5 =
* Modifica marginale per futura estensibilità

= 2.8 =
* Abilitazione di tutte le valute consentite da Monetaweb: EUR, USD, CHF, GBP
* Aggiornati documentazione ed esempi (currency switcher)

= 2.7.1 =
* Retro compatibilità Woocommerce < 2.5

= 2.7 =
* Nuova funzione per il salvataggio dei logs, rimozione errore in caso di pagamenti non autorizzati nel pannello merchant
* Nuovo filtro "wc_setefi_payment_args", in sostituzione di “wc_setefi_hosted_page_language”
* Nuovi filtri “wc_setefi_payment_args” e “wc_setefi_checkout_icon”
* Nuova azione “wc_setefi_payment_response”
* Aggiornati documentazione ed esempi


= 2.6 =
* Correzione bug con WPML  

= 2.5 =
* Implementazione della funzione nativa api_request_url() per il response url  
* Rimosso utilizzo globals

= 2.4 =
* Implementazione storni / rimborsi automatici
* Supporto Transaction ID
* Documentazione migliorata

= 2.3 =
* Migliorata integrazione plugin Qtranslate-x e WPML
* Documentazione per siti multilingua

= 2.2 =
* Opzione per il fix curl direttamente nell'amministrazione del plugin
* Migliore validazione del campo description nel pagamento inviato al gateway
* Correzione e integrazione stringhe dei file di lingua
* Correzione path del file logs

= 2.1 =
* Fix curl per alcuni webserver (talvolta necessario per l’ambiente di test)

= 2.0 =
* Implementazione api 2.0 di Monetaweb
* Correzioni variabili e text-domain per consistenza con nome plugin
* Introduzione filter “wc_setefi_hosted_page_language”

= 1.1 =
* Aggiunta compatibilità con Woocommerce 2.1

= 1.0 =
* Prima release