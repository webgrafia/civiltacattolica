<?php
/*
The Page Loop
=============
*/
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <article>
                <div class="article_wrapper" >
                    <div class="section_title margin-top-20">
                        <h1 class="title"><?php the_title(); ?></h1>
                    </div>

                    <div class="article_content">
                        <?php the_content(); ?>
                        <?php wp_link_pages(); ?>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>

<?php endwhile; else: ?>
<?php wp_redirect(get_bloginfo('siteurl').'/404', 404); ?>
<?php exit; ?>
<?php endif; ?>
