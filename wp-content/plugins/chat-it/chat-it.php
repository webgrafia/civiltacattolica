<?php
/**
 * Plugin Name: Chat it!
 * Plugin URI:  https://www.teamdev.com/
 * Description: WordPress share buttons for Viber, WhatsApp, Telegram, Skype, Messenger.
 * Version:     1.1
 * Author:      TeamDev Ltd
 * Author URI:  https://www.teamdev.com/
 * Text Domain: phone-share
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class Phone_Share {

	const BASE_NAME = 'phone_share';

	/**
	 * Plugin settings
	 */
	private static $options = false;

	private static $instance = null;

	private $option_names = array( 'general', 'telegram', 'viber', 'whatsapp', 'facebook', 'skype' );

	/**
	 * Creates or returns an instance of this class.
	 */
	public static function instance() {
		// If an instance hasn't been created and set to $instance create an instance and set it to $instance.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Hook into the appropriate actions when the class is constructed
	 */
	public function __construct() {

		self::$options = $this->fetch_options();

		if ( is_admin() ) {
			require_once $this->get_plugin_dir_path() . 'admin/class-phone-share-admin.php';
		} else {
			require_once $this->get_plugin_dir_path() . 'includes/class-phone-share-shortcode.php';
			add_action( 'wp_enqueue_scripts', array( $this, 'register_styles' ) );
			add_filter( 'the_content', array( $this, 'insert_phone_share_links' ) );
		}
	}

	public function get_plugin_dir_path() {
		return plugin_dir_path( __FILE__ );
	}

	public function get_plugin_basename() {
		return plugin_basename( __FILE__ );
	}

	public function get_plugin_url() {
		return plugin_dir_url( __FILE__ );
	}

	public function get_protocol() {
		if ( is_ssl() ) {
			return "https";
		} else {
			return "http";
		}
	}

	public function register_styles() {
		wp_enqueue_style( 'phone-share-style', $this->get_plugin_url() . '/css/style.css' );
	}

	public function get_options( $tab, $option = '' ) {
		$option_name = $this->get_option_name( $tab );
		if ( $option && isset( self::$options[ $option_name ][ $option ] ) ) {
			return self::$options[ $option_name ][ $option ];
		} else if ( $option && false !== self::$options[ $option_name ] ) {
			return array();
		} else if ( $option ) {
			return $this->get_default_options( $tab, $option );
		}

		return self::$options[ $option_name ];
	}

	public function get_option_name( $name ) {
		if ( in_array( $name, $this->option_names ) ) {
			return self::BASE_NAME . '_' . $name;
		}

		return '';
	}

	public function fetch_options() {
		$options = array();
		foreach ( $this->option_names as $name ) {
			$option_name             = $this->get_option_name( $name );
			$options[ $option_name ] = get_option( $option_name );
		}

		return $options;
	}

	public function get_default_options( $tab = '', $field = '' ) {
		$options = array(
			'general'  => array(
				'messengers' => array( 'telegram', 'viber', 'whatsapp', 'facebook', 'skype' ),
				'post_types' => array( 'post', 'page', 'product', 'articolo'), //*** TODO - all post types
				'position'   => array( 'before_content', 'after_content' )
			),
			'telegram' => array(
				'title'            => __( 'Telegram', 'phone-share' ),
				'text_color'       => '#FFFFFF',
				'background_color' => '#34ABE0',
				'icon_style'       => 'round',
				'show_on'          => array( 'ios', 'android', 'other_mobile' ),
			),
			'viber'    => array(
				'title'            => __( 'Viber', 'phone-share' ),
				'text_color'       => '#FFFFFF',
				'background_color' => '#7C529E',
				'icon_style'       => 'round',
				'show_on'          => array( 'ios', 'android', 'other_mobile' ),
			),
			'whatsapp' => array(
				'title'            => __( 'WhatsApp', 'phone-share' ),
				'text_color'       => '#FFFFFF',
				'background_color' => '#60B82D',
				'icon_style'       => 'round',
				'show_on'          => array( 'ios', 'android', 'other_mobile' ),
			),
			'facebook' => array(
				'title'            => __( 'Messenger', 'phone-share' ),
				'text_color'       => '#FFFFFF',
				'background_color' => '#3B5998',
				'icon_style'       => 'round',
				'show_on'          => array( 'ios', 'android', 'other_mobile' ),
			),
			'skype'    => array(
				'title'            => __( 'Skype', 'phone-share' ),
				'text_color'       => '#FFFFFF',
				'background_color' => '#00AFF0',
				'icon_style'       => 'round',
				'show_on'          => array( 'ios', 'android', 'other_mobile' ),
			),
		);
		if ( isset( $options[ $tab ][ $field ] ) ) {
			return $options[ $tab ][ $field ];
		}

		return $options;
	}

	public function get_messengers() {
		return array(
			'telegram' => array(
				'title' => $this->get_options( 'telegram', 'title' ),
				'href'  => add_query_arg( array(
					'url'  => rawurlencode( get_permalink() ),
					'text' => rawurlencode( get_the_title() )
				), "tg://msg_url" ),
			),
			'facebook' => array(
				'title' => $this->get_options( 'facebook', 'title' ),
				'href'  => add_query_arg( array(
					'link' => rawurlencode( get_permalink() )
				), "fb-messenger://share" ),

			),
			'whatsapp' => array(
				'title' => $this->get_options( 'whatsapp', 'title' ),
				'href'  => add_query_arg( array(
					'text' => rawurlencode( get_the_title() . ' ' . get_permalink() )
				), "whatsapp://send" ),
			),
			'viber'    => array(
				'title' => $this->get_options( 'viber', 'title' ),
				'href'  => add_query_arg( array(
					'text' => rawurlencode( get_the_title() . ' ' . get_permalink() )
				), "viber://forward" ),
			),
			'skype'    => array(
				'href'  => add_query_arg( array(
					'url'  => rawurlencode( get_permalink() ),
					'text' => rawurlencode( get_the_title() )
				), "https://web.skype.com/share" ),
				'title' => $this->get_options( 'skype', 'title' ),
			)
		);
	}

	public function get_links() {
		ob_start();
		$this->load_template( 'share-buttons' );
		return ob_get_clean();
	}

	public function load_template( $template ) {
		$template_dir = $this->get_plugin_dir_path(). '/templates/';
		$template_path = $template_dir. $template. '.php';
		if ( file_exists( $template_path ) ) {
			include $template_path;
		}
	}

	public function is_allowed() {
		$is_allow = false;
		if ( in_array( get_post_type(), $this->get_options( 'general', 'post_types' ) ) ) {
			$is_allow = true;
		}

		return $is_allow;
	}

	public function get_current_platform() {
		$is_ipod    = stripos( $_SERVER['HTTP_USER_AGENT'], "iPod" );
		$is_iphone  = stripos( $_SERVER['HTTP_USER_AGENT'], "iPhone" );
		$is_ipad    = stripos( $_SERVER['HTTP_USER_AGENT'], "iPad" );
		$is_android = stripos( $_SERVER['HTTP_USER_AGENT'], "Android" );

		$platform = '';
		if ( ! wp_is_mobile() ) {
			$platform = 'desktop';
		} else if ( $is_ipad || $is_iphone || $is_ipod ) {
			$platform = 'ios';
		} else if ( $is_android ) {
			$platform = 'android';
		} else if ( wp_is_mobile() ) {
			$platform = 'other_mobile';
		}

		return $platform;
	}

	public function insert_phone_share_links( $content ) {
		if ( ! $this->is_allowed() ) {
			return $content;
		}
		$before_content = '';
		$after_content  = '';
		$position       = $this->get_options( 'general', 'position' );
		if ( in_array( 'before_content', $position ) ) {
			$before_content = $this->get_links();
		}
		if ( in_array( 'after_content', $position ) ) {
			$after_content = $this->get_links();
		}

		return $before_content . $content . $after_content;
	}

}

function PHONE_SHARE() {
	return Phone_Share::instance();
}

PHONE_SHARE();