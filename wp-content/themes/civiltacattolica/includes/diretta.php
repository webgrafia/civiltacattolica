<?php
$diretta_in_corso = get_field("diretta_in_corso", "option");
if($diretta_in_corso) {
    $immagine_diretta = get_field("immagine_diretta", "option");
   // dd($immagine_diretta);

    $titolo_diretta = get_field("titolo_diretta", "option");
    $tipologia_diretta = get_field("tipologia_diretta", "option");
    $link_livestream = get_field("link_livestream", "option");

    $thumb = wp_get_attachment_image_src($immagine_diretta["ID"], "slider");
    $immagine = $thumb[0];
    if(!$immagine)
        $immagine = get_bloginfo("template_url")."/placeholders/placeholder_2000x900.jpg";

    ?>

    <div class="lead_wrapper lead_video" style="background-image: url('<?php echo $immagine; ?>');">
        <div class="lead_container">
            <div class="lead_text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>
                                <span class="category"><?php echo $tipologia_diretta; ?></span>
                                <a href="<?php echo $link_livestream; ?>" target="_blank"><?php echo $titolo_diretta; ?></a>
                                <span class="live_btn"><a href="<?php echo $link_livestream; ?>" target="_blank"><i class="icon icon-icon-arrow-right-large"></i> In
                                        diretta ora su</a></span>
                            </h2>
                        </div>
                        <!-- /col-md-12 -->
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /lead_text -->
        </div>
        <!-- /lead_container -->
    </div><!-- /lead_wrapper -->
<?php

}