<?php get_template_part('includes/header'); ?>

<?php
$term =	$wp_query->queried_object;

$objimg = get_field("immagine", $term->taxonomy."_".$term->term_id);
if($objimg)
    $img = $objimg["sizes"]["slider"];
else
    $img = get_bloginfo("template_url")."/placeholders/placeholder_2000x900_02.jpg";



// recupero tutti gli articoli di questo numero
$arg = array(
    'posts_per_page' => -1,
    'post_type' => 'articolo',
    'tax_query' => array(
        array(
            'taxonomy' => 'quaderno',
            'field' => 'term_id',
            'terms' => $term->term_id,
        )
    )
);
$articoli = get_posts($arg);
?>
<div class="copybook_cover_wrapper" style="background-image: url('<?php echo $img; ?>');">
    <div class="copybook_cover_container">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="copybook_cover_bg match-height">
                        <div class="copybook_cover_content">
                            <p class="date"><b><?php echo get_field("data_pubblicazione", "quaderno_" . $term->term_id); ?></b></p>
                            <h1>Quaderno <strong><?php echo $term->name; ?></strong></h1>
                            <ul>
                                <?php
                                for($i=0;$i<count($articoli);$i++){
                                    if($articoli[$i]->ID){
//                                        $sottotitolo = get_field("sottotitolo", $articoli[$i]->ID);
//                                        if($sottotitolo) $sottotitolo = "<br>".$sottotitolo;
					$sottotitolo = "";
                                        echo '<li><a href="'.get_permalink($articoli[$i]->ID).'">'.mb_strtoupper($articoli[$i]->post_title).$sottotitolo.'</a></li>';

                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div><!-- /col-md-5 -->
                <div class="col-md-4 col-md-offset-3">
                    <div class="copybook_cover_button match-height">
                       <?php if($buylink = cc_link_buy_quaderno($term)) echo '<a class="btn btn-primary btn-lg btn-anchor btn-block" href="'.$buylink.'">Acquista il Quaderno</a>'; ?>
                    </div>
                </div><!-- /col-md-4 -->
            </div><!-- /row -->
        </div><!-- /container -->
    </div><!-- /copybook_cover_container -->
</div><!-- /copybook_cover_wrapper -->
<div class="section section_grey_light section_articles">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <main role="main">
                    <div class="copybooks_container copybooks_container_large copybooks_container_large_full">
                        <?php
                        $c=0;
                        $arrayusers = array();
                        $arrayargs = array();
                        foreach($articoli as $articolo){
                            if($c) echo "<hr>";
                            print_articolo_lista($articolo);

                            // salvo in un array gli autori
                            if(!in_array($articolo->post_author, $arrayusers))
                                array_push($arrayusers,$articolo->post_author);
                            // e gli argomenti
                            $argomenti = get_the_terms($articolo, "argomento");
                            if($argomenti){
                            foreach ($argomenti as $argomento) {
                                if(!in_array($argomento->term_id, $arrayargs))
                                    array_push($arrayargs,$argomento->term_id);
                            }
                            }

                            $c++;
                        }
                        ?>
                        <div class="all">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <?php if($buylink) { ?>
                                        <a class="btn btn-primary btn-lg btn-anchor btn-block" href="<?php echo $buylink; ?>">Acquista il Quaderno</a>
                                    <?php  } ?>
                                </div><!-- /col-md-8 -->
                            </div><!-- /row -->
                        </div>
                    </div><!-- /copybooks_container -->
                </main>
            </div><!-- /col-md-8 -->
            <div class="col-md-4">
                <aside class="sidebar">



                    <?php
                    // news
                    $home_enable_news = get_field("home_enable_news", "option");
                    if($home_enable_news) {
                        $numero_news = get_field("numero_news", "option");
                        if (!$numero_news)
                            $numero_news = 2;
                        $arg = array(
                            'posts_per_page' => $numero_news,
                            'post_type' => 'news',
                        );
                        $news = get_posts($arg);
                        ?>
                        <div
                            class="copybooks_container copybooks_container_small copybooks_container_number_right margin-bottom-30 block-news">

                            <div class="copybook_number copybook_number_right copybook_number_news">
                                <a href="<?php echo get_post_type_archive_link("news"); ?>">
                                    <p class="number"><em>News</em></p>
                                </a>
                            </div> <?php
                            $v = 0;
                            foreach ($news as $n) {
                                print_minibox_lista($n, $v, false);
                                $v++;
                            }
                            ?>
                            <div class="all">
                                <a href="<?php echo get_post_type_archive_link("news"); ?>">Vedi tutte le news</a>
                            </div>
                        </div>
                        <!-- /copybooks_container -->

                        <?php
                    }
                    ?>


                    <h3 class="sidebar_title">Autori di questo quaderno</h3>
                    <div class="author_list">
                        <?php
                        foreach ($arrayusers as $userid) {
                            $userimg = get_field("foto", "user_".$userid);
                            if($userimg)
                               $img = $userimg["sizes"]["userthumb"];
                            else
                                $img = get_bloginfo("template_url")."/img/logo.svg";
                        ?>
                            <div class="author_container">
                                <div class="author_thumb">
                                    <img src="<?php echo $img; ?>" alt="<?php echo esc_attr(get_userdata($userid)->display_name); ?>">
                                </div><!-- /author_thumb -->
                                <p>
                                <a href="<?php echo get_author_posts_url($userid); ?>"><?php echo get_userdata($userid)->display_name; ?></a>
                                </p>
                            </div><!-- /author_container -->
                        <?php
                        }
                        ?>
                    </div><!-- /author_list -->



                    <?php
                    $args = array(
	                    'post_type' => 'recensione',
	                    'posts_per_page' => -1,
	                    'tax_query' => array(
		                    array(
			                    'taxonomy' => 'quaderno',
			                    'field' => 'term_id',
			                    'terms' => $term->term_id,
		                    )
	                    )
                    );

                    $dalquaderno = get_posts($args);
                    if(count($dalquaderno)) {
	                    ?>
                        <h3 class="sidebar_title recensioni_title">Rassegna Bibliografica</h3>
                        <div class="author_list">
		                    <?php
		                    foreach ($dalquaderno as $dalquad) {
		                    $postimg = get_field("immagine_libro", $dalquad->ID);
		                   // print_r($postimg);
		                    if($postimg) {
			                    $img = $postimg["sizes"]["medium"];
		                    }else {
			                    $img = get_bloginfo( "template_url" ) . "/img/logo.svg";
		                    }
			                    ?>
                                <div class="author_container recensione_list_container">
                                    <div class="recensione_thumb">
                                        <img src="<?php echo $img; ?>" alt="<?php echo esc_attr($dalquad->post_title); ?>">
                                    </div><!-- /author_thumb -->
                                    <p>
                                        <a href="<?php echo get_permalink( $dalquad ); ?>"><?php echo strtoupper($dalquad->post_title); ?></a>
                                    </p>
                                </div><!-- /author_container -->
			                    <?php
		                    }
		                    ?>
                        </div><!-- /author_list -->
	                    <?php
                    }
                    ?>

                    <div class="row margin-bottom-20 padding-left-15 padding-right-15" style="text-align:center;">
                        <a href="https://www.spreaker.com/user/civcatt" target="_BLANK"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/<?php echo rand(1, 3); ?>_1.png" style="max-width: 100%;"></a>
                    </div>



            <h3 class="sidebar_title">Argomenti</h3>

                    <div class="tags_list">
                        <?php
                        foreach ($arrayargs as $termid) {
                            $argom=get_term($termid);
                            echo '<a href="'.get_term_link($argom).'">'.$argom->name.'</a>';
                        }

                        ?>
                    </div>







                    <h3 class="sidebar_title">ultimi Quaderni</h3>
                    <?php
                    $quaderni = get_terms('quaderno', array('hide_empty' => false, 'orderby' => "name", "order" => "desc","meta_query" => array(
                        'relation' => 'OR',
                        array(
                            'key'       => 'pubblicazione_posticipata',
                            'compare'   => 'NOT EXISTS'
                        ),
                        array(
                            'key'       => 'pubblicazione_posticipata',
                            'value'       => '1',
                            'compare'   => '!='
                        )
                    ), "number" => 5));
                    $qshow=array();
                    foreach ($quaderni as $q) {
                        if($q->term_id != $term->term_id){
                            $qshow[]=$q;
                        }
                    }

                    ?>
                    <div class="copybooks_numbers">
                        <div class="row margin-bottom-30">
                            <div class="col-md-6 col-xs-6">
                                <?php echo copybook_number($qshow[0], "none", true); ?>
                            </div><!-- /col-md-6 -->

                            <div class="col-md-6 col-xs-6">
                                <?php echo copybook_number($qshow[1], "none", true); ?>
                            </div><!-- /col-md-6 -->

                        </div><!-- /row -->

                        <div class="row margin-bottom-30">
                            <div class="col-md-6 col-xs-6">
                                <?php echo copybook_number($qshow[2], "none", true); ?>
                            </div><!-- /col-md-6 -->

                            <div class="col-md-6 col-xs-6">
                                <?php echo copybook_number($qshow[3], "none", true); ?>
                            </div><!-- /col-md-6 -->

                        </div><!-- /row -->

                    </div>
                    <?php get_template_part('includes/box-abbonamenti'); ?>



                </aside>

            </div><!-- /col-md-4 -->

        </div><!-- /row -->

    </div><!-- /container -->

</div><!-- /section -->


</div><!-- /main_container -->


<?php get_template_part('includes/footer'); ?>
