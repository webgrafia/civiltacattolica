<?php
// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die();
}

class Phone_Share_Shortcode {

	public function __construct() {
		add_shortcode( 'chat-it', array( $this, 'share' ) );
	}

	public function share( $atts ) {

		if( ! empty( $atts ) ) {
			foreach ( $atts as $key => $attr ) {
				$atts[ $key ] = explode( ',', $attr );
			}
		}

		$options = shortcode_atts( array(
			'messengers' => array( 'telegram', 'viber', 'whatsapp', 'facebook', 'skype' ),
			'show_on'    => array( 'ios', 'android', 'desktop', 'other_mobile' ),
		), $atts );

		if ( in_array( PHONE_SHARE()->get_current_platform(), $options['show_on'] ) ) {

			$messengers = PHONE_SHARE()->get_messengers();
			ob_start();
			?>
			<div class="phone-share-container">
				<?php foreach ( $options['messengers'] as $name ): ?>
					<?php
					$custom_styles = sprintf( "color:%s; background-color:%s",
						PHONE_SHARE()->get_options( $name, 'text_color' ), PHONE_SHARE()->get_options( $name, 'background_color' )
					);
					?>
					<div style="<?php echo $custom_styles ?>" class="phone-share-item">
						<?php $icon_style = PHONE_SHARE()->get_options( $name, 'icon_style' ); ?>
						<?php if ( 'hide' !== $icon_style ): ?>
							<i class="<?php echo "$name-icon-$icon_style" ?>"></i>
						<?php endif; ?>
						<a style="<?php echo $custom_styles ?>; border-bottom: 0;"
						   href="<?php echo $messengers[ $name ]['href'] ?>"
						   target="_blank"
						   data-action="open-popup"
						   class="pshare-button"
						   title="<?php _e( 'Share on', 'phone-share' ) ?> <?php echo $messengers[ $name ]['title'] ?>"
						   rel="nofollow"
						>
							<?php echo $messengers[ $name ]['title'] ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
			<?php
			return ob_get_clean();

		}

		return '';

	}

}

new Phone_Share_Shortcode();