<?php
/**
 * Plugin Name:       Importer
 * Description:       Importer
 * Version:           1.0.0
 * Author:            Manafactory
 * Author URI:        http://manafactory.it/
 */

add_action('admin_menu', 'radio_capital_import_page');
include_once("include/xml2array.php");
include_once("config.php");
global $mydb;

function radio_capital_import_page()
{
    add_options_page('Import', 'Import', 'manage_options', 'cc_import', 'cc_import');
}


function cc_import()
{
    global $mydb;
    global $wpdb;

    set_time_limit(20000000);

    $numperpage = 10;

    $np = $_REQUEST['np'];
    if ($np == "") $np = 1;

    $skip = ($numperpage * $np) - $numperpage;
    $newp = $np + 1;
    $stop = false;
    ?>

    <div class="wrap">
        <h2>Pagina di import</h2>
        <br>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=quaderni">Quaderni</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=autori">Autori</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=articoli">Articoli</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=autoriadmin">Fix autori se admin</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=fixautoripost">Fix autori</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=articolisenzaautore">Fix articoli senza autore</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=utenti">Utenti</a></p>

        <p><a href="options-general.php?page=<?php echo $_GET["page"]; ?>&import=csv">CSV numeri quaderno</a></p>


        <?php


    if ($_GET['import'] == "csv") {
            $url = get_bloginfo("url")."/wp-content/plugins/import/include/quaderni_volumi.csv";
        echo $url;
        echo "<br>";
        $stop = true;
        $row = 1;
        if (($handle = fopen($url, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $quaderno = $data[0];
                $volume = $data[1];

                // recupero il quaderno
                $quad = get_term_by("name", $quaderno, "quaderno");
                if($quad){
                    $quadkey = "quaderno_".$quad->term_id;
                    $volume_old = get_field("volume", $quadkey);
                   // echo "---".$quadkey." = ".$volume_old."---";
                    if($volume_old == ""){
                        update_field("volume", $volume, $quadkey);
                        echo "Aggiorno volume ".$volume." a ".$quadkey;

                    }else{
                        echo "Volume già settato per il quaderno ".$quaderno.": ".$volume_old;
                    }
                    echo "<br>";
                }

          //      print_r($quad);
       //  echo $quaderno." - ".$volume."<br>";
            }
            fclose($handle);
        }

    }
        if ($_GET['import'] == "utenti") {
        $query = "SELECT * FROM `cliente` WHERE 1 LIMIT  " . $numperpage . " OFFSET " . $skip;

        echo "<p>" . $query . "</p>";
        $rows = $mydb->get_results($query);

        if (!$rows) {
            echo "Import concluso";
            $stop = true;
        }
        foreach ($rows as $obj) {
            echo $obj->id_cliente;
            echo " ";
            echo $obj->nome;
            echo " ";
            echo $obj->cognome;
            echo " ";
            echo $obj->email;
            echo " ";
            echo $obj->username;
            echo " ";
            echo $obj->password;
            echo " ";

            $user_id = wc_create_new_customer( $obj->email, $obj->username, $obj->password );

            update_user_meta( $user_id, "first_name", $obj->nome );
            update_user_meta( $user_id, "last_name", $obj->cognome );

            update_user_meta( $user_id, "billing_first_name", $obj->nome );
            update_user_meta( $user_id, "billing_last_name", $obj->cognome );
            update_user_meta( $user_id, "billing_email", $obj->email );
            update_user_meta( $user_id, "billing_address_1", $obj->indirizzo ." ".$obj->civico);
            update_user_meta( $user_id, "billing_postcode", $obj->cap);
            update_user_meta( $user_id, "billing_state", $obj->nazione);
            update_user_meta( $user_id, "billing_country", $obj->comune_localita);

            update_user_meta( $user_id, "shipping_first_name", $obj->nome );
            update_user_meta( $user_id, "shipping_last_name", $obj->cognome );
            update_user_meta( $user_id, "shipping_address_1", $obj->indirizzo ." ".$obj->civico);
            update_user_meta( $user_id, "shipping_postcode", $obj->cap);
            update_user_meta( $user_id, "shipping_state", $obj->nazione);
            update_user_meta( $user_id, "shipping_country", $obj->comune_localita);





            echo "<hr>";


        }
    }
    if ($_GET['import'] == "articolisenzaautore") {
        $query = "SELECT articolo.*, articolo_x_autore.id_autore FROM `articolo`, articolo_x_autore WHERE articolo.id_articolo = articolo_x_autore.id_artitcolo LIMIT  " . $numperpage . " OFFSET " . $skip;

        echo "<p>" . $query . "</p>";
        $rows = $mydb->get_results($query);

        if (!$rows) {
            echo "Import concluso";
            $stop = true;
        }
        foreach ($rows as $obj) {

            // controllo se non esiste l'autore
            $query2 = "select * from autore WHERE id_autore = '".$obj->id_autore."'";

            echo "<p>" . $query2 . "</p>";
            $rows2 = $mydb->get_results($query2);
           // dd($rows2);
            if(!$rows2){
                // non esiste l'autore, aggiorno il post

                $args = array(
                    'post_type' => 'articolo',
                    'meta_query' => array(
                        array(
                            'key' => '_orig_id',
                            'value' => $obj->id_articolo,
                            'compare' => '=',
                        )
                    )
                );
                $posts = get_posts($args);
                foreach ($posts as $post) {
                    if($post->post_author != 1){
                        echo "non esiste l'autore, aggiorno il post ".$post->post_title." ".$post->ID."<br>";

                        $my_post = array(
                            'ID'           => $post->ID,
                            'post_author'   => 1
                        );

                        wp_update_post($my_post);
                       // exit;


                    }
                }


            }


        }
    }


        if ($_GET['import'] == "fixautoripost") {
        // ciclo su tutti gli autori importati
        $args = array(
            'orderby'      => 'login',
            'order'        => 'ASC',
            'offset'       => $skip,
            'number'       => $numperpage,
            'count_total'  => false,
            'fields'       => 'all',
        );
        $allusers = get_users( $args );

        foreach ($allusers as $alluser) {
         //   dd($alluser);

            $origid = false;
            $origid = get_user_meta($alluser->ID, "_orig_id", true);

            if(($alluser->ID > 1) && ($alluser->data->user_login != "civiltacattolica") && ($alluser->data->user_login != "webgrafi.a") && ($alluser->data->user_login != "amministrazione")){
                if($origid){
                    echo $origid;
                    echo "<hr>";

// recupero dal vecchio DB gli articoli con questo id:
                    $query = "SELECT articolo.id_articolo FROM `articolo`, articolo_x_autore WHERE articolo.id_articolo = articolo_x_autore.id_artitcolo AND articolo_x_autore.id_autore =".$origid;
                    echo "<p>" . $query . "</p>";
                    $rows = $mydb->get_results($query);
                    foreach ($rows as $obj) {
//                        dd($obj);

                        $orig_id_articolo = $obj->id_articolo;

                        echo "articolo orig: ".$orig_id_articolo."<br>";

                        // recupero il post con articolo originale associato
                        $args = array(
                            'post_type' => 'articolo',
                            'meta_query' => array(
                                array(
                                    'key' => '_orig_id',
                                    'value' => $orig_id_articolo,
                                    'compare' => '=',
                                )
                            )
                        );
                        $posts = get_posts($args);
                        foreach ($posts as $post) {
                            // dovrebbe essercene soli 1
                            if($post->post_author != $alluser->ID){

                                 echo "<b>".$post->post_title." ".$post->ID." - articolo con autore sbagliato!</b><br>";

                                // correggo
                                $my_post = array(
                                    'ID'           => $post->ID,
                                    'post_author'   => $alluser->ID
                                );
                                wp_update_post( $my_post );
                            }
                        }
                    }

                    }else {
                    dd($alluser);
                    echo "autore ".$alluser->ID." senza ID!";
                    exit();
                }

            }// controllo non admin
        }// ciclo sugli user

        // controllo l'id autore originale e recupero tutti i post associati

        // per ogni post associato sul db originale, ne recupero la versione locale e gli assegno lid autore corretto

    }





        if ($_GET['import'] == "autoriadmin") {

        $query = "SELECT articolo.*, articolo_x_autore.id_autore FROM `articolo`, articolo_x_autore WHERE articolo.id_articolo = articolo_x_autore.id_artitcolo LIMIT  " . $numperpage . " OFFSET " . $skip;

        echo "<p>" . $query . "</p>";
        $rows = $mydb->get_results($query);

        if (!$rows) {
            echo "Import concluso";
            $stop = true;
        }
        foreach ($rows as $obj) {
            $args = array(
                'post_type' => 'articolo',
                'meta_query' => array(
                    array(
                        'key' => '_orig_id',
                        'value' => $obj->id_articolo,
                        'compare' => '=',
                    )
                )
            );
            $posts = get_posts($args);
            if ($posts) {
                if($posts[0]->post_author == 1){

                    // recupero l'utente già importato
                    $argsuser = array(
                        'meta_query' => array(
                            array(
                                'key' => '_orig_id',
                                'value' => $obj->id_autore,
                                'compare' => '=',
                            )
                        )
                    );

                    $utenti = get_users($argsuser);
                    if($utenti){
                        $iduser = $utenti["0"]->id;
                        $iduser = $utenti["0"]->id;
                        $my_post = array(
                            'ID'           => $posts[0]->ID,
                            'post_author'   => $iduser
                        );

                        wp_update_post($my_post);
                        echo "aggiorno post ".$posts[0]->ID;
                    }





                }

            }
        }

    }
        /********* Articoli *********/
        if ($_GET['import'] == "articoli") {
            $query = "SELECT articolo.*, articolo_x_autore.id_autore FROM `articolo`, articolo_x_autore WHERE articolo.id_articolo = articolo_x_autore.id_artitcolo LIMIT  " . $numperpage . " OFFSET " . $skip;

            echo "<p>" . $query . "</p>";
            $rows = $mydb->get_results($query);

            if (!$rows) {
                echo "Import concluso";
                $stop = true;
            }
            foreach ($rows as $obj) :

             //   dd($obj);
                /*
                 stdClass Object
(
    [id_articolo] => 15
    [id_tipo_articolo] => 6
    [id_quaderno] => 18
    [titolo] => NOTE SULLA BOLIVIA
    [abstract] =>Le elezioni politiche del 2005 in Bolivia, con la vittoria del primo presidente indio, Evo Morales, hanno segnato un’importante svolta nella storia del Paese. Sotto i precedenti Governi liberali, accentratori e corrotti, diversi moti popolari di protesta avevano preparato il successo del Movimento per il Socialismo di Morales. Le prime riforme in campo amministrativo, giudiziario ed economico sono state positive. Rimangono però forti tensioni tra le classi e tra le etnie, e spinte autonomiste. Non mancano contrasti con gli Stati Uniti per l’insufficiente lotta contro il narcotraffico, e con la Chiesa cattolica per il progetto di «Costituzione atea». C’è, dunque, ancora molto da fare sulla via della pacificazione e della giustizia sociale. L’Autore è il superiore provinciale dei gesuiti in Bolivia.
    [articolo_completo] =>
    [pagina_inizio] => 311
    [pagina_fine] => 319
    [rank] => 11
    [prezzo] => 4.00
    [sconto] =>
    [pdf] => 1
    [id_autore] => 31
)

                 */

                $args = array(
                    'post_type' => 'articolo',
                    'meta_query' => array(
                        array(
                            'key' => '_orig_id',
                            'value' => $obj->id_articolo,
                            'compare' => '=',
                        )
                    )
                );
                $posts = get_posts($args);
                if (!$posts) {


                    // cerco il quaderno
                    $argsquad = array(
                        'taxonomy' => 'quaderno',
                        'hide_empty' => false,
                        'meta_query' => array(
                            array(
                                'key' => '_orig_id',
                                'value' => $obj->id_quaderno,
                                'compare' => '=',
                            )
                        )
                    );

                    $quaderni = get_terms($argsquad);

// recupero la data del quaderno
                    $data_quaderno = get_field("field_578502792d2c0", "quaderno_" . $quaderni[0]->term_id);
//echo $data_quaderno;
                    $arrq = explode(".", $data_quaderno);
                    $giornoora = $arrq[2]."-".$arrq[1]."-".$arrq[0]." 00:00:00";
//                    print_r($quaderni);




                    // recupero l'utente già importato
                    $argsuser = array(
                        'meta_query' => array(
                            array(
                                'key' => '_orig_id',
                                'value' => $obj->id_autore,
                                'compare' => '=',
                            )
                        )
                    );

                    $utenti = get_users($argsuser);
                    if($utenti)
                        $iduser = $utenti["0"]->id;
                    else
                        $iduser = 1;


                    $art_args = array(
                        'post_title' => $obj->titolo,
                        'post_content' => $obj->abstract,
                        'post_type' => "articolo",
                        'post_status' => 'publish',
                        'post_author' => $iduser,
                        'post_date' => $giornoora,

                    );

                    $id_articolo = wp_insert_post($art_args);

                    echo "<p>Inserisco l'articolo " . $id_articolo. "</p>";

                    // aggiungo l'id originale per evitare doppioni
                    update_post_meta($id_articolo, "_orig_id", $obj->id_articolo);
                    update_post_meta($id_articolo, "_orig_obj", $obj);

                    update_field("field_5788fbe09e5d4", $obj->pagina_inizio, $id_articolo);

                    //  creo la relazione con
                    if ($quaderni) {
                        wp_set_object_terms($id_articolo, $quaderni[0]->term_id,"quaderno");
                    }



                }else{

                    $id_articolo = $posts[0]->ID;
                    echo "<p>Prodotto " . $id_articolo . " gia' esistente</p>";

                }

//  exit;
            endforeach;

        }


        /********* Autori *********/
        if ($_GET['import'] == "autori") {
            $query = "select * from autore WHERE 1 LIMIT  " . $numperpage . " OFFSET " . $skip;

            echo "<p>" . $query . "</p>";
            $rows = $mydb->get_results($query);

            if (!$rows) {
                echo "Import concluso";
                $stop = true;
            }
            foreach ($rows as $obj) :
                /*
                stdClass Object
                (
                    [id_autore] => 13
                    [id_carica] => 3
                    [nome] => Francesco
                    [cognome] => Occhetta
                )
                */

                $args = array(
                    'meta_query' => array(
                        array(
                            'key' => '_orig_id',
                            'value' => $obj->id_autore,
                            'compare' => '=',
                        )
                    )
                );

                $utenti = get_users($args);
                if (!$utenti) {

                    $username = strtolower(trim($obj->nome) . "-" . trim($obj->cognome));
                    $password = wp_generate_password($length = 12, $include_standard_special_chars = false);
                    $email = strtolower(trim($obj->nome) . "-" . trim($obj->cognome)) . "@laciviltacattolica.it";
                    //    $id_utente = wp_create_user( $username, $password, $email );


                    $userdata = array(
                        'user_login' => $username,
                        'user_url' => get_bloginfo("url"),
                        'user_pass' => $password,
                        'user_email' => $email,
                        'first_name' => $obj->nome,
                        'last_name' => $obj->cognome,
                        'display_name' => $obj->nome . " " . $obj->cognome,
                        'role' => "author",
                    );

                    $id_utente = wp_insert_user($userdata);

                    if(!is_wp_error($id_utente)){
                        // aggiungo i metadati
                        add_user_meta($id_utente, "_orig_id", $obj->id_autore);
                        add_user_meta($id_utente, "_orig_obj", $obj);


                        echo "<p>Utente " . $id_utente . " creato</p>";

                    }else{
                        print_r($id_utente);
                        // cerco l'utente con questo username e aggiorno il meta

                        $existinguser = get_user_by("login", $username);

                        add_user_meta($existinguser->ID, "_orig_id", $obj->id_autore);
                        add_user_meta($existinguser->ID, "_orig_obj", $obj);
                    }

                } else {
                    $id_utente = $utenti["0"]->id;
                    echo "<p>Utente " . $id_utente . " gia' esistente</p>";
                }


          

            endforeach;
        }


        /********* Quaderni *********/
        if ($_GET['import'] == "quaderni") {

            $query = "select * from quaderno WHERE 1 LIMIT  " . $numperpage . " OFFSET " . $skip;
            echo "<p>" . $query . "</p>";
            $rows = $mydb->get_results($query);

            if (!$rows) {
                echo "Import concluso";
                $stop = true;
            }


            foreach ($rows as $obj) :

                /*
            stdClass Object
            (
                [id_quaderno] => 216
                [numero] => 3615
                [anno_pubblicazione] => 2001-02-03
                [numero_pubblicazione_annuo] => 3
                [anno_cv] => 152
                [trimestre] => I
                [pagina_inizio] => 229
                [pagina_fine] => 334
                [giacenza] => 0
                [prezzo] => 8.00
                [sconto] =>
            )
                 */
                $args = array(
                    'taxonomy' => 'quaderno',
                    'hide_empty' => false,
                    'meta_query' => array(
                        array(
                            'key' => '_orig_id',
                            'value' => $obj->id_quaderno,
                            'compare' => '=',
                        )
                    )
                );

                $quaderni = get_terms($args);
             //   print_r($quaderni);
                if (!$quaderni) {
                    // inserisco il dato
                    $new_quaderno = wp_insert_term(
                        wp_strip_all_tags($obj->numero),
                        'quaderno' // the taxonomy
                    );

                    if (!is_wp_error($new_quaderno))
                        $id_quaderno = $new_quaderno["term_id"];

                    if ($id_quaderno) {
                        update_term_meta($id_quaderno, "_orig_id", $obj->id_quaderno);


                        // aggiungo l'intero json per processi futuri
                        update_term_meta($id_quaderno, "_orig_obj", $obj);

                        // data_pubblicazione
                        $datapub = strtotime($obj->anno_pubblicazione);
                        if($datapub > time())
                            $datapub = time();
                        update_field("field_578502792d2c0", $datapub , "quaderno_" . $id_quaderno);
                    }
                    echo "<p>Inserisco il quaderno " . $id_quaderno . "</p>";
                } else {
                    $id_quaderno = $quaderni["0"]->term_id;
                    echo "<p>Quaderno " . $obj->numero . " gia' esistente</p>";
                }

                // inserisco il prodotto legato al quaderno per la vendita

                $args = array(
                    'post_type' => 'product',
                    'meta_query' => array(
                        array(
                            'key' => '_orig_id',
                            'value' => $obj->id_quaderno,
                            'compare' => '=',
                        )
                    )
                );
                $posts = get_posts($args);
                if (!$posts) {

                    $prod_args = array(
                        'post_title' => "Quaderno " . $obj->numero,
                        'post_type' => "product",
                        'post_status' => 'publish',

                    );

                    $id_product = wp_insert_post($prod_args);

                    echo "<p>Inserisco il prodotto " . $id_product . "</p>";

                    // aggiungo l'id originale per evitare doppioni
                    update_post_meta($id_product, "_orig_id", $obj->id_quaderno);
                    update_post_meta($id_product, "_orig_obj", $obj);
                    update_post_meta($id_product, "_regular_price", $obj->prezzo);
                    update_post_meta($id_product, '_price', $obj->prezzo);

                    wp_set_object_terms($id_product, 'simple', 'product_type');
                    update_post_meta($id_product, '_visibility', 'visible');
                    update_post_meta($id_product, '_stock_status', 'instock');
                    update_post_meta($id_product, 'total_sales', '0');
                    update_post_meta($id_product, '_downloadable', 'no');
                    update_post_meta($id_product, '_virtual', 'no');
                    update_post_meta($id_product, '_sale_price', '');
                    update_post_meta($id_product, '_purchase_note', '');
                    update_post_meta($id_product, '_featured', 'no');
                    update_post_meta($id_product, '_weight', '');
                    update_post_meta($id_product, '_length', '');
                    update_post_meta($id_product, '_width', '');
                    update_post_meta($id_product, '_height', '');
                    update_post_meta($id_product, '_sku', $id_quaderno);
                    update_post_meta($id_product, '_product_attributes', array());
                    update_post_meta($id_product, '_sale_price_dates_from', '');
                    update_post_meta($id_product, '_sale_price_dates_to', '');
                    update_post_meta($id_product, '_sold_individually', '');
                    update_post_meta($id_product, '_manage_stock', 'no');
                    update_post_meta($id_product, '_backorders', 'no');
                    update_post_meta($id_product, '_stock', '');

                    set_post_thumbnail($id_product, IMAGE_PRODUCT_ID);

                    // aggiungo la relazione con la categoria
                    $quadcat = get_term_by("name", "Quaderni", "product_cat");

                    wp_add_object_terms($id_product, $quadcat->term_id, "product_cat");

                } else {
                    $id_product = $posts[0]->ID;
                    echo "<p>Prodotto " . $id_product . " gia' esistente</p>";
                }

                echo "aggiungo relazione " . $id_quaderno . " " . $quadcat->term_id;
                // aggiunto la relazione tra quaderno e prodotto
                // prodotto_correlato
                update_field("field_5787b1eaef96d", $id_product, "quaderno_" . $id_quaderno);


                //  exit;
            endforeach;


        }

        ?>





        <?php
        if ($_REQUEST['import'] && !$stop) {
            ?>
            <script language="javascript" type="text/javascript">
                window.location.href = "<?php echo get_bloginfo('url')."/wp-admin/options-general.php?page=".$_GET["page"]."&import=".$_REQUEST['import']."&np=".$newp; ?>";
            </script>
        <?php
        }
        ?>
    </div>

<?php
}

