<?php
$messengers    = $this->get_messengers();
$style_options = array( 'background_color', 'text_color', 'icon_style', 'show_on' );
?>
<div class="phone-share-container">
	<?php foreach ( $this->get_options( 'general', 'messengers' ) as $name ): ?>
		<?php
		$styles  = array();
		$options = array();

		foreach ( $style_options as $option ) {
			$options[ $option ] = $this->get_options( $name, $option );
		}

		if( ! in_array( $this->get_current_platform(), $options['show_on'] ) ) {
			continue;
		}

		if ( 'button' == $options['icon_style'] ) {
			$styles['button'] = "style=color:{$options['text_color']};background-color:{$options['background_color']}";
		} else {
			$styles['icon']       = "style=background-color:{$options['background_color']}";
			$styles['text_color'] = "style=color:{$options['background_color']}";
		}
		?>

		<div class="phone-share-item">
			<a <?php if ( isset( $styles['button'] ) ): echo $styles['button']; endif; ?>
				href="<?php echo $messengers[ $name ]['href'] ?>"
				target="_blank"
				data-action="open-popup"
				class="pshare-button <?php echo "link-$name-{$options['icon_style']}" ?>"
				title="<?php _e( 'Share on', 'phone-share' ) ?> <?php echo $messengers[ $name ]['title'] ?>"
				rel="nofollow">

				<i <?php if ( isset( $styles['icon'] ) ): echo $styles['icon']; endif; ?>
					class="<?php echo "share-icon-$name" ?>"></i>

				<?php if ( $messengers[ $name ]['title'] ): ?>
					<span <?php if ( isset( $styles['text_color'] ) ): echo $styles['text_color']; endif; ?>
						class="phone-share-title">
                        <?php echo $messengers[ $name ]['title'] ?>
                    </span>
				<?php endif; ?>
			</a>
		</div>
	<?php endforeach; ?>
</div>