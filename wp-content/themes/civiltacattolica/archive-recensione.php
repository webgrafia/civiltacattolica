<?php get_template_part('includes/header');
global $wp_query;

?>

<main role="main">
    <div class="section section_large_padding section_white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="author_main">
                        <div class="author_main_image">
                            <img src="https://www.laciviltacattolica.it/wp-content/uploads/2018/03/book-610189_960_720-500x500.jpg">
                        </div>
                        <!-- /article_large_image -->
                        <div class="author_social">
                        </div>
                    </div>
                </div>
                <!-- /col-md-4 -->
                <div class="col-md-8">
                    <div class="author_content">
                        <h1>Rassegna bibliografica</h1>

                        <p>Le recensioni della Rassegna bibliografica de "La Civiltà Cattolica" sono curate dai padri gesuiti del Collegio degli scrittori della rivista e da collaboratori non gesuiti.</p>                        </div>
                </div>
                <!-- /col-md-8 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <div class="section section_large_padding section_grey_light">
        <div class="container">
            <div class="row">

                <div class="col-md-12">
					<?php
					if(is_tax()){
						?>
                        <div class="author_content"><?php
							the_archive_title( '<h1>', '</h1>' );
							?></div>

						<?php
					}
					?>


						<?php
						global $wp_query;
						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						if (have_posts()) {

							while (have_posts()) { the_post();
							echo '<div class="col-md-6 margin-bottom-50">';
								print_recensione_lista($post);
								echo '</div>';
							}
						}
						?>

                    <!-- /grid -->
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="pager">
						<?php
						$big = 999999999; // need an unlikely integer
						echo paginate_links(array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, $paged ),
							'total' => $wp_query->max_num_pages
						));
						?>

                    </div>
                </div>
                <!-- /col-md-12 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /section -->

</main>


</div><!-- /main_container -->


<?php get_template_part('includes/footer'); ?>
