<?php

function return_custom_price_for_instant_book($price, $product) {
    // controllo se il prodotto è nella categoria instant book
    $instantbook = false;
    $categories = $product->get_category_ids();
    foreach ($categories as $catID){
        $catobj = get_term_by("id", $catID, "product_cat");
        if($catobj->slug == "accenti"){
            $instantbook = true;
        }
    }

    if(!$instantbook)
        return $price;

    // verifico se l'utente è abbonato
    $current_user = wp_get_current_user();
    if ( 0 == $current_user->ID ) {
        // Not logged in.
        return $price;
    } else {
        if(is_user_abbonato_attivo($current_user->ID)){
            return 0;
        }
    }

    return $price;
}

add_filter('woocommerce_product_get_price', 'return_custom_price_for_instant_book', 10, 2);


/**
 * check abbonato attivo
 */

function is_user_abbonato_attivo($user_ID){
    // utente esistente, verifico se ha attivo un abbonamento
    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => $user_ID,
        'post_type'   => wc_get_order_types(),
        'post_status' => array("wc-completed", "wc-processing"),
    ) );


    foreach($customer_orders as $customer_order) {
        $order = new WC_Order($customer_order->ID);
        $items = $order->get_items();
        foreach ($items as $item) {
            $term_list = wp_get_post_terms($item["product_id"], 'product_cat', array("fields" => "all"));

            foreach ($term_list as $product_cat) {

                if ($product_cat->slug == "abbonamenti") {
                    if (strpos(strtolower($item["name"]), "1 anno")) {
                        $timeend = strtotime($order->get_date_completed() . " + 1 YEAR");
                    } else if (strpos(strtolower($item["name"]), "2 anni")) {
                        $timeend = strtotime($order->get_date_completed() . " + 2 YEARS");
                    } else if (strpos(strtolower($item["name"]), "3 anni")) {
                        $timeend = strtotime($order->get_date_completed() . " + 3 YEARS");
                    } else if (strpos(strtolower($item["name"]), "4 anni")) {
                        $timeend = strtotime($order->get_date_completed() . " + 4 YEARS");
                    } else if (strpos(strtolower($item["name"]), "5 anni")) {
                        $timeend = strtotime($order->get_date_completed() . " + 5 YEARS");
                    }

                    if ($timeend > time()) {
                        return true;
                    }

                }
            }

        }
    }
    return false;
}

//add_action( 'woocommerce_cart_calculate_fees','cc_wholesale_discount' );

// WooCommerce functions
if ( ! function_exists( 'wbst_woocommerce_setup' ) ) :
  function wbst_woocommerce_setup() {
    add_theme_support( 'woocommerce' );
  }
endif;
add_action( 'after_setup_theme', 'wbst_woocommerce_setup' );
/*
Set Default Thumbnail Sizes for WooCommerce Product Pages, on Theme Activation
*/
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'dazzling_woocommerce_image_dimensions', 1 );
/*
Define image sizes
*/
function dazzling_woocommerce_image_dimensions() {
  $catalog = array(
		'width' 	=> '350',	// px
		'height'	=> '453',	// px
		'crop'		=> 1 		// true
	);
	$single = array(
		'width' 	=> '570',	// px
		'height'	=> '708',	// px
		'crop'		=> 1 		// true
	);
	$thumbnail = array(
		'width' 	=> '350',	// px
		'height'	=> '453',	// px
		'crop'		=> 0 		// false
	);
	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}
/*
 * Add basic WooCommerce template support
 *
 */
// Remove original WooCommerce wrappers
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
// Add WBST wrappers
add_action('woocommerce_before_main_content', 'wbst_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'wbst_wrapper_end', 10);
function wbst_wrapper_start() {
  echo '<main id="main" class="site-main" role="main">';
}
function wbst_wrapper_end() {
  echo '</main>';
}
/*
Place a cart icon with number of items and total cost in the menu bar.
*/
function wbst_woomenucart($menu, $args) {
	// Check if WooCommerce is active and add a new item to a menu assigned to "Navbar Upper Right" (Primary Navigation Menu) location
	if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) || 'navbar-upper-right' !== $args->theme_location )
		return $menu;
	ob_start();
		global $woocommerce;
		$viewing_cart = __('View your shopping cart', 'wbst');
		$start_shopping = __('Start shopping', 'wbst');
		$cart_url = $woocommerce->cart->get_cart_url();
		$shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
		$cart_contents_count = $woocommerce->cart->cart_contents_count;
		$cart_contents = sprintf(_n('%d item', '%d items', $cart_contents_count, 'wbst'), $cart_contents_count);
		$cart_total = $woocommerce->cart->get_cart_total();
    if ($cart_contents_count == 0) {
      $menu_item = '<li class="pull-right"><a class="woo-menu-cart" href="'. $shop_page_url .'" title="'. $start_shopping .'">';
    } else {
      $menu_item = '<li class="pull-right"><a class="woo-menu-cart" href="'. $cart_url .'" title="'. $viewing_cart .'">';
    }
    $menu_item .= '<i class="fa fa-shopping-cart"></i> ';
    $menu_item .= $cart_contents.' - '. $cart_total;
    $menu_item .= '</a></li>';
		echo $menu_item;
	$social = ob_get_clean();
	return $menu . $social;
}
add_filter('wp_nav_menu_items','wbst_woomenucart', 10, 2);


add_action("init", "cc_check_abbonamento");

function cc_check_abbonamento(){
    if(!isset($_GET["checkabbonamento"]) || $_GET["checkabbonamento"] != "true")
        return;

    $skipold=false;

    if(!isset($_GET["user"]) || $_GET["user"] == ""){
        $data = array("issubscribed"=>false,"message"=>"Missing User");
        echo "singlesignon(" . json_encode($data) . ");";
        $skipold=true;
        exit;
    }


    if(!isset($_GET["password"]) || $_GET["password"] == "" ){
        $data = array("issubscribed"=>false,"message"=>"Missing Password");
        echo "singlesignon(" . json_encode($data) . ");";
        $skipold=true;
        exit;
    }



    $user = get_user_by( 'login', $_GET["user"]);
    if ( $user && wp_check_password( $_GET["password"], $user->data->user_pass, $user->ID) ){

        if(is_user_abbonato_attivo($user->ID)){
            // abbonamento ancora valido
            $skipold=true;

            $data["civiltacattolica"]["issubscribed"] = true;
            $data["civiltacattolica"]["startedon"] = date("c",strtotime($order->order_date));
            $data["civiltacattolica"]["expireson"] = date("c",$timeend);
            echo "singlesignon(",json_encode($data),");";
            exit;
        }
    }

    if(!$skipold){
        // todo: controllo sul vecchio sistema
        // ricopio il vecchio codice
        if(!function_exists("mysql_connect")){
            $data = array("issubscribed"=>false,"message"=>"mysql_connect not exist");
            echo "singlesignon(" . json_encode($data) . ");";
            exit;
        }

        $con = mysql_connect("10.0.0.208","civicatt","Formula188");
        if (!$con)
        {
            $data = array("issubscribed"=>false,"message"=>mysql_error());
            echo "singlesignon(" . json_encode($data) . ");";
            exit;

        }
        mysql_select_db("civicatt", $con);
        $result = mysql_query("SELECT * FROM abbonamenti where codice='$_GET[user]' and fattura='$_GET[password]' and datsca>=CURDATE()");
        if (mysql_num_rows($result)) {
            while($row = mysql_fetch_array($result))
            {
                $datainizio= $row['DATABB'];
                $datafine= $row['DATSCA'];
            }
            $skipold=true;
            $data["civiltacattolica"]["issubscribed"] = true;
            $data["civiltacattolica"]["startedon"] = $datainizio."T00:00:00+01:00";
            $data["civiltacattolica"]["expireson"] = $datafine."T23:59:00+01:00";
            echo "singlesignon(",json_encode($data),");";

        }else{
            $data = array("issubscribed"=>false,"message"=>"Abbonamento non attivo o dati account non corretti");
            echo "singlesignon(" . json_encode($data) . ");";

        }
        mysql_close($con);
        exit;
    }


        $data = array("issubscribed"=>false,"message"=>"Abbonamento non attivo o dati account non corretti");
        echo "singlesignon(" . json_encode($data) . ");";
        exit;

}
