<?php
$slider = get_field("slider", "option");
if(is_array($slider) && (count($slider) > 0)) {
?>
    <div class="section section_white section_nopadding">
        <div class="owl-carousel carousel-theme carousel_base">
            <?php
            foreach($slider as $slide){
                if($slide["tipo_di_articolo"] == "manuale"){
                    $titolo = $slide["titolo"];
                    $immagine = $slide["immagine"]["sizes"]["slider"];
                    $link = $slide["link"];
                }else{
                    $titolo = $slide["articolo"][0]->post_title;
                    $link = get_permalink($slide["articolo"][0]->ID);
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($slide["articolo"][0]->ID), 'slider');
                    $immagine = $thumb[0];
                }
            ?>
                <div class="item">
                    <div class="lead_wrapper" style="background-image: url('<?php echo $immagine; ?>');">
                        <div class="lead_container">
                            <div class="lead_text">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <?php
                                            if($link == "")
                                                echo '<h2 style="color:#fff;">'.$titolo.'</h2>';
                                            else
                                                echo '<h2><a href="'.$link.'">'.$titolo.'</a></h2>';
                                            ?>

                                        </div>
                                        <!-- /col-md-12 -->
                                    </div>
                                    <!-- /row -->
                                </div>
                                <!-- /container -->
                            </div>
                            <!-- /lead_text -->
                        </div>
                        <!-- /lead_container -->
                    </div>
                    <!-- /lead_wrapper -->
                </div>
                <!-- /item -->
            <?php
            }
            ?>
        </div>
        <!-- /carousel_base -->
    </div><!-- /section -->
<?php
}
