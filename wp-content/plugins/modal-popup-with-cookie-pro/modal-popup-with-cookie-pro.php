<?php
/**
 * Plugin Name: Modal Popup with Cookie Integration Pro
 * Plugin URI: http://www.wponlinesupport.com/
 * Description: Create and manage powerful promotion modal popups for your WordPress blog or website. Plugin that will help you to grab your visitors’ attention to introduce them your offers, discounts or other kind of promotional notices.
 * Text Domain: modal-popup-with-cookie
 * Domain Path: /languages/
 * Author: WP Online Support 
 * Version: 1.1.1
 * Author URI: http://www.wponlinesupport.com/
 *
 * @package WordPress
 * @author SP Technolab
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

/**
 * Basic plugin definitions
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
if( !defined( 'MPWC_PRO_VERSION' ) ) {
	define( 'MPWC_PRO_VERSION', '1.1.1' );	// Version of plugin
}
if( !defined( 'MPWC_PRO_DIR' ) ) {
	define( 'MPWC_PRO_DIR', dirname( __FILE__ ) );	// Plugin dir
}
if( !defined( 'MPWC_PRO_URL' ) ) {
	define( 'MPWC_PRO_URL', plugin_dir_url( __FILE__ ) );	// Plugin url
}
if( !defined( 'MPWC_PRO_PLUGIN_BASENAME' ) ) {
    define( 'MPWC_PRO_PLUGIN_BASENAME', plugin_basename( __FILE__ ) ); // Plugin base name
}
if( !defined( 'MPWC_PRO_POPUP_POST_TYPE' ) ) {
	define( 'MPWC_PRO_POPUP_POST_TYPE', 'mpwc_pro_popup' );	// Plugin meta prefix
}
if( !defined( 'MPWC_PRO_META_PREFIX' ) ) {
	define( 'MPWC_PRO_META_PREFIX', '_mpwc_pro_' );	// Plugin meta prefix
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_load_textdomain() {
	
	global $wp_version;

    // Set filter for plugin's languages directory
    $mpwc_pro_lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
    $mpwc_pro_lang_dir = apply_filters( 'mpwc_pro_languages_directory', $mpwc_pro_lang_dir );

    // Traditional WordPress plugin locale filter.
    $get_locale = get_locale();

    if ( $wp_version >= 4.7 ) {
        $get_locale = get_user_locale();
    }

    // Traditional WordPress plugin locale filter
    $locale = apply_filters( 'plugin_locale',  $get_locale, 'modal-popup-with-cookie' );
    $mofile = sprintf( '%1$s-%2$s.mo', 'modal-popup-with-cookie', $locale );

    // Setup paths to current locale file
    $mofile_global  = WP_LANG_DIR . '/plugins/' . basename( MPWC_PRO_DIR ) . '/' . $mofile;

    if ( file_exists( $mofile_global ) ) { // Look in global /wp-content/languages/plugin-name folder
        load_textdomain( 'modal-popup-with-cookie', $mofile_global );
    } else { // Load the default language files
        load_plugin_textdomain( 'modal-popup-with-cookie', false, $mpwc_pro_lang_dir );
    }
}

// Action to load plugin text domain
add_action('plugins_loaded', 'mpwc_pro_load_textdomain');

/***** Updater Code Starts *****/
define( 'EDD_MPWC_STORE_URL', 'https://www.wponlinesupport.com' );
define( 'EDD_MPWC_ITEM_NAME', 'Modal Popup with Cookie Integration Pro' );

// Plugin Updator Class
if( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {	
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

/**
 * Updater Function
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function edd_mpwc_pro_plugin_updater() {
	
	$license_key = trim( get_option( 'edd_mpwc_license_key' ) );

	$edd_updater = new EDD_SL_Plugin_Updater( EDD_MPWC_STORE_URL, __FILE__, array(
            'version' 	=> MPWC_PRO_VERSION,      // current version number
            'license' 	=> $license_key,          // license key (used get_option above to retrieve from DB)
            'item_name' => EDD_MPWC_ITEM_NAME,    // name of this plugin
            'author' 	=> 'WP Online Support'    // author of this plugin
		)
	);

}
add_action( 'admin_init', 'edd_mpwc_pro_plugin_updater', 0 );
include( dirname( __FILE__ ) . '/edd-mpwc-plugin.php' );
/***** Updater Code Ends *****/

/**
 * Activation Hook
 * 
 * Register plugin activation hook.
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
register_activation_hook( __FILE__, 'mpwc_pro_install' );

/**
 * Deactivation Hook
 * 
 * Register plugin deactivation hook.
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
register_deactivation_hook( __FILE__, 'mpwc_pro_uninstall');

/**
 * Plugin Activation Function
 * Does the initial setup, sets the default values for the plugin options
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_install(){

	// Get settings for the plugin
	$mpwc_pro_options = get_option( 'mpwc_pro_options' );
	
	if( empty( $mpwc_pro_options ) ) { // Check plugin version option
		
		// set default settings
		mpwc_pro_default_settings();

		// Update plugin version to option
		update_option( 'mpwc_pro_plugin_version', '1.0.1' );
	}

	// Custom post type function
    mpwc_pro_register_post_types();   

    // IMP to call to generate new rules
    flush_rewrite_rules();

    if( is_plugin_active('wp-modal-popup-with-cookie-integration/wp-modal-popup.php') ){
        add_action('update_option_active_plugins', 'mpwc_pro_deactivate_free_version');
    }
}

/**
 * Deactivate free plugin
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_deactivate_free_version() {
	deactivate_plugins('wp-modal-popup-with-cookie-integration/wp-modal-popup.php', true);
}

/**
 * Plugin Deactivation Function
 * Delete  plugin options
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_uninstall(){
	// IMP need to flush rules for custom registered post type
    flush_rewrite_rules();
}

/**
 * Function to display admin notice of activated plugin.
 * 
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */
function mpwc_pro_admin_notice() {
    
    $dir = WP_PLUGIN_DIR . '/wp-modal-popup-with-cookie-integration/wp-modal-popup.php';
    
    // If PRO plugin is active and free plugin exist
    if( is_plugin_active( 'wp-modal-popup-with-cookie-integration/wp-modal-popup.php' ) && file_exists($dir) ) {
        
        global $pagenow;

        if( $pagenow == 'plugins.php' && current_user_can('install_plugins') ) {
			echo '<div id="message" class="updated notice is-dismissible"><p><strong>Thank you for activating  Modal Popup with Cookie Integration Pro</strong>.<br /> It looks like you had FREE version <strong>(<em> Modal Popup with Cookie Integration</em>)</strong> of this plugin activated. To avoid conflicts the extra version has been deactivated and we recommend you delete it. </p></div>';
        }
    }
}
add_action( 'admin_notices', 'mpwc_pro_admin_notice');

// Global Variables
global $mpwc_pro_options;

// Function File
require_once( MPWC_PRO_DIR . '/includes/mpwc-functions.php' );
$mpwc_pro_options = mpwc_pro_get_settings();

// Plugin Post Type File
require_once( MPWC_PRO_DIR . '/includes/mpwc-post-types.php' );

// Script Class
require_once( MPWC_PRO_DIR . '/includes/class-mpwc-script.php' );

// Admin Class
require_once( MPWC_PRO_DIR . '/includes/admin/class-mpwc-admin.php' );

// Public Class
require_once( MPWC_PRO_DIR . '/includes/class-mpwc-public.php' );


// Load admin files
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
	require_once( MPWC_PRO_DIR . '/includes/admin/mpwc-how-it-work.php' );
}