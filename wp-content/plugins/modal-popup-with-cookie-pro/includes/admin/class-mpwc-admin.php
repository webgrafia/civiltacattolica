<?php
/**
 * Admin Class
 *
 * Handles the Admin side functionality of plugin
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Mpwc_Pro_Admin {
	
	function __construct() {

		// Action to add metabox
		add_action( 'add_meta_boxes', array($this, 'mpwc_pro_post_sett_metabox') );

		// Action to save metabox
		add_action( 'save_post', array($this, 'mpwc_pro_save_metabox_value') );
		
		// Action to register admin menu
		add_action( 'admin_menu', array($this, 'mpwc_pro_register_menu') );

		// Action to register plugin settings
		add_action ( 'admin_init', array($this,'mpwc_pro_register_settings') );

		// Filter to add row data
		add_filter( 'post_row_actions', array($this, 'mpwc_pro_post_row_action'), 10, 2 );

		//Add action to duplicate modap popup
		add_action( 'admin_action_mpwc_pro_duplicate_post_as_draft', array($this,'mpwc_pro_duplicate_post_as_draft' ));

		// Action to add custom column to Slider listing
		add_filter( 'manage_'.MPWC_PRO_POPUP_POST_TYPE.'_posts_columns', array($this, 'mpwc_pro_posts_columns') );

		// Action to add custom column data to Slider listing
		add_action('manage_'.MPWC_PRO_POPUP_POST_TYPE.'_posts_custom_column', array($this, 'mpwc_pro_post_columns_data'), 10, 2);

		// Filter to add plugin links
		add_filter( 'plugin_row_meta', array( $this, 'mpwc_pro_plugin_row_meta' ), 10, 2 );
		
	}

	/**
	 * Post Settings Metabox
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.2.5
	 */
	function mpwc_pro_post_sett_metabox() {
		// Add metabox in popup posts
		add_meta_box( 'mpwc-pro-post-sett', __( 'Modal Popup Pro - Settings', 'modal-popup-with-cookie' ), array($this, 'mpwc_pro_popup_sett_metabox_content'), MPWC_PRO_POPUP_POST_TYPE, 'normal', 'high' );

		// Add metabox on  page  and posts for custom popup
		$mpwc_support_post_type = mpwc_pro_get_option('mpwc_pro_post_types');
		if(!empty($mpwc_support_post_type)){
			add_meta_box( 'mpwc-pro-popup-sett', __( 'Modal Popup Pro - Select Popup', 'modal-popup-with-cookie' ), array($this, 'mpwc_pro_popup_select_cnt'), $mpwc_support_post_type, 'normal', 'high' );
		}	
	}

	/**
	 * Post Settings Metabox HTML
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_popup_sett_metabox_content()
	{
		include_once( MPWC_PRO_DIR .'/includes/admin/metabox/mpwc-popup-setting.php');
	}

	/**
	 * Post,Page Select Modal Poup Metabox HTML
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_popup_select_cnt()
	{
		include_once( MPWC_PRO_DIR .'/includes/admin/metabox/mpwc-popup-selectpopup.php');
	}

	/**
	 * Function to save metabox values
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_save_metabox_value( $post_id )
	{
		
		global $post_type;
		$prefix = MPWC_PRO_META_PREFIX; // Taking metabox prefix
		// Taking popup id from page and post
		if(isset($_POST[$prefix.'welcome_popupid']))
		{			
			$welcome_popupid	= mpwc_pro_slashes_deep($_POST[$prefix.'welcome_popupid']);
			$exit_popupid		= mpwc_pro_slashes_deep($_POST[$prefix.'exit_popupid']);

			update_post_meta($post_id, $prefix.'welcome_popupid', $welcome_popupid);
			update_post_meta($post_id, $prefix.'exit_popupid', $exit_popupid);
		}
		
		if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )                	// Check Autosave
		|| ( ! isset( $_POST['post_ID'] ) || $post_id != $_POST['post_ID'] )  	// Check Revision
		|| ( $post_type !=  MPWC_PRO_POPUP_POST_TYPE ) )              					// Check if current post type is supported.
		{
		  return $post_id;
		}		
		
		// Taking variables
		$designs 						= isset($_POST[$prefix.'designs']) ? mpwc_pro_slashes_deep($_POST[$prefix.'designs']) : '';
		$popuptype 						= isset($_POST[$prefix.'popuptype']) ? mpwc_pro_slashes_deep($_POST[$prefix.'popuptype']) : '';
		$main_heading 					= isset($_POST[$prefix.'main_heading']) ? mpwc_pro_slashes_deep($_POST[$prefix.'main_heading']) : '';
		$sub_heading 					= isset($_POST[$prefix.'sub_heading']) ? mpwc_pro_slashes_deep($_POST[$prefix.'sub_heading']) : '';
		$action_btn_txt 				= isset($_POST[$prefix.'action_btn_txt']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_txt']) : '';
		$action_btn_link 				= isset($_POST[$prefix.'action_btn_link']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_link']) : '';
		$when_popup_appear 				= isset($_POST[$prefix.'when_popup_appear']) ? mpwc_pro_slashes_deep($_POST[$prefix.'when_popup_appear']) : '';
		$delay 							= isset($_POST[$prefix.'delay']) ? mpwc_pro_slashes_deep($_POST[$prefix.'delay']) : '';
		$x_second 						= isset($_POST[$prefix.'x_second']) ? mpwc_pro_slashes_deep($_POST[$prefix.'x_second']) : '';
		$x_scroll 						= isset($_POST[$prefix.'x_scroll']) ? mpwc_pro_slashes_deep($_POST[$prefix.'x_scroll']) : '';
		$disappear 						= isset($_POST[$prefix.'disappear']) ? mpwc_pro_slashes_deep($_POST[$prefix.'disappear']) : '';
		$exptime 						= isset($_POST[$prefix.'exptime']) ? mpwc_pro_slashes_deep($_POST[$prefix.'exptime']) : '';
		$hideclsbtn 					= isset($_POST[$prefix.'hideclsbtn']) ? mpwc_pro_slashes_deep($_POST[$prefix.'hideclsbtn']) : '';
		$clsonesc 						= isset($_POST[$prefix.'clsonesc']) ? mpwc_pro_slashes_deep($_POST[$prefix.'clsonesc']) : 0;
		$height 						= isset($_POST[$prefix.'height']) ? mpwc_pro_slashes_deep($_POST[$prefix.'height']) : '';
		$width 							= isset($_POST[$prefix.'width']) ? mpwc_pro_slashes_deep($_POST[$prefix.'width']) : '';
		$backgroundclr 					= isset($_POST[$prefix.'backgroundclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'backgroundclr']) : '';	
		$overlay_backgroundclr 			= isset($_POST[$prefix.'overlay_backgroundclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'overlay_backgroundclr']) : '';
		$overlay_background_opacity 	= isset($_POST[$prefix.'overlay_background_opacity']) ? mpwc_pro_slashes_deep($_POST[$prefix.'overlay_background_opacity']) : '';		
		$backgroundimg 					= isset($_POST[$prefix.'backgroundimg']) ? mpwc_pro_slashes_deep($_POST[$prefix.'backgroundimg']) : '';
		$backgroundimg_repeat 			= isset($_POST[$prefix.'backgroundimg_repeat']) ? mpwc_pro_slashes_deep($_POST[$prefix.'backgroundimg_repeat']) : '';		
		$main_head_font_size 			= isset($_POST[$prefix.'main_head_font_size']) ? mpwc_pro_slashes_deep($_POST[$prefix.'main_head_font_size']) : '';
		$main_head_fontclr 				= isset($_POST[$prefix.'main_head_fontclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'main_head_fontclr']) : '';
		$sub_head_font_size 			= isset($_POST[$prefix.'sub_head_font_size']) ? mpwc_pro_slashes_deep($_POST[$prefix.'sub_head_font_size']) : '';
		$sub_head_fontclr 				= isset($_POST[$prefix.'sub_head_fontclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'sub_head_fontclr']) : '';
		$action_btn_link_fontclr 		= isset($_POST[$prefix.'action_btn_link_fontclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_link_fontclr']) : '';
		$action_btn_link_fontsize 		= isset($_POST[$prefix.'action_btn_link_fontsize']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_link_fontsize']) : '';
		$action_btn_link_hover_fontclr 	= isset($_POST[$prefix.'action_btn_link_hover_fontclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_link_hover_fontclr']) : '';
		$action_btn_link_bgclr 			= isset($_POST[$prefix.'action_btn_link_bgclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_link_bgclr']) : '';
		$action_btn_link_hover_bgclr 	= isset($_POST[$prefix.'action_btn_link_hover_bgclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'action_btn_link_hover_bgclr']) : '';
		$fontclr 						= isset($_POST[$prefix.'fontclr']) ? mpwc_pro_slashes_deep($_POST[$prefix.'fontclr']) : '';
		$border_width 					= isset($_POST[$prefix.'border_width']) ? mpwc_pro_slashes_deep($_POST[$prefix.'border_width']) : '';
		$border_radius 					= isset($_POST[$prefix.'border_radius']) ? mpwc_pro_slashes_deep($_POST[$prefix.'border_radius']) : '';
		$border_color					= isset($_POST[$prefix.'border_color']) ? mpwc_pro_slashes_deep($_POST[$prefix.'border_color']) : '';
		$custom_css						= isset($_POST[$prefix.'custom_css']) ? mpwc_pro_slashes_deep($_POST[$prefix.'custom_css']) : '';
		$custom_js						= isset($_POST[$prefix.'custom_js']) ? mpwc_pro_slashes_deep($_POST[$prefix.'custom_js']) : '';

		//Save OR Update postmeta
		update_post_meta($post_id, $prefix.'designs', $designs);
		update_post_meta($post_id, $prefix.'popuptype', $popuptype);
		update_post_meta($post_id, $prefix.'main_heading', $main_heading);
		update_post_meta($post_id, $prefix.'sub_heading', $sub_heading);
		update_post_meta($post_id, $prefix.'action_btn_txt', $action_btn_txt);
		update_post_meta($post_id, $prefix.'action_btn_link', $action_btn_link);
		update_post_meta($post_id, $prefix.'when_popup_appear', $when_popup_appear);
		update_post_meta($post_id, $prefix.'x_second', $x_second);
		update_post_meta($post_id, $prefix.'x_scroll', $x_scroll);
		update_post_meta($post_id, $prefix.'delay', $delay);
		update_post_meta($post_id, $prefix.'disappear', $disappear);		
		update_post_meta($post_id, $prefix.'exptime', $exptime);
		update_post_meta($post_id, $prefix.'hideclsbtn', $hideclsbtn);
		update_post_meta($post_id, $prefix.'clsonesc', $clsonesc);
		update_post_meta($post_id, $prefix.'height', $height);
		update_post_meta($post_id, $prefix.'width', $width);
		update_post_meta($post_id, $prefix.'backgroundclr', $backgroundclr);
		update_post_meta($post_id, $prefix.'backgroundimg', $backgroundimg);
		update_post_meta($post_id, $prefix.'backgroundimg_repeat', $backgroundimg_repeat);
		update_post_meta($post_id, $prefix.'overlay_backgroundclr', $overlay_backgroundclr);
		update_post_meta($post_id, $prefix.'overlay_background_opacity', $overlay_background_opacity);		
		update_post_meta($post_id, $prefix.'main_head_font_size', $main_head_font_size);
		update_post_meta($post_id, $prefix.'main_head_fontclr', $main_head_fontclr);
		update_post_meta($post_id, $prefix.'sub_head_font_size', $sub_head_font_size);
		update_post_meta($post_id, $prefix.'sub_head_fontclr', $sub_head_fontclr);
		update_post_meta($post_id, $prefix.'action_btn_link_fontclr', $action_btn_link_fontclr);
		update_post_meta($post_id, $prefix.'action_btn_link_fontsize', $action_btn_link_fontsize);
		update_post_meta($post_id, $prefix.'action_btn_link_hover_fontclr', $action_btn_link_hover_fontclr);
		update_post_meta($post_id, $prefix.'action_btn_link_bgclr', $action_btn_link_bgclr);
		update_post_meta($post_id, $prefix.'action_btn_link_hover_bgclr', $action_btn_link_hover_bgclr);
		update_post_meta($post_id, $prefix.'fontclr', $fontclr);
		update_post_meta($post_id, $prefix.'border_width', $border_width);
		update_post_meta($post_id, $prefix.'border_radius', $border_radius);
		update_post_meta($post_id, $prefix.'border_color', $border_color);
		update_post_meta($post_id, $prefix.'custom_css', $custom_css);
		update_post_meta($post_id, $prefix.'custom_js', $custom_js);
	}

	/**
	 * Function to register admin menus
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_register_menu() {
		add_submenu_page( 'edit.php?post_type='.MPWC_PRO_POPUP_POST_TYPE, __('Settings', 'modal-popup-with-cookie'), __('Settings', 'modal-popup-with-cookie'), 'manage_options', 'mpwc-pro-settings', array($this, 'mpwc_pro_settings_page') );
	}

	/**
	 * Function to handle the setting page html
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_settings_page() {
		include_once( MPWC_PRO_DIR . '/includes/admin/settings/mpwc-settings.php' );
	}

	/**
	 * Function register setings
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_register_settings(){
		register_setting( 'mpwc_pro_plugin_options', 'mpwc_pro_options', array($this, 'mpwc_pro_validate_options') );
	}

	/**
	 * Validate Settings Options
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_validate_options( $input ) {
		if(isset($_REQUEST['mpwc-settings-submit'])){
		$input['mpwc_pro_enable'] 						= isset($input['mpwc_pro_enable']) 	? 1 : 0;
		$input['mpwc_pro_cookie_prefix'] 				= isset($input['mpwc_pro_cookie_prefix']) 	? $input['mpwc_pro_cookie_prefix']: '';
		$input['mpwc_pro_enable_mobile'] 				= isset($input['mpwc_pro_enable_mobile']) 	? $input['mpwc_pro_enable_mobile']: 0;
		$input['mpwc_pro_post_types']					= isset($input['mpwc_pro_post_types']) ? $input['mpwc_pro_post_types']: array();
		$input['mpwc_pro_default_popup'] 				= isset($input['mpwc_pro_default_popup']) 	? $input['mpwc_pro_default_popup']: 0;
		$input['mpwc_pro_display_in'] 					= isset($input['mpwc_pro_display_in']) 	? $input['mpwc_pro_display_in']: array();
		$input['mpwc_pro_exit_default_popup'] 			= isset($input['mpwc_pro_exit_default_popup']) 	? $input['mpwc_pro_exit_default_popup']: 0;
		$input['mpwc_pro_exit_display_in'] 				= isset($input['mpwc_pro_exit_display_in']) 	? $input['mpwc_pro_exit_display_in']: array();
		$input['mpwc_pro_cookie_enable'] 				= isset($input['mpwc_pro_cookie_enable']) 	? 1: 0;
		
		}
		if(isset($_REQUEST['mpwc-settings-reset'])){
			$input = mpwc_pro_reset_default_settings();
		}
		return $input;
	}

	/**
	 * Function to add custom quick links at post listing page
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_post_row_action($actions, $post ) {
		
		if (current_user_can('edit_posts')) {
		$actions['mpwc_duplicate'] = '<a href="admin.php?action=mpwc_pro_duplicate_post_as_draft&post=' . $post->ID . '" title="Duplicate this item" rel="permalink">'. __('Duplicate','modal-popup-with-cookie').'</a>';
		}
		if( $post->post_type == MPWC_PRO_POPUP_POST_TYPE ) {
			return array_merge( array( 'mpwc_id' => 'ID: ' . $post->ID ), $actions );
		}
		return $actions;
	}

	function mpwc_pro_duplicate_post_as_draft()
	{
		global $wpdb;
		
		if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'mpwc_pro_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
			wp_die(__('No post to duplicate has been supplied!','modal-popup-with-cookie'));
		}

		/*
		 * get the original post id
		 */
		$post_id = (isset($_GET['post']) ? absint( $_GET['post'] ) : absint( $_POST['post'] ) );
		/*
		 * and all the original post data then
		 */
		$post = get_post( $post_id );

		$current_user = wp_get_current_user();
		$new_post_author = $current_user->ID;

		/*
		 * if post data exists, create the post duplicate
		 */
		if (isset( $post ) && $post != null) {

			/*
			 * new post data 
			 */
			$args = array(
				'comment_status' => $post->comment_status,
				'ping_status'    => $post->ping_status,
				'post_author'    => $new_post_author,
				'post_content'   => $post->post_content,
				'post_excerpt'   => $post->post_excerpt,
				'post_name'      => $post->post_name,
				'post_parent'    => $post->post_parent,
				'post_password'  => $post->post_password,
				'post_status'    => 'draft',
				'post_title'     => $post->post_title.' '.__('Duplicate','modal-popup-with-cookie'),
				'post_type'      => $post->post_type,
				'to_ping'        => $post->to_ping,
				'menu_order'     => $post->menu_order
			);

			/*
			 * insert the new duplicate post by wp_insert_post() function
			 */
			$new_post_id = wp_insert_post( $args );
			
			$post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
			if (count($post_meta_infos)!=0) {
				$sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
				foreach ($post_meta_infos as $meta_info) {
					$meta_key = $meta_info->meta_key;
					$meta_value = addslashes($meta_info->meta_value);
					$sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
				}
				$sql_query.= implode(" UNION ALL ", $sql_query_sel);

				$wpdb->query($sql_query);
			}

			/*
			 * redirect to the edit post screen for the new draft
			 */
			wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
			exit;
		}
		else{
			wp_die(__('Post creation failed, could not find original post: ','modal-popup-with-cookie') . $post_id);
		}
	}

	/**
	 * Add custom column to Post listing page
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_posts_columns( $columns ){

		$new_columns['mpwc_popup_type'] 	= __( 'When Appear', 'modal-popup-with-cookie' );
	    
	    $columns = mpwc_pro_add_array( $columns, $new_columns, 1, true );

	    return $columns;
	}

	/**
	 * Add custom column data to Post listing page
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_post_columns_data( $column, $post_id ) {

		global $post;

	    switch ($column) {
			case 'mpwc_popup_type':				
				echo mpwc_get_popup_type( $post_id);
				break;
		}
	}

	/**
	 * Function to plugin row meta
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_plugin_row_meta( $links, $file ) {
		
		if ( $file == MPWC_PRO_PLUGIN_BASENAME ) {
			
			$row_meta = array(
				'docs'    => '<a href="' . esc_url('https://www.wponlinesupport.com/pro-plugin-document/document-modal-popup-with-cookie-pro') . '" title="' . esc_attr( __( 'View Documentation', 'modal-popup-with-cookie' ) ) . '" target="_blank">' . __( 'Docs', 'modal-popup-with-cookie' ) . '</a>',
				'support' => '<a href="' . esc_url('https://www.wponlinesupport.com/welcome-wp-online-support-forum/') . '" title="' . esc_attr( __( 'Visit Customer Support Forum', 'modal-popup-with-cookie' ) ) . '" target="_blank">' . __( 'Support', 'modal-popup-with-cookie' ) . '</a>',
			);
			return array_merge( $links, $row_meta );
		}
		return (array) $links;
	}	
}

$mpwc_pro_admin = new Mpwc_Pro_Admin();