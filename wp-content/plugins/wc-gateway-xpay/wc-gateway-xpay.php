<?php
/*
 * Plugin Name: WooCommerce X-Pay / Quipago (CartaSì)
 * Plugin URI: http://www.mauromascia.com/shop/product/woocommerce-xpay-payment-gateway-cartasi/
 * Description: Redirect Gateway X-Pay / QuiPago (CartaSì) per WooCommerce.
 * Version: 20160803
 * Author: Mauro Mascia
 * Author URI: http://www.mauromascia.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Support: info@mauromascia.com
 * Copyright: © 2013-2016 MAURO MASCIA
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

include_once( 'mmlib/mmascia-wc-lib_20160615.php' );
include_once( 'inc/class-wc-gateway-xpay-helper.php' );

add_action( 'plugins_loaded', 'init_wc_gateway_xpay' );
function init_wc_gateway_xpay() {
  $mmlib = new MMascia_WC_Lib_20160615( __FILE__ );

  if ( ! class_exists( 'WC_Payment_Gateways' ) ) return;
  if ( ! $mmlib->check_fatal_suhosin( 'WooCommerce X-Pay / Quipago (CartaSì)' ) ) return;
  if ( ! $mmlib->is_wc_gte_21() ) return;

  $mmlib->mmascia_start_uvs();

  /**
   * Add the gateway to WooCommerce.
   */
  add_filter( 'woocommerce_payment_gateways', 'add_wc_gateway_xpay' );
  function add_wc_gateway_xpay( $methods ) {
    $methods[] = 'WC_Gateway_XPay';
    return $methods;
  }

  class WC_Gateway_XPay extends WC_Payment_Gateway {

    public function __construct() {
      // Load my lib
      $mmlib = new MMascia_WC_Lib_20160615( __FILE__ );

      $this->mmlib               = $mmlib;
      $this->plugin_url          = $mmlib->plugin_url;
      $this->plugin_path         = $mmlib->plugin_path;
      $this->id                  = $mmlib->plugin_slug;
      $this->plugin_slug_dashed  = $mmlib->plugin_slug_dashed;

      $this->textdomain          = $this->id;
      $this->logfile             = $this->id;
      
      $this->logo                = $this->plugin_url.'images/xpay.png';
      $this->icon                = apply_filters( 'wc_gateway_xpay_cards_icon_path', $this->plugin_url.'images/xpay-cards.png' );
      $this->method_title        = apply_filters('wc-gateway-xpay-method-title', 'XPay (Cartasi)');
      $this->error_messages      = array();

      $this->mmlib->__init_gateway( $this );

      // Don't output a payment_box containing direct payment form
      $this->has_fields = false;

      // Define user set variables.
      $this->title               = $this->settings['title'];
      $this->description         = $this->settings['description'];
      $this->alias               = $this->settings['alias'];
      $this->mac                 = $this->settings['mac'];
      $this->max_retry           = ! empty( $this->settings['max_retry'] ) ? $this->settings['max_retry'] : -1;
      $this->empty_cart_on_ko    = isset( $this->settings['empty_cart_on_ko']) && $this->settings['empty_cart_on_ko'] == "yes" ? true : false;
      $this->order_prefix        = WC_Gateway_XPay_Helper::get_cleaned_prefix( $this->settings['order_prefix'] );

      $this->is_sandbox          = $this->settings['is_sandbox'] == "yes" ? true : false;
      $this->liveurl             = WC_Gateway_XPay_Helper::get_live_url( $this->is_sandbox );

      $this->show_order_message  = $this->settings['show_order_message'] == "yes" ? true : false;
      $this->debug               = $this->settings['debug'] == "yes" ? true : false;

      $this->mmlib->set_gw( $this );

      $this->supports = array(
        'products',
        'refunds'
      );

      // Actions
      add_action( 'woocommerce_receipt_'.$this->id, array( $this, 'receipt_page' ) );
      add_action( 'woocommerce_update_options_payment_gateways_'.$this->id, array( $this, 'process_admin_options' ) );

      $this->check_gateway_response();
    }

    /**
     * Check if this gateway is enabled and available in the base currency being traded with.
     */
    function is_valid_for_use() {
      if ( $this->enabled != 'yes' ) {
        return false;
      }

      // Set available currencies
      $allowed_currency_codes = array(
        'EUR' => 'Euro' // ONLY EURO AT THE MOMENT
      );

      $woocommerce_currency = get_option( 'woocommerce_currency' );

      if ( ! array_key_exists( $woocommerce_currency, $allowed_currency_codes ) ) {
        $err = 'La valuta impostata su WooCommerce è ' . $woocommerce_currency .
          ' mentre con XPay è possibile utilizzare ' . implode( ', ', array_keys( $allowed_currency_codes ) );
        $this->mmlib->log_add( $err );
        return false;
      }

      if ( empty( $this->alias ) || empty( $this->mac ) ) {
        $this->mmlib->log_add( 'ATTENZIONE: è necessario valorizzare i campi Alias e chiave di MAC.' );
        return false;
      }

      return true;
    }

    /**
     * Admin Panel Options
     */
    public function admin_options() {
      ?>
        <div class="mmascia-gateway-admin-main">
          <div class="mmascia-gateway-message">
            <a href="http://www.mauromascia.com" alt="by Mauro Mascia" target="_blank">
              <img src="<?php echo $this->mmlib->mmlogo; ?>" title="by Mauro Mascia" id="mmascia-logo"/>
            </a>
            <img src="<?php echo $this->logo; ?>" id="mmascia-gateway-logo"/>
            <br>
            <p>
              <?php _e( "Accept payments from credit/debit cards through the XPay Virtual POS. After adding the order informations, the customer will be redirected on the secure XPay's page to finish the transaction, entering his credit card's informations. These credit card informations will not be stored anywhere in this system, but will be handled solely and exclusively by XPay.", $this->textdomain ); ?>
            </p>
          </div>
          <br>
          <div class="mmascia-gateway-message mmascia-gateway-form">
              <table class="form-table"><?php $this->generate_settings_html(); ?></table>
          </div>
        </div>
      <?php
    }

    /**
     * Generate the receipt page.
     */
    function receipt_page( $order_id ) {
      $order = $this->mmlib->wc_get_order( $order_id );
      $input_params = $this->get_params( $order );
      echo $this->mmlib->get_gw_form( $this->liveurl, "get", $input_params, $order );
    }

    /**
     * Define parameters.
     *
     * @param object $order
     */
    function get_params( $order ) {
      // Remove the decimals: 23,50 must become 2350
      $total = (float)$order->order_total * 100;

      // Set the order ID with prefix
      $codTrans = $this->set_order_id( $order->id, $order, true );

      // Calculate the mac to be sent
      $sha1_str = 'codTrans=' . $codTrans . 'divisa=EURimporto=' . $total . $this->mac;
      $this->mac_full = sha1( $sha1_str );

      $this->mmlib->log_add( "[get_params]: SHA1 before: " . $sha1_str );
      $this->mmlib->log_add( "[get_params]: SHA1 after: " . $this->mac_full );

      return apply_filters('wc-gateway-xpay-get-parameters', array(
        'alias'       => $this->alias,
        'mac'         => $this->mac_full,
        'importo'     => $total,
        // Il codice della divisa in cui l'importo è espresso. Unico valore ammesso: EUR (Euro).
        'divisa'      => 'EUR',
        'codTrans'    => $codTrans,
        'languageId'  => WC_Gateway_XPay_Helper::get_language( $this->mmlib ),
        'mail'        => $order->billing_email,
        'url'         => WC_Gateway_XPay_Helper::get_url_ok( $order, $this->mmlib ),
        'url_back'    => WC_Gateway_XPay_Helper::get_url_ko( $order, $this->mmlib ),
      ), $order );
    }

    /**
     * Process the payment and return the result
     */
    function process_payment( $order_id ) {
      $this->mmlib->log_add( "[process_payment]: Processing payment for order n. " . $order_id );

      $order = new WC_Order( $order_id );
      $params = $this->get_params( $order );
      $query_params = http_build_query( $params, '', '&' );

      return array(
        'result' => 'success',
        'redirect' => $this->liveurl . '?' . $query_params,
      );

    }

    /**
     * Process a refund if supported.
     * @param  int    $order_id
     * @param  float  $amount
     * @param  string $reason
     * @return bool True or false based on success, or a WP_Error object
     */
    public function process_refund( $order_id, $amount = null, $reason = '' ) {
      include_once( 'inc/class-wc-gateway-xpay-refund.php' );
      return WC_Gateway_XPay_Refund::process_xpay_refund( $order_id, $amount, $reason, $this );
    }

    function id_incrementale( $order_id, $add_incr = false ) {
      // Aggiungo un id incrementale in modo che in caso di annullo XPay veda un id differente e non dia errori
      $id_incr = get_post_meta( $order_id, '_xpay_annullo_id_incr', true );
      
      if (empty($id_incr)) {
        $id_incr = 1;
      }
      
      if ($add_incr) {
        $id_incr+= 1;
        update_post_meta( $order_id, '_xpay_annullo_id_incr', $id_incr );
      }

      return $id_incr;
    }

    /**
     * Sets a prefixed ID, useful if the same XPay account is used in multiple
     * stores. This prevent to process the same order ID and prevents the
     * error: "duplicate order".
     */
    function set_order_id( $order_id, $order = false, $add_incr = false ) {

      // Sequential Order Number Pro Compatibility
      if ( function_exists( 'wc_seq_order_number_pro' ) && is_object( $order ) ) {
        $id = $order->get_order_number();
      }
      else {
        $id = $this->order_prefix . $order_id;
      }

      $id_incr = $this->id_incrementale( $order_id, $add_incr );

      $id = $id.'r'.sprintf('%03d', $id_incr);
      $this->mmlib->log_add( "[set_order_id] SET ID incrementale $id per ordine $order_id" );

      return $id;
    }

    /**
     * Check for valid response
     */
    function check_gateway_response() {

      if ( !empty( $_GET['esito'] ) && !empty( $_GET['mac'] ) && !empty( $_GET['codTrans'] ) ) {

        $wc_order_id = WC_Gateway_XPay_Helper::get_order_id( $_GET['codTrans'], $this->order_prefix, $this->mmlib );

        if ( $wc_order_id ) {

          $order = $this->mmlib->wc_get_order( $wc_order_id );
          $codTrans = $this->set_order_id( $order->id, $order );

          if ( $order && $order->status !== 'cancelled' ) {
            if ( $_GET['esito'] == 'OK' ) {
              // Aggiunto per supporto ad XPay addon.
              $sid = isset($_GET['session_id']) && $_GET['session_id'] != 'null' ? $_GET['session_id'] : '';
              // Check the response code
              // @see 8.2. "mac messaggio esito pagamento" on pdf docs
              $authcode = 'codTrans=' . $codTrans . $sid;
              $authcode.= 'esito=' . $_GET['esito'];
              $authcode.= 'importo=' . $_GET['importo'];
              $authcode.= 'divisa=' . $_GET['divisa'];
              $authcode.= 'data=' . $_GET['data'];
              $authcode.= 'orario=' . $_GET['orario'];
              $authcode.= 'codAut=' . $_GET['codAut'];
              $mac_full = sha1( $authcode . $this->mac );

              do_action( 'wc-gateway-xpay-gateway-response', $_GET, $wc_order_id );

              // Check the authenticity of the received parameters.
              if ( $mac_full == $_GET['mac'] ) {
                if ( $order->status !== 'completed' ) {
                  if ( $order->status == 'processing' ) {
                    // This is the second call - do nothing
                  }
                  else {
                    $order_link = '<a href="' . $this->mmlib->wc_url( 'view_order', $order ) . '">' . $order->id . '</a>';
                    $this->msg['message'] = sprintf( __( 'Thank you for shopping with us. Your transaction %s has been processed correctly. We will ship your order to you soon.', $this->textdomain ), $order_link );
                    $this->msg['class'] = 'woocommerce_message';
                    $mes = sprintf( __( 'The order %s was placed successfully.', $this->textdomain ), $codTrans );

                    // Update order status, add admin order note and empty the cart
                    $this->mmlib->wc_order_completed( $order, $mes, $_GET['codAut'] );

                    update_post_meta( $order->id, '_wc_gestpay_xpay_cod_aut', sanitize_text_field($_GET['codAut']) );

                    $this->redirect( $order );
                  }
                }
              }
              else {
                $this->msg['class'] = 'woocommerce_error';
                $this->msg['message'] = 'ERRORE - MAC differenti: ' . $codTrans . " [{$_GET['data']} {$_GET['orario']}]";

                $this->mmlib->log_add( "[check_gateway_response] " . $this->msg['message'] );
              }
            }
            else if ( $_GET['esito'] == 'ANNULLO' ) {
              // L'utente ha premuto il bottone per annullare, lo riporteremo nella pagina di pagamento.
              
              $mes = "Ordine n. " . $order->id . " annullato dall'utente [" . date("Y-m-d H:i:s") . "]";
              $this->mmlib->log_add( "[check_gateway_response] " . $mes );
              $order->add_order_note( $mes );
            }
            else { // $_GET['esito'] == 'ERRORE' o $_GET['esito'] == 'KO'

              $this->mmlib->log_add( "[check_gateway_response] GET: " . var_export( $_GET, true ) );

              // XPay allows a max number of retries per order when it fails
              $order_wp_option = str_replace( '-', '_', $this->id ) . "__" . $order->id;
              $order_wp_option_val = get_option( $order_wp_option, $default = false );

              if ( !empty( $order_wp_option_val ) ) {
                $order_wp_option_val += 1;
                update_option( $order_wp_option, $order_wp_option_val );
              }
              else {
                $order_wp_option_val = 1;
                add_option( $order_wp_option, $order_wp_option_val );
              }

              $this->mmlib->log_add( "[check_gateway_response] Max retry: " . $this->max_retry );

              if ( $this->max_retry > 0 && $order_wp_option_val >= $this->max_retry ) {
                $this->msg['message'] = sprintf( __( 'The order of %s has been canceled due to exceeding the threshold of %s attempts of the order payment.', $this->textdomain ), $codTrans, $this->max_retry > 0 ? $this->max_retry : 3 );
                delete_option( $order_wp_option );
                $order->cancel_order( $this->msg['message'] );
              }
              else {
                // add admin order note
                $this->msg['message'] = sprintf( __( 'We are sorry but the order %s is failed and was cancelled (reason: %s).', $this->textdomain ), $codTrans, $_GET['messaggio'] );
              }

              $this->msg['class'] = 'woocommerce_error';
              $order->add_order_note( $this->msg['message'] );
              $this->mmlib->log_add( "[check_gateway_response] ERROR: " . $this->msg['message'] );

              if ( $this->empty_cart_on_ko ) {
                $this->mmlib->wc_empty_cart();
              }

              // Add autosubmit form
              if ( $this->show_order_message ) {
                wc_add_notice($this->msg['message'], 'error');
              }

              $this->redirect( $order, false );
            }
          }
          else {
            $this->msg['class'] = 'woocommerce_error';
            $this->msg['message'] = sprintf( __( 'We are sorry but the order %s is failed and was cancelled (reason: %s).', $this->textdomain ), $codTrans, $_GET['messaggio'] );
          }
        }
        else {
          $this->msg['class'] = 'woocommerce_error';
          $this->msg['message'] = "ERRORE: La rimozione del prefisso dall'ID dell'ordine è fallita. Si provi ad utilizzare un prefisso differente per l'ordine.";
        }

        if ( $this->show_order_message ) {
          add_action( 'the_content', array( &$this, 'show_message' ) );
        }

      }
      elseif ( !empty( $_SERVER['REQUEST_URI'] ) && !isset( $_SERVER['XPAY_REFRESHED'] ) && strpos( $_SERVER['REQUEST_URI'], 'codTrans' ) ) {
        // Alcuni utenti hanno lamentato un problema di non ricezione dei parametri in GET.
        // Un workaround che ho trovato è quello di prelevare le info dalla REQUEST_URI.
        $request_uri = parse_url( $_SERVER['REQUEST_URI'] );
        
        if ( is_array( $request_uri ) && !empty( $request_uri['query'] ) ) {
          parse_str( $request_uri['query'], $query );

          if ( !empty( $query['esito'] ) && !empty( $query['mac'] ) && !empty( $query['codTrans'] ) ) {

            $_GET = $query;

            $_SERVER['XPAY_REFRESHED'] = true;

            $this->check_gateway_response();
          }
        }
      }
    }

    function redirect( $order, $ok = true ) {
      $url = $ok ? WC_Gateway_XPay_Helper::get_url_ok( $order, $this->mmlib ) : WC_Gateway_XPay_Helper::get_url_ko( $order, $this->mmlib );
      wp_redirect( $url );
      die();
    }

    function show_message( $content = "" ) {
      if ( isset( $this->msg ) ) {
        $message = <<<HTML
          <div class="xpay-box {$this->msg['class']}">
            {$this->msg['message']}
          </div>
HTML;
      }
      else {
        $message = '';
      }

      return $message . $content;
    }

  }

}

add_action( 'init', 'wc_gateway_xpay_check_gateway_response_new_wc', 999 );
function wc_gateway_xpay_check_gateway_response_new_wc() {
  if ( isset( $_GET['esito'] ) && isset($_GET['codTrans']) && isset( $_GET['codAut'] ) ) {
    $xpay = new WC_Gateway_XPay();
    $xpay->check_gateway_response();
  }
}