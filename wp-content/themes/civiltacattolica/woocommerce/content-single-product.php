<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>


<div id="product-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="row">
        <div class="col-md-12">

            <article>
                <div class="article_wrapper" >

                        <?php
                        $terms = wp_get_post_terms($post->ID, "product_cat");
                        foreach($terms as $term){
                            if(($term->slug == "pubblicazioni") || ($term->slug == "accenti")  || ($term->slug == "escritos-fiorito")){

?>
                    <style type="text/css">
                        .images{
                            width: 200px !important;
                            float:right !important;
                            margin-left:20px !important;
                            margin-top:20px !important;
                        }


                        @media screen and (max-width: 500px){
                            .images{
                                width: 100px !important;
                                margin:0px !important;
                            }
                            .images img{
                                width: 100% !important;
                                height: auto !important;
                            }

                            }
                    </style>
                    <?php
                                do_action( 'woocommerce_before_single_product_summary' );


                            }
                        }
                        do_action( 'woocommerce_single_product_summary' );
                        ?>


                </div>
            </article>




</div>
    </div>

    <meta itemprop="url" content="<?php the_permalink(); ?>" />

</div><!-- #product-<?php the_ID(); ?> -->


