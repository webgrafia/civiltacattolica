<?php
if ( ! wp_next_scheduled( 'publish_quaderni_programmati' ) ) {
    wp_schedule_event( time(), 'hourly', 'publish_quaderni_programmati' );
}

add_action( 'publish_quaderni_programmati', 'publish_quaderni_programmati_function' );

function publish_quaderni_programmati_function() {
    // recupero tutti i quaderni che hanno il flag di pubblicazione posticipata
    $quaderni = get_terms('quaderno', array('hide_empty' => false, 'orderby' => "name", "order" => "desc",  "meta_query" => array(
        array(
            'key'       => 'pubblicazione_posticipata',
            'value'       => true,
            'compare'   => '=='
        )
    ),"number" => 100));

    foreach ($quaderni as $q){
        // controllo se la data di pubblicazione è scaduta
        $data = get_field("data_pubblicazione_posticipata", $q);
        if($data == ""){
            update_field("pubblicazione_posticipata", "", $q);
        }else{
            $timestamp_q = strtotime($data);
            $timestamp_now = time();
            if($timestamp_now > $timestamp_q){
                // azzero il flag
                update_field("pubblicazione_posticipata", "", $q);
                update_field("data_pubblicazione_posticipata", "", $q);
            }
        }

    }
}


