<?php
global $wp_query;
wp_enqueue_script('jquery-ui-datepicker');
wp_enqueue_style('jquery-style', get_bloginfo("template_url").'/css/datepicker.css');

get_template_part('includes/header');
$paged = get_query_var('paged') ? get_query_var('paged') : 1;


//echo $GLOBALS['wp_query']->request;

?>
<script type="text/javascript">
    jQuery(document).ready(function() {
        if(jQuery("#cerca_in_google_books:checked").val() == 1){
            jQuery("#autore").hide();
            jQuery("#search_from").hide();
            jQuery("#search_to").hide();
            jQuery(".onlytitle_div").hide();
            jQuery("#ptypes").hide();
        }
        jQuery( "#cerca_in_google_books" ).on( "click", showHideGbook );
        function showHideGbook(){
            if(jQuery("#cerca_in_google_books:checked").val() == 1){
                jQuery("#autore").hide();
                jQuery("#search_from").hide();
                jQuery("#search_to").hide();
                jQuery(".onlytitle_div").hide();
                jQuery("#ptypes").hide();

            }else{
                jQuery("#autore").show();
                jQuery("#search_from").show();
                jQuery("#search_to").show();
                jQuery(".onlytitle_div").show();
                jQuery("#ptypes").show();
            }
        }

        showHideAuthor();
        jQuery( "input[name=ptype]" ).on( "click", showHideAuthor );
        function showHideAuthor(){
            if(jQuery("input[name=ptype]:checked").val() == "articolo" || jQuery("input[name=ptype]:checked").val() == "recensione"){
                jQuery("#autore").show();
            }else{

                jQuery("#autore").hide();
            }

        }

    });
</script>
<main role="main">

    <div class="section section_grey_light">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="title_large padding-bottom-40">
                        <h1><?php if($_GET["s"] == "") echo "Ricerca Avanzata"; else echo "Risultati ricerca"; ?></h1>
                    </div>

                </div><!-- /col-md-12-->

            </div><!-- /row -->

            <div class="search_form form-group">

                <form method="get" action="<?php echo get_bloginfo("url"); ?>">



                        <div class="row margin-bottom-40">

                        <div class="col-md-6">

                            <input class="form-control" type="text" title="Cerca" placeholder="Cerca" name="s" value="<?php the_search_query(); ?>">

                        </div><!-- /col-md-6 -->

                        <div class="col-md-6">

                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5">
                                        <input type="checkbox" id="cerca_in_google_books" value="1" name="gbooks" <?php if($_GET["gbooks"] != "") echo " checked='checked' " ?>>
                                        <label for="cerca_in_google_books">Cerca in Google Books</label>
                                    </div>

                                </div><!-- /col-md-6 -->

                            </div><!-- /row -->

                        </div><!-- /col-md-6 -->

                    </div><!-- /row -->

                    <hr class="margin-bottom-40">
                    <div class="row margin-bottom-40" id="ptypes">

                        <div class="col-sm-12">
                            <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5 radioinline">
                                <input type="radio" id="pt_articolo" name="ptype" value="articolo" <?php if(($_GET["ptype"] == "") || ($_GET["ptype"] == "articolo")) echo " checked "; ?>>
                                <label for="pt_articolo">Articoli</label>
                            </div>
                            <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5 radioinline">
                                <input type="radio" id="pt_news" name="ptype" value="news" <?php if($_GET["ptype"] == "news") echo " checked "; ?>>
                                <label for="pt_news">News</label>
                            </div>
                            <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5 radioinline">
                                <input type="radio" id="pt_conferenza" name="ptype" value="conferenza" <?php if($_GET["ptype"] == "conferenza") echo " checked "; ?>>
                                <label for="pt_conferenza">Conferenze</label>
                            </div>
                            <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5 radioinline">
                                <input type="radio" id="pt_libri" name="ptype" value="libri" <?php if($_GET["ptype"] == "libri") echo " checked "; ?>>
                                <label for="pt_libri">Libri</label>
                            </div>
                            <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5 radioinline">
                                <input type="radio" id="pt_rassegna" name="ptype" value="rassegna" <?php if($_GET["ptype"] == "rassegna") echo " checked "; ?>>
                                <label for="pt_rassegna">Rassegna Stampa</label>
                            </div>
                            <div class="form-group form-group-checkbox margin-bottom-0 margin-top-5 radioinline">
                                <input type="radio" id="pt_recensione" name="ptype" value="recensione" <?php if($_GET["ptype"] == "recensione") echo " checked "; ?>>
                                <label for="pt_recensione">Recensioni</label>
                            </div>

                        </div>
                    </div>
                    <div class="row margin-bottom-50">

                        <div class="col-md-3 col-sm-3 onlytitle_div">
                            <input type="checkbox"  class="form-control"  id="onlytitle" name="onlytitle" value="1" <?php if($_GET["onlytitle"] == "1") echo "checked"; ?>/>
                            <label for="onlytitle">Cerca solo nel titolo</label>
                        </div>


                        <div class="col-md-3 col-sm-3">

                            <input type="text"  class="form-control" placeholder="A partire dal" id="search_from" name="search_from" value="<?php echo $_GET["search_from"]; ?>"/>

                            <script type="text/javascript">

                                jQuery(document).ready(function() {
                                    jQuery('#search_from').datepicker({
                                        dateFormat : 'dd-mm-yy'
                                    });
                                });

                            </script>

                        </div><!-- /col-md-3 -->

                        <div class="col-md-3 col-sm-3">
                            <input type="text"  class="form-control" placeholder="Fino al" id="search_to" name="search_to" value="<?php echo $_GET["search_to"]; ?>"/>

                            <script type="text/javascript">

                                jQuery(document).ready(function() {
                                    jQuery('#search_to').datepicker({
                                        dateFormat : 'dd-mm-yy'
                                    });
                                });

                            </script>

                        </div><!-- /col-md-3 -->
                        <div class="col-md-3 col-sm-3">
		                    <?php

		                    add_filter('wp_dropdown_users_args', 'cc_filter_wp_dropdown_users_args');
		                    cc_wp_dropdown_users("name=autore&selected=".$_GET["autore"]."&class=form-control&show_option_none=Seleziona un autore&show=last_first");
		                    remove_filter('wp_dropdown_users_args', 'cc_filter_wp_dropdown_users_args');

		                    ?>
                        </div><!-- /col-md-3 -->




                    </div><!-- /row -->
                    <div class="row margin-bottom-50">
                        <div class="col-md-8 col-sm-8">
                        <input type="submit" class="btn btn-default btn-lg btn-block" value="Filtra">
                        </div>
                    </div>
                </form>

            </div>


            <div class="row margin-bottom-20">

                <div class="col-md-12">
                    <?php
                    if($_GET["gbooks"] == 1) {
                        require_once('functions/gbooksearch.php');
                        $search = new Gbooksearch();

                        $searchresults = $search->searchBooks("\"la civiltà cattolica\" ".$_GET["s"], array("maxResults" => 10));

                        if($searchresults) {
                            $c=0;
                            foreach($searchresults["items"] as $gbook){
                                $c++;
                 //               if(($c == 1) || ($c == 4) || ($c == 7))
                   //                 echo "<div class='row'>";
                                ?>
                                <div class="article_simple">
                                    <p class="date"><?php echo $gbook["volumeInfo"]["publishedDate"] ?></p>
                                    <h3><a href="<?php echo $gbook["volumeInfo"]["canonicalVolumeLink"]; ?>"><?php echo $gbook["volumeInfo"]["title"]; ?></a></h3>
                                    <?php if(trim($gbook["searchInfo"]["textSnippet"]) != ""){
                                        ?>
                                        <p>...<?php echo $gbook["searchInfo"]["textSnippet"];  ?>...</p>
                                    <?php
                                    } ?>
                                    <p class="author"><?php
                                        $a=0;
                                        foreach ($gbook["volumeInfo"]["authors"] as $author) {
                                            if($a) echo ", ";
                                            echo $author;
                                            $a++;

                                        }
                                        ?></p>
                                </div>
                            <?php
	                 //           if(($c == 3) || ($c == 6) || ($c == 9) || $c == $wp_query->post_count)
	                 //           echo "</div>";

                            }

                        }else{
                            echo "<h3>Nessun risultato su Google Books, prova a modificare i parametri di ricerca!</h3>";
                        }

                    }else{
                       // if($_GET["s"] != ""){
                        if(true){
                            if (have_posts()) {
                                ?>
                    <div class="nogrid scroll">
                                <?php
                                $c=0;


                                while (have_posts()) { the_post();
	                                $c++;
	                                if(($c == 1) || ($c == 4) || ($c == 7))
	                                echo "<div class='row margin-bottom-20'>";
                                    ?>
                                    <div class="col-sm-4">
                                        <div class="copybooks_container copybooks_container_small copybooks_container_small_author">
                                            <?php
                                            //if($_GET["ptype"] == "libri"){
                                              //   $prod = new WC_Product( $post->ID );
	                                           // print_book_lista($prod);
                                            //}else{
	                                            print_box_lista($post, false, true, true);
                                            //}
                                            ?>
                                        </div>
                                        <!-- /copybooks_container -->
                                    </div><!-- /grid-item -->
                                    <?php
	                                if(($c == 3) || ($c == 6) || ($c == 9) || $c == $wp_query->post_count)
	                                echo "</div>";

	                                /*
									?>
									<div class="article_simple">
										<h3><a href="<?php the_permalink(); ?>"><?php echo mb_strtoupper(get_the_title()); ?></a></h3>
										<p>
											<b><a href="<?php echo get_author_posts_url($post->post_author); ?>"><?php echo get_userdata($post->post_author)->display_name; ?></a>
												<?php
												$altro_autore = get_field("altro_autore");
												if(!is_wp_error($altro_autore)){
													if($altro_autore != ""){
														echo " - ".$altro_autore;
													}

												}
												?></b>
										</p>
										<p class="article_details">
										<?php
										$quad  = wp_get_post_terms($post->ID, "quaderno");
										if(!is_wp_error($quad)){
											if(is_array($quad)) {
												$quadlink = get_term_link($quad[0]);
												if(!is_wp_error($quadlink)){

													?>
													<a href="<?php echo $quadlink; ?>">Quaderno <?php echo($quad[0]->name); ?></a>
													|
													<?php

													if($numpag = get_field("numero_di_pagina",$post->ID)){
														echo "  pag. ".$numpag;
														if($numpagfine = get_field("numero_di_pagina_fine",$post->ID))
															echo " - ".$numpagfine;
														echo " | ";
													}
													if($data = get_field("data_pubblicazione", $quad[0]->taxonomy."_".$quad[0]->term_id)){
														echo "Anno ".date("Y", strtotime($data))." | ";
													}

													if($volume = get_field("volume", $quad[0]->taxonomy."_".$quad[0]->term_id)){
														echo "Volume ".$volume."  ";
													}

												}
											}
										}
										?>
										</p>
										<p><?php echo wp_trim_words($post->post_content, 40, "..."); ?></p>


									</div><!-- /article_simple -->
								<?php
*/

                                }
                                ?>
                        </div>
                        <?php
                            }else{
                                echo "<h3>Nessun risultato, prova a modificare i parametri di ricerca!</h3>";
                            }

                        }

                    }

                    ?>
                </div><!-- /col-md-8 -->

            </div><!-- /row -->

            <?php
           // if($_GET["s"] != ""){
                ?>
                <div class="row">

                    <div class="col-md-12">

                        <div class="pager">

                            <?php
                            $big = 999999999; // need an unlikely integer
                            echo paginate_links(array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => $paged,
                                'total' => $wp_query->max_num_pages
                            ));
                            ?>

                        </div>

                    </div><!-- /col-md-12 -->

                </div><!-- /row -->

            <?php
           // }
            ?>

        </div><!-- /container -->

    </div><!-- /section -->

</main>


<?php get_template_part('includes/footer'); ?>
