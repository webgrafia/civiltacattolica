<?php
/*
 * Do-Validate-Then-Update (for Wordpress Developers)
 *
 * URI: http://www.mauromascia.com/shop/product/do-validate-then-update-system-for-wordpress-developers
 * Version: 1.0
 * Author: Mauro Mascia (baba_mmx)
 * Author URI: http://www.mauromascia.com
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Support: info@mauromascia.com
 * Copyright: © 2013 Mauro Mascia
 *
 * Thanks to:
 * @ Jeremy Clark (clark-technet.com) https://github.com/jeremyclark13/automatic-theme-plugin-update
 * @ http://wp.tutsplus.com/tutorials/plugins/a-guide-to-the-wordpress-http-api-automatic-plugin-updates/
 * @ The book "Professional WordPress Plugin Development" by Brad Williams, Ozh Richard, Justin Tadlock
 * @ WooThemes - WooCommerce Software Add-on (http://www.woothemes.com/products/software-add-on/)
 *
 * Side notes
 * http://www.gnu.org/licenses/gpl-faq.html#GPLCommercially
 *
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

// Add a global value to the buttons
$activate_button = __( "Activate" );
$deactivate_button = __( "Deactivate" );
$clear_button = __( "Clear" );

// Post-ing actions: activate/deactivate
if ( isset( $_POST['submit'] ) && $_POST['submit'] == $activate_button ) {
  $this->license = isset( $_POST['license-key'] ) ? $_POST['license-key'] : '';
  $this->email = isset( $_POST['email'] ) ? $_POST['email'] : '';

  // Update the value of an option that was already added.
  update_option( $this->wp_option_license_key, $this->license );
  update_option( $this->wp_option_email, $this->email );

  $this->do_action( 'activation', true );
}
elseif ( isset( $_POST['submit'] ) && $_POST['submit'] == $deactivate_button ) {
  $this->do_action( 'deactivation', true );
}
elseif ( isset( $_POST['submit'] ) && $_POST['submit'] == $clear_button ) {
  // Removes option by name.
  update_option( $this->wp_option_license_key, '' );
  update_option( $this->wp_option_email, '' );
}
else {
  $this->do_action( 'is_active', true );
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <title>Update-Validation System (for Developers) by Mauro Mascia (info@mauromascia.com)</title>
  </head>
  <body style="background-color: #fff; padding: 50px;">
    <div class="rounded">
      <h3><?php echo $this->strings['page_head']; ?> </h3>

      <div>
        <p><?php echo $this->strings['page_content']; ?> </p>

        <form method="post" action="<?php echo $this->ajax_url(); ?>">
          <div style="padding-top: 5px;">
            <label for="email" style="float:left; width: 100px">Email:</label>
            <input type="text" name="email" size="30" maxlength="128" value="<?php echo get_option( $this->wp_option_email, '' ) ?>" />
          </div>
          <div style="padding-top: 5px;">
            <label for="license-key" style="float:left; width: 100px">License Key:</label>
            <input type="text" name="license-key" size="50" maxlength="128" value="<?php echo get_option( $this->wp_option_license_key, '' ) ?>" />
          </div>
          <div style="margin-top: 25px; padding-top: 15px; border-top: 1px solid #999;">
            <input class="action-activate" type="submit" name="submit" value="<?php echo $activate_button; ?>" />
            <input class="action-deactivate" type="submit" name="submit" value="<?php echo $deactivate_button; ?>" />
            <input class="action-clear" type="submit" name="submit" value="<?php echo $clear_button; ?>" />
          </div>
        </form>
      </div>
    </div>
	</body>
</html>