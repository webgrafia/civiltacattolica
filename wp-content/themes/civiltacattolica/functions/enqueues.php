<?php

function wbst_enqueues()
{

    /*
        <link rel="stylesheet" href="<?php bloginfo("template_url"); ?>/css/main.css">

        <script src="<?php bloginfo("template_url"); ?>/js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="<?php bloginfo("template_url"); ?>/js/vendor/respond.min.js"></script>
      */


//	wp_register_script('jquery', get_bloginfo('template_url').'/js/jquery-1.11.3.min.js', __FILE__, false, '1.11.3', true);
//	wp_enqueue_script( 'jquery' );

//	wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.4', null);
//	wp_enqueue_style('bootstrap-css');

//  	wp_register_style('wbst-css', get_template_directory_uri() . '/css/wbst.css', false, null);
//	wp_enqueue_style('wbst-css');


    wp_register_style('cc-css', get_template_directory_uri() . '/css/main.css', false, "0.3");
    wp_enqueue_style('cc-css');


    wp_register_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.6.2.min.js', false, null, false);
    wp_enqueue_script('modernizr');


    wp_register_script('respond', get_template_directory_uri() . '/js/vendor/respond.min.js', false, null, false);
    wp_enqueue_script('respond');


//  	wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', false, null, true);
//	wp_enqueue_script('bootstrap-js');

//	wp_register_script('wbst-js', get_template_directory_uri() . '/js/wbst.js', false, null, true);
//	wp_enqueue_script('wbst-js');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'wbst_enqueues', 100);


function cc_gmap_js()
{
    ?>

    <script type="text/javascript" src="//maps.google.com/maps/api/js?key=<?php echo GMAP_API_KEY; ?>"></script>

    <script src="<?php bloginfo("template_url"); ?>/gmaps/gmaps.js"></script>

    <script type="text/javascript">

        function sticky_relocate() {
            var window_top = $(window).scrollTop();
            var div_top = $('.main_container').offset().top;
            if (window_top > div_top) {
                $('.sticky-wrapper').addClass('is-sticky');
                $('body').addClass('sticked-menu');
            } else {
                $('.sticky-wrapper').removeClass('is-sticky');
                $('body').removeClass('sticked-menu');
            }
        }

        $(function () {
            $(window).scroll(sticky_relocate);
            sticky_relocate();
        });

        var map;
        $(document).ready(function () {
            map = new GMaps({
                el: '#map',
                lat: 45.437123,
                lng: 12.333819,
                zoom: 2,
                scrollwheel: false,
                zoomControl: false,
                zoomControlOpt: {
                    style: "SMALL",
                    position: "TOP_LEFT"
                },
                panControl: false,
                streetViewControl: false,
                mapTypeControl: false,
                overviewMapControl: false,
            });


            var styles = [
                {
                    stylers: [
                        {hue: "#00ffe6"},
                        {saturation: -20}
                    ]
                }
                , {
                    featureType: "water",
                    stylers: [
                        {visibility: "on"},
                        {color: "#dddddd"}
                    ]
                }, {
                    featureType: "administrative",
                    elementType: "geometry",
                    stylers: [
                        {visibility: "off"},
                        {color: "#b1b1b1"}

                    ]
                }

                , {
                    featureType: "landscape",
                    stylers: [
                        {visibility: "off"},
                        {color: "#b1b1b1"}

                    ]
                },

                {
                    featureType: "landscape.natural.landcover",

                    stylers: [

                        {visibility: "off"}

                    ]
                },

                {
                    featureType: "landscape.man_made",

                    stylers: [

                        {visibility: "off"}

                    ]
                },

                {
                    featureType: "administrative.country",
                    elementType: "labels",

                    stylers: [

                        {visibility: "off"}

                    ]
                },

                {
                    featureType: "administrative",
                    elementType: "labels",

                    stylers: [

                        {visibility: "off"}

                    ]
                },

                {
                    featureType: "landscape",

                    stylers: [

                        {visibility: "on"},
                        {color: "#b1b1b1"}

                    ]
                },

                {
                    featureType: "poi",

                    stylers: [

                        {visibility: "off"}

                    ]
                },


                {
                    featureType: "poi",
                    elementType: "labels",

                    stylers: [

                        {visibility: "off"}

                    ]
                },


                {
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [

                        {visibility: "off"}

                    ]
                }, {
                    featureType: "road",
                    elementType: "labels",
                    stylers: [
                        {visibility: "off"}
                    ]
                }
            ];

            map.addStyle({
                styledMapName: "Styled Map",
                styles: styles,
                mapTypeId: "map_style"
            });

            map.setStyle("map_style");
            <?php
            // recupero la lista delle categorie geografiche

            $geoaree = get_terms( array(
                'taxonomy' => 'geoarea',
                'hide_empty' => true
            ) );


             foreach ($geoaree as $geoarea) {
             unset($latlong);
             $geofield = get_field( "coordinate", $geoarea->taxonomy."_".$geoarea->term_id);


             if(!is_array($geofield)){
		        $latlong = unserialize($geofield);
             }else{
	     		        $latlong = ($geofield);
             }

             if($latlong["lat"]){
             ?>
            map.addMarker({
                lat: <?php echo $latlong["lat"]; ?>,
                lng: <?php echo $latlong["lng"]; ?>,
                icon: "<?php bloginfo("template_url"); ?>/img/marker-small.png",
                title: "<?php echo $geoarea->name; ?>",
                infoWindow: {
                    content: '<p><a href="<?php echo get_term_link($geoarea) ?>"><?php echo addslashes($geoarea->name); ?></a></p>'
                }
            });
            <?php
            }
            }
             ?>


        });
    </script>
<?php

}