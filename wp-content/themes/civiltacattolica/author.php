<?php get_template_part('includes/header'); ?>

<?php

$author_id = get_the_author_meta('ID');
$foto = get_field('foto', 'user_' . $author_id);
$facebook = get_field('facebook', 'user_' . $author_id);
$twitter = get_field('twitter', 'user_' . $author_id);
$linkedin = get_field('linkedin', 'user_' . $author_id);


//dd($foto);

if($foto)
    $userpic = $foto["sizes"]["conferenza"];
else
    $userpic = get_bloginfo("template_url")."/img/logo.svg";

global $wp_query;
$paged = get_query_var('paged') ? get_query_var('paged') : 1;

?>

<main role="main">
    <?php
    if($paged == 1) {
        $description = get_the_author_meta( 'description' );
        ?>
        <div class="section section_large_padding section_white">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="<?php if($description != "") echo "author_main"; else echo "author_main_nodescr"; ?>">
                            <div class="author_main_image">
                                <img src="<?php echo $userpic; ?>">
                            </div>
                            <!-- /article_large_image -->
                            <div class="author_social">
                                <?php if ($facebook) { ?><a href="<?php echo $facebook; ?>"><i
                                        class="icon icon-icon-facebook-1"></i></a><?php } ?>
                                <?php if ($twitter) { ?><a href="<?php echo $twitter; ?>"><i
                                        class="icon icon-icon-twitter-1"></i></a><?php } ?>
                                <?php if ($linkedin) { ?><a href="<?php echo $linkedin; ?>"><i
                                        class="icon icon-icon-linkedin"></i></a><?php } ?>
                            </div>
                        </div>
                    </div>
                    <!-- /col-md-4 -->
                    <div class="col-md-8">
                        <div class="author_content">
                            <h1><?php echo get_the_author(); ?></h1>

                            <?php
                                if($description != "") echo "<p>".$description."</p>";
                            ?>
                        </div>
                    </div>
                    <!-- /col-md-8 -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /section -->
    <?php
    }
    ?>

    <div class="section section_large_padding section_grey_light">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="grid scroll">
                    <?php


                  /*      $args = array(
                            "post_type" => "articolo",
                            "post_author" => $author_id,
                            'posts_per_page' => 9,
                            'paged' => $paged
                        );

                        $wp_query = new WP_Query($args);
                 */
                        if (have_posts()) {
                            while (have_posts()) { the_post();
                            ?>
                            <div class="grid-item grid-item-small">
                                <div class="copybooks_container copybooks_container_small copybooks_container_small_author">
                                    <?php  print_box_lista($post, false, true); ?>
                                </div>
                                <!-- /copybooks_container -->
                            </div><!-- /grid-item -->
                        <?php
                        }
                    }
                    ?>
                    </div>
                    <!-- /grid -->
                </div>
                <!-- /col-md-12 -->
            </div>
            <!-- /row -->

            <div class="row">
                <div class="col-md-12">
                    <div class="pager">
                        <?php
                        $big = 999999999; // need an unlikely integer
                            echo paginate_links(array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                  'current' => $paged,
                             'total' => $wp_query->max_num_pages
                        ));
                        ?>

                    </div>
                </div>
                <!-- /col-md-12 -->

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /section -->

</main>


</div><!-- /main_container -->

<?php get_template_part('includes/footer');

