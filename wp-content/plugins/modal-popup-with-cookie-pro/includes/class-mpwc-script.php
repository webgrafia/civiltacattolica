<?php
/**
 * Script Class
 *
 * Handles the script and style functionality of plugin
 *
 * @package Modal Popup With Cookie Pro
 * @since 1.0.0
 */

// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

class Mpwc_Pro_Script {
	
	function __construct() {
		
		// Action to add style on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'mpwc_pro_front_end_style') );

		// Action to add script on frontend
		add_action( 'wp_enqueue_scripts', array($this, 'mpwc_pro_front_end_script'), 15 );

		// Action to add style in backend
		add_action( 'admin_enqueue_scripts', array($this, 'mpwc_pro_admin_style') );

		// Action to add script in backend
		add_action( 'admin_enqueue_scripts', array($this, 'mpwc_pro_admin_script') );
	}
	
	/**
	 * Enqueue front end styles
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_front_end_style(){		

		// Registring Public Style
		wp_register_style( 'mpwc-pro-public', MPWC_PRO_URL.'assets/css/mpwc-pro-public.css', null, MPWC_PRO_VERSION );
		wp_enqueue_style('mpwc-pro-public');
	}

	/**
	 * Enqueue front script
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_front_end_script(){		
		
		// Registring Popup script
		wp_register_script( 'mpwc-pro-public-popup', MPWC_PRO_URL.'assets/js/mpwc-pro-public-popup.js', array('jquery'), MPWC_PRO_VERSION, true );
		wp_localize_script( 'mpwc-pro-public-popup', 'Mpwc_Pro_Popup', array(
																	'enable'		=> mpwc_pro_get_option('mpwc_pro_enable'),
																	'cookie_prefix'	=> mpwc_pro_get_option('mpwc_pro_cookie_prefix')																	
																	));	
		wp_enqueue_script('mpwc-pro-public-popup');	
	}

	/**
	 * Enqueue admin styles
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_admin_style( $hook ) {
		global $wp_version,$typenow;
		// Pages array
		$pages_array = array( 'mpwc_pro_popup' );

		// If page is plugin setting page then enqueue script
		if( in_array($typenow, $pages_array) ) {
			wp_register_style( 'jquery-ui-core', MPWC_PRO_URL.'assets/css/jquery-ui.css', null, MPWC_PRO_VERSION );
			wp_enqueue_style('jquery-ui-core');
			wp_register_style( 'mpwc-pro-admin', MPWC_PRO_URL.'assets/css/mpwc-pro-admin.css', null, MPWC_PRO_VERSION );
			wp_enqueue_style('mpwc-pro-admin');
			// Enqueu built in style for color picker
			if( wp_style_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_style( 'wp-color-picker' );
			} else {
				wp_enqueue_style( 'farbtastic' );
			}
		}
	}

	/**
	 * Enqueue admin script
	 * 
	 * @package Modal Popup With Cookie Pro
	 * @since 1.0.0
	 */
	function mpwc_pro_admin_script( $hook ) {

		global $wp_version,$typenow;
		
		$new_ui = $wp_version >= '3.5' ? '1' : '0'; // Check wordpress version for older scripts

		// Pages array
		$pages_array = array( 'mpwc_pro_popup' );

		// If page is plugin setting page then enqueue script
		if( in_array($typenow, $pages_array) ) {

			// Enqueu built-in script for color picker
			if( wp_script_is( 'wp-color-picker', 'registered' ) ) { // Since WordPress 3.5
				wp_enqueue_script( 'wp-color-picker' );
			} else {
				wp_enqueue_script( 'farbtastic' );
			}
			wp_enqueue_script( 'jquery-ui-slider' );
			// Registring admin script
			wp_register_script( 'mpwc-pro-admin-popup', MPWC_PRO_URL.'assets/js/mpwc-pro-admin-popup.js', array('jquery'), MPWC_PRO_VERSION, true );
			wp_localize_script( 'mpwc-pro-admin-popup', 'Mpwc_Pro_Admin', array(
																	'new_ui' =>	$new_ui
																));
			wp_enqueue_script( 'mpwc-pro-admin-popup' );
		}
	}
}

$mpwc_pro_script = new Mpwc_Pro_Script();