<?php get_template_part('includes/header'); ?>
<main role="main">

    <div class="section section_grey_light">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="title_large">
                        <h1>News</h1>
                    </div>

                </div><!-- /col-md-12-->

            </div><!-- /row -->


            <div class="row">

                <div class="col-md-8">

                    <main role="main">

<?php
global $post;
if(have_posts()): while(have_posts()): the_post();
    setup_postdata($post);
    $imgsize = "thumbnail";
    $thumb = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), $imgsize);
    $immagine = $thumb[0];
    $sottotitolo = get_field("sottotitolo", $post->ID);

    $link = get_permalink($post->ID);

    ?>

    <div class="copybook copybook_mini article_simple article_news">
        <div class="copybook_thumb" >

            <?php if($immagine) echo '<a href="' . $link . '"><img src="' . $immagine . '" alt=""></a>'; ?>
        </div>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
        <p><?php echo $sottotitolo; ?></p>
    </div><!-- /article_simple -->
<hr>

    <?php endwhile; ?>

               <div class="pager">
                <?php
                $big = 999999999; // need an unlikely integer
                echo paginate_links(array(
                    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                    'format' => '?paged=%#%',
                    'current' => $paged,
                    'total' => $wp_query->max_num_pages,
                    'end_size' => 3
                ));
                ?>
            </div>
<?php endif; ?>
                    </main>
                </div><!-- /col-md-8 -->

                <div class="col-md-4">

                <?php get_template_part('includes/box-abbonamenti'); ?>

                    <div class="row margin-top-20 padding-left-15 padding-right-15" style="text-align:center;">
                        <a href="https://www.spreaker.com/user/civcatt" target="_BLANK"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner/<?php echo rand(1, 3); ?>_1.png" style="max-width: 100%;"></a>
                    </div>


                </div><!-- /col-md-4 -->

            </div><!-- /row -->

        </div><!-- /container -->

    </div><!-- /section -->

</main>


</div><!-- /main_container -->




<?php get_template_part('includes/footer'); ?>

